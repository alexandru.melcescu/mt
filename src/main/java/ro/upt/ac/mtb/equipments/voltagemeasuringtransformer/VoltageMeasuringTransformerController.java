package ro.upt.ac.mtb.equipments.voltagemeasuringtransformer;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.Optional;
import java.util.TreeMap;

import org.hibernate.SessionFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.io.ByteArrayResource;
import org.springframework.core.io.Resource;
import org.springframework.data.repository.query.Param;
import org.springframework.http.HttpHeaders;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.validation.ObjectError;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.multipart.MultipartFile;
import org.thymeleaf.TemplateEngine;
import org.thymeleaf.context.Context;
import org.thymeleaf.templatemode.TemplateMode;
import org.thymeleaf.templateresolver.ClassLoaderTemplateResolver;
import org.thymeleaf.util.StringUtils;
import org.xhtmlrenderer.pdf.ITextRenderer;

import jakarta.servlet.http.HttpServletRequest;
import jakarta.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.io.OutputStream;
import java.sql.SQLException;
import java.time.LocalDateTime;
import ro.upt.ac.mtb.equipments.electricalequipment.EquipmentManual;
import ro.upt.ac.mtb.equipments.electricalequipment.EquipmentManualRepository;
import ro.upt.ac.mtb.equipments.services.CSVFileGenerator;
import ro.upt.ac.mtb.equipments.services.ExportableToCSV;

@Controller
@RequestMapping("voltageMeasuringTransformer")
public class VoltageMeasuringTransformerController
{
	@Autowired
	VoltageMeasuringTransformerRepository voltageMeasuringTransformerRepository;
	
	@Autowired
	EquipmentManualRepository equipmentManualRepository;
	
	@Autowired
	CSVFileGenerator csvFileGenerator;
	
	@Autowired
	SessionFactory sessionFactory;
	
	private static final String EQUIPMENT = "voltageMeasuringTransformer";
	
	@PostMapping("/search")
	public String search(@Param("searchTerm") String searchTerm, @Param("searchField") String searchField, Model model) {
		//String searchTerm = (String) model.getAttribute("searchTerm");
		List<VoltageMeasuringTransformer> data = null;
		System.out.println(searchTerm + " " + searchField);
		
		data = performSearch(searchField, searchTerm);
		
		if (data.size() != 0) {
		    model.addAttribute("equipmentObjects", data);
		}
		model.addAttribute("equipment", EQUIPMENT);
		model.addAttribute("searchFields", getHeadingsWithFields().entrySet());
		model.addAttribute("search", Boolean.TRUE);
		model.addAttribute("searchField", searchField);
		model.addAttribute("searchTerm", searchTerm);
		return EQUIPMENT + "/" + EQUIPMENT + "-read";
	}
	
	private List<VoltageMeasuringTransformer> performSearch(String searchField, String searchTerm) {
		try {
			if (searchField.equalsIgnoreCase("maximumNetworkVoltageUnitOfMeasure")) {
				// from a string to an Integer
				return voltageMeasuringTransformerRepository.findByMaximumNetworkVoltageUnitOfMeasure(searchTerm);
			} else if (searchField.equalsIgnoreCase("maximumNetworkVoltageRequired")) {
				return voltageMeasuringTransformerRepository.findByMaximumNetworkVoltageRequired(searchTerm);
			} else if (searchField.equalsIgnoreCase("maximumNetworkVoltageOffered")) {
				return voltageMeasuringTransformerRepository.findByMaximumNetworkVoltageOffered(searchTerm);
			} else if (searchField.equalsIgnoreCase("ratedVoltageOfThePrimaryWindingUnitOfMeasure")) {
				// from a string to an Integer
				return voltageMeasuringTransformerRepository.findByRatedVoltageOfThePrimaryWindingUnitOfMeasure(searchTerm);
			} else if (searchField.equalsIgnoreCase("ratedVoltageOfThePrimaryWindingRequired")) {
				return voltageMeasuringTransformerRepository.findByRatedVoltageOfThePrimaryWindingRequired(searchTerm);
			} else if (searchField.equalsIgnoreCase("ratedVoltageOfThePrimaryWindingOffered")) {
				return voltageMeasuringTransformerRepository.findByRatedVoltageOfThePrimaryWindingOffered(searchTerm);
			} else if (searchField.equalsIgnoreCase("equipmentSeries")) {
				return voltageMeasuringTransformerRepository.findByEquipmentSeries(searchTerm);
			} else if (searchField.equalsIgnoreCase("manufacturingYear")) {
				return voltageMeasuringTransformerRepository.findByManufacturingYear(Integer.parseInt(searchTerm));
			} else if (searchField.equalsIgnoreCase("pifYear")) {
				return voltageMeasuringTransformerRepository.findByPifYear(Integer.parseInt(searchTerm));
			} else if (searchField.equalsIgnoreCase("installationCompany")) {
				return voltageMeasuringTransformerRepository.findByInstallationCompany(searchTerm);
			} else if (searchField.equalsIgnoreCase("pifBulletin")) {
				return voltageMeasuringTransformerRepository.findByPifBulletin(searchTerm);
			} else if (searchField.equalsIgnoreCase("exploitationCenter")) {
				return voltageMeasuringTransformerRepository.findByExploitationCenter(searchTerm);
			} else if (searchField.equalsIgnoreCase("transformationCenter")) {
				return voltageMeasuringTransformerRepository.findByTransformationStation(searchTerm);
			} else if (searchField.equalsIgnoreCase("voltageLevel")) {
				return voltageMeasuringTransformerRepository.findByVoltageLevel(searchTerm);
			} else if (searchField.equalsIgnoreCase("assemblyCell")) {
				return voltageMeasuringTransformerRepository.findByAssemblyCell(searchTerm);
			} else if (searchField.equalsIgnoreCase("assemblyPhase")) {
				return voltageMeasuringTransformerRepository.findByAssemblyPhase(searchTerm);
			} else if (searchField.equalsIgnoreCase("accountingorWarehouseRegistrationCode")) {
				return voltageMeasuringTransformerRepository.findByAccountingorWarehouseRegistrationCode(searchTerm);
			} else if (searchField.equalsIgnoreCase("fixedAssetComponentNumber")) {
				return voltageMeasuringTransformerRepository.findByFixedAssetComponentNumber(searchTerm);				
			} else {
				return voltageMeasuringTransformerRepository.search(searchTerm);
			}		
		} catch (NumberFormatException e) {
			return voltageMeasuringTransformerRepository.search(searchTerm);
		}
	}
	
	@PostMapping("/uploadFile")
    public String uploadFile(@RequestParam("file") MultipartFile file, Model model) {
        EquipmentManual equipmentManual = new EquipmentManual();

        // setting up the entity of the equipment manual
        equipmentManual.setId(EQUIPMENT);
        equipmentManual.setOperatingManualFileType(file.getContentType());
        equipmentManual.setOperatingManualFileName(file.getOriginalFilename());
        
        try {
			//equipmentManual.setOperatingManual(file.getBytes());
        	
        	// create a blob object so that we can store anything bigger and 255 bytes
        	equipmentManual.setOperatingManual(sessionFactory.openSession().getLobHelper().createBlob(file.getBytes()));
			equipmentManualRepository.save(equipmentManual);
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
        
        return read(model); 
	}
	
	@GetMapping("/downloadFile")
    public ResponseEntity <Resource> downloadFile(HttpServletRequest request) {
        // Load file as Resource
        Optional<EquipmentManual> equipmentManual = equipmentManualRepository.findById(EQUIPMENT);

        if (equipmentManual.isPresent()) {
        	try {
				return ResponseEntity.ok()
				        .contentType(MediaType.parseMediaType(equipmentManual.get().getOperatingManualFileType()))
				        .header(HttpHeaders.CONTENT_DISPOSITION, "attachment; filename=\"" + equipmentManual.get().getOperatingManualFileName() + "\"")
				        .body(new ByteArrayResource(equipmentManual.get().getOperatingManual().getBytes(1, (int) equipmentManual.get().getOperatingManual().length())));
			} catch (SQLException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
        }
        return ResponseEntity.notFound()
        		.build();
    }
	
	
	@GetMapping("/create")
	public String create(VoltageMeasuringTransformer voltageMeasuringTransformer, Model model)
	{
		model.addAttribute("equipment", EQUIPMENT);
		model.addAttribute("equipmentObject", new VoltageMeasuringTransformer());
		return EQUIPMENT + "/" + EQUIPMENT + "-create";
	}

	@PostMapping("/create-save")
	// @ModelAttribute("equipmentObject") connects the create-save method to the create html page of the equipment
	public String createSave(@Validated @ModelAttribute("equipmentObject") VoltageMeasuringTransformer voltageMeasuringTransformer, BindingResult result, Model model)
	{
		if(result.hasErrors())
		{
			// without line 144 we don't have the equipment object in the request, since we need it 
			model.addAttribute("equipmentObject", voltageMeasuringTransformer);
			return EQUIPMENT + "/" + EQUIPMENT + "-create";
		}
		
		// update the time stamp before saving the record
	    voltageMeasuringTransformer.setUpdateTimeStamp(LocalDateTime.now());		
		voltageMeasuringTransformerRepository.save(voltageMeasuringTransformer);
		return "redirect:read";
	}
	
	@GetMapping("/read")
	public String read(Model model) 
	{
		model.addAttribute("equipment", EQUIPMENT);
		List<VoltageMeasuringTransformer> voltageMeasuringTransformers = voltageMeasuringTransformerRepository.findAll();
		if (voltageMeasuringTransformers.size() != 0) 
		{
			System.out.println(voltageMeasuringTransformers); 
			model.addAttribute("equipmentObjects", voltageMeasuringTransformers);
		}
		model.addAttribute("searchFields", getHeadingsWithFields().entrySet());
		return EQUIPMENT + "/" + EQUIPMENT + "-read";
	}
	
	@GetMapping("/edit/{id}")
	public String edit(@PathVariable("id") Long id, Model model) 
	{
		model.addAttribute("equipment", EQUIPMENT);
		Optional<VoltageMeasuringTransformer> voltageMeasuringTransformer = voltageMeasuringTransformerRepository.findById(id);
	    model.addAttribute("equipmentObject", voltageMeasuringTransformer.get());
	    
		return EQUIPMENT + "/" + EQUIPMENT + "-update";
	}
	
	@PostMapping("/update/{id}")
	public String update(@PathVariable("id") Long id, @Validated @ModelAttribute("equipmentObject") VoltageMeasuringTransformer voltageMeasuringTransformer, BindingResult result, Model model) 
	{
	    // check to see that the user is updating using fresh data
		if (voltageMeasuringTransformerRepository.findByIdAndUpdateTimeStamp(id, voltageMeasuringTransformer.getUpdateTimeStamp()).isEmpty()) {
			// if it's empty = not dealing with fresh data
			// reject allows 191 to have errors
			// each time the addError() function gets used, it means there is a global error
			result.addError(new ObjectError("global", "Another user has already updated the data"));
			// message is coming from line 190
			model.addAttribute("globalError", "Another user has already updated the data");
		}
		
	    if(result.hasErrors()) 
	    {
	    	voltageMeasuringTransformer.setId(id);
			model.addAttribute("equipmentObject", voltageMeasuringTransformer);
			return EQUIPMENT + "/" + EQUIPMENT + "-update";
	    }
	    // update the time stamp
	    voltageMeasuringTransformer.setUpdateTimeStamp(LocalDateTime.now());
	    // save the record
	    voltageMeasuringTransformerRepository.save(voltageMeasuringTransformer);
	    
		return "redirect:../read";
	}
	
	@GetMapping("/delete/{id}")
	public String delete(@PathVariable("id") Long id, Model model) 
	{
		Optional<VoltageMeasuringTransformer> voltageMeasuringTransformer = voltageMeasuringTransformerRepository.findById(id);
	    
		voltageMeasuringTransformerRepository.delete(voltageMeasuringTransformer.get());
		return "redirect:../read";
	}	
	
	@GetMapping("/exportToCSV")
	public void exportToCSV(@Param("searchTerm") String searchTerm, @Param("searchField") String searchField, HttpServletResponse response, Model model) throws IOException {
		response.setContentType("text/csv");
		//  how it gets saved
		response.addHeader("Content-Disposition", "attachment; filename = \"" + StringUtils.capitalize(EQUIPMENT) + "s.csv\"");

		// "attachment; filename = \"Separators.csv\"" is the header 
		List<ExportableToCSV> data = new ArrayList<>();
		if (searchField != null && searchTerm != null && !searchTerm.trim().equals("") ) {
			data.addAll(performSearch(searchField, searchTerm));
		} else {
			data.addAll(voltageMeasuringTransformerRepository.findAll()); // gets every record in the separator table and adds it to the list that gets exported
		}
		csvFileGenerator.write(getHeadings(), data, response.getWriter()); // write to the response object with the header
	}
	// controller handles the headings, entities handle the data
	
	private static String[] getHeadings() {
		String[] fields = new String[18];
		// fields at position zero
		fields[0] = "Maximum Network Voltage Unit Of Measure";
		fields[1] = "Maximum Network Voltage Required";
		fields[2] = "Maximum Network Voltage Offered";
		fields[3] = "Rated Voltage Of The Primary Winding Unit Of Measure";
		fields[4] = "Rated Voltage Of The Primary Winding Required";
		fields[5] = "Rated Voltage Of The Primary Winding Offered";
		fields[6] = "Equipment Series";
		fields[7] = "Manufacturing Year";
		fields[8] = "Pif Year";
		fields[9] = "Installation Company";
		fields[10] = "Pif Bulletin";
		fields[11] = "Exploitation Center";
		fields[12] = "Transformation Station";
		fields[13] = "Voltage Level";
		fields[14] = "Assembly Cell";
		fields[15] = "Assembly Phase";
		fields[16] = "Accounting or Warehouse Registration Code";
		fields[17] = "Fixed Asset Component Number";
		return fields;
	}
	
	private static String[] getFieldNames() {
		String[] fields = new String[18];
		// fields at position zero
		fields[0] = "maximumNetworkVoltageUnitOfMeasure";
		fields[1] = "maximumNetworkVoltageRequired";
		fields[2] = "maximumNetworkVoltageOffered";
		fields[3] = "ratedVoltageOfThePrimaryWindingUnitOfMeasure";
		fields[4] = "ratedVoltageOfThePrimaryWindingRequired";
		fields[5] = "ratedVoltageOfThePrimaryWindingOffered";
		fields[6] = "equipmentSeries";
		fields[7] = "manufacturingYear";
		fields[8] = "pifYear";
		fields[9] = "installationCompany";
		fields[10] = "pifBulletin";
		fields[11] = "exploitationCenter";
		fields[12] = "transformationStation";
		fields[13] = "voltageLevel";
		fields[14] = "assemblyCell";
		fields[15] = "assemblyPhase";
		fields[16] = "accountingorWarehouseRegistrationCode";
		fields[17] = "fixedAssetComponentNumber";

		return fields;
	}
	
	private static Map<String, String> getHeadingsWithFields() {
		Map<String, String> map = new TreeMap<>();
		map.put("* Search All Fields", "*");
		String[] headings = getHeadings();
		String[] fieldNames = getFieldNames();
		for (int x = 0; x < headings.length; x++) {
			map.put(headings[x], fieldNames[x]);
		}
		
		return map;
	}

	@GetMapping("/exportToPDF")
	public void exportToPDF(HttpServletResponse response, Model model) throws IOException {
		/* String outputFolder = "C:\\Users\\MY PC\\Downloads" + File.arrester + "Arresters.pdf";
		 OutputStream outputStream = new FileOutputStream(outputFolder);
		*/
		
		// getOutputStream() allows the PDF to be opened in the browser rather than a specific location on the server
		OutputStream outputStream = response.getOutputStream();

		response.addHeader("Content-Disposition", "attachment; filename = \"" + StringUtils.capitalize(EQUIPMENT) + "s.pdf\"");
		
	    ITextRenderer renderer = new ITextRenderer();
	    renderer.setDocumentFromString(parseThymeleafTemplate());
	    renderer.layout();
	    renderer.createPDF(outputStream);

	    outputStream.close();
	}
	
	
	private String parseThymeleafTemplate() {
	    ClassLoaderTemplateResolver templateResolver = new ClassLoaderTemplateResolver();
	    templateResolver.setSuffix(".html");
	    templateResolver.setTemplateMode(TemplateMode.HTML);

	    TemplateEngine templateEngine = new TemplateEngine();
	    templateEngine.setTemplateResolver(templateResolver);
	    
	    // model.addAttribute("equipment", "separator");
		List<VoltageMeasuringTransformer> voltageMeasuringTransformers = voltageMeasuringTransformerRepository.findAll();

	    Context context = new Context();
	    context.setVariable("equipmentObjects", voltageMeasuringTransformers);

	    return templateEngine.process("templates/voltageMeasuringTransformer/voltageMeasuringTransformer-pdf", context);
	}
}