package ro.upt.ac.mtb.equipments.separatoractuationdevice;

import java.util.List;
import java.util.Optional;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import java.time.LocalDateTime;

@Repository
public interface SeparatorActuationDeviceRepository extends JpaRepository<SeparatorActuationDevice,Integer> {
	Optional<SeparatorActuationDevice> findById(Long id);
	
	List<SeparatorActuationDevice> findByDeviceTypeForMainKnives(String deviceTypeForMainKnives);
	List<SeparatorActuationDevice> findByDeviceTypeForGroundingKnife(String deviceTypeForGroundingKnife);
	List<SeparatorActuationDevice> findByVoltageOperationForElectricMotor(Integer voltageOperationForElectricMotor);
	List<SeparatorActuationDevice> findByLimitOperationForElectricMotor(Integer limitOperationForElectricMotor);
	List<SeparatorActuationDevice> findByMaximumPowerOperationForElectricMotor(Integer maximumPowerOperationForElectricMotor);
	List<SeparatorActuationDevice> findByVoltageControlForMainKnives(Integer voltageControlForMainKnives);
	List<SeparatorActuationDevice> findByLimitControlForMainKnives(Integer limitControlForMainKnives);
	List<SeparatorActuationDevice> findByMaximumPowerControlForMainKnives(Integer maximumPowerControlForMainKnives);
	List<SeparatorActuationDevice> findByModeOfAchievingTheLockingDeviceInPosition(String modeOfAchievingTheLockingDeviceInPosition);
	List<SeparatorActuationDevice> findByAdditionalResistance(String additionalResistance);
	List<SeparatorActuationDevice> findByDegreeOfProtection(String degreeOfProtection);
	
	List<SeparatorActuationDevice> findByEquipmentSeries(String equipmentSeries);
	List<SeparatorActuationDevice> findByManufacturingYear(Integer manufacturingYear);
	List<SeparatorActuationDevice> findByPifYear(Integer pifYear);
	List<SeparatorActuationDevice> findByInstallationCompany(String installationCompany);
	List<SeparatorActuationDevice> findByPifBulletin(String pifBulletin);
	List<SeparatorActuationDevice> findByExploitationCenter(String exploitationCenter);
	List<SeparatorActuationDevice> findByTransformationStation(String transformationStation);
	List<SeparatorActuationDevice> findByVoltageLevel(String voltageLevel);
	List<SeparatorActuationDevice> findByAssemblyCell(String assemblyCell);
	List<SeparatorActuationDevice> findByAssemblyPhase(String assemblyPhase);
	List<SeparatorActuationDevice> findByAccountingorWarehouseRegistrationCode(String accountingorWarehouseRegistrationCode);
	List<SeparatorActuationDevice> findByFixedAssetComponentNumber(String fixedAssetComponentNumber);
	
	@Query("SELECT sadc FROM SeparatorActuationDevice sadc"
			+ " WHERE sadc.deviceTypeForMainKnives LIKE %?1%"
			+ " OR sadc.deviceTypeForGroundingKnife LIKE %?1%"
			+ " OR sadc.voltageOperationForElectricMotor LIKE %?1%"
			+ " OR sadc.limitOperationForElectricMotor LIKE %?1%"
			+ " OR sadc.maximumPowerOperationForElectricMotor LIKE %?1%"
			+ " OR sadc.voltageControlForMainKnives LIKE %?1%"
			+ " OR sadc.limitControlForMainKnives LIKE %?1%"
			+ " OR sadc.maximumPowerControlForMainKnives LIKE %?1%"
			+ " OR sadc.modeOfAchievingTheLockingDeviceInPosition LIKE %?1%"
			+ " OR sadc.additionalResistance LIKE %?1%"
			+ " OR sadc.degreeOfProtection LIKE %?1%"
			
			+ " OR sadc.equipmentSeries LIKE %?1%"
			+ " OR sadc.manufacturingYear LIKE %?1%"
			+ " OR sadc.pifYear LIKE %?1%"
			+ " OR sadc.installationCompany LIKE %?1%"
			+ " OR sadc.pifBulletin LIKE %?1%"
			+ " OR sadc.exploitationCenter LIKE %?1%"
			+ " OR sadc.transformationStation LIKE %?1%"
			+ " OR sadc.voltageLevel LIKE %?1%"
			+ " OR sadc.assemblyCell LIKE %?1%"
			+ " OR sadc.assemblyPhase LIKE %?1%"
			+ " OR sadc.accountingorWarehouseRegistrationCode LIKE %?1%"
			+ " OR sadc.fixedAssetComponentNumber LIKE %?1%"
			)
	List<SeparatorActuationDevice> search(String searchTerm);
	
	//returns an equipment object by id and by time stamp
	Optional<SeparatorActuationDevice> findByIdAndUpdateTimeStamp(Long id, LocalDateTime ts);
}
