package ro.upt.ac.mtb.equipments.electricalequipment;

import java.util.Optional;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface EquipmentManualRepository extends JpaRepository<EquipmentManual,String> {
	Optional<EquipmentManual> findById(String id);
}