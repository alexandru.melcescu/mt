package ro.upt.ac.mtb.equipments.switchequipment;
 
 import java.util.Arrays;
import java.util.stream.Stream;

import jakarta.persistence.Entity;
import ro.upt.ac.mtb.equipments.electricalequipment.ElectricalEquipment;
import ro.upt.ac.mtb.equipments.services.ExportableToCSV;
 
 //  Switch 
  @Entity
 public class SwitchEquipment extends ElectricalEquipment implements ExportableToCSV{
 
	// tech sheet
 	private String equipmentCode;
 	private String circuitBreakerType;
 	private String nominalBreakingCapacityOfSmallInductiveCurrents;
 	private String kilometricFaultDisconnectingCapacity;
 	private String nominalOperatingSequence;
 	private Integer poleNumber;
 	private Integer nominalVoltage;
 	private Integer nominalInsulation;
 	private Integer nominalCurrent;
 	private Integer nominalBreakingCapacityOfNoLoadLineCurrents;
 	private Integer nominalBreakingCapacityOfNoLoadCableCurrents;
 	private Integer nominalBreakingCapacityOfCapacitorBankCurrents;
 	private Integer maximumShortCircuitClosingCapacity;
 	private Integer nominalClosingTime;
 	private Integer nominalOpeningTime;
 	private Integer nominalInterruptionTime;
 	private Double nominalBreakingCapacityOfShortCircuitCurrentsAtTerminal;
 	private Double admissibleDurationOfShortCircuitCurrents;
 	
 	public SwitchEquipment() {
 		
 	}

	public String getEquipmentCode() {
		return equipmentCode;
	}

	public void setEquipmentCode(String equipmentCode) {
		this.equipmentCode = equipmentCode;
	}

	public String getCircuitBreakerType() {
		return circuitBreakerType;
	}

	public void setCircuitBreakerType(String circuitBreakerType) {
		this.circuitBreakerType = circuitBreakerType;
	}

	public String getNominalBreakingCapacityOfSmallInductiveCurrents() {
		return nominalBreakingCapacityOfSmallInductiveCurrents;
	}

	public void setNominalBreakingCapacityOfSmallInductiveCurrents(String nominalBreakingCapacityOfSmallInductiveCurrents) {
		this.nominalBreakingCapacityOfSmallInductiveCurrents = nominalBreakingCapacityOfSmallInductiveCurrents;
	}

	public String getKilometricFaultDisconnectingCapacity() {
		return kilometricFaultDisconnectingCapacity;
	}

	public void setKilometricFaultDisconnectingCapacity(String kilometricFaultDisconnectingCapacity) {
		this.kilometricFaultDisconnectingCapacity = kilometricFaultDisconnectingCapacity;
	}

	public String getNominalOperatingSequence() {
		return nominalOperatingSequence;
	}

	public void setNominalOperatingSequence(String nominalOperatingSequence) {
		this.nominalOperatingSequence = nominalOperatingSequence;
	}

	public Integer getPoleNumber() {
		return poleNumber;
	}

	public void setPoleNumber(Integer poleNumber) {
		this.poleNumber = poleNumber;
	}

	public Integer getNominalVoltage() {
		return nominalVoltage;
	}

	public void setNominalVoltage(Integer nominalVoltage) {
		this.nominalVoltage = nominalVoltage;
	}

	public Integer getNominalInsulation() {
		return nominalInsulation;
	}

	public void setNominalInsulation(Integer nominalInsulation) {
		this.nominalInsulation = nominalInsulation;
	}

	public Integer getNominalCurrent() {
		return nominalCurrent;
	}

	public void setNominalCurrent(Integer nominalCurrent) {
		this.nominalCurrent = nominalCurrent;
	}

	public Integer getNominalBreakingCapacityOfNoLoadLineCurrents() {
		return nominalBreakingCapacityOfNoLoadLineCurrents;
	}

	public void setNominalBreakingCapacityOfNoLoadLineCurrents(Integer nominalBreakingCapacityOfNoLoadLineCurrents) {
		this.nominalBreakingCapacityOfNoLoadLineCurrents = nominalBreakingCapacityOfNoLoadLineCurrents;
	}

	public Integer getNominalBreakingCapacityOfNoLoadCableCurrents() {
		return nominalBreakingCapacityOfNoLoadCableCurrents;
	}

	public void setNominalBreakingCapacityOfNoLoadCableCurrents(Integer nominalBreakingCapacityOfNoLoadCableCurrents) {
		this.nominalBreakingCapacityOfNoLoadCableCurrents = nominalBreakingCapacityOfNoLoadCableCurrents;
	}

	public Integer getNominalBreakingCapacityOfCapacitorBankCurrents() {
		return nominalBreakingCapacityOfCapacitorBankCurrents;
	}

	public void setNominalBreakingCapacityOfCapacitorBankCurrents(Integer nominalBreakingCapacityOfCapacitorBankCurrents) {
		this.nominalBreakingCapacityOfCapacitorBankCurrents = nominalBreakingCapacityOfCapacitorBankCurrents;
	}

	public Integer getMaximumShortCircuitClosingCapacity() {
		return maximumShortCircuitClosingCapacity;
	}

	public void setMaximumShortCircuitClosingCapacity(Integer maximumShortCircuitClosingCapacity) {
		this.maximumShortCircuitClosingCapacity = maximumShortCircuitClosingCapacity;
	}

	public Integer getNominalClosingTime() {
		return nominalClosingTime;
	}

	public void setNominalClosingTime(Integer nominalClosingTime) {
		this.nominalClosingTime = nominalClosingTime;
	}

	public Integer getNominalOpeningTime() {
		return nominalOpeningTime;
	}

	public void setNominalOpeningTime(Integer nominalOpeningTime) {
		this.nominalOpeningTime = nominalOpeningTime;
	}

	public Integer getNominalInterruptionTime() {
		return nominalInterruptionTime;
	}

	public void setNominalInterruptionTime(Integer nominalInterruptionTime) {
		this.nominalInterruptionTime = nominalInterruptionTime;
	}

	public Double getNominalBreakingCapacityOfShortCircuitCurrentsAtTerminal() {
		return nominalBreakingCapacityOfShortCircuitCurrentsAtTerminal;
	}

	public void setNominalBreakingCapacityOfShortCircuitCurrentsAtTerminal(
			Double nominalBreakingCapacityOfShortCircuitCurrentsAtTerminal) {
		this.nominalBreakingCapacityOfShortCircuitCurrentsAtTerminal = nominalBreakingCapacityOfShortCircuitCurrentsAtTerminal;
	}

	public Double getAdmissibleDurationOfShortCircuitCurrents() {
		return admissibleDurationOfShortCircuitCurrents;
	}

	public void setAdmissibleDurationOfShortCircuitCurrents(Double admissibleDurationOfShortCircuitCurrents) {
		this.admissibleDurationOfShortCircuitCurrents = admissibleDurationOfShortCircuitCurrents;
	}
 
	@Override
	public String[] getFields() {
		String[] fields = new String[18];
		// fields at position zero
		fields[0] = equipmentCode;
		fields[1] = circuitBreakerType;
		fields[2] = nominalBreakingCapacityOfSmallInductiveCurrents;
		fields[3] = kilometricFaultDisconnectingCapacity;
		fields[4] = nominalOperatingSequence;
		fields[5] = poleNumber == null ? "" : String.valueOf(poleNumber);
		fields[6] = nominalVoltage == null ? "" : String.valueOf(nominalVoltage);
		fields[7] = nominalInsulation == null ? "" : String.valueOf(nominalInsulation);
		fields[8] = nominalCurrent == null ? "" : String.valueOf(nominalCurrent);
		fields[9] = nominalBreakingCapacityOfNoLoadLineCurrents == null ? "" : String.valueOf(nominalBreakingCapacityOfNoLoadLineCurrents);
		fields[10] = nominalBreakingCapacityOfNoLoadCableCurrents == null ? "" : String.valueOf(nominalBreakingCapacityOfNoLoadCableCurrents);
		fields[11] = nominalBreakingCapacityOfCapacitorBankCurrents == null ? "" : String.valueOf(nominalBreakingCapacityOfCapacitorBankCurrents);
		fields[12] = maximumShortCircuitClosingCapacity == null ? "" : String.valueOf(maximumShortCircuitClosingCapacity);
		fields[13] = nominalClosingTime == null ? "" : String.valueOf(nominalClosingTime);
		fields[14] = nominalOpeningTime == null ? "" : String.valueOf(nominalOpeningTime);
		fields[15] = nominalInterruptionTime == null ? "" : String.valueOf(nominalInterruptionTime);
		fields[16] = nominalBreakingCapacityOfShortCircuitCurrentsAtTerminal == null ? "" : String.valueOf(nominalBreakingCapacityOfShortCircuitCurrentsAtTerminal);
		fields[17] = admissibleDurationOfShortCircuitCurrents == null ? "" : String.valueOf(admissibleDurationOfShortCircuitCurrents);
		return Stream.concat(Arrays.stream(fields), Arrays.stream(super.getFields()))
				.toArray(String[]::new);
	}
 }