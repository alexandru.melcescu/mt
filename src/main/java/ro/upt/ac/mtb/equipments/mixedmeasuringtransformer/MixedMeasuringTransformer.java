package ro.upt.ac.mtb.equipments.mixedmeasuringtransformer;

import java.util.Arrays;
import java.util.stream.Stream;

import jakarta.persistence.Entity;
import ro.upt.ac.mtb.equipments.electricalequipment.ElectricalEquipment;
import ro.upt.ac.mtb.equipments.services.ExportableToCSV;

// Mixed Measuring Transformer

@Entity
public class MixedMeasuringTransformer extends ElectricalEquipment implements ExportableToCSV {
	
	// tech sheet 
	private String designingPurpose;
	private Integer primaryCurrent;
	private Integer secondaryCurrent;
	private Integer burden;
	private Double accuracyClass;	
	
	public MixedMeasuringTransformer() {
	}
	

	public String getDesigningPurpose() {
		return designingPurpose;
	}


	public void setDesigningPurpose(String designingPurpose) {
		this.designingPurpose = designingPurpose;
	}


	public Integer getPrimaryCurrent() {
		return primaryCurrent;
	}


	public void setPrimaryCurrent(Integer primaryCurrent) {
		this.primaryCurrent = primaryCurrent;
	}


	public Integer getSecondaryCurrent() {
		return secondaryCurrent;
	}


	public void setSecondaryCurrent(Integer secondaryCurrent) {
		this.secondaryCurrent = secondaryCurrent;
	}


	public Integer getBurden() {
		return burden;
	}


	public void setBurden(Integer burden) {
		this.burden = burden;
	}


	public Double getAccuracyClass() {
		return accuracyClass;
	}


	public void setAccuracyClass(Double accuracyClass) {
		this.accuracyClass = accuracyClass;
	}


	@Override
	public String[] getFields() {
		String[] fields = new String[5];
		// fields at position zero
		fields[0] = designingPurpose;
		fields[1] = primaryCurrent == null ? "" : String.valueOf(primaryCurrent);
		fields[2] = secondaryCurrent == null ? "" : String.valueOf(secondaryCurrent);
		fields[3] = burden == null ? "" : String.valueOf(burden);
		fields[4] = accuracyClass == null ? "" : String.valueOf(accuracyClass);
		return Stream.concat(Arrays.stream(fields), Arrays.stream(super.getFields()))
				.toArray(String[]::new);
	}
}