package ro.upt.ac.mtb.equipments.groundingknife;

import java.util.Arrays;
import java.util.stream.Stream;

import jakarta.persistence.Entity;
import ro.upt.ac.mtb.equipments.electricalequipment.ElectricalEquipment;
import ro.upt.ac.mtb.equipments.services.ExportableToCSV;

// Separator

@Entity
public class GroundingKnife extends ElectricalEquipment implements ExportableToCSV{

	// tech sheet
	private String equipmentCode;
	private String installationType;
	private String structureType;
	private Integer current;
	private Integer voltage;
	private Integer frequency;
	private String phaseDifference;
	private String relatedShortTimeWithstandCurrent;
	private String altitude;
	private String transportPackage;
	private String tradeMark;
	private String origin;
	private String productionCapacity;

	public GroundingKnife() {
		
	}

	public String getEquipmentCode() {
		return equipmentCode;
	}

	public void setEquipmentCode(String equipmentCode) {
		this.equipmentCode = equipmentCode;
	}

	public String getInstallationType() {
		return installationType;
	}

	public void setInstallationType(String installationType) {
		this.installationType = installationType;
	}

	public String getStructureType() {
		return structureType;
	}

	public void setStructureType(String structureType) {
		this.structureType = structureType;
	}

	public Integer getCurrent() {
		return current;
	}

	public void setCurrent(Integer current) {
		this.current = current;
	}

	public Integer getVoltage() {
		return voltage;
	}

	public void setVoltage(Integer voltage) {
		this.voltage = voltage;
	}

	public Integer getFrequency() {
		return frequency;
	}

	public void setFrequency(Integer frequency) {
		this.frequency = frequency;
	}

	public String getPhaseDifference() {
		return phaseDifference;
	}

	public void setPhaseDifference(String phaseDifference) {
		this.phaseDifference = phaseDifference;
	}

	public String getRelatedShortTimeWithstandCurrent() {
		return relatedShortTimeWithstandCurrent;
	}

	public void setRelatedShortTimeWithstandCurrent(String relatedShortTimeWithstandCurrent) {
		this.relatedShortTimeWithstandCurrent = relatedShortTimeWithstandCurrent;
	}

	public String getAltitude() {
		return altitude;
	}

	public void setAltitude(String altitude) {
		this.altitude = altitude;
	}

	public String getTransportPackage() {
		return transportPackage;
	}

	public void setTransportPackage(String transportPackage) {
		this.transportPackage = transportPackage;
	}

	public String getTradeMark() {
		return tradeMark;
	}

	public void setTradeMark(String tradeMark) {
		this.tradeMark = tradeMark;
	}

	public String getOrigin() {
		return origin;
	}

	public void setOrigin(String origin) {
		this.origin = origin;
	}

	public String getProductionCapacity() {
		return productionCapacity;
	}

	public void setProductionCapacity(String productionCapacity) {
		this.productionCapacity = productionCapacity;
	}
	
	@Override
	public String[] getFields() {
		String[] fields = new String[13];
		// fields at position zero
		fields[0] = equipmentCode;
		fields[1] = installationType;
		fields[2] = structureType;
		fields[3] = current == null ? "" : String.valueOf(current);
		fields[4] = voltage == null ? "" : String.valueOf(voltage);
		fields[5] = frequency == null ? "" : String.valueOf(frequency);
		fields[6] = phaseDifference;
		fields[7] = relatedShortTimeWithstandCurrent;
		fields[8] = altitude;
		fields[9] = transportPackage;
		fields[10] = tradeMark;
		fields[11] = origin;
		fields[12] = productionCapacity;
		return Stream.concat(Arrays.stream(fields), Arrays.stream(super.getFields()))
				.toArray(String[]::new);	
	}
}