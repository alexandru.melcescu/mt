package ro.upt.ac.mtb.equipments.switchequipment;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.Optional;
import java.util.TreeMap;

import org.hibernate.SessionFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.io.ByteArrayResource;
import org.springframework.core.io.Resource;
import org.springframework.data.repository.query.Param;
import org.springframework.http.HttpHeaders;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.validation.ObjectError;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.multipart.MultipartFile;
import org.thymeleaf.TemplateEngine;
import org.thymeleaf.context.Context;
import org.thymeleaf.templatemode.TemplateMode;
import org.thymeleaf.templateresolver.ClassLoaderTemplateResolver;
import org.thymeleaf.util.StringUtils;
import org.xhtmlrenderer.pdf.ITextRenderer;

import jakarta.servlet.http.HttpServletRequest;
import jakarta.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.io.OutputStream;
import java.sql.SQLException;
import java.time.LocalDateTime;
import ro.upt.ac.mtb.equipments.electricalequipment.EquipmentManual;
import ro.upt.ac.mtb.equipments.electricalequipment.EquipmentManualRepository;
import ro.upt.ac.mtb.equipments.services.CSVFileGenerator;
import ro.upt.ac.mtb.equipments.services.ExportableToCSV;

@Controller
@RequestMapping("switch")
public class SwitchEquipmentController
{
	@Autowired
	SwitchEquipmentRepository switchEquipmentRepository;
	
	@Autowired
	EquipmentManualRepository equipmentManualRepository;
	
	@Autowired
	CSVFileGenerator csvFileGenerator;
	
	@Autowired
	SessionFactory sessionFactory;
	
	private static final String EQUIPMENT = "switch";
	
	@PostMapping("/search")
	public String search(@Param("searchTerm") String searchTerm, @Param("searchField") String searchField, Model model) {
		//String searchTerm = (String) model.getAttribute("searchTerm");
		List<SwitchEquipment> data = null;
		System.out.println(searchTerm + " " + searchField);
		
		data = performSearch(searchField, searchTerm);
		
		if (data.size() != 0) {
		    model.addAttribute("equipmentObjects", data);
		}
		model.addAttribute("equipment", EQUIPMENT);
		model.addAttribute("searchFields", getHeadingsWithFields().entrySet());
		model.addAttribute("search", Boolean.TRUE);
		model.addAttribute("searchField", searchField);
		model.addAttribute("searchTerm", searchTerm);
		return EQUIPMENT + "/" + EQUIPMENT + "-read";
	}
	
	private List<SwitchEquipment> performSearch(String searchField, String searchTerm) {
		try {
			if (searchField.equalsIgnoreCase("equipmentCode")) {
				// from a string to an Integer
				return switchEquipmentRepository.findByEquipmentCode(searchTerm);
			} else if (searchField.equalsIgnoreCase("circuitBreakerType")) {
				return switchEquipmentRepository.findByCircuitBreakerType(searchTerm);
			} else if (searchField.equalsIgnoreCase("nominalBreakingCapacityOfSmallInductiveCurrents")) {
				return switchEquipmentRepository.findByNominalBreakingCapacityOfSmallInductiveCurrents(searchTerm);
			} else if (searchField.equalsIgnoreCase("nominalOperatingSequence")) {
				// from a string to an Integer
				return switchEquipmentRepository.findByNominalOperatingSequence(searchTerm);
			} else if (searchField.equalsIgnoreCase("poleNumber")) {
				// from a string to an Integer
				return switchEquipmentRepository.findByPoleNumber(Integer.parseInt(searchTerm));
			} else if (searchField.equalsIgnoreCase("nominalVoltage")) {
				// from a string to an Integer
				return switchEquipmentRepository.findByNominalVoltage(Integer.parseInt(searchTerm));
			} else if (searchField.equalsIgnoreCase("nominalInsulation")) {
				// from a string to an Integer
				return switchEquipmentRepository.findByNominalInsulation(Integer.parseInt(searchTerm));
			} else if (searchField.equalsIgnoreCase("nominalCurrent")) {
				return switchEquipmentRepository.findByNominalCurrent(Integer.parseInt(searchTerm));
			} else if (searchField.equalsIgnoreCase("nominalBreakingCapacityOfNoLoadLineCurrents")) {
				// from a string to an Integer
				return switchEquipmentRepository.findByNominalBreakingCapacityOfNoLoadLineCurrents(Integer.parseInt(searchTerm));
			} else if (searchField.equalsIgnoreCase("nominalBreakingCapacityOfNoLoadCableCurrents")) {
				return switchEquipmentRepository.findByNominalBreakingCapacityOfNoLoadCableCurrents(Integer.parseInt(searchTerm));
			} else if (searchField.equalsIgnoreCase("nominalBreakingCapacityOfCapacitorBankCurrents")) {
				return switchEquipmentRepository.findByNominalBreakingCapacityOfCapacitorBankCurrents(Integer.parseInt(searchTerm));
			} else if (searchField.equalsIgnoreCase("maximumShortCircuitClosingCapacity")) {
				return switchEquipmentRepository.findByMaximumShortCircuitClosingCapacity(Integer.parseInt(searchTerm));
			} else if (searchField.equalsIgnoreCase("nominalClosingTime")) {
				return switchEquipmentRepository.findByNominalClosingTime(Integer.parseInt(searchTerm));
			} else if (searchField.equalsIgnoreCase("nominalOpeningTime")) {
				return switchEquipmentRepository.findByNominalOpeningTime(Integer.parseInt(searchTerm));
			} else if (searchField.equalsIgnoreCase("nominalInterruptionTime")) {
				return switchEquipmentRepository.findByNominalInterruptionTime(Integer.parseInt(searchTerm));
			} else if (searchField.equalsIgnoreCase("nominalBreakingCapacityOfShortCircuitCurrentsAtTerminal")) {
				return switchEquipmentRepository.findByNominalBreakingCapacityOfShortCircuitCurrentsAtTerminal(Double.parseDouble(searchTerm));
			} else if (searchField.equalsIgnoreCase("admissibleDurationOfShortCircuitCurrents")) {
				return switchEquipmentRepository.findByAdmissibleDurationOfShortCircuitCurrents(Double.parseDouble(searchTerm));
			} else if (searchField.equalsIgnoreCase("equipmentSeries")) {
				return switchEquipmentRepository.findByEquipmentSeries(searchTerm);
			} else if (searchField.equalsIgnoreCase("manufacturingYear")) {
				return switchEquipmentRepository.findByManufacturingYear(Integer.parseInt(searchTerm));
			} else if (searchField.equalsIgnoreCase("pifYear")) {
				return switchEquipmentRepository.findByPifYear(Integer.parseInt(searchTerm));
			} else if (searchField.equalsIgnoreCase("installationCompany")) {
				return switchEquipmentRepository.findByInstallationCompany(searchTerm);
			} else if (searchField.equalsIgnoreCase("pifBulletin")) {
				return switchEquipmentRepository.findByPifBulletin(searchTerm);
			} else if (searchField.equalsIgnoreCase("exploitationCenter")) {
				return switchEquipmentRepository.findByExploitationCenter(searchTerm);
			} else if (searchField.equalsIgnoreCase("transformationCenter")) {
				return switchEquipmentRepository.findByTransformationStation(searchTerm);
			} else if (searchField.equalsIgnoreCase("voltageLevel")) {
				return switchEquipmentRepository.findByVoltageLevel(searchTerm);
			} else if (searchField.equalsIgnoreCase("assemblyCell")) {
				return switchEquipmentRepository.findByAssemblyCell(searchTerm);
			} else if (searchField.equalsIgnoreCase("assemblyPhase")) {
				return switchEquipmentRepository.findByAssemblyPhase(searchTerm);
			} else if (searchField.equalsIgnoreCase("accountingorWarehouseRegistrationCode")) {
				return switchEquipmentRepository.findByAccountingorWarehouseRegistrationCode(searchTerm);
			} else if (searchField.equalsIgnoreCase("fixedAssetComponentNumber")) {
				return switchEquipmentRepository.findByFixedAssetComponentNumber(searchTerm);				
			} else {
				return switchEquipmentRepository.search(searchTerm);
			}		
		} catch (NumberFormatException e) {
			return switchEquipmentRepository.search(searchTerm);
		}
	}

	@PostMapping("/uploadFile")
    public String uploadFile(@RequestParam("file") MultipartFile file, Model model) {
        EquipmentManual equipmentManual = new EquipmentManual();

        // setting up the entity of the equipment manual
        equipmentManual.setId(EQUIPMENT);
        equipmentManual.setOperatingManualFileType(file.getContentType());
        equipmentManual.setOperatingManualFileName(file.getOriginalFilename());
        
        try {
			//equipmentManual.setOperatingManual(file.getBytes());
        	
        	// create a blob object so that we can store anything bigger and 255 bytes
        	equipmentManual.setOperatingManual(sessionFactory.openSession().getLobHelper().createBlob(file.getBytes()));
			equipmentManualRepository.save(equipmentManual);
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
        
        return read(model); 
	}
	
	@GetMapping("/downloadFile")
    public ResponseEntity <Resource> downloadFile(HttpServletRequest request) {
        // Load file as Resource
        Optional<EquipmentManual> equipmentManual = equipmentManualRepository.findById(EQUIPMENT);

        if (equipmentManual.isPresent()) {
        	try {
				return ResponseEntity.ok()
				        .contentType(MediaType.parseMediaType(equipmentManual.get().getOperatingManualFileType()))
				        .header(HttpHeaders.CONTENT_DISPOSITION, "attachment; filename=\"" + equipmentManual.get().getOperatingManualFileName() + "\"")
				        .body(new ByteArrayResource(equipmentManual.get().getOperatingManual().getBytes(1, (int) equipmentManual.get().getOperatingManual().length())));
			} catch (SQLException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
        }
        return ResponseEntity.notFound()
        		.build();
    }
	
	@GetMapping("/create")
	public String create(SwitchEquipment switchEquipment, Model model)
	{
		model.addAttribute("equipment", EQUIPMENT);
		model.addAttribute("equipmentObject", new SwitchEquipment());
		return EQUIPMENT + "/" + EQUIPMENT + "-create";
	}

	@PostMapping("/create-save")
	// @ModelAttribute("equipmentObject") connects the create-save method to the create html page of the equipment
	public String createSave(@Validated @ModelAttribute("equipmentObject") SwitchEquipment switchEquipment, BindingResult result, Model model)
	{
		if(result.hasErrors())
		{
			// without line 144 we don't have the equipment object in the request, since we need it 
			model.addAttribute("equipmentObject", switchEquipment);
			return EQUIPMENT + "/" + EQUIPMENT + "-create";
		}
		
		// update the time stamp before saving the record
	    switchEquipment.setUpdateTimeStamp(LocalDateTime.now());		
		switchEquipmentRepository.save(switchEquipment);
		return "redirect:read";
	}
	
	@GetMapping("/read")
	public String read(Model model) 
	{
		model.addAttribute("equipment", EQUIPMENT);
		List<SwitchEquipment> switches = switchEquipmentRepository.findAll();
		if (switches.size() != 0) 
		{
			System.out.println(switches); 
			model.addAttribute("equipmentObjects", switches);
		}
		model.addAttribute("searchFields", getHeadingsWithFields().entrySet());
		return EQUIPMENT + "/" + EQUIPMENT + "-read";
	}
	
	@GetMapping("/edit/{id}")
	public String edit(@PathVariable("id") Long id, Model model) 
	{
		model.addAttribute("equipment", EQUIPMENT);
		Optional<SwitchEquipment> switchEquipment = switchEquipmentRepository.findById(id);
	    model.addAttribute("equipmentObject", switchEquipment.get());
	    
		return EQUIPMENT + "/" + EQUIPMENT + "-update";
	}
	
	@PostMapping("/update/{id}")
	public String update(@PathVariable("id") Long id, @Validated @ModelAttribute("equipmentObject") SwitchEquipment switchEquipment, BindingResult result, Model model) 
	{
	    // check to see that the user is updating using fresh data
		if (switchEquipmentRepository.findByIdAndUpdateTimeStamp(id, switchEquipment.getUpdateTimeStamp()).isEmpty()) {
			// if it's empty = not dealing with fresh data
			// reject allows 191 to have errors
			// each time the addError() function gets used, it means there is a global error
			result.addError(new ObjectError("global", "Another user has already updated the data"));
			// message is coming from line 190
			model.addAttribute("globalError", "Another user has already updated the data");
		}
		
	    if(result.hasErrors()) 
	    {
	    	switchEquipment.setId(id);
			model.addAttribute("equipmentObject", switchEquipment);
			return EQUIPMENT + "/" + EQUIPMENT + "-update";
	    }
	    // update the time stamp
	    switchEquipment.setUpdateTimeStamp(LocalDateTime.now());
	    // save the record
	    switchEquipmentRepository.save(switchEquipment);
	    
		return "redirect:../read";
	}
	
	@GetMapping("/delete/{id}")
	public String delete(@PathVariable("id") Long id, Model model) 
	{
		Optional<SwitchEquipment> switchEquipment = switchEquipmentRepository.findById(id);
	    
		switchEquipmentRepository.delete(switchEquipment.get());
		return "redirect:../read";
	}	
	
	@GetMapping("/exportToCSV")
	public void exportToCSV(@Param("searchTerm") String searchTerm, @Param("searchField") String searchField, HttpServletResponse response, Model model) throws IOException {
		response.setContentType("text/csv");
		//  how it gets saved
		response.addHeader("Content-Disposition", "attachment; filename = \"" + StringUtils.capitalize(EQUIPMENT) + "s.csv\"");

		// "attachment; filename = \"Separators.csv\"" is the header 
		List<ExportableToCSV> data = new ArrayList<>();
		if (searchField != null && searchTerm != null && !searchTerm.trim().equals("") ) {
			data.addAll(performSearch(searchField, searchTerm));
		} else {
			data.addAll(switchEquipmentRepository.findAll()); // gets every record in the separator table and adds it to the list that gets exported
		}
		csvFileGenerator.write(getHeadings(), data, response.getWriter()); // write to the response object with the header
	}
	// controller handles the headings, entities handle the data
	
	private static String[] getHeadings() {
		String[] fields = new String[30];
		// fields at position zero
		fields[0] = "Equipment Code";
		fields[1] = "Circuit Breaker Type";
		fields[2] = "Nominal Breaking Capacity Of Small Inductive Currents";
		fields[3] = "Kilometric Fault Disconnecting Capacity";
		fields[4] = "Nominal Operating Sequence";
		fields[5] = "Pole Number";
		fields[6] = "Nominal Voltage";
		fields[7] = "Nominal Insulation";
		fields[8] = "Nominal Current";
		fields[9] = "Nominal Breaking Capacity Of No Load Line Currents";
		fields[10] = "Nominal Breaking Capacity Of No Load Cable Currents";
		fields[11] = "Nominal Breaking Capacity Of Capacitor Bank Currents";
		fields[12] = "Maximum Short Circuit Closing Capacity";
		fields[13] = "Nominal Closing Time";
		fields[14] = "Nominal Opening Time";
		fields[15] = "Nominal Interruption Time";
		fields[16] = "Nominal Breaking Capacity Of Short Circuit Currents At Terminal";
		fields[17] = "Admissible Duration Of Short Circuit Currents";
		fields[18] = "Equipment Series";
		fields[19] = "Manufacturing Year";
		fields[20] = "Pif Year";
		fields[21] = "Installation Company";
		fields[22] = "Pif Bulletin";
		fields[23] = "Exploitation Center";
		fields[24] = "Transformation Station";
		fields[25] = "Voltage Level";
		fields[26] = "Assembly Cell";
		fields[27] = "Assembly Phase";
		fields[28] = "Accounting or Warehouse Registration Code";
		fields[29] = "Fixed Asset Component Number";

		return fields;
	}
	
	private static String[] getFieldNames() {
		String[] fields = new String[30];
		// fields at position zero
		fields[0] = "equipmentCode";
		fields[1] = "circuitBreakerType";
		fields[2] = "nominalBreakingCapacityOfSmallInductiveCurrents";
		fields[3] = "kilometricFaultDisconnectingCapacity";
		fields[4] = "nominalOperatingSequence";
		fields[5] = "poleNumber";
		fields[6] = "nominalVoltage";
		fields[7] = "nominalInsulation";
		fields[8] = "nominalCurrent";
		fields[9] = "nominalBreakingCapacityOfNoLoadLineCurrents";
		fields[10] = "nominalBreakingCapacityOfNoLoadCableCurrents";
		fields[11] = "nominalBreakingCapacityOfCapacitorBankCurrents";
		fields[12] = "maximumShortCircuitClosingCapacity";
		fields[13] = "nominalClosingTime";
		fields[14] = "nominalOpeningTime";
		fields[15] = "nominalInterruptionTime";
		fields[16] = "nominalBreakingCapacityOfShortCircuitCurrentsAtTerminal";
		fields[17] = "admissibleDurationOfShortCircuitCurrents";
		fields[18] = "equipmentSeries";
		fields[19] = "manufacturingYear";
		fields[20] = "pifYear";
		fields[21] = "installationCompany";
		fields[22] = "pifBulletin";
		fields[23] = "exploitationCenter";
		fields[24] = "transformationStation";
		fields[25] = "voltageLevel";
		fields[26] = "assemblyCell";
		fields[27] = "assemblyPhase";
		fields[28] = "accountingorWarehouseRegistrationCode";
		fields[29] = "fixedAssetComponentNumber";

		return fields;
	}
	
	private static Map<String, String> getHeadingsWithFields() {
		Map<String, String> map = new TreeMap<>();
		map.put("* Search All Fields", "*");
		String[] headings = getHeadings();
		String[] fieldNames = getFieldNames();
		for (int x = 0; x < headings.length; x++) {
			map.put(headings[x], fieldNames[x]);
		}
		
		return map;
	}


	@GetMapping("/exportToPDF")
	public void exportToPDF(HttpServletResponse response, Model model) throws IOException {
		/* String outputFolder = "C:\\Users\\MY PC\\Downloads" + File.arrester + "Arresters.pdf";
		 OutputStream outputStream = new FileOutputStream(outputFolder);
		*/
		
		// getOutputStream() allows the PDF to be opened in the browser rather than a specific location on the server
		OutputStream outputStream = response.getOutputStream();

		response.addHeader("Content-Disposition", "attachment; filename = \"" + StringUtils.capitalize(EQUIPMENT) + "es.pdf\"");
		
	    ITextRenderer renderer = new ITextRenderer();
	    renderer.setDocumentFromString(parseThymeleafTemplate());
	    renderer.layout();
	    renderer.createPDF(outputStream);

	    outputStream.close();
	}
	
	
	private String parseThymeleafTemplate() {
	    ClassLoaderTemplateResolver templateResolver = new ClassLoaderTemplateResolver();
	    templateResolver.setSuffix(".html");
	    templateResolver.setTemplateMode(TemplateMode.HTML);

	    TemplateEngine templateEngine = new TemplateEngine();
	    templateEngine.setTemplateResolver(templateResolver);
	    
	    // model.addAttribute("equipment", "separator");
		List<SwitchEquipment> switches = switchEquipmentRepository.findAll();

	    Context context = new Context();
	    context.setVariable("equipmentObjects", switches);

	    return templateEngine.process("templates/switch/switch-pdf", context);
	}
}