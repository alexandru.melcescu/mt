package ro.upt.ac.mtb.equipments.groundingknife;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.Optional;
import java.util.TreeMap;

import org.hibernate.SessionFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.io.ByteArrayResource;
import org.springframework.core.io.Resource;
import org.springframework.data.repository.query.Param;
import org.springframework.http.HttpHeaders;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.validation.ObjectError;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.multipart.MultipartFile;
import org.thymeleaf.TemplateEngine;
import org.thymeleaf.context.Context;
import org.thymeleaf.templatemode.TemplateMode;
import org.thymeleaf.templateresolver.ClassLoaderTemplateResolver;
import org.xhtmlrenderer.pdf.ITextRenderer;

import jakarta.servlet.http.HttpServletRequest;
import jakarta.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.io.OutputStream;
import java.sql.SQLException;
import java.time.LocalDateTime;
import ro.upt.ac.mtb.equipments.electricalequipment.EquipmentManual;
import ro.upt.ac.mtb.equipments.electricalequipment.EquipmentManualRepository;
import ro.upt.ac.mtb.equipments.services.CSVFileGenerator;
import ro.upt.ac.mtb.equipments.services.ExportableToCSV;

@Controller
@RequestMapping("groundingKnife")
public class GroundingKnifeController
{
	@Autowired
	GroundingKnifeRepository groundingKnifeRepository;
	
	@Autowired
	EquipmentManualRepository equipmentManualRepository;
	
	@Autowired
	CSVFileGenerator csvFileGenerator;
	
	@Autowired
	SessionFactory sessionFactory;
	
	private static final String EQUIPMENT = "groundingKnife";
	
	@PostMapping("/search")
	public String search(@Param("searchTerm") String searchTerm, @Param("searchField") String searchField, Model model) {
		//String searchTerm = (String) model.getAttribute("searchTerm");
		List<GroundingKnife> data = null;
		System.out.println(searchTerm + " " + searchField);
		
		data = performSearch(searchField, searchTerm);
		
		if (data.size() != 0) {
		    model.addAttribute("equipmentObjects", data);
		}
		model.addAttribute("equipment", EQUIPMENT);
		model.addAttribute("searchFields", getHeadingsWithFields().entrySet());
		model.addAttribute("search", Boolean.TRUE);
		model.addAttribute("searchField", searchField);
		model.addAttribute("searchTerm", searchTerm);
		return EQUIPMENT + "/" + EQUIPMENT + "-read";
	}
	
	private List<GroundingKnife> performSearch(String searchField, String searchTerm) {
		try {
			if (searchField.equalsIgnoreCase("equipmentCode")) {
				return groundingKnifeRepository.findByEquipmentCode(searchTerm);
			} else if (searchField.equalsIgnoreCase("installationType")) {
				return groundingKnifeRepository.findByInstallationType(searchTerm);
			} else if (searchField.equalsIgnoreCase("structureType")) {
				return groundingKnifeRepository.findByStructureType(searchTerm);
			} else if (searchField.equalsIgnoreCase("current")) {
				return groundingKnifeRepository.findByCurrent(Integer.parseInt(searchTerm));
			} else if (searchField.equalsIgnoreCase("voltage")) {
				return groundingKnifeRepository.findByVoltage(Integer.parseInt(searchTerm));
			} else if (searchField.equalsIgnoreCase("frequency")) {
				return groundingKnifeRepository.findByFrequency(Integer.parseInt(searchTerm));
			} else if (searchField.equalsIgnoreCase("phaseDifference")) {
				return groundingKnifeRepository.findByPhaseDifference(searchTerm);
			} else if (searchField.equalsIgnoreCase("relatedShortTimeWithstandCurrent")) {
				return groundingKnifeRepository.findByRelatedShortTimeWithstandCurrent(searchTerm);
			} else if (searchField.equalsIgnoreCase("altitude")) {
				return groundingKnifeRepository.findByAltitude(searchTerm);
			} else if (searchField.equalsIgnoreCase("transportPackage")) {
				return groundingKnifeRepository.findByTransportPackage(searchTerm);
			} else if (searchField.equalsIgnoreCase("tradeMark")) {
				return groundingKnifeRepository.findByTradeMark(searchTerm);
			} else if (searchField.equalsIgnoreCase("origin")) {
				return groundingKnifeRepository.findByOrigin(searchTerm);
			} else if (searchField.equalsIgnoreCase("productionCapacity")) {
				return groundingKnifeRepository.findByProductionCapacity(searchTerm);
			} else if (searchField.equalsIgnoreCase("equipmentSeries")) {
				return groundingKnifeRepository.findByEquipmentSeries(searchTerm);
			} else if (searchField.equalsIgnoreCase("manufacturingYear")) {
				return groundingKnifeRepository.findByManufacturingYear(Integer.parseInt(searchTerm));
			} else if (searchField.equalsIgnoreCase("pifYear")) {
				return groundingKnifeRepository.findByPifYear(Integer.parseInt(searchTerm));
			} else if (searchField.equalsIgnoreCase("installationCompany")) {
				return groundingKnifeRepository.findByInstallationCompany(searchTerm);
			} else if (searchField.equalsIgnoreCase("pifBulletin")) {
				return groundingKnifeRepository.findByPifBulletin(searchTerm);
			} else if (searchField.equalsIgnoreCase("exploitationCenter")) {
				return groundingKnifeRepository.findByExploitationCenter(searchTerm);
			} else if (searchField.equalsIgnoreCase("transformationCenter")) {
				return groundingKnifeRepository.findByTransformationStation(searchTerm);
			} else if (searchField.equalsIgnoreCase("voltageLevel")) {
				return groundingKnifeRepository.findByVoltageLevel(searchTerm);
			} else if (searchField.equalsIgnoreCase("assemblyCell")) {
				return groundingKnifeRepository.findByAssemblyCell(searchTerm);
			} else if (searchField.equalsIgnoreCase("assemblyPhase")) {
				return groundingKnifeRepository.findByAssemblyPhase(searchTerm);
			} else if (searchField.equalsIgnoreCase("accountingorWarehouseRegistrationCode")) {
				return groundingKnifeRepository.findByAccountingorWarehouseRegistrationCode(searchTerm);
			} else if (searchField.equalsIgnoreCase("fixedAssetComponentNumber")) {
				return groundingKnifeRepository.findByFixedAssetComponentNumber(searchTerm);				
			} else {
				return groundingKnifeRepository.search(searchTerm);
			}		
		} catch (NumberFormatException e) {
			return groundingKnifeRepository.search(searchTerm);
		}
	}
	
	@PostMapping("/uploadFile")
    public String uploadFile(@RequestParam("file") MultipartFile file, Model model) {
        EquipmentManual equipmentManual = new EquipmentManual();

        // setting up the entity of the equipment manual
        equipmentManual.setId(EQUIPMENT);
        equipmentManual.setOperatingManualFileType(file.getContentType());
        equipmentManual.setOperatingManualFileName(file.getOriginalFilename());
        
        try {
			//equipmentManual.setOperatingManual(file.getBytes());
        	
        	// create a blob object so that we can store anything bigger and 255 bytes
        	equipmentManual.setOperatingManual(sessionFactory.openSession().getLobHelper().createBlob(file.getBytes()));
			equipmentManualRepository.save(equipmentManual);
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
        
        return read(model); 
	}
	
	@GetMapping("/downloadFile")
    public ResponseEntity <Resource> downloadFile(HttpServletRequest request) {
        // Load file as Resource
        Optional<EquipmentManual> equipmentManual = equipmentManualRepository.findById(EQUIPMENT);

        if (equipmentManual.isPresent()) {
        	try {
				return ResponseEntity.ok()
				        .contentType(MediaType.parseMediaType(equipmentManual.get().getOperatingManualFileType()))
				        .header(HttpHeaders.CONTENT_DISPOSITION, "attachment; filename=\"" + equipmentManual.get().getOperatingManualFileName() + "\"")
				        .body(new ByteArrayResource(equipmentManual.get().getOperatingManual().getBytes(1, (int) equipmentManual.get().getOperatingManual().length())));
			} catch (SQLException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
        }
        return ResponseEntity.notFound()
        		.build();
    }
	
	@GetMapping("/create")
	public String create(GroundingKnife groundingKnife, Model model)
	{
		model.addAttribute("equipment", EQUIPMENT);
		model.addAttribute("equipmentObject", new GroundingKnife());
		return EQUIPMENT + "/" + EQUIPMENT + "-create";
	}

	@PostMapping("/create-save")
	// @ModelAttribute("equipmentObject") connects the create-save method to the create html page of the equipment
	public String createSave(@Validated @ModelAttribute("equipmentObject") GroundingKnife groundingKnife, BindingResult result, Model model)
	{
		if(result.hasErrors())
		{
			// without line 144 we don't have the equipment object in the request, since we need it 
			model.addAttribute("equipmentObject", groundingKnife);
			return EQUIPMENT + "/" + EQUIPMENT + "-create";
		}
		
		// update the time stamp before saving the record
	    groundingKnife.setUpdateTimeStamp(LocalDateTime.now());		
		groundingKnifeRepository.save(groundingKnife);
		return "redirect:read";
	}
	
	@GetMapping("/read")
	public String read(Model model) 
	{
		model.addAttribute("equipment", EQUIPMENT);
		List<GroundingKnife> groundingKnives = groundingKnifeRepository.findAll();
		if (groundingKnives.size() != 0) 
		{
			System.out.println(groundingKnives); 
			model.addAttribute("equipmentObjects", groundingKnives);
		}
		model.addAttribute("searchFields", getHeadingsWithFields().entrySet());
		return EQUIPMENT + "/" + EQUIPMENT + "-read";
	}
	
	@GetMapping("/edit/{id}")
	public String edit(@PathVariable("id") Long id, Model model) 
	{
		model.addAttribute("equipment", EQUIPMENT);
		Optional<GroundingKnife> groundingKnife = groundingKnifeRepository.findById(id);
	    model.addAttribute("equipmentObject", groundingKnife.get());
	    
		return EQUIPMENT + "/" + EQUIPMENT + "-update";
	}
	
	@PostMapping("/update/{id}")
	public String update(@PathVariable("id") Long id, @Validated @ModelAttribute("equipmentObject") GroundingKnife groundingKnife, BindingResult result, Model model) 
	{
	    // check to see that the user is updating using fresh data
		if (groundingKnifeRepository.findByIdAndUpdateTimeStamp(id, groundingKnife.getUpdateTimeStamp()).isEmpty()) {
			// if it's empty = not dealing with fresh data
			// reject allows 191 to have errors
			// each time the addError() function gets used, it means there is a global error
			result.addError(new ObjectError("global", "Another user has already updated the data"));
			// message is coming from line 190
			model.addAttribute("globalError", "Another user has already updated the data");
		}
		
	    if(result.hasErrors()) 
	    {
	    	groundingKnife.setId(id);
			model.addAttribute("equipmentObject", groundingKnife);
			return EQUIPMENT + "/" + EQUIPMENT + "-update";
	    }
	    // update the time stamp
	    groundingKnife.setUpdateTimeStamp(LocalDateTime.now());
	    // save the record
	    groundingKnifeRepository.save(groundingKnife);
	    
		return "redirect:../read";
	}
	
	@GetMapping("/delete/{id}")
	public String delete(@PathVariable("id") Long id, Model model) 
	{
		Optional<GroundingKnife> groundingKnife = groundingKnifeRepository.findById(id);
	    
		groundingKnifeRepository.delete(groundingKnife.get());
		return "redirect:../read";
	}	
	
	@GetMapping("/exportToCSV")
	public void export(HttpServletResponse response, Model model) throws IOException {
		response.setContentType("text/csv");
		//  how it gets saved
		response.addHeader("Content-Disposition", "attachment; filename = \"GroundingKnives.csv\"");
		// "attachment; filename = \"Separators.csv\"" is the header 
		List<ExportableToCSV> data = new ArrayList<>();
		data.addAll(groundingKnifeRepository.findAll()); // gets every record in the separator table and adds it to the list that gets exported
		csvFileGenerator.write(getHeadings(), data, response.getWriter()); // write to the response object with the header
	}
	// controller handles the headings, entities handle the data
	
	private static String[] getHeadings() {
		String[] fields = new String[25];
		// fields at position zero
		fields[0] = "Equipment Code";
		fields[1] = "Installation Type";
		fields[2] = "Structure Type";
		fields[3] = "Current";
		fields[4] = "Voltage";
		fields[5] = "Frequency";
		fields[6] = "Phase Difference";
		fields[7] = "Related Short Time Withstand Current";
		fields[8] = "Altitude";
		fields[9] = "Transport Package";
		fields[10] = "Trademark";
		fields[11] = "Origin";
		fields[12] = "Production Capacity";
		fields[13] = "Equipment Series";
		fields[14] = "Manufacturing Year";
		fields[15] = "Pif Year";
		fields[16] = "Installation Company";
		fields[17] = "Pif Bulletin";
		fields[18] = "Exploitation Center";
		fields[19] = "Transformation Station";
		fields[20] = "Voltage Level";
		fields[21] = "Assembly Cell";
		fields[22] = "Assembly Phase";
		fields[23] = "Accounting or Warehouse Registration Code";
		fields[24] = "Fixed Asset Component Number";
		return fields;
	}
	
	private static String[] getFieldNames() {
		String[] fields = new String[25];
		// fields at position zero
		fields[0] = "equipmentCode";
		fields[1] = "installationType";
		fields[2] = "structureType";
		fields[3] = "current";
		fields[4] = "voltage";
		fields[5] = "frequency";
		fields[6] = "phaseDifference";
		fields[7] = "relatedShortTimeWithstandCurrent";
		fields[8] = "altitude";
		fields[9] = "transportPackage";
		fields[10] = "tradeMark";
		fields[11] = "origin";
		fields[12] = "productionCapacity";
		fields[13] = "equipmentSeries";
		fields[14] = "manufacturingYear";
		fields[15] = "pifYear";
		fields[16] = "installationCompany";
		fields[17] = "pifBulletin";
		fields[18] = "exploitationCenter";
		fields[19] = "transformationStation";
		fields[20] = "voltageLevel";
		fields[21] = "assemblyCell";
		fields[22] = "assemblyPhase";
		fields[23] = "accountingorWarehouseRegistrationCode";
		fields[24] = "fixedAssetComponentNumber";
		return fields;
	}
	
	private static Map<String, String> getHeadingsWithFields() {
		Map<String, String> map = new TreeMap<>();
		map.put("* Search All Fields", "*");
		String[] headings = getHeadings();
		String[] fieldNames = getFieldNames();
		for (int x = 0; x < headings.length; x++) {
			map.put(headings[x], fieldNames[x]);
		}
		
		return map;
	}

	@GetMapping("/exportToPDF")
	public void exportToPDF(HttpServletResponse response, Model model) throws IOException {
		/* String outputFolder = "C:\\Users\\MY PC\\Downloads" + File.arrester + "Arresters.pdf";
		 OutputStream outputStream = new FileOutputStream(outputFolder);
		*/
		
		// getOutputStream() allows the PDF to be opened in the browser rather than a specific location on the server
		OutputStream outputStream = response.getOutputStream();

		response.addHeader("Content-Disposition", "attachment; filename = \"GroundingKnives.pdf\"");
		
	    ITextRenderer renderer = new ITextRenderer();
	    renderer.setDocumentFromString(parseThymeleafTemplate());
	    renderer.layout();
	    renderer.createPDF(outputStream);

	    outputStream.close();
	}
	
	
	private String parseThymeleafTemplate() {
	    ClassLoaderTemplateResolver templateResolver = new ClassLoaderTemplateResolver();
	    templateResolver.setSuffix(".html");
	    templateResolver.setTemplateMode(TemplateMode.HTML);

	    TemplateEngine templateEngine = new TemplateEngine();
	    templateEngine.setTemplateResolver(templateResolver);
	    
	    // model.addAttribute("equipment", "separator");
		List<GroundingKnife> groundingKnives = groundingKnifeRepository.findAll();

	    Context context = new Context();
	    context.setVariable("equipmentObjects", groundingKnives);

	    return templateEngine.process("templates/groundingKnife/groundingKnife-pdf", context);
	}
}