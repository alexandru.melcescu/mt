package ro.upt.ac.mtb.equipments.currentmeasuringtransformer;

import java.util.Arrays;
import java.util.stream.Stream;

import jakarta.persistence.Entity;
import ro.upt.ac.mtb.equipments.electricalequipment.ElectricalEquipment;
import ro.upt.ac.mtb.equipments.services.ExportableToCSV;

// Current Measuring Transformer

@Entity
public class CurrentMeasuringTransformer extends ElectricalEquipment implements ExportableToCSV
{
	private String equipmentCode;
	private Double nominalVoltage;

	private Double primaryNominalCurrent;

	private Double secondaryNominalCurrent1;
	private Double secondaryNominalCurrent2;
	private Double secondaryNominalCurrent3;
	private Double secondaryNominalCurrent4;
	private Double secondaryNominalCurrent5;
	private Double secondaryNominalCurrent6;

	private Double secondaryPower1;
	private Double secondaryPower2;
	private Double secondaryPower3;
	private Double secondaryPower4;
	private Double secondaryPower5;
	private Double secondaryPower6;

	private Double secondaryPrecisionClass1;
	private Double secondaryPrecisionClass2;
	private Double secondaryPrecisionClass3;
	private Double secondaryPrecisionClass4;
	private Double secondaryPrecisionClass5;
	private Double secondaryPrecisionClass6;

	private String operatingEnvironment;
	private String insulationType;
	private String operatingMode;
	private String manufacturer;
	
	public CurrentMeasuringTransformer() {
	}


	public String getEquipmentCode() {
		return equipmentCode;
	}


	public void setEquipmentCode(String equipmentCode) {
		this.equipmentCode = equipmentCode;
	}


	public Double getNominalVoltage() {
		return nominalVoltage;
	}


	public void setNominalVoltage(Double nominalVoltage) {
		this.nominalVoltage = nominalVoltage;
	}


	public Double getPrimaryNominalCurrent() {
		return primaryNominalCurrent;
	}


	public void setPrimaryNominalCurrent(Double primaryNominalCurrent) {
		this.primaryNominalCurrent = primaryNominalCurrent;
	}


	public Double getSecondaryNominalCurrent1() {
		return secondaryNominalCurrent1;
	}


	public void setSecondaryNominalCurrent1(Double secondaryNominalCurrent1) {
		this.secondaryNominalCurrent1 = secondaryNominalCurrent1;
	}


	public Double getSecondaryNominalCurrent2() {
		return secondaryNominalCurrent2;
	}


	public void setSecondaryNominalCurrent2(Double secondaryNominalCurrent2) {
		this.secondaryNominalCurrent2 = secondaryNominalCurrent2;
	}


	public Double getSecondaryNominalCurrent3() {
		return secondaryNominalCurrent3;
	}


	public void setSecondaryNominalCurrent3(Double secondaryNominalCurrent3) {
		this.secondaryNominalCurrent3 = secondaryNominalCurrent3;
	}


	public Double getSecondaryNominalCurrent4() {
		return secondaryNominalCurrent4;
	}


	public void setSecondaryNominalCurrent4(Double secondaryNominalCurrent4) {
		this.secondaryNominalCurrent4 = secondaryNominalCurrent4;
	}


	public Double getSecondaryNominalCurrent5() {
		return secondaryNominalCurrent5;
	}


	public void setSecondaryNominalCurrent5(Double secondaryNominalCurrent5) {
		this.secondaryNominalCurrent5 = secondaryNominalCurrent5;
	}


	public Double getSecondaryNominalCurrent6() {
		return secondaryNominalCurrent6;
	}


	public void setSecondaryNominalCurrent6(Double secondaryNominalCurrent6) {
		this.secondaryNominalCurrent6 = secondaryNominalCurrent6;
	}


	public Double getSecondaryPower1() {
		return secondaryPower1;
	}


	public void setSecondaryPower1(Double secondaryPower1) {
		this.secondaryPower1 = secondaryPower1;
	}


	public Double getSecondaryPower2() {
		return secondaryPower2;
	}


	public void setSecondaryPower2(Double secondaryPower2) {
		this.secondaryPower2 = secondaryPower2;
	}


	public Double getSecondaryPower3() {
		return secondaryPower3;
	}


	public void setSecondaryPower3(Double secondaryPower3) {
		this.secondaryPower3 = secondaryPower3;
	}


	public Double getSecondaryPower4() {
		return secondaryPower4;
	}


	public void setSecondaryPower4(Double secondaryPower4) {
		this.secondaryPower4 = secondaryPower4;
	}


	public Double getSecondaryPower5() {
		return secondaryPower5;
	}


	public void setSecondaryPower5(Double secondaryPower5) {
		this.secondaryPower5 = secondaryPower5;
	}


	public Double getSecondaryPower6() {
		return secondaryPower6;
	}


	public void setSecondaryPower6(Double secondaryPower6) {
		this.secondaryPower6 = secondaryPower6;
	}


	public Double getSecondaryPrecisionClass1() {
		return secondaryPrecisionClass1;
	}


	public void setSecondaryPrecisionClass1(Double secondaryPrecisionClass1) {
		this.secondaryPrecisionClass1 = secondaryPrecisionClass1;
	}


	public Double getSecondaryPrecisionClass2() {
		return secondaryPrecisionClass2;
	}


	public void setSecondaryPrecisionClass2(Double secondaryPrecisionClass2) {
		this.secondaryPrecisionClass2 = secondaryPrecisionClass2;
	}


	public Double getSecondaryPrecisionClass3() {
		return secondaryPrecisionClass3;
	}


	public void setSecondaryPrecisionClass3(Double secondaryPrecisionClass3) {
		this.secondaryPrecisionClass3 = secondaryPrecisionClass3;
	}


	public Double getSecondaryPrecisionClass4() {
		return secondaryPrecisionClass4;
	}


	public void setSecondaryPrecisionClass4(Double secondaryPrecisionClass4) {
		this.secondaryPrecisionClass4 = secondaryPrecisionClass4;
	}


	public Double getSecondaryPrecisionClass5() {
		return secondaryPrecisionClass5;
	}


	public void setSecondaryPrecisionClass5(Double secondaryPrecisionClass5) {
		this.secondaryPrecisionClass5 = secondaryPrecisionClass5;
	}


	public Double getSecondaryPrecisionClass6() {
		return secondaryPrecisionClass6;
	}


	public void setSecondaryPrecisionClass6(Double secondaryPrecisionClass6) {
		this.secondaryPrecisionClass6 = secondaryPrecisionClass6;
	}


	public String getOperatingEnvironment() {
		return operatingEnvironment;
	}


	public void setOperatingEnvironment(String operatingEnvironment) {
		this.operatingEnvironment = operatingEnvironment;
	}


	public String getInsulationType() {
		return insulationType;
	}


	public void setInsulationType(String insulationType) {
		this.insulationType = insulationType;
	}


	public String getOperatingMode() {
		return operatingMode;
	}


	public void setOperatingMode(String operatingMode) {
		this.operatingMode = operatingMode;
	}


	public String getManufacturer() {
		return manufacturer;
	}


	public void setManufacturer(String manufacturer) {
		this.manufacturer = manufacturer;
	}
	
	@Override
	public String[] getFields() {
		String[] fields = new String[25];
		// fields at position zero
		fields[0] = equipmentCode;
		// "== null ? "" :" ==> if-else, ? = ternary operator,
		// if nominal voltage is null, then return an empty string; else, return a number as a string
		// write this line of code only for nullable columns that are not strings, such as Double for nominal voltage
		fields[1] = nominalVoltage == null ? "" : String.valueOf(nominalVoltage);
		fields[2] = primaryNominalCurrent == null ? "" : String.valueOf(primaryNominalCurrent);
		fields[3] = secondaryNominalCurrent1 == null ? "" : String.valueOf(secondaryNominalCurrent1);
		fields[4] = secondaryNominalCurrent2 == null ? "" : String.valueOf(secondaryNominalCurrent2);
		fields[5] = secondaryNominalCurrent3 == null ? "" : String.valueOf(secondaryNominalCurrent3);
		fields[6] = secondaryNominalCurrent4 == null ? "" : String.valueOf(secondaryNominalCurrent4);
		fields[7] = secondaryNominalCurrent5 == null ? "" : String.valueOf(secondaryNominalCurrent5);
		fields[8] = secondaryNominalCurrent6 == null ? "" : String.valueOf(secondaryNominalCurrent6);
		fields[9] = secondaryPower1 == null ? "" : String.valueOf(secondaryPower1);
		fields[10] = secondaryPower2 == null ? "" : String.valueOf(secondaryPower2);
		fields[11] = secondaryPower3 == null ? "" : String.valueOf(secondaryPower3);
		fields[12] = secondaryPower4 == null ? "" : String.valueOf(secondaryPower4);
		fields[13] = secondaryPower5 == null ? "" : String.valueOf(secondaryPower5);
		fields[14] = secondaryPower6 == null ? "" : String.valueOf(secondaryPower6);
		fields[15] = secondaryPrecisionClass1 == null ? "" : String.valueOf(secondaryPrecisionClass1);
		fields[16] = secondaryPrecisionClass2 == null ? "" : String.valueOf(secondaryPrecisionClass2);
		fields[17] = secondaryPrecisionClass3 == null ? "" : String.valueOf(secondaryPrecisionClass3);
		fields[18] = secondaryPrecisionClass4 == null ? "" : String.valueOf(secondaryPrecisionClass4);
		fields[19] = secondaryPrecisionClass5 == null ? "" : String.valueOf(secondaryPrecisionClass5);
		fields[20] = secondaryPrecisionClass6 == null ? "" : String.valueOf(secondaryPrecisionClass6);
		fields[21] = operatingEnvironment;
		fields[22] = insulationType;
		fields[23] = operatingMode;
		fields[24] = manufacturer;
		return Stream.concat(Arrays.stream(fields), Arrays.stream(super.getFields()))
				.toArray(String[]::new);
	}
}
