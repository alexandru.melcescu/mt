package ro.upt.ac.mtb.equipments.transformationunits;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.Optional;
import java.util.TreeMap;

import org.hibernate.SessionFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.io.ByteArrayResource;
import org.springframework.core.io.Resource;
import org.springframework.data.repository.query.Param;
import org.springframework.http.HttpHeaders;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.validation.ObjectError;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.multipart.MultipartFile;
import org.thymeleaf.TemplateEngine;
import org.thymeleaf.context.Context;
import org.thymeleaf.templatemode.TemplateMode;
import org.thymeleaf.templateresolver.ClassLoaderTemplateResolver;
import org.thymeleaf.util.StringUtils;
import org.xhtmlrenderer.pdf.ITextRenderer;

import jakarta.servlet.http.HttpServletRequest;
import jakarta.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.io.OutputStream;
import java.sql.SQLException;
import java.time.LocalDateTime;
import ro.upt.ac.mtb.equipments.electricalequipment.EquipmentManual;
import ro.upt.ac.mtb.equipments.electricalequipment.EquipmentManualRepository;
import ro.upt.ac.mtb.equipments.services.CSVFileGenerator;
import ro.upt.ac.mtb.equipments.services.ExportableToCSV;

@Controller
@RequestMapping("transformationUnits")
public class TransformationUnitsController
{
	@Autowired
	TransformationUnitsRepository transformationUnitsRepository;
	
	@Autowired
	EquipmentManualRepository equipmentManualRepository;
	
	@Autowired
	CSVFileGenerator csvFileGenerator;
	
	@Autowired
	SessionFactory sessionFactory;
	
	private static final String EQUIPMENT = "transformationUnits";
	
	@PostMapping("/search")
	public String search(@Param("searchTerm") String searchTerm, @Param("searchField") String searchField, Model model) {
		//String searchTerm = (String) model.getAttribute("searchTerm");
		List<TransformationUnits> data = null;
		System.out.println(searchTerm + " " + searchField);
		
		data = performSearch(searchField, searchTerm);
		
		if (data.size() != 0) {
		    model.addAttribute("equipmentObjects", data);
		}
		model.addAttribute("equipment", EQUIPMENT);
		model.addAttribute("searchFields", getHeadingsWithFields().entrySet());
		model.addAttribute("search", Boolean.TRUE);
		model.addAttribute("searchField", searchField);
		model.addAttribute("searchTerm", searchTerm);
		return EQUIPMENT + "/" + EQUIPMENT + "-read";
	}
	
	private List<TransformationUnits> performSearch(String searchField, String searchTerm) {
		try {
			if (searchField.equalsIgnoreCase("minimumAirTemperatureValue")) {
				// from a string to an Integer
				return transformationUnitsRepository.findByMinimumAirTemperatureValue(searchTerm);
			} else if (searchField.equalsIgnoreCase("minimumAirTemperatureObservation")) {
				return transformationUnitsRepository.findByMinimumAirTemperatureObservation(searchTerm);
			} else if (searchField.equalsIgnoreCase("maximumAirTemperatureValue")) {
				return transformationUnitsRepository.findByMaximumAirTemperatureValue(searchTerm);
			} else if (searchField.equalsIgnoreCase("maximumAirTemperatureObservation")) {
				// from a string to an Integer
				return transformationUnitsRepository.findByMaximumAirTemperatureObservation(searchTerm);
			} else if (searchField.equalsIgnoreCase("maximumAltitudeValue")) {
				return transformationUnitsRepository.findByMaximumAltitudeValue(searchTerm);
			} else if (searchField.equalsIgnoreCase("maximumAltitudeObservation")) {
				return transformationUnitsRepository.findByMaximumAltitudeObservation(searchTerm);
			} else if (searchField.equalsIgnoreCase("ratedVoltageOfMediumVoltageCircuitsValue")) {
				return transformationUnitsRepository.findByRatedVoltageOfMediumVoltageCircuitsValue(searchTerm);
			} else if (searchField.equalsIgnoreCase("ratedVoltageOfMediumVoltageCircuitsObservation")) {
				return transformationUnitsRepository.findByRatedVoltageOfMediumVoltageCircuitsObservation(searchTerm);
			} else if (searchField.equalsIgnoreCase("equipmentSeries")) {
				return transformationUnitsRepository.findByEquipmentSeries(searchTerm);
			} else if (searchField.equalsIgnoreCase("manufacturingYear")) {
				return transformationUnitsRepository.findByManufacturingYear(Integer.parseInt(searchTerm));
			} else if (searchField.equalsIgnoreCase("pifYear")) {
				return transformationUnitsRepository.findByPifYear(Integer.parseInt(searchTerm));
			} else if (searchField.equalsIgnoreCase("installationCompany")) {
				return transformationUnitsRepository.findByInstallationCompany(searchTerm);
			} else if (searchField.equalsIgnoreCase("pifBulletin")) {
				return transformationUnitsRepository.findByPifBulletin(searchTerm);
			} else if (searchField.equalsIgnoreCase("exploitationCenter")) {
				return transformationUnitsRepository.findByExploitationCenter(searchTerm);
			} else if (searchField.equalsIgnoreCase("transformationCenter")) {
				return transformationUnitsRepository.findByTransformationStation(searchTerm);
			} else if (searchField.equalsIgnoreCase("voltageLevel")) {
				return transformationUnitsRepository.findByVoltageLevel(searchTerm);
			} else if (searchField.equalsIgnoreCase("assemblyCell")) {
				return transformationUnitsRepository.findByAssemblyCell(searchTerm);
			} else if (searchField.equalsIgnoreCase("assemblyPhase")) {
				return transformationUnitsRepository.findByAssemblyPhase(searchTerm);
			} else if (searchField.equalsIgnoreCase("accountingorWarehouseRegistrationCode")) {
				return transformationUnitsRepository.findByAccountingorWarehouseRegistrationCode(searchTerm);
			} else if (searchField.equalsIgnoreCase("fixedAssetComponentNumber")) {
				return transformationUnitsRepository.findByFixedAssetComponentNumber(searchTerm);				
			} else {
				return transformationUnitsRepository.search(searchTerm);
			}		
		} catch (NumberFormatException e) {
			return transformationUnitsRepository.search(searchTerm);
		}
	}
	
	@PostMapping("/uploadFile")
    public String uploadFile(@RequestParam("file") MultipartFile file, Model model) {
        EquipmentManual equipmentManual = new EquipmentManual();

        // setting up the entity of the equipment manual
        equipmentManual.setId(EQUIPMENT);
        equipmentManual.setOperatingManualFileType(file.getContentType());
        equipmentManual.setOperatingManualFileName(file.getOriginalFilename());
        
        try {
			//equipmentManual.setOperatingManual(file.getBytes());
        	
        	// create a blob object so that we can store anything bigger and 255 bytes
        	equipmentManual.setOperatingManual(sessionFactory.openSession().getLobHelper().createBlob(file.getBytes()));
			equipmentManualRepository.save(equipmentManual);
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
        
        return read(model); 
	}
	
	@GetMapping("/downloadFile")
    public ResponseEntity <Resource> downloadFile(HttpServletRequest request) {
        // Load file as Resource
        Optional<EquipmentManual> equipmentManual = equipmentManualRepository.findById(EQUIPMENT);

        if (equipmentManual.isPresent()) {
        	try {
				return ResponseEntity.ok()
				        .contentType(MediaType.parseMediaType(equipmentManual.get().getOperatingManualFileType()))
				        .header(HttpHeaders.CONTENT_DISPOSITION, "attachment; filename=\"" + equipmentManual.get().getOperatingManualFileName() + "\"")
				        .body(new ByteArrayResource(equipmentManual.get().getOperatingManual().getBytes(1, (int) equipmentManual.get().getOperatingManual().length())));
			} catch (SQLException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
        }
        return ResponseEntity.notFound()
        		.build();
    }
	
	
	@GetMapping("/create")
	public String create(TransformationUnits transformationUnits, Model model)
	{
		model.addAttribute("equipment", EQUIPMENT);
		model.addAttribute("equipmentObject", new TransformationUnits());
		return EQUIPMENT + "/" + EQUIPMENT + "-create";
	}

	@PostMapping("/create-save")
	// @ModelAttribute("equipmentObject") connects the create-save method to the create html page of the equipment
	public String createSave(@Validated @ModelAttribute("equipmentObject") TransformationUnits transformationUnits, BindingResult result, Model model)
	{
		if(result.hasErrors())
		{
			// without line 144 we don't have the equipment object in the request, since we need it 
			model.addAttribute("equipmentObject", transformationUnits);
			return EQUIPMENT + "/" + EQUIPMENT + "-create";
		}
		
		// update the time stamp before saving the record
	    transformationUnits.setUpdateTimeStamp(LocalDateTime.now());		
		transformationUnitsRepository.save(transformationUnits);
		return "redirect:read";
	}
	
	@GetMapping("/read")
	public String read(Model model) 
	{
		model.addAttribute("equipment", EQUIPMENT);
		List<TransformationUnits> transformationUnits = transformationUnitsRepository.findAll();
		if (transformationUnits.size() != 0) 
		{
			System.out.println(transformationUnits); 
			model.addAttribute("equipmentObjects", transformationUnits);
		}
		model.addAttribute("searchFields", getHeadingsWithFields().entrySet());
		return EQUIPMENT + "/" + EQUIPMENT + "-read";
	}
	
	@GetMapping("/edit/{id}")
	public String edit(@PathVariable("id") Long id, Model model) 
	{
		model.addAttribute("equipment", EQUIPMENT);
		Optional<TransformationUnits> transformationUnits = transformationUnitsRepository.findById(id);
	    model.addAttribute("equipmentObject", transformationUnits.get());
	    
		return EQUIPMENT + "/" + EQUIPMENT + "-update";
	}
	
	@PostMapping("/update/{id}")
	public String update(@PathVariable("id") Long id, @Validated @ModelAttribute("equipmentObject") TransformationUnits transformationUnits, BindingResult result, Model model) 
	{
	    // check to see that the user is updating using fresh data
		if (transformationUnitsRepository.findByIdAndUpdateTimeStamp(id, transformationUnits.getUpdateTimeStamp()).isEmpty()) {
			// if it's empty = not dealing with fresh data
			// reject allows 191 to have errors
			// each time the addError() function gets used, it means there is a global error
			result.addError(new ObjectError("global", "Another user has already updated the data"));
			// message is coming from line 190
			model.addAttribute("globalError", "Another user has already updated the data");
		}
		
	    if(result.hasErrors()) 
	    {
	    	transformationUnits.setId(id);
			model.addAttribute("equipmentObject", transformationUnits);
			return EQUIPMENT + "/" + EQUIPMENT + "-update";
	    }
	    // update the time stamp
	    transformationUnits.setUpdateTimeStamp(LocalDateTime.now());
	    // save the record
	    transformationUnitsRepository.save(transformationUnits);
	    
		return "redirect:../read";
	}
	
	@GetMapping("/delete/{id}")
	public String delete(@PathVariable("id") Long id, Model model) 
	{
		Optional<TransformationUnits> transformationUnits = transformationUnitsRepository.findById(id);
	    
		transformationUnitsRepository.delete(transformationUnits.get());
		return "redirect:../read";
	}	
	
	@GetMapping("/exportToCSV")
	public void exportToCSV(@Param("searchTerm") String searchTerm, @Param("searchField") String searchField, HttpServletResponse response, Model model) throws IOException {
		response.setContentType("text/csv");
		//  how it gets saved
		response.addHeader("Content-Disposition", "attachment; filename = \"" + StringUtils.capitalize(EQUIPMENT) + "s.csv\"");

		// "attachment; filename = \"Separators.csv\"" is the header 
		List<ExportableToCSV> data = new ArrayList<>();
		if (searchField != null && searchTerm != null && !searchTerm.trim().equals("") ) {
			data.addAll(performSearch(searchField, searchTerm));
		} else {
			data.addAll(transformationUnitsRepository.findAll()); // gets every record in the separator table and adds it to the list that gets exported
		}
		csvFileGenerator.write(getHeadings(), data, response.getWriter()); // write to the response object with the header
	}
	// controller handles the headings, entities handle the data
	
	private static String[] getHeadings() {
		String[] fields = new String[20];
		// fields at position zero
		fields[0] = "Minimum Air Temperature Value";
		fields[1] = "Minimum Air Temperature Observation";
		fields[2] = "Maximum Air Temperature Value";
		fields[3] = "Maximum Air Temperature Observation";
		fields[4] = "Maximum Altitude Value";
		fields[5] = "Maximum Altitude Observation";
		fields[6] = "Rated Voltage Of Medium Voltage Circuits Value";
		fields[7] = "Rated Voltage Of Medium Voltage Circuits Observation";
		fields[8] = "Equipment Series";
		fields[9] = "Manufacturing Year";
		fields[10] = "Pif Year";
		fields[11] = "Installation Company";
		fields[12] = "Pif Bulletin";
		fields[13] = "Exploitation Center";
		fields[14] = "Transformation Station";
		fields[15] = "Voltage Level";
		fields[16] = "Assembly Cell";
		fields[17] = "Assembly Phase";
		fields[18] = "Accounting or Warehouse Registration Code";
		fields[19] = "Fixed Asset Component Number";
		
		return fields;
	}
	
	private static String[] getFieldNames() {
		String[] fields = new String[20];
		// fields at position zero
		fields[0] = "minimumAirTemperatureValue";
		fields[1] = "minimumAirTemperatureObservation";
		fields[2] = "maximumAirTemperatureValue";
		fields[3] = "maximumAirTemperatureObservation";
		fields[4] = "maximumAltitudeValue";
		fields[5] = "maximumAltitudeObservation";
		fields[6] = "ratedVoltageOfMediumVoltageCircuitsValue";
		fields[7] = "ratedVoltageOfMediumVoltageCircuitsObservation";
		fields[8] = "equipmentSeries";
		fields[9] = "manufacturingYear";
		fields[10] = "pifYear";
		fields[11] = "installationCompany";
		fields[12] = "pifBulletin";
		fields[13] = "exploitationCenter";
		fields[14] = "transformationStation";
		fields[15] = "voltageLevel";
		fields[16] = "assemblyCell";
		fields[17] = "assemblyPhase";
		fields[18] = "accountingorWarehouseRegistrationCode";
		fields[19] = "fixedAssetComponentNumber";

		return fields;
	}
	
	private static Map<String, String> getHeadingsWithFields() {
		Map<String, String> map = new TreeMap<>();
		map.put("* Search All Fields", "*");
		String[] headings = getHeadings();
		String[] fieldNames = getFieldNames();
		for (int x = 0; x < headings.length; x++) {
			map.put(headings[x], fieldNames[x]);
		}
		
		return map;
	}
	
	@GetMapping("/exportToPDF")
	public void exportToPDF(HttpServletResponse response, Model model) throws IOException {
		/* String outputFolder = "C:\\Users\\MY PC\\Downloads" + File.arrester + "Arresters.pdf";
		 OutputStream outputStream = new FileOutputStream(outputFolder);
		*/
		
		// getOutputStream() allows the PDF to be opened in the browser rather than a specific location on the server
		OutputStream outputStream = response.getOutputStream();

		response.addHeader("Content-Disposition", "attachment; filename = \"" + StringUtils.capitalize(EQUIPMENT) + ".pdf\"");
		
	    ITextRenderer renderer = new ITextRenderer();
	    renderer.setDocumentFromString(parseThymeleafTemplate());
	    renderer.layout();
	    renderer.createPDF(outputStream);

	    outputStream.close();
	}
	
	
	private String parseThymeleafTemplate() {
	    ClassLoaderTemplateResolver templateResolver = new ClassLoaderTemplateResolver();
	    templateResolver.setSuffix(".html");
	    templateResolver.setTemplateMode(TemplateMode.HTML);

	    TemplateEngine templateEngine = new TemplateEngine();
	    templateEngine.setTemplateResolver(templateResolver);
	    
	    // model.addAttribute("equipment", "separator");
		List<TransformationUnits> arresters = transformationUnitsRepository.findAll();

	    Context context = new Context();
	    context.setVariable("equipmentObjects", arresters);

	    return templateEngine.process("templates/transformationUnits/transformationUnits-pdf", context);
	}
}