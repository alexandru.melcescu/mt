package ro.upt.ac.mtb.equipments.groundingknifeactuationdevice;

import java.util.Arrays;
import java.util.stream.Stream;

import jakarta.persistence.Entity;
import ro.upt.ac.mtb.equipments.electricalequipment.ElectricalEquipment;
import ro.upt.ac.mtb.equipments.services.ExportableToCSV;

// Grounding Knife Actuation Device

@Entity
public class GroundingKnifeActuationDevice extends ElectricalEquipment implements ExportableToCSV {
	
	//tech sheet
	private String bladeMaterial;
	private String handleMaterial;
	private Double bladeLength;
	private Integer bladeThickness;
	private Double handleLength;
	private Double currentCapacity;
	private String insulationResistance;
	private String operatingTemperature;
	private String humidityResistance;
	
	public GroundingKnifeActuationDevice() {
	}
	

	public String getBladeMaterial() {
		return bladeMaterial;
	}


	public void setBladeMaterial(String bladeMaterial) {
		this.bladeMaterial = bladeMaterial;
	}


	public String getHandleMaterial() {
		return handleMaterial;
	}


	public void setHandleMaterial(String handleMaterial) {
		this.handleMaterial = handleMaterial;
	}


	public Double getBladeLength() {
		return bladeLength;
	}


	public void setBladeLength(Double bladeLength) {
		this.bladeLength = bladeLength;
	}


	public Integer getBladeThickness() {
		return bladeThickness;
	}


	public void setBladeThickness(Integer bladeThickness) {
		this.bladeThickness = bladeThickness;
	}


	public Double getHandleLength() {
		return handleLength;
	}


	public void setHandleLength(Double handleLength) {
		this.handleLength = handleLength;
	}


	public Double getCurrentCapacity() {
		return currentCapacity;
	}


	public void setCurrentCapacity(Double currentCapacity) {
		this.currentCapacity = currentCapacity;
	}


	public String getInsulationResistance() {
		return insulationResistance;
	}


	public void setInsulationResistance(String insulationResistance) {
		this.insulationResistance = insulationResistance;
	}


	public String getOperatingTemperature() {
		return operatingTemperature;
	}


	public void setOperatingTemperature(String operatingTemperature) {
		this.operatingTemperature = operatingTemperature;
	}


	public String getHumidityResistance() {
		return humidityResistance;
	}


	public void setHumidityResistance(String humidityResistance) {
		this.humidityResistance = humidityResistance;
	}


	@Override
	public String[] getFields() {
		String[] fields = new String[9];
		// fields at position 
		fields[0] = bladeMaterial;
		fields[1] = handleMaterial;
		fields[2] = bladeLength == null ? "" : String.valueOf(bladeLength);
		fields[3] = bladeThickness == null ? "" : String.valueOf(bladeThickness);
		fields[4] = handleLength == null ? "" : String.valueOf(handleLength);
		fields[5] = currentCapacity == null ? "" : String.valueOf(currentCapacity);
		fields[6] = insulationResistance;
		fields[7] = operatingTemperature;
		fields[8] = humidityResistance;
		return Stream.concat(Arrays.stream(fields), Arrays.stream(super.getFields()))
				.toArray(String[]::new);	
	}
}