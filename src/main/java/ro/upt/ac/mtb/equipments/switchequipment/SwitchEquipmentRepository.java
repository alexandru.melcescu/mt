package ro.upt.ac.mtb.equipments.switchequipment;

import java.util.List;
import java.util.Optional;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import java.time.LocalDateTime;

@Repository
public interface SwitchEquipmentRepository extends JpaRepository<SwitchEquipment, Long> {
	Optional<SwitchEquipment> findById(Long id);
	
	List<SwitchEquipment> findByEquipmentCode(String equipmentCode);
	List<SwitchEquipment> findByCircuitBreakerType(String circuitBreakerType);
	List<SwitchEquipment> findByNominalBreakingCapacityOfSmallInductiveCurrents(String nominalBreakingCapacityOfSmallInductiveCurrents);
	List<SwitchEquipment> findByKilometricFaultDisconnectingCapacity(String kilometricFaultDisconnectingCapacity);
	List<SwitchEquipment> findByNominalOperatingSequence(String nominalOperatingSequence);
	List<SwitchEquipment> findByPoleNumber(Integer poleNumber);
	List<SwitchEquipment> findByNominalVoltage(Integer nominalVoltage);
	List<SwitchEquipment> findByNominalInsulation(Integer nominalInsulation);
	List<SwitchEquipment> findByNominalCurrent(Integer nominalCurrent);
	List<SwitchEquipment> findByNominalBreakingCapacityOfNoLoadLineCurrents(Integer nominalBreakingCapacityOfNoLoadLineCurrents);
	List<SwitchEquipment> findByNominalBreakingCapacityOfNoLoadCableCurrents(Integer nominalBreakingCapacityOfNoLoadCableCurrents);
	List<SwitchEquipment> findByNominalBreakingCapacityOfCapacitorBankCurrents(Integer nominalBreakingCapacityOfCapacitorBankCurrents);
	List<SwitchEquipment> findByMaximumShortCircuitClosingCapacity(Integer maximumShortCircuitClosingCapacity);
	List<SwitchEquipment> findByNominalClosingTime(Integer nominalClosingTime);
	List<SwitchEquipment> findByNominalOpeningTime(Integer nominalOpeningTime);
	List<SwitchEquipment> findByNominalInterruptionTime(Integer nominalInterruptionTime);
	List<SwitchEquipment> findByNominalBreakingCapacityOfShortCircuitCurrentsAtTerminal(Double nominalBreakingCapacityOfShortCircuitCurrentsAtTerminal);
	List<SwitchEquipment> findByAdmissibleDurationOfShortCircuitCurrents(Double admissibleDurationOfShortCircuitCurrents);
	
	List<SwitchEquipment> findByEquipmentSeries(String equipmentSeries);
	List<SwitchEquipment> findByManufacturingYear(Integer manufacturingYear);
	List<SwitchEquipment> findByPifYear(Integer pifYear);
	List<SwitchEquipment> findByInstallationCompany(String installationCompany);
	List<SwitchEquipment> findByPifBulletin(String pifBulletin);
	List<SwitchEquipment> findByExploitationCenter(String exploitationCenter);
	List<SwitchEquipment> findByTransformationStation(String transformationStation);
	List<SwitchEquipment> findByVoltageLevel(String voltageLevel);
	List<SwitchEquipment> findByAssemblyCell(String assemblyCell);
	List<SwitchEquipment> findByAssemblyPhase(String assemblyPhase);
	List<SwitchEquipment> findByAccountingorWarehouseRegistrationCode(String accountingorWarehouseRegistrationCode);
	List<SwitchEquipment> findByFixedAssetComponentNumber(String fixedAssetComponentNumber);
	
	@Query("SELECT sw FROM SwitchEquipment sw"
			+ " WHERE sw.equipmentCode LIKE %?1%"
			+ " OR sw.circuitBreakerType LIKE %?1%"
			+ " OR sw.nominalBreakingCapacityOfSmallInductiveCurrents LIKE %?1%"
			+ " OR sw.kilometricFaultDisconnectingCapacity LIKE %?1%"
			+ " OR sw.nominalOperatingSequence LIKE %?1%"
			+ " OR sw.poleNumber LIKE %?1%"
			+ " OR sw.nominalVoltage LIKE %?1%"
			+ " OR sw.nominalInsulation LIKE %?1%"
			+ " OR sw.nominalCurrent LIKE %?1%"
			+ " OR sw.nominalBreakingCapacityOfNoLoadLineCurrents LIKE %?1%"
			+ " OR sw.nominalBreakingCapacityOfNoLoadCableCurrents LIKE %?1%"
			+ " OR sw.nominalBreakingCapacityOfCapacitorBankCurrents LIKE %?1%"
			+ " OR sw.maximumShortCircuitClosingCapacity LIKE %?1%"
			+ " OR sw.nominalClosingTime LIKE %?1%"
			+ " OR sw.nominalOpeningTime LIKE %?1%"
			+ " OR sw.nominalInterruptionTime LIKE %?1%"
			+ " OR sw.nominalBreakingCapacityOfShortCircuitCurrentsAtTerminal LIKE %?1%"
			+ " OR sw.admissibleDurationOfShortCircuitCurrents LIKE %?1%"
			
			+ " OR sw.equipmentSeries LIKE %?1%"
			+ " OR sw.manufacturingYear LIKE %?1%"
			+ " OR sw.pifYear LIKE %?1%"
			+ " OR sw.installationCompany LIKE %?1%"
			+ " OR sw.pifBulletin LIKE %?1%"
			+ " OR sw.exploitationCenter LIKE %?1%"
			+ " OR sw.transformationStation LIKE %?1%"
			+ " OR sw.voltageLevel LIKE %?1%"
			+ " OR sw.assemblyCell LIKE %?1%"
			+ " OR sw.assemblyPhase LIKE %?1%"
			+ " OR sw.accountingorWarehouseRegistrationCode LIKE %?1%"
			+ " OR sw.fixedAssetComponentNumber LIKE %?1%"
			)
	List<SwitchEquipment> search(String searchTerm);

	//returns an equipment object by id and by time stamp
	Optional<SwitchEquipment> findByIdAndUpdateTimeStamp(Long id, LocalDateTime ts);
}
