package ro.upt.ac.mtb.equipments.mixedmeasuringtransformer;

import java.util.List;
import java.util.Optional;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import java.time.LocalDateTime;

@Repository
public interface MixedMeasuringTransformerRepository extends JpaRepository<MixedMeasuringTransformer,Long> {
	Optional<MixedMeasuringTransformer> findById(Long id);
	
	List<MixedMeasuringTransformer> findByDesigningPurpose(String designingPurpose);
	List<MixedMeasuringTransformer> findByPrimaryCurrent(Integer primaryCurrent);
	List<MixedMeasuringTransformer> findBySecondaryCurrent(Integer secondaryCurrent);
	List<MixedMeasuringTransformer> findByBurden(Integer burden);
	List<MixedMeasuringTransformer> findByAccuracyClass(Double accuracyClass);
	
	List<MixedMeasuringTransformer> findByEquipmentSeries(String equipmentSeries);
	List<MixedMeasuringTransformer> findByManufacturingYear(Integer manufacturingYear);
	List<MixedMeasuringTransformer> findByPifYear(Integer pifYear);
	List<MixedMeasuringTransformer> findByInstallationCompany(String installationCompany);
	List<MixedMeasuringTransformer> findByPifBulletin(String pifBulletin);
	List<MixedMeasuringTransformer> findByExploitationCenter(String exploitationCenter);
	List<MixedMeasuringTransformer> findByTransformationStation(String transformationStation);
	List<MixedMeasuringTransformer> findByVoltageLevel(String voltageLevel);
	List<MixedMeasuringTransformer> findByAssemblyCell(String assemblyCell);
	List<MixedMeasuringTransformer> findByAssemblyPhase(String assemblyPhase);
	List<MixedMeasuringTransformer> findByAccountingorWarehouseRegistrationCode(String accountingorWarehouseRegistrationCode);
	List<MixedMeasuringTransformer> findByFixedAssetComponentNumber(String fixedAssetComponentNumber);
	
	@Query("SELECT mmt FROM MixedMeasuringTransformer mmt"
			+ " WHERE mmt.designingPurpose LIKE %?1%"
			+ " OR mmt.primaryCurrent LIKE %?1%"
			+ " OR mmt.secondaryCurrent LIKE %?1%"
			+ " OR mmt.burden LIKE %?1%"
			+ " OR mmt.accuracyClass LIKE %?1%"
			
			+ " OR mmt.equipmentSeries LIKE %?1%"
			+ " OR mmt.manufacturingYear LIKE %?1%"
			+ " OR mmt.pifYear LIKE %?1%"
			+ " OR mmt.installationCompany LIKE %?1%"
			+ " OR mmt.pifBulletin LIKE %?1%"
			+ " OR mmt.exploitationCenter LIKE %?1%"
			+ " OR mmt.transformationStation LIKE %?1%"
			+ " OR mmt.voltageLevel LIKE %?1%"
			+ " OR mmt.assemblyCell LIKE %?1%"
			+ " OR mmt.assemblyPhase LIKE %?1%"
			+ " OR mmt.accountingorWarehouseRegistrationCode LIKE %?1%"
			+ " OR mmt.fixedAssetComponentNumber LIKE %?1%"
			)
	List<MixedMeasuringTransformer> search(String searchTerm);
	
	//returns an equipment object by id and by time stamp
	Optional<MixedMeasuringTransformer> findByIdAndUpdateTimeStamp(Long id, LocalDateTime ts);
}
