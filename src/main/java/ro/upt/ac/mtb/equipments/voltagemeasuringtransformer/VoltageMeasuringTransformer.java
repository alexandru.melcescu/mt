package ro.upt.ac.mtb.equipments.voltagemeasuringtransformer;

import java.util.Arrays;
import java.util.stream.Stream;

import jakarta.persistence.Entity;
import ro.upt.ac.mtb.equipments.electricalequipment.ElectricalEquipment;
import ro.upt.ac.mtb.equipments.services.ExportableToCSV;

// Voltage Measuring Transformer

@Entity
public class VoltageMeasuringTransformer extends ElectricalEquipment implements ExportableToCSV{
	
	//tech sheet
	private String maximumNetworkVoltageUnitOfMeasure;
	private String maximumNetworkVoltageRequired;
	private String maximumNetworkVoltageOffered;
	private String ratedVoltageOfThePrimaryWindingUnitOfMeasure;
	private String ratedVoltageOfThePrimaryWindingRequired;
	private String ratedVoltageOfThePrimaryWindingOffered;
	
	public VoltageMeasuringTransformer() {
	}

	public String getMaximumNetworkVoltageUnitOfMeasure() {
		return maximumNetworkVoltageUnitOfMeasure;
	}

	public void setMaximumNetworkVoltageUnitOfMeasure(String maximumNetworkVoltageUnitOfMeasure) {
		this.maximumNetworkVoltageUnitOfMeasure = maximumNetworkVoltageUnitOfMeasure;
	}

	public String getMaximumNetworkVoltageRequired() {
		return maximumNetworkVoltageRequired;
	}

	public void setMaximumNetworkVoltageRequired(String maximumNetworkVoltageRequired) {
		this.maximumNetworkVoltageRequired = maximumNetworkVoltageRequired;
	}

	public String getMaximumNetworkVoltageOffered() {
		return maximumNetworkVoltageOffered;
	}

	public void setMaximumNetworkVoltageOffered(String maximumNetworkVoltageOffered) {
		this.maximumNetworkVoltageOffered = maximumNetworkVoltageOffered;
	}

	public String getRatedVoltageOfThePrimaryWindingUnitOfMeasure() {
		return ratedVoltageOfThePrimaryWindingUnitOfMeasure;
	}

	public void setRatedVoltageOfThePrimaryWindingUnitOfMeasure(String ratedVoltageOfThePrimaryWindingUnitOfMeasure) {
		this.ratedVoltageOfThePrimaryWindingUnitOfMeasure = ratedVoltageOfThePrimaryWindingUnitOfMeasure;
	}

	public String getRatedVoltageOfThePrimaryWindingRequired() {
		return ratedVoltageOfThePrimaryWindingRequired;
	}

	public void setRatedVoltageOfThePrimaryWindingRequired(String ratedVoltageOfThePrimaryWindingRequired) {
		this.ratedVoltageOfThePrimaryWindingRequired = ratedVoltageOfThePrimaryWindingRequired;
	}

	public String getRatedVoltageOfThePrimaryWindingOffered() {
		return ratedVoltageOfThePrimaryWindingOffered;
	}

	public void setRatedVoltageOfThePrimaryWindingOffered(String ratedVoltageOfThePrimaryWindingOffered) {
		this.ratedVoltageOfThePrimaryWindingOffered = ratedVoltageOfThePrimaryWindingOffered;
	}

	@Override
	public String[] getFields() {
		String[] fields = new String[6];
		// fields at position zero
		fields[0] = maximumNetworkVoltageUnitOfMeasure;
		fields[1] = maximumNetworkVoltageRequired;
		fields[2] = maximumNetworkVoltageOffered;
		fields[3] = ratedVoltageOfThePrimaryWindingUnitOfMeasure;
		fields[4] = ratedVoltageOfThePrimaryWindingRequired;
		fields[5] = ratedVoltageOfThePrimaryWindingOffered;
		return Stream.concat(Arrays.stream(fields), Arrays.stream(super.getFields()))
				.toArray(String[]::new);
	}
}