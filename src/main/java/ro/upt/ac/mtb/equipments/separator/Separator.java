package ro.upt.ac.mtb.equipments.separator;

import java.util.Arrays;
import java.util.stream.Stream;

import jakarta.persistence.Entity;
import jakarta.persistence.Table;
import java.time.LocalDate;
import ro.upt.ac.mtb.equipments.electricalequipment.ElectricalEquipment;
import ro.upt.ac.mtb.equipments.services.ExportableToCSV;

// Separator

@Entity
@Table(name = "separator_table")
public class Separator extends ElectricalEquipment implements ExportableToCSV
{
	//TODO: remove manual link from all equipments
	// tech data
	private String equipmentCode;
	private String type;
	private String manufacturer;

	private Double nominalVoltage;
	private Double primaryCurrent;

	private String ambientEnvironment;
	private String insulationType;
	private String operatingMode;
	private LocalDate lastMaintenanceDate;
	private LocalDate nextScheduledMaintenanceDate;
	private String status;
	
	private String maintenancePerformedBy;
	private String remarks;
	
	public Separator() {
	}
	
	public String getEquipmentCode() {
		return equipmentCode;
	}

	public void setEquipmentCode(String equipmentCode) {
		this.equipmentCode = equipmentCode;
	}

	public String getType() {
		return type;
	}

	public void setType(String type) {
		this.type = type;
	}

	public String getManufacturer() {
		return manufacturer;
	}

	public void setManufacturer(String manufacturer) {
		this.manufacturer = manufacturer;
	}

	public Double getNominalVoltage() {
		return nominalVoltage;
	}

	public void setNominalVoltage(Double nominalVoltage) {
		this.nominalVoltage = nominalVoltage;
	}

	public Double getPrimaryCurrent() {
		return primaryCurrent;
	}

	public void setPrimaryCurrent(Double primaryCurrent) {
		this.primaryCurrent = primaryCurrent;
	}

	public String getAmbientEnvironment() {
		return ambientEnvironment;
	}

	public void setAmbientEnvironment(String ambientEnvironment) {
		this.ambientEnvironment = ambientEnvironment;
	}

	public String getInsulationType() {
		return insulationType;
	}

	public void setInsulationType(String insulationType) {
		this.insulationType = insulationType;
	}

	public String getOperatingMode() {
		return operatingMode;
	}

	public void setOperatingMode(String operatingMode) {
		this.operatingMode = operatingMode;
	}

	public LocalDate getLastMaintenanceDate() {
		return lastMaintenanceDate;
	}

	public void setLastMaintenanceDate(LocalDate lastMaintenanceDate) {
		this.lastMaintenanceDate = lastMaintenanceDate;
	}

	public LocalDate getNextScheduledMaintenanceDate() {
		return nextScheduledMaintenanceDate;
	}

	public void setNextScheduledMaintenanceDate(LocalDate nextScheduledMaintenanceDate) {
		this.nextScheduledMaintenanceDate = nextScheduledMaintenanceDate;
	}

	public String getStatus() {
		return status;
	}

	public void setStatus(String status) {
		this.status = status;
	}

	public String getMaintenancePerformedBy() {
		return maintenancePerformedBy;
	}

	public void setMaintenancePerformedBy(String maintenancePerformedBy) {
		this.maintenancePerformedBy = maintenancePerformedBy;
	}

	public String getRemarks() {
		return remarks;
	}

	public void setRemarks(String remarks) {
		this.remarks = remarks;
	}

	@Override
	public String[] getFields() {
		String[] fields = new String[13];
		// fields at position zero
		fields[0] = equipmentCode;
		fields[1] = type;
		fields[2] = manufacturer;
		fields[3] = nominalVoltage == null ? "" : String.valueOf(nominalVoltage);
		fields[4] = primaryCurrent == null ? "" : String.valueOf(primaryCurrent);
		fields[5] = ambientEnvironment;
		fields[6] = insulationType;
		fields[7] = operatingMode;
		fields[8] = lastMaintenanceDate == null ? "" : String.valueOf(lastMaintenanceDate);
		fields[9] = nextScheduledMaintenanceDate == null ? "" : String.valueOf(nextScheduledMaintenanceDate);
		fields[10] = status;
		fields[11] = maintenancePerformedBy;
		fields[12] = remarks;
		return Stream.concat(Arrays.stream(fields), Arrays.stream(super.getFields()))
				.toArray(String[]::new);
	}
}