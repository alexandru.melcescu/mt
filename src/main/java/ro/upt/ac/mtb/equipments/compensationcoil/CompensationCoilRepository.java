package ro.upt.ac.mtb.equipments.compensationcoil;

import java.util.List;
import java.util.Optional;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import java.time.LocalDateTime;

@Repository
public interface CompensationCoilRepository extends JpaRepository<CompensationCoil,Long> {
	Optional<CompensationCoil> findById(Long id);
	
	List<CompensationCoil> findByArticleNumber(String articleNumber);
	List<CompensationCoil> findByProductDesign(String productDesign);
	List<CompensationCoil> findByUsableWith(String usableWith);
	
	List<CompensationCoil> findByEquipmentSeries(String equipmentSeries);
	List<CompensationCoil> findByManufacturingYear(Integer manufacturingYear);
	List<CompensationCoil> findByPifYear(Integer pifYear);
	List<CompensationCoil> findByInstallationCompany(String installationCompany);
	List<CompensationCoil> findByPifBulletin(String pifBulletin);
	List<CompensationCoil> findByExploitationCenter(String exploitationCenter);
	List<CompensationCoil> findByTransformationStation(String transformationStation);
	List<CompensationCoil> findByVoltageLevel(String voltageLevel);
	List<CompensationCoil> findByAssemblyCell(String assemblyCell);
	List<CompensationCoil> findByAssemblyPhase(String assemblyPhase);
	List<CompensationCoil> findByAccountingorWarehouseRegistrationCode(String accountingorWarehouseRegistrationCode);
	List<CompensationCoil> findByFixedAssetComponentNumber(String fixedAssetComponentNumber);


	@Query("SELECT cc FROM CompensationCoil cc"
			+ " WHERE cc.articleNumber LIKE %?1%"
			+ " OR cc.productDesign LIKE %?1%"
			+ " OR cc.usableWith LIKE %?1%"
			
			+ " OR cc.equipmentSeries LIKE %?1%"
			+ " OR cc.manufacturingYear LIKE %?1%"
			+ " OR cc.pifYear LIKE %?1%"
			+ " OR cc.installationCompany LIKE %?1%"
			+ " OR cc.pifBulletin LIKE %?1%"
			+ " OR cc.exploitationCenter LIKE %?1%"
			+ " OR cc.transformationStation LIKE %?1%"
			+ " OR cc.voltageLevel LIKE %?1%"
			+ " OR cc.assemblyCell LIKE %?1%"
			+ " OR cc.assemblyPhase LIKE %?1%"
			+ " OR cc.accountingorWarehouseRegistrationCode LIKE %?1%"
			+ " OR cc.fixedAssetComponentNumber LIKE %?1%"
			)
	List<CompensationCoil> search(String searchTerm);
	
	//returns an equipment object by id and by time stamp
	Optional<CompensationCoil> findByIdAndUpdateTimeStamp(Long id, LocalDateTime ts);
}
