package ro.upt.ac.mtb.equipments.compensationcoil;

import java.util.Arrays;
import java.util.stream.Stream;

import jakarta.persistence.Entity;
import ro.upt.ac.mtb.equipments.electricalequipment.ElectricalEquipment;
import ro.upt.ac.mtb.equipments.services.ExportableToCSV;

// Compensation Coil

@Entity
public class CompensationCoil extends ElectricalEquipment implements ExportableToCSV{
	// tech sheet
	private String articleNumber;
	private String productDesign;
	private String usableWith;
	
	public CompensationCoil() {
	}

	public String getArticleNumber() {
		return articleNumber;
	}

	public void setArticleNumber(String articleNumber) {
		this.articleNumber = articleNumber;
	}

	public String getProductDesign() {
		return productDesign;
	}

	public void setProductDesign(String productDesign) {
		this.productDesign = productDesign;
	}

	public String getUsableWith() {
		return usableWith;
	}

	public void setUsableWith(String usableWith) {
		this.usableWith = usableWith;
	}
	
	@Override
	public String[] getFields() {
		String[] fields = new String[3];
		// fields at position zero
		fields[0] = articleNumber;
		fields[1] = productDesign;
		fields[2] = usableWith;
		return Stream.concat(Arrays.stream(fields), Arrays.stream(super.getFields()))
				.toArray(String[]::new);
	}
}