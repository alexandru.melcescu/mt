package ro.upt.ac.mtb.equipments.currentmeasuringtransformer;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.Optional;
import java.util.TreeMap;

import org.hibernate.SessionFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.io.ByteArrayResource;
import org.springframework.core.io.Resource;
import org.springframework.data.repository.query.Param;
import org.springframework.http.HttpHeaders;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.validation.ObjectError;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.multipart.MultipartFile;
import org.thymeleaf.TemplateEngine;
import org.thymeleaf.context.Context;
import org.thymeleaf.templatemode.TemplateMode;
import org.thymeleaf.templateresolver.ClassLoaderTemplateResolver;
import org.thymeleaf.util.StringUtils;
import org.xhtmlrenderer.pdf.ITextRenderer;

import jakarta.servlet.http.HttpServletRequest;
import jakarta.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.io.OutputStream;
import java.sql.SQLException;
import java.time.LocalDateTime;
import ro.upt.ac.mtb.equipments.electricalequipment.EquipmentManual;
import ro.upt.ac.mtb.equipments.electricalequipment.EquipmentManualRepository;
import ro.upt.ac.mtb.equipments.services.CSVFileGenerator;
import ro.upt.ac.mtb.equipments.services.ExportableToCSV;

@Controller
@RequestMapping("currentMeasuringTransformer")
public class CurrentMeasuringTransformerController
{
	@Autowired
	CurrentMeasuringTransformerRepository currentMeasuringTransformerRepository;
	
	@Autowired
	EquipmentManualRepository equipmentManualRepository;
	
	@Autowired
	CSVFileGenerator csvFileGenerator;
	
	@Autowired
	SessionFactory sessionFactory;
	
	private static final String EQUIPMENT = "currentMeasuringTransformer";
	
	@PostMapping("/search")
	public String search(@Param("searchTerm") String searchTerm, @Param("searchField") String searchField, Model model) {
		//String searchTerm = (String) model.getAttribute("searchTerm");
		List<CurrentMeasuringTransformer> data = null;
		System.out.println(searchTerm + " " + searchField);
		
		data = performSearch(searchField, searchTerm);
		
		if (data.size() != 0) {
		    model.addAttribute("equipmentObjects", data);
		}
		model.addAttribute("equipment", EQUIPMENT);
		model.addAttribute("searchFields", getHeadingsWithFields().entrySet());
		model.addAttribute("search", Boolean.TRUE);
		model.addAttribute("searchField", searchField);
		model.addAttribute("searchTerm", searchTerm);
		return EQUIPMENT + "/" + EQUIPMENT + "-read";
	}
	
	private List<CurrentMeasuringTransformer> performSearch(String searchField, String searchTerm) {
		try {
			if (searchField.equalsIgnoreCase("equipmentCode")) {
				return currentMeasuringTransformerRepository.findByEquipmentCode(searchTerm);
			} else if (searchField.equalsIgnoreCase("nominalVoltage")) {
				return currentMeasuringTransformerRepository.findByNominalVoltage(Double.parseDouble(searchTerm));
			} else if (searchField.equalsIgnoreCase("primaryNominalCurrent")) {
				return currentMeasuringTransformerRepository.findByPrimaryNominalCurrent(Double.parseDouble(searchTerm));
			} else if (searchField.equalsIgnoreCase("secondaryNominalCurrent1")) {
				return currentMeasuringTransformerRepository.findBySecondaryNominalCurrent1(Double.parseDouble(searchTerm));
			} else if (searchField.equalsIgnoreCase("secondaryNominalCurrent2")) {
				return currentMeasuringTransformerRepository.findBySecondaryNominalCurrent2(Double.parseDouble(searchTerm));
			} else if (searchField.equalsIgnoreCase("secondaryNominalCurrent3")) {
				return currentMeasuringTransformerRepository.findBySecondaryNominalCurrent3(Double.parseDouble(searchTerm));
			} else if (searchField.equalsIgnoreCase("secondaryNominalCurrent4")) {
				return currentMeasuringTransformerRepository.findBySecondaryNominalCurrent4(Double.parseDouble(searchTerm));
			} else if (searchField.equalsIgnoreCase("secondaryNominalCurrent5")) {
				return currentMeasuringTransformerRepository.findBySecondaryNominalCurrent5(Double.parseDouble(searchTerm));
			} else if (searchField.equalsIgnoreCase("secondaryNominalCurrent6")) {
				return currentMeasuringTransformerRepository.findBySecondaryNominalCurrent6(Double.parseDouble(searchTerm));
			} else if (searchField.equalsIgnoreCase("secondaryPower1")) {
				return currentMeasuringTransformerRepository.findBySecondaryPower1(Double.parseDouble(searchTerm));
			} else if (searchField.equalsIgnoreCase("secondaryPower2")) {
				return currentMeasuringTransformerRepository.findBySecondaryPower2(Double.parseDouble(searchTerm));
			} else if (searchField.equalsIgnoreCase("secondaryPower3")) {
				return currentMeasuringTransformerRepository.findBySecondaryPower3(Double.parseDouble(searchTerm));
			} else if (searchField.equalsIgnoreCase("secondaryPower4")) {
				return currentMeasuringTransformerRepository.findBySecondaryPower4(Double.parseDouble(searchTerm));
			} else if (searchField.equalsIgnoreCase("secondaryPower5")) {
				return currentMeasuringTransformerRepository.findBySecondaryPower5(Double.parseDouble(searchTerm));
			} else if (searchField.equalsIgnoreCase("secondaryPower6")) {
				return currentMeasuringTransformerRepository.findBySecondaryPower6(Double.parseDouble(searchTerm));
			} else if (searchField.equalsIgnoreCase("secondaryPrecisionClass1")) {
				return currentMeasuringTransformerRepository.findBySecondaryPrecisionClass1(Double.parseDouble(searchTerm));
			} else if (searchField.equalsIgnoreCase("secondaryPrecisionClass2")) {
				return currentMeasuringTransformerRepository.findBySecondaryPrecisionClass2(Double.parseDouble(searchTerm));
			} else if (searchField.equalsIgnoreCase("secondaryPrecisionClass3")) {
				return currentMeasuringTransformerRepository.findBySecondaryPrecisionClass3(Double.parseDouble(searchTerm));
			} else if (searchField.equalsIgnoreCase("secondaryPrecisionClass4")) {
				return currentMeasuringTransformerRepository.findBySecondaryPrecisionClass4(Double.parseDouble(searchTerm));
			} else if (searchField.equalsIgnoreCase("secondaryPrecisionClass5")) {
				return currentMeasuringTransformerRepository.findBySecondaryPrecisionClass5(Double.parseDouble(searchTerm));
			} else if (searchField.equalsIgnoreCase("secondaryPrecisionClass6")) {
				return currentMeasuringTransformerRepository.findBySecondaryPrecisionClass6(Double.parseDouble(searchTerm));
			} else if (searchField.equalsIgnoreCase("operatingEnvironment")) {
				return currentMeasuringTransformerRepository.findByOperatingEnvironment(searchTerm);
			} else if (searchField.equalsIgnoreCase("insulationType")) {
				return currentMeasuringTransformerRepository.findByInsulationType(searchTerm);
			} else if (searchField.equalsIgnoreCase("operatingMode")) {
				return currentMeasuringTransformerRepository.findByOperatingMode(searchTerm);
			} else if (searchField.equalsIgnoreCase("manufacturer")) {
				return currentMeasuringTransformerRepository.findByManufacturer(searchTerm);
			} else if (searchField.equalsIgnoreCase("equipmentSeries")) {
				return currentMeasuringTransformerRepository.findByEquipmentSeries(searchTerm);
			} else if (searchField.equalsIgnoreCase("manufacturingYear")) {
				return currentMeasuringTransformerRepository.findByManufacturingYear(Integer.parseInt(searchTerm));
			} else if (searchField.equalsIgnoreCase("pifYear")) {
				return currentMeasuringTransformerRepository.findByPifYear(Integer.parseInt(searchTerm));
			} else if (searchField.equalsIgnoreCase("installationCompany")) {
				return currentMeasuringTransformerRepository.findByInstallationCompany(searchTerm);
			} else if (searchField.equalsIgnoreCase("pifBulletin")) {
				return currentMeasuringTransformerRepository.findByPifBulletin(searchTerm);
			} else if (searchField.equalsIgnoreCase("exploitationCenter")) {
				return currentMeasuringTransformerRepository.findByExploitationCenter(searchTerm);
			} else if (searchField.equalsIgnoreCase("transformationCenter")) {
				return currentMeasuringTransformerRepository.findByTransformationStation(searchTerm);
			} else if (searchField.equalsIgnoreCase("voltageLevel")) {
				return currentMeasuringTransformerRepository.findByVoltageLevel(searchTerm);
			} else if (searchField.equalsIgnoreCase("assemblyCell")) {
				return currentMeasuringTransformerRepository.findByAssemblyCell(searchTerm);
			} else if (searchField.equalsIgnoreCase("assemblyPhase")) {
				return currentMeasuringTransformerRepository.findByAssemblyPhase(searchTerm);
			} else if (searchField.equalsIgnoreCase("accountingorWarehouseRegistrationCode")) {
				return currentMeasuringTransformerRepository.findByAccountingorWarehouseRegistrationCode(searchTerm);
			} else if (searchField.equalsIgnoreCase("fixedAssetComponentNumber")) {
				return currentMeasuringTransformerRepository.findByFixedAssetComponentNumber(searchTerm);				
			} else {
				return currentMeasuringTransformerRepository.search(searchTerm);
			}		
		} catch (NumberFormatException e) {
			return currentMeasuringTransformerRepository.search(searchTerm);
		}
	}
	
	@PostMapping("/uploadFile")
    public String uploadFile(@RequestParam("file") MultipartFile file, Model model) {
        EquipmentManual equipmentManual = new EquipmentManual();

        // setting up the entity of the equipment manual
        equipmentManual.setId(EQUIPMENT);
        equipmentManual.setOperatingManualFileType(file.getContentType());
        equipmentManual.setOperatingManualFileName(file.getOriginalFilename());
        
        try {
			//equipmentManual.setOperatingManual(file.getBytes());
        	
        	// create a blob object so that we can store anything bigger and 255 bytes
        	equipmentManual.setOperatingManual(sessionFactory.openSession().getLobHelper().createBlob(file.getBytes()));
			equipmentManualRepository.save(equipmentManual);
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
        
        return read(model); 
//        String fileDownloadUri = ServletUriComponentsBuilder.fromCurrentContextPath()
//            .path("/Downloads/")
//            .path(equipmentManual.getOperatingManualFileName())
//            .toUriString();

//        return new Response(equipmentManual.getOperatingManualFileName(), fileDownloadUri,
//            file.getContentType(), file.getSize());
    }
	
	@GetMapping("/downloadFile")
    public ResponseEntity <Resource> downloadFile(HttpServletRequest request) {
        // Load file as Resource
        Optional<EquipmentManual> equipmentManual = equipmentManualRepository.findById(EQUIPMENT);

        if (equipmentManual.isPresent()) {
        	try {
				return ResponseEntity.ok()
				        .contentType(MediaType.parseMediaType(equipmentManual.get().getOperatingManualFileType()))
				        .header(HttpHeaders.CONTENT_DISPOSITION, "attachment; filename=\"" + equipmentManual.get().getOperatingManualFileName() + "\"")
				        .body(new ByteArrayResource(equipmentManual.get().getOperatingManual().getBytes(1, (int) equipmentManual.get().getOperatingManual().length())));
			} catch (SQLException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
        }
        return ResponseEntity.notFound()
        		.build();
    }

//    @PostMapping("/uploadMultipleFiles")
//    public List < Response > uploadMultipleFiles(@RequestParam("files") MultipartFile[] files) {
//        return Arrays.asList(files)
//            .stream()
//            .map(file -> uploadFile(file))
//            .collect(Collectors.toList());
//    }
	
	@GetMapping("/create")
	public String create(CurrentMeasuringTransformer currentMeasuringTransformer, Model model)
	{
		model.addAttribute("equipment", EQUIPMENT);
		model.addAttribute("equipmentObject", new CurrentMeasuringTransformer());
		return EQUIPMENT + "/" + EQUIPMENT + "-create";
	}

	@PostMapping("/create-save")
	// @ModelAttribute("equipmentObject") connects the create-save method to the create html page of the equipment
	public String createSave(@Validated @ModelAttribute("equipmentObject") CurrentMeasuringTransformer currentMeasuringTransformer, BindingResult result, Model model)
	{
		if(result.hasErrors())
		{
			// without line 144 we don't have the equipment object in the request, since we need it 
			model.addAttribute("equipmentObject", currentMeasuringTransformer);
			return EQUIPMENT + "/" + EQUIPMENT + "-create";
		}
		
		// update the time stamp before saving the record
	    currentMeasuringTransformer.setUpdateTimeStamp(LocalDateTime.now());		
		currentMeasuringTransformerRepository.save(currentMeasuringTransformer);
		return "redirect:read";
	}
	
	@GetMapping("/read")
	public String read(Model model) 
	{
		model.addAttribute("equipment", EQUIPMENT);
		List<CurrentMeasuringTransformer> currentMeasuringTransformers = currentMeasuringTransformerRepository.findAll();
		if (currentMeasuringTransformers.size() != 0) 
		{
			System.out.println(currentMeasuringTransformers); 
			model.addAttribute("equipmentObjects", currentMeasuringTransformers);
		}
		model.addAttribute("searchFields", getHeadingsWithFields().entrySet());
		return EQUIPMENT + "/" + EQUIPMENT + "-read";
	}
	
	@GetMapping("/edit/{id}")
	public String edit(@PathVariable("id") Long id, Model model) 
	{
		model.addAttribute("equipment", EQUIPMENT);
		Optional<CurrentMeasuringTransformer> currentMeasuringTransformer = currentMeasuringTransformerRepository.findById(id);
	    model.addAttribute("equipmentObject", currentMeasuringTransformer.get());
	    
		return EQUIPMENT + "/" + EQUIPMENT + "-update";
	}
	
	@PostMapping("/update/{id}")
	public String update(@PathVariable("id") Long id, @Validated @ModelAttribute("equipmentObject") CurrentMeasuringTransformer currentMeasuringTransformer, BindingResult result, Model model) 
	{
	    // check to see that the user is updating using fresh data
		if (currentMeasuringTransformerRepository.findByIdAndUpdateTimeStamp(id, currentMeasuringTransformer.getUpdateTimeStamp()).isEmpty()) {
			// if it's empty = not dealing with fresh data
			// reject allows 191 to have errors
			// each time the addError() function gets used, it means there is a global error
			result.addError(new ObjectError("global", "Another user has already updated the data"));
			// message is coming from line 190
			model.addAttribute("globalError", "Another user has already updated the data");
		}
		
	    if(result.hasErrors()) 
	    {
	    	currentMeasuringTransformer.setId(id);
			model.addAttribute("equipmentObject", currentMeasuringTransformer);
			return EQUIPMENT + "/" + EQUIPMENT + "-update";
	    }
	    // update the time stamp
	    currentMeasuringTransformer.setUpdateTimeStamp(LocalDateTime.now());
	    // save the record
	    currentMeasuringTransformerRepository.save(currentMeasuringTransformer);
	    
		return "redirect:../read";
	}
	
	@GetMapping("/delete/{id}")
	public String delete(@PathVariable("id") Long id, Model model) 
	{
		Optional<CurrentMeasuringTransformer> currentMeasuringTransformer = currentMeasuringTransformerRepository.findById(id);
	    
		currentMeasuringTransformerRepository.delete(currentMeasuringTransformer.get());
		return "redirect:../read";
	}	
	
	@GetMapping("/exportToCSV")
	public void export(HttpServletResponse response, Model model) throws IOException {
		response.setContentType("text/csv");
		//  how it gets saved
		response.addHeader("Content-Disposition", "attachment; filename = \"" + StringUtils.capitalize(EQUIPMENT) + "s.csv\"");
		// "attachment; filename = \"Separators.csv\"" is the header 
		List<ExportableToCSV> data = new ArrayList<>();
		data.addAll(currentMeasuringTransformerRepository.findAll()); // gets every record in the separator table and adds it to the list that gets exported
		csvFileGenerator.write(getHeadings(), data, response.getWriter()); // write to the response object with the header
	}
	// controller handles the headings, entities handle the data
	
	private static String[] getHeadings() {
		String[] fields = new String[37];
		// fields at position zero
		fields[0] = "Equipment Code";
		fields[1] = "Nominal Voltage";
		fields[2] = "Primary Nominal Current";
		fields[3] = "Secondary Nominal Current 1";
		fields[4] = "Secondary Nominal Current 2";
		fields[5] = "Secondary Nominal Current 3";
		fields[6] = "Secondary Nominal Current 4";
		fields[7] = "Secondary Nominal Current 5";
		fields[8] = "Secondary Nominal Current 6";
		fields[9] = "Secondary Power 1";
		fields[10] = "Secondary Power 2";
		fields[11] = "Secondary Power 3";
		fields[12] = "Secondary Power 4";
		fields[13] = "Secondary Power 5";
		fields[14] = "Secondary Power 6";
		fields[15] = "Secondary Precision Class 1";
		fields[16] = "Secondary Precision Class 2";
		fields[17] = "Secondary Precision Class 3";
		fields[18] = "Secondary Precision Class 4";
		fields[19] = "Secondary Precision Class 5";
		fields[20] = "Secondary Precision Class 6";
		fields[21] = "Operating Environment";
		fields[22] = "Insulation Type";
		fields[23] = "Operating Mode";
		fields[24] = "Manufacturer";
		fields[25] = "Equipment Series";
		fields[26] = "Manufacturing Year";
		fields[27] = "Pif Year";
		fields[28] = "Installation Company";
		fields[29] = "Pif Bulletin";
		fields[30] = "Exploitation Center";
		fields[31] = "Transformation Station";
		fields[32] = "Voltage Level";
		fields[33] = "Assembly Cell";
		fields[34] = "Assembly Phase";
		fields[35] = "Accounting or Warehouse Registration Code";
		fields[36] = "Fixed Asset Component Number";
		return fields;
	}
	
	private static String[] getFieldNames() {
		String[] fields = new String[37];
		// fields at position zero
		fields[0] = "equipmentCode";
		fields[1] = "nominalVoltage";
		fields[2] = "primaryNominalCurrent";
		fields[3] = "secondaryNominalCurrent1";
		fields[4] = "secondaryNominalCurrent2";
		fields[5] = "secondaryNominalCurrent3";
		fields[6] = "secondaryNominalCurrent4";
		fields[7] = "secondaryNominalCurrent5";
		fields[8] = "secondaryNominalCurrent6";
		fields[9] = "secondaryPower1";
		fields[10] = "secondaryPower2";
		fields[11] = "secondaryPower3";
		fields[12] = "secondaryPower4";
		fields[13] = "secondaryPower5";
		fields[14] = "secondaryPower6";
		fields[15] = "secondaryPrecisionClass1";
		fields[16] = "secondaryPrecisionClass2";
		fields[17] = "secondaryPrecisionClass3";
		fields[18] = "secondaryPrecisionClass4";
		fields[19] = "secondaryPrecisionClass5";
		fields[20] = "secondaryPrecisionClass6";
		fields[21] = "operatingEnvironment";
		fields[22] = "insulationType";
		fields[23] = "operatingMode";
		fields[24] = "manufacturer";
		fields[25] = "equipmentSeries";
		fields[26] = "manufacturingYear";
		fields[27] = "pifYear";
		fields[28] = "installationCompany";
		fields[29] = "pifBulletin";
		fields[30] = "exploitationCenter";
		fields[31] = "transformationStation";
		fields[32] = "voltageLevel";
		fields[33] = "assemblyCell";
		fields[34] = "assemblyPhase";
		fields[35] = "accountingorWarehouseRegistrationCode";
		fields[36] = "fixedAssetComponentNumber";
		return fields;
	}
	
	private static Map<String, String> getHeadingsWithFields() {
		Map<String, String> map = new TreeMap<>();
		map.put("* Search All Fields", "*");
		String[] headings = getHeadings();
		String[] fieldNames = getFieldNames();
		for (int x = 0; x < headings.length; x++) {
			map.put(headings[x], fieldNames[x]);
		}
		
		return map;
	}

	@GetMapping("/exportToPDF")
	public void exportToPDF(HttpServletResponse response, Model model) throws IOException {
		/* String outputFolder = "C:\\Users\\MY PC\\Downloads" + File.arrester + "Arresters.pdf";
		 OutputStream outputStream = new FileOutputStream(outputFolder);
		*/
		
		// getOutputStream() allows the PDF to be opened in the browser rather than a specific location on the server
		OutputStream outputStream = response.getOutputStream();

		response.addHeader("Content-Disposition", "attachment; filename = \"" + StringUtils.capitalize(EQUIPMENT) + "s.pdf\"");
		
	    ITextRenderer renderer = new ITextRenderer();
	    renderer.setDocumentFromString(parseThymeleafTemplate());
	    renderer.layout();
	    renderer.createPDF(outputStream);

	    outputStream.close();
	}
	
	
	private String parseThymeleafTemplate() {
	    ClassLoaderTemplateResolver templateResolver = new ClassLoaderTemplateResolver();
	    templateResolver.setSuffix(".html");
	    templateResolver.setTemplateMode(TemplateMode.HTML);

	    TemplateEngine templateEngine = new TemplateEngine();
	    templateEngine.setTemplateResolver(templateResolver);
	    
	    // model.addAttribute("equipment", "separator");
		List<CurrentMeasuringTransformer> arresters = currentMeasuringTransformerRepository.findAll();

	    Context context = new Context();
	    context.setVariable("equipmentObjects", arresters);

	    return templateEngine.process("templates/currentMeasuringTransformer/currentMeasuringTransformer-pdf", context);
	}
}