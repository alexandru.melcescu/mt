package ro.upt.ac.mtb.equipments.switchactuationdevice;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.Optional;
import java.util.TreeMap;

import org.hibernate.SessionFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.io.ByteArrayResource;
import org.springframework.core.io.Resource;
import org.springframework.data.repository.query.Param;
import org.springframework.http.HttpHeaders;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.validation.ObjectError;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.multipart.MultipartFile;
import org.thymeleaf.TemplateEngine;
import org.thymeleaf.context.Context;
import org.thymeleaf.templatemode.TemplateMode;
import org.thymeleaf.templateresolver.ClassLoaderTemplateResolver;
import org.thymeleaf.util.StringUtils;
import org.xhtmlrenderer.pdf.ITextRenderer;

import jakarta.servlet.http.HttpServletRequest;
import jakarta.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.io.OutputStream;
import java.sql.SQLException;
import java.time.LocalDateTime;
import ro.upt.ac.mtb.equipments.electricalequipment.EquipmentManual;
import ro.upt.ac.mtb.equipments.electricalequipment.EquipmentManualRepository;
import ro.upt.ac.mtb.equipments.services.CSVFileGenerator;
import ro.upt.ac.mtb.equipments.services.ExportableToCSV;

@Controller
@RequestMapping("switchActuationDevice")
public class SwitchActuationDeviceController
{
	@Autowired
	SwitchActuationDeviceRepository switchActuationDeviceRepository;
	
	@Autowired
	EquipmentManualRepository equipmentManualRepository;
	
	@Autowired
	CSVFileGenerator csvFileGenerator;
	
	@Autowired
	SessionFactory sessionFactory;
	
	private static final String EQUIPMENT = "switchActuationDevice";
	
	@PostMapping("/search")
	public String search(@Param("searchTerm") String searchTerm, @Param("searchField") String searchField, Model model) {
		//String searchTerm = (String) model.getAttribute("searchTerm");
		List<SwitchActuationDevice> data = null;
		System.out.println(searchTerm + " " + searchField);
		
		data = performSearch(searchField, searchTerm);
		
		if (data.size() != 0) {
		    model.addAttribute("equipmentObjects", data);
		}
		model.addAttribute("equipment", EQUIPMENT);
		model.addAttribute("searchFields", getHeadingsWithFields().entrySet());
		model.addAttribute("search", Boolean.TRUE);
		model.addAttribute("searchField", searchField);
		model.addAttribute("searchTerm", searchTerm);
		return EQUIPMENT + "/" + EQUIPMENT + "-read";
	}
	
	private List<SwitchActuationDevice> performSearch(String searchField, String searchTerm) {
		try {
			if (searchField.equalsIgnoreCase("ratedVoltageUnitOfMeasure")) {
				// from a string to an Integer
				return switchActuationDeviceRepository.findByRatedVoltageUnitOfMeasure(searchTerm);
			} else if (searchField.equalsIgnoreCase("ratedVoltageValue")) {
				return switchActuationDeviceRepository.findByRatedVoltageValue(Integer.parseInt(searchTerm));
			} else if (searchField.equalsIgnoreCase("ratedVoltageDataPresentedByTheBinder")) {
				return switchActuationDeviceRepository.findByRatedVoltageDataPresentedByTheBinder(searchTerm);
			} else if (searchField.equalsIgnoreCase("operatingVoltageUnitOfMeasure")) {
				return switchActuationDeviceRepository.findByOperatingVoltageUnitOfMeasure(searchTerm);
			} else if (searchField.equalsIgnoreCase("operatingVoltageValue")) {
				return switchActuationDeviceRepository.findByOperatingVoltageValue(searchTerm);
			} else if (searchField.equalsIgnoreCase("operatingPresentedByTheBinder")) {
				return switchActuationDeviceRepository.findByOperatingPresentedByTheBinder(searchTerm);
			} else if (searchField.equalsIgnoreCase("ratedCurrentUnitOfMeasure")) {
				return switchActuationDeviceRepository.findByRatedCurrentUnitOfMeasure(searchTerm);
			} else if (searchField.equalsIgnoreCase("ratedCurrentValue")) {
				return switchActuationDeviceRepository.findByRatedCurrentValue(Integer.parseInt(searchTerm));
			} else if (searchField.equalsIgnoreCase("ratedCurrentDataPresentedByTheBinder")) {
				return switchActuationDeviceRepository.findByRatedCurrentDataPresentedByTheBinder(searchTerm);
			} else if (searchField.equalsIgnoreCase("workingDisconnectingCapacityUnitOfMeasure")) {
				return switchActuationDeviceRepository.findByWorkingDisconnectingCapacityUnitOfMeasure(searchTerm);
			} else if (searchField.equalsIgnoreCase("workingDisconnectingCapacityValue")) {
				return switchActuationDeviceRepository.findByWorkingDisconnectingCapacityValue(Integer.parseInt(searchTerm));
			} else if (searchField.equalsIgnoreCase("workingDisconnectingCapacityDataPresentedByTheBinder")) {
				return switchActuationDeviceRepository.findByWorkingDisconnectingCapacityDataPresentedByTheBinder(searchTerm);
			} else if (searchField.equalsIgnoreCase("maximumDisconnectingCapacityUnitOfMeasure")) {
				return switchActuationDeviceRepository.findByMaximumDisconnectingCapacityUnitOfMeasure(searchTerm);
			} else if (searchField.equalsIgnoreCase("maximumDisconnectingCapacityValue")) {
				return switchActuationDeviceRepository.findByMaximumDisconnectingCapacityValue(Integer.parseInt(searchTerm));
			} else if (searchField.equalsIgnoreCase("maximumDisconnectingCapacityDataPresentedByTheBinder")) {
				return switchActuationDeviceRepository.findByMaximumDisconnectingCapacityDataPresentedByTheBinder(searchTerm);
			} else if (searchField.equalsIgnoreCase("protectionDegreeUnitOfMeasure")) {
				return switchActuationDeviceRepository.findByProtectionDegreeUnitOfMeasure(searchTerm);
			} else if (searchField.equalsIgnoreCase("protectionDegreeValue")) {
				return switchActuationDeviceRepository.findByProtectionDegreeValue(searchTerm);
			} else if (searchField.equalsIgnoreCase("protectionDegreeDataPresentedByTheBinder")) {
				return switchActuationDeviceRepository.findByProtectionDegreeDataPresentedByTheBinder(searchTerm);
			} else if (searchField.equalsIgnoreCase("auxiliarySignalingContactsUnitOfMeasure")) {
				return switchActuationDeviceRepository.findByAuxiliarySignalingContactsUnitOfMeasure(searchTerm);
			} else if (searchField.equalsIgnoreCase("auxiliarySignalingContactsValue")) {
				return switchActuationDeviceRepository.findByAuxiliarySignalingContactsValue(Integer.parseInt(searchTerm));
			} else if (searchField.equalsIgnoreCase("auxiliarySignalingContactsDataPresentedByTheBinder")) {
				return switchActuationDeviceRepository.findByAuxiliarySignalingContactsDataPresentedByTheBinder(searchTerm);
			} else if (searchField.equalsIgnoreCase("equipmentSeries")) {
				return switchActuationDeviceRepository.findByEquipmentSeries(searchTerm);
			} else if (searchField.equalsIgnoreCase("manufacturingYear")) {
				return switchActuationDeviceRepository.findByManufacturingYear(Integer.parseInt(searchTerm));
			} else if (searchField.equalsIgnoreCase("pifYear")) {
				return switchActuationDeviceRepository.findByPifYear(Integer.parseInt(searchTerm));
			} else if (searchField.equalsIgnoreCase("installationCompany")) {
				return switchActuationDeviceRepository.findByInstallationCompany(searchTerm);
			} else if (searchField.equalsIgnoreCase("pifBulletin")) {
				return switchActuationDeviceRepository.findByPifBulletin(searchTerm);
			} else if (searchField.equalsIgnoreCase("exploitationCenter")) {
				return switchActuationDeviceRepository.findByExploitationCenter(searchTerm);
			} else if (searchField.equalsIgnoreCase("transformationCenter")) {
				return switchActuationDeviceRepository.findByTransformationStation(searchTerm);
			} else if (searchField.equalsIgnoreCase("voltageLevel")) {
				return switchActuationDeviceRepository.findByVoltageLevel(searchTerm);
			} else if (searchField.equalsIgnoreCase("assemblyCell")) {
				return switchActuationDeviceRepository.findByAssemblyCell(searchTerm);
			} else if (searchField.equalsIgnoreCase("assemblyPhase")) {
				return switchActuationDeviceRepository.findByAssemblyPhase(searchTerm);
			} else if (searchField.equalsIgnoreCase("accountingorWarehouseRegistrationCode")) {
				return switchActuationDeviceRepository.findByAccountingorWarehouseRegistrationCode(searchTerm);
			} else if (searchField.equalsIgnoreCase("fixedAssetComponentNumber")) {
				return switchActuationDeviceRepository.findByFixedAssetComponentNumber(searchTerm);				
			} else {
				return switchActuationDeviceRepository.search(searchTerm);
			}		
		} catch (NumberFormatException e) {
			return switchActuationDeviceRepository.search(searchTerm);
		}
	}
	
	@PostMapping("/uploadFile")
    public String uploadFile(@RequestParam("file") MultipartFile file, Model model) {
        EquipmentManual equipmentManual = new EquipmentManual();

        // setting up the entity of the equipment manual
        equipmentManual.setId(EQUIPMENT);
        equipmentManual.setOperatingManualFileType(file.getContentType());
        equipmentManual.setOperatingManualFileName(file.getOriginalFilename());
        
        try {
			//equipmentManual.setOperatingManual(file.getBytes());
        	
        	// create a blob object so that we can store anything bigger and 255 bytes
        	equipmentManual.setOperatingManual(sessionFactory.openSession().getLobHelper().createBlob(file.getBytes()));
			equipmentManualRepository.save(equipmentManual);
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
        
        return read(model); 
	}
	
	@GetMapping("/downloadFile")
    public ResponseEntity <Resource> downloadFile(HttpServletRequest request) {
        // Load file as Resource
        Optional<EquipmentManual> equipmentManual = equipmentManualRepository.findById(EQUIPMENT);

        if (equipmentManual.isPresent()) {
        	try {
				return ResponseEntity.ok()
				        .contentType(MediaType.parseMediaType(equipmentManual.get().getOperatingManualFileType()))
				        .header(HttpHeaders.CONTENT_DISPOSITION, "attachment; filename=\"" + equipmentManual.get().getOperatingManualFileName() + "\"")
				        .body(new ByteArrayResource(equipmentManual.get().getOperatingManual().getBytes(1, (int) equipmentManual.get().getOperatingManual().length())));
			} catch (SQLException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
        }
        return ResponseEntity.notFound()
        		.build();
    }
	
	@GetMapping("/create")
	public String create(SwitchActuationDevice switchActuationDevice, Model model)
	{
		model.addAttribute("equipment", EQUIPMENT);
		model.addAttribute("equipmentObject", new SwitchActuationDevice());
		return EQUIPMENT + "/" + EQUIPMENT + "-create";
	}

	@PostMapping("/create-save")
	// @ModelAttribute("equipmentObject") connects the create-save method to the create html page of the equipment
	public String createSave(@Validated @ModelAttribute("equipmentObject") SwitchActuationDevice switchActuationDevice, BindingResult result, Model model)
	{
		if(result.hasErrors())
		{
			// without line 144 we don't have the equipment object in the request, since we need it 
			model.addAttribute("equipmentObject", switchActuationDevice);
			return EQUIPMENT + "/" + EQUIPMENT + "-create";
		}
		
		// update the time stamp before saving the record
	    switchActuationDevice.setUpdateTimeStamp(LocalDateTime.now());		
		switchActuationDeviceRepository.save(switchActuationDevice);
		return "redirect:read";
	}
	
	@GetMapping("/read")
	public String read(Model model) 
	{
		model.addAttribute("equipment", EQUIPMENT);
		List<SwitchActuationDevice> switchActuationDevices = switchActuationDeviceRepository.findAll();
		if (switchActuationDevices.size() != 0) 
		{
			System.out.println(switchActuationDevices); 
			model.addAttribute("equipmentObjects", switchActuationDevices);
		}
		model.addAttribute("searchFields", getHeadingsWithFields().entrySet());
		return EQUIPMENT + "/" + EQUIPMENT + "-read";
	}
	
	@GetMapping("/edit/{id}")
	public String edit(@PathVariable("id") Long id, Model model) 
	{
		model.addAttribute("equipment", EQUIPMENT);
		Optional<SwitchActuationDevice> switchActuationDevice = switchActuationDeviceRepository.findById(id);
	    model.addAttribute("equipmentObject", switchActuationDevice.get());
	    
		return EQUIPMENT + "/" + EQUIPMENT + "-update";
	}
	
	@PostMapping("/update/{id}")
	public String update(@PathVariable("id") Long id, @Validated @ModelAttribute("equipmentObject") SwitchActuationDevice switchActuationDevice, BindingResult result, Model model) 
	{
	    // check to see that the user is updating using fresh data
		if (switchActuationDeviceRepository.findByIdAndUpdateTimeStamp(id, switchActuationDevice.getUpdateTimeStamp()).isEmpty()) {
			// if it's empty = not dealing with fresh data
			// reject allows 191 to have errors
			// each time the addError() function gets used, it means there is a global error
			result.addError(new ObjectError("global", "Another user has already updated the data"));
			// message is coming from line 190
			model.addAttribute("globalError", "Another user has already updated the data");
		}
		
	    if(result.hasErrors()) 
	    {
	    	switchActuationDevice.setId(id);
			model.addAttribute("equipmentObject", switchActuationDevice);
			return EQUIPMENT + "/" + EQUIPMENT + "-update";
	    }
	    // update the time stamp
	    switchActuationDevice.setUpdateTimeStamp(LocalDateTime.now());
	    // save the record
	    switchActuationDeviceRepository.save(switchActuationDevice);
	    
		return "redirect:../read";
	}
	
	@GetMapping("/delete/{id}")
	public String delete(@PathVariable("id") Long id, Model model) 
	{
		Optional<SwitchActuationDevice> switchActuationDevice = switchActuationDeviceRepository.findById(id);
	    
		switchActuationDeviceRepository.delete(switchActuationDevice.get());
		return "redirect:../read";
	}	
	
	@GetMapping("/exportToCSV")
	public void exportToCSV(@Param("searchTerm") String searchTerm, @Param("searchField") String searchField, HttpServletResponse response, Model model) throws IOException {
		response.setContentType("text/csv");
		//  how it gets saved
		response.addHeader("Content-Disposition", "attachment; filename = \"" + StringUtils.capitalize(EQUIPMENT) + "s.csv\"");

		// "attachment; filename = \"Separators.csv\"" is the header 
		List<ExportableToCSV> data = new ArrayList<>();
		if (searchField != null && searchTerm != null && !searchTerm.trim().equals("") ) {
			data.addAll(performSearch(searchField, searchTerm));
		} else {
			data.addAll(switchActuationDeviceRepository.findAll()); // gets every record in the separator table and adds it to the list that gets exported
		}
		csvFileGenerator.write(getHeadings(), data, response.getWriter()); // write to the response object with the header
	}
	// controller handles the headings, entities handle the data
	
	private static String[] getHeadings() {
		String[] fields = new String[33];
		// fields at position zero
		fields[0] = "Rated Voltage Unit Of Measure";
		fields[1] = "Rated Voltage Value";
		fields[2] = "Rated Voltage Data Presented By The Binder";
		fields[3] = "Operating Voltage Unit Of Measure";
		fields[4] = "Operating Voltage Value";
		fields[5] = "Operating Presented By The Binder";
		fields[6] = "Rated Current Unit Of Measure";
		fields[7] = "Rated Current Value";
		fields[8] = "Rated Current Data Presented By The Binder";
		fields[9] = "Working Disconnecting Capacity Unit Of Measure";
		fields[10] = "Working Disconnecting Capacity Value";
		fields[11] = "Working Disconnecting Capacity Data Presented By The Binder";
		fields[12] = "Maximum Disconnecting Capacity Unit Of Measure";
		fields[13] = "Maximum Disconnecting Capacity Value";
		fields[14] = "Maximum Disconnecting Capacity Data Presented By The Binder";
		fields[15] = "Protection Degree Unit Of Measure";
		fields[16] = "Protection Degree Value";
		fields[17] = "Protection Degree Data Presented By The Binder";
		fields[18] = "Auxiliary Signaling Contacts Unit Of Measure";
		fields[19] = "Auxiliary Signaling Contacts Value";
		fields[20] = "Auxiliary Signaling Contacts Data Presented By The Binder";
		fields[21] = "Equipment Series";
		fields[22] = "Manufacturing Year";
		fields[23] = "Pif Year";
		fields[24] = "Installation Company";
		fields[25] = "Pif Bulletin";
		fields[26] = "Exploitation Center";
		fields[27] = "Transformation Station";
		fields[28] = "Voltage Level";
		fields[29] = "Assembly Cell";
		fields[30] = "Assembly Phase";
		fields[31] = "Accounting or Warehouse Registration Code";
		fields[32] = "Fixed Asset Component Number";
		return fields;
	}

	private static String[] getFieldNames() {
		String[] fields = new String[33];
		// fields at position zero
		fields[0] = "ratedVoltageUnitOfMeasure";
		fields[1] = "ratedVoltageValue";
		fields[2] = "ratedVoltageDataPresentedByTheBinder";
		fields[3] = "operatingVoltageUnitOfMeasure";
		fields[4] = "operatingVoltageValue";
		fields[5] = "operatingPresentedByTheBinder";
		fields[6] = "ratedCurrentUnitOfMeasure";
		fields[7] = "ratedCurrentValue";
		fields[8] = "ratedCurrentDataPresentedByTheBinder";
		fields[9] = "workingDisconnectingCapacityUnitOfMeasure";
		fields[10] = "workingDisconnectingCapacityValue";
		fields[11] = "workingDisconnectingCapacityDataPresentedByTheBinder";
		fields[12] = "maximumDisconnectingCapacityUnitOfMeasure";
		fields[13] = "maximumDisconnectingCapacityValue";
		fields[14] = "maximumDisconnectingCapacityDataPresentedByTheBinder";
		fields[15] = "protectionDegreeUnitOfMeasure";
		fields[16] = "protectionDegreeValue";
		fields[17] = "protectionDegreeDataPresentedByTheBinder";
		fields[18] = "auxiliarySignalingContactsUnitOfMeasure";
		fields[19] = "auxiliarySignalingContactsValue";
		fields[20] = "auxiliarySignalingContactsDataPresentedByTheBinder";
		fields[21] = "equipmentSeries";
		fields[22] = "manufacturingYear";
		fields[23] = "pifYear";
		fields[24] = "installationCompany";
		fields[25] = "pifBulletin";
		fields[26] = "exploitationCenter";
		fields[27] = "transformationStation";
		fields[28] = "voltageLevel";
		fields[29] = "assemblyCell";
		fields[30] = "assemblyPhase";
		fields[31] = "accountingorWarehouseRegistrationCode";
		fields[32] = "fixedAssetComponentNumber";
		return fields;
	}

	private static Map<String, String> getHeadingsWithFields() {
		Map<String, String> map = new TreeMap<>();
		map.put("* Search All Fields", "*");
		String[] headings = getHeadings();
		String[] fieldNames = getFieldNames();
		for (int x = 0; x < headings.length; x++) {
			map.put(headings[x], fieldNames[x]);
		}
		
		return map;
	}
	
	@GetMapping("/exportToPDF")
	public void exportToPDF(HttpServletResponse response, Model model) throws IOException {
		/* String outputFolder = "C:\\Users\\MY PC\\Downloads" + File.arrester + "Arresters.pdf";
		 OutputStream outputStream = new FileOutputStream(outputFolder);
		*/
		
		// getOutputStream() allows the PDF to be opened in the browser rather than a specific location on the server
		OutputStream outputStream = response.getOutputStream();

		response.addHeader("Content-Disposition", "attachment; filename = \"" + StringUtils.capitalize(EQUIPMENT) + "s.pdf\"");
		
	    ITextRenderer renderer = new ITextRenderer();
	    renderer.setDocumentFromString(parseThymeleafTemplate());
	    renderer.layout();
	    renderer.createPDF(outputStream);

	    outputStream.close();
	}
	
	
	private String parseThymeleafTemplate() {
	    ClassLoaderTemplateResolver templateResolver = new ClassLoaderTemplateResolver();
	    templateResolver.setSuffix(".html");
	    templateResolver.setTemplateMode(TemplateMode.HTML);

	    TemplateEngine templateEngine = new TemplateEngine();
	    templateEngine.setTemplateResolver(templateResolver);
	    
	    // model.addAttribute("equipment", "separator");
		List<SwitchActuationDevice> switchActuationDevices = switchActuationDeviceRepository.findAll();

	    Context context = new Context();
	    context.setVariable("equipmentObjects", switchActuationDevices);

	    return templateEngine.process("templates/switchActuationDevice/switchActuationDevice-pdf", context);
	}
}