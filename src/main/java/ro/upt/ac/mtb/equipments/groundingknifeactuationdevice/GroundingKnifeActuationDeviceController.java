package ro.upt.ac.mtb.equipments.groundingknifeactuationdevice;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.Optional;
import java.util.TreeMap;

import org.hibernate.SessionFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.io.ByteArrayResource;
import org.springframework.core.io.Resource;
import org.springframework.data.repository.query.Param;
import org.springframework.http.HttpHeaders;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.validation.ObjectError;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.multipart.MultipartFile;
import org.thymeleaf.TemplateEngine;
import org.thymeleaf.context.Context;
import org.thymeleaf.templatemode.TemplateMode;
import org.thymeleaf.templateresolver.ClassLoaderTemplateResolver;
import org.thymeleaf.util.StringUtils;
import org.xhtmlrenderer.pdf.ITextRenderer;

import jakarta.servlet.http.HttpServletRequest;
import jakarta.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.io.OutputStream;
import java.sql.SQLException;
import java.time.LocalDateTime;
import ro.upt.ac.mtb.equipments.electricalequipment.EquipmentManual;
import ro.upt.ac.mtb.equipments.electricalequipment.EquipmentManualRepository;
import ro.upt.ac.mtb.equipments.services.CSVFileGenerator;
import ro.upt.ac.mtb.equipments.services.ExportableToCSV;

@Controller
@RequestMapping("groundingKnifeActuationDevice")
public class GroundingKnifeActuationDeviceController
{
	@Autowired
	GroundingKnifeActuationDeviceRepository groundingKnifeActuationDeviceRepository;
	
	@Autowired
	EquipmentManualRepository equipmentManualRepository;
	
	@Autowired
	CSVFileGenerator csvFileGenerator;
	
	@Autowired
	SessionFactory sessionFactory;
	
	private static final String EQUIPMENT = "groundingKnifeActuationDevice";
	
	@PostMapping("/search")
	public String search(@Param("searchTerm") String searchTerm, @Param("searchField") String searchField, Model model) {
		//String searchTerm = (String) model.getAttribute("searchTerm");
		List<GroundingKnifeActuationDevice> data = null;
		System.out.println(searchTerm + " " + searchField);
		
		data = performSearch(searchField, searchTerm);
		
		if (data.size() != 0) {
		    model.addAttribute("equipmentObjects", data);
		}
		model.addAttribute("equipment", EQUIPMENT);
		model.addAttribute("searchFields", getHeadingsWithFields().entrySet());
		model.addAttribute("search", Boolean.TRUE);
		model.addAttribute("searchField", searchField);
		model.addAttribute("searchTerm", searchTerm);
		return EQUIPMENT + "/" + EQUIPMENT + "-read";
	}
	
	private List<GroundingKnifeActuationDevice> performSearch(String searchField, String searchTerm) {
		try {
			if (searchField.equalsIgnoreCase("bladeMaterial")) {
				return groundingKnifeActuationDeviceRepository.findByBladeMaterial(searchTerm);
			} else if (searchField.equalsIgnoreCase("handleMaterial")) {
				return groundingKnifeActuationDeviceRepository.findByHandleMaterial(searchTerm);
			} else if (searchField.equalsIgnoreCase("bladeLength")) {
				return groundingKnifeActuationDeviceRepository.findByBladeLength(Double.parseDouble(searchTerm));
			} else if (searchField.equalsIgnoreCase("bladeThickness")) {
				return groundingKnifeActuationDeviceRepository.findByBladeThickness(Integer.parseInt(searchTerm));
			} else if (searchField.equalsIgnoreCase("handleLength")) {
				return groundingKnifeActuationDeviceRepository.findByHandleLength(Double.parseDouble(searchTerm));
			} else if (searchField.equalsIgnoreCase("currentCapacity")) {
				return groundingKnifeActuationDeviceRepository.findByCurrentCapacity(Double.parseDouble(searchTerm));
			} else if (searchField.equalsIgnoreCase("insulationResistance")) {
				return groundingKnifeActuationDeviceRepository.findByInsulationResistance(searchTerm);
			} else if (searchField.equalsIgnoreCase("operatingTemperature")) {
				return groundingKnifeActuationDeviceRepository.findByOperatingTemperature(searchTerm);
			} else if (searchField.equalsIgnoreCase("humidityResistance")) {
				return groundingKnifeActuationDeviceRepository.findByHumidityResistance(searchTerm);
			} else if (searchField.equalsIgnoreCase("equipmentSeries")) {
				return groundingKnifeActuationDeviceRepository.findByEquipmentSeries(searchTerm);
			} else if (searchField.equalsIgnoreCase("manufacturingYear")) {
				return groundingKnifeActuationDeviceRepository.findByManufacturingYear(Integer.parseInt(searchTerm));
			} else if (searchField.equalsIgnoreCase("pifYear")) {
				return groundingKnifeActuationDeviceRepository.findByPifYear(Integer.parseInt(searchTerm));
			} else if (searchField.equalsIgnoreCase("installationCompany")) {
				return groundingKnifeActuationDeviceRepository.findByInstallationCompany(searchTerm);
			} else if (searchField.equalsIgnoreCase("pifBulletin")) {
				return groundingKnifeActuationDeviceRepository.findByPifBulletin(searchTerm);
			} else if (searchField.equalsIgnoreCase("exploitationCenter")) {
				return groundingKnifeActuationDeviceRepository.findByExploitationCenter(searchTerm);
			} else if (searchField.equalsIgnoreCase("transformationCenter")) {
				return groundingKnifeActuationDeviceRepository.findByTransformationStation(searchTerm);
			} else if (searchField.equalsIgnoreCase("voltageLevel")) {
				return groundingKnifeActuationDeviceRepository.findByVoltageLevel(searchTerm);
			} else if (searchField.equalsIgnoreCase("assemblyCell")) {
				return groundingKnifeActuationDeviceRepository.findByAssemblyCell(searchTerm);
			} else if (searchField.equalsIgnoreCase("assemblyPhase")) {
				return groundingKnifeActuationDeviceRepository.findByAssemblyPhase(searchTerm);
			} else if (searchField.equalsIgnoreCase("accountingorWarehouseRegistrationCode")) {
				return groundingKnifeActuationDeviceRepository.findByAccountingorWarehouseRegistrationCode(searchTerm);
			} else if (searchField.equalsIgnoreCase("fixedAssetComponentNumber")) {
				return groundingKnifeActuationDeviceRepository.findByFixedAssetComponentNumber(searchTerm);				
			} else {
				return groundingKnifeActuationDeviceRepository.search(searchTerm);
			}		
		} catch (NumberFormatException e) {
			return groundingKnifeActuationDeviceRepository.search(searchTerm);
		}
	}
	
	@PostMapping("/uploadFile")
    public String uploadFile(@RequestParam("file") MultipartFile file, Model model) {
        EquipmentManual equipmentManual = new EquipmentManual();

        // setting up the entity of the equipment manual
        equipmentManual.setId(EQUIPMENT);
        equipmentManual.setOperatingManualFileType(file.getContentType());
        equipmentManual.setOperatingManualFileName(file.getOriginalFilename());
        
        try {
			//equipmentManual.setOperatingManual(file.getBytes());
        	
        	// create a blob object so that we can store anything bigger and 255 bytes
        	equipmentManual.setOperatingManual(sessionFactory.openSession().getLobHelper().createBlob(file.getBytes()));
			equipmentManualRepository.save(equipmentManual);
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
        
        return read(model); 
	}
	
	@GetMapping("/downloadFile")
    public ResponseEntity <Resource> downloadFile(HttpServletRequest request) {
        // Load file as Resource
        Optional<EquipmentManual> equipmentManual = equipmentManualRepository.findById(EQUIPMENT);

        if (equipmentManual.isPresent()) {
        	try {
				return ResponseEntity.ok()
				        .contentType(MediaType.parseMediaType(equipmentManual.get().getOperatingManualFileType()))
				        .header(HttpHeaders.CONTENT_DISPOSITION, "attachment; filename=\"" + equipmentManual.get().getOperatingManualFileName() + "\"")
				        .body(new ByteArrayResource(equipmentManual.get().getOperatingManual().getBytes(1, (int) equipmentManual.get().getOperatingManual().length())));
			} catch (SQLException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
        }
        return ResponseEntity.notFound()
        		.build();
    }
	
	
	@GetMapping("/create")
	public String create(GroundingKnifeActuationDevice groundingKnifeActuationDevice, Model model)
	{
		model.addAttribute("equipment", EQUIPMENT);
		model.addAttribute("equipmentObject", new GroundingKnifeActuationDevice());
		return EQUIPMENT + "/" + EQUIPMENT + "-create";
	}

	@PostMapping("/create-save")
	// @ModelAttribute("equipmentObject") connects the create-save method to the create html page of the equipment
	public String createSave(@Validated @ModelAttribute("equipmentObject") GroundingKnifeActuationDevice groundingKnifeActuationDevice, BindingResult result, Model model)
	{
		if(result.hasErrors())
		{
			// without line 144 we don't have the equipment object in the request, since we need it 
			model.addAttribute("equipmentObject", groundingKnifeActuationDevice);
			return EQUIPMENT + "/" + EQUIPMENT + "-create";
		}
		
		// update the time stamp before saving the record
	    groundingKnifeActuationDevice.setUpdateTimeStamp(LocalDateTime.now());		
		groundingKnifeActuationDeviceRepository.save(groundingKnifeActuationDevice);
		return "redirect:read";
	}
	
	@GetMapping("/read")
	public String read(Model model) 
	{
		model.addAttribute("equipment", EQUIPMENT);
		List<GroundingKnifeActuationDevice> groundingKnifeActuationDevices = groundingKnifeActuationDeviceRepository.findAll();
		if (groundingKnifeActuationDevices.size() != 0) 
		{
			System.out.println(groundingKnifeActuationDevices); 
			model.addAttribute("equipmentObjects", groundingKnifeActuationDevices);
		}
		model.addAttribute("searchFields", getHeadingsWithFields().entrySet());
		return EQUIPMENT + "/" + EQUIPMENT + "-read";
	}
	
	@GetMapping("/edit/{id}")
	public String edit(@PathVariable("id") Long id, Model model) 
	{
		model.addAttribute("equipment", EQUIPMENT);
		Optional<GroundingKnifeActuationDevice> groundingKnifeActuationDevice = groundingKnifeActuationDeviceRepository.findById(id);
	    model.addAttribute("equipmentObject", groundingKnifeActuationDevice.get());
	    
		return EQUIPMENT + "/" + EQUIPMENT + "-update";
	}
	
	@PostMapping("/update/{id}")
	public String update(@PathVariable("id") Long id, @Validated @ModelAttribute("equipmentObject") GroundingKnifeActuationDevice groundingKnifeActuationDevice, BindingResult result, Model model) 
	{
	    // check to see that the user is updating using fresh data
		if (groundingKnifeActuationDeviceRepository.findByIdAndUpdateTimeStamp(id, groundingKnifeActuationDevice.getUpdateTimeStamp()).isEmpty()) {
			// if it's empty = not dealing with fresh data
			// reject allows 191 to have errors
			// each time the addError() function gets used, it means there is a global error
			result.addError(new ObjectError("global", "Another user has already updated the data"));
			// message is coming from line 190
			model.addAttribute("globalError", "Another user has already updated the data");
		}
		
	    if(result.hasErrors()) 
	    {
	    	groundingKnifeActuationDevice.setId(id);
			model.addAttribute("equipmentObject", groundingKnifeActuationDevice);
			return EQUIPMENT + "/" + EQUIPMENT + "-update";
	    }
	    // update the time stamp
	    groundingKnifeActuationDevice.setUpdateTimeStamp(LocalDateTime.now());
	    // save the record
	    groundingKnifeActuationDeviceRepository.save(groundingKnifeActuationDevice);
	    
		return "redirect:../read";
	}
	
	@GetMapping("/delete/{id}")
	public String delete(@PathVariable("id") Long id, Model model) 
	{
		Optional<GroundingKnifeActuationDevice> groundingKnifeActuationDevice = groundingKnifeActuationDeviceRepository.findById(id);
	    
		groundingKnifeActuationDeviceRepository.delete(groundingKnifeActuationDevice.get());
		return "redirect:../read";
	}	
	
	@GetMapping("/exportToCSV")
	public void export(HttpServletResponse response, Model model) throws IOException {
		response.setContentType("text/csv");
		//  how it gets saved
		response.addHeader("Content-Disposition", "attachment; filename = \"" + StringUtils.capitalize(EQUIPMENT) + "s.csv\"");
		// "attachment; filename = \"Separators.csv\"" is the header 
		List<ExportableToCSV> data = new ArrayList<>();
		data.addAll(groundingKnifeActuationDeviceRepository.findAll()); // gets every record in the separator table and adds it to the list that gets exported
		csvFileGenerator.write(getHeadings(), data, response.getWriter()); // write to the response object with the header
	}
	// controller handles the headings, entities handle the data
	
	private static String[] getHeadings() {
		String[] fields = new String[21];
		// fields at position zero
		fields[0] = "Blade Material";
		fields[1] = "Handle Material";
		fields[2] = "Blade Length";
		fields[3] = "Blade Thickness";
		fields[4] = "Handle Length";
		fields[5] = "Current Capacity";
		fields[6] = "Insulation Resistance";
		fields[7] = "Operating Temperature";
		fields[8] = "Humidity Resistance";
		fields[9] = "Equipment Series";
		fields[10] = "Manufacturing Year";
		fields[11] = "Pif Year";
		fields[12] = "Installation Company";
		fields[13] = "Pif Bulletin";
		fields[14] = "Exploitation Center";
		fields[15] = "Transformation Station";
		fields[16] = "Voltage Level";
		fields[17] = "Assembly Cell";
		fields[18] = "Assembly Phase";
		fields[19] = "Accounting or Warehouse Registration Code";
		fields[20] = "Fixed Asset Component Number";
		return fields;
	}
	
	private static String[] getFieldNames() {
		String[] fields = new String[21];
		// fields at position zero
		fields[0] = "bladeMaterial";
		fields[1] = "handleMaterial";
		fields[2] = "bladeLength";
		fields[3] = "bladeThickness";
		fields[4] = "handleLength";
		fields[5] = "currentCapacity";
		fields[6] = "insulationResistance";
		fields[7] = "operatingTemperature";
		fields[8] = "humidityResistance";
		fields[9] = "equipmentSeries";
		fields[10] = "manufacturingYear";
		fields[11] = "pifYear";
		fields[12] = "installationCompany";
		fields[13] = "pifBulletin";
		fields[14] = "exploitationCenter";
		fields[15] = "transformationStation";
		fields[16] = "voltageLevel";
		fields[17] = "assemblyCell";
		fields[18] = "assemblyPhase";
		fields[19] = "accountingorWarehouseRegistrationCode";
		fields[20] = "fixedAssetComponentNumber";
		return fields;
	}

	private static Map<String, String> getHeadingsWithFields() {
		Map<String, String> map = new TreeMap<>();
		map.put("* Search All Fields", "*");
		String[] headings = getHeadings();
		String[] fieldNames = getFieldNames();
		for (int x = 0; x < headings.length; x++) {
			map.put(headings[x], fieldNames[x]);
		}
		
		return map;
	}
	
	@GetMapping("/exportToPDF")
	public void exportToPDF(HttpServletResponse response, Model model) throws IOException {
		/* String outputFolder = "C:\\Users\\MY PC\\Downloads" + File.arrester + "Arresters.pdf";
		 OutputStream outputStream = new FileOutputStream(outputFolder);
		*/
		
		// getOutputStream() allows the PDF to be opened in the browser rather than a specific location on the server
		OutputStream outputStream = response.getOutputStream();

		response.addHeader("Content-Disposition", "attachment; filename = \"" + StringUtils.capitalize(EQUIPMENT) + "s.pdf\"");
		
	    ITextRenderer renderer = new ITextRenderer();
	    renderer.setDocumentFromString(parseThymeleafTemplate());
	    renderer.layout();
	    renderer.createPDF(outputStream);

	    outputStream.close();
	}
	
	
	private String parseThymeleafTemplate() {
	    ClassLoaderTemplateResolver templateResolver = new ClassLoaderTemplateResolver();
	    templateResolver.setSuffix(".html");
	    templateResolver.setTemplateMode(TemplateMode.HTML);

	    TemplateEngine templateEngine = new TemplateEngine();
	    templateEngine.setTemplateResolver(templateResolver);
	    
	    // model.addAttribute("equipment", "separator");
		List<GroundingKnifeActuationDevice> groundingKnifeActuationDevices = groundingKnifeActuationDeviceRepository.findAll();

	    Context context = new Context();
	    context.setVariable("equipmentObjects", groundingKnifeActuationDevices);

	    return templateEngine.process("templates/groundingKnifeActuationDevice/groundingKnifeActuationDevice-pdf", context);
	}
}