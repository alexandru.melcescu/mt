package ro.upt.ac.mtb.equipments.arresterwithvariableresistance;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.Optional;
import java.util.TreeMap;

import org.hibernate.SessionFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.io.ByteArrayResource;
import org.springframework.core.io.Resource;
import org.springframework.data.repository.query.Param;
import org.springframework.http.HttpHeaders;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.validation.ObjectError;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.multipart.MultipartFile;
import org.thymeleaf.TemplateEngine;
import org.thymeleaf.context.Context;
import org.thymeleaf.templatemode.TemplateMode;
import org.thymeleaf.templateresolver.ClassLoaderTemplateResolver;
import org.thymeleaf.util.StringUtils;
import org.xhtmlrenderer.pdf.ITextRenderer;

import jakarta.servlet.http.HttpServletRequest;
import jakarta.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.io.OutputStream;
import java.sql.SQLException;
import java.time.LocalDateTime;
import ro.upt.ac.mtb.equipments.electricalequipment.EquipmentManual;
import ro.upt.ac.mtb.equipments.electricalequipment.EquipmentManualRepository;
import ro.upt.ac.mtb.equipments.services.CSVFileGenerator;
import ro.upt.ac.mtb.equipments.services.ExportableToCSV;

@Controller
@RequestMapping("arresterWithVariableResistance")
public class ArresterWithVariableResistanceController
{
	@Autowired
	ArresterWithVariableResistanceRepository arresterWithVariableResistanceRepository;
	
	@Autowired
	EquipmentManualRepository equipmentManualRepository;
	
	@Autowired
	CSVFileGenerator csvFileGenerator;
	
	@Autowired
	SessionFactory sessionFactory;
	
	private static final String EQUIPMENT = "arresterWithVariableResistance";
	
	// These descriptions apply to all controllers
	
	/*
	 * The search method:
	 * 1. This endpoint uses two request parameters, one for the searchField, and one for the searchTerm 
	 * 2. The method then invokes the performSearch() method
	 * 3. The endpoint prepares the model for the html template with all required display fields using the addAttribute calls
	 */	
	@PostMapping("/search")
	public String search(@Param("searchTerm") String searchTerm, @Param("searchField") String searchField, Model model) {
		//String searchTerm = (String) model.getAttribute("searchTerm");
		List<ArresterWithVariableResistance> data = null;
		System.out.println(searchTerm + " " + searchField);
		
		data = performSearch(searchField, searchTerm);
		
		// adding the data to the model, if rows were returned that met the criteria
		if (data.size() != 0) {
		    model.addAttribute("equipmentObjects", data);
		}
		model.addAttribute("equipment", EQUIPMENT);
		model.addAttribute("searchFields", getHeadingsWithFields().entrySet());
		model.addAttribute("search", Boolean.TRUE);
		model.addAttribute("searchField", searchField);
		model.addAttribute("searchTerm", searchTerm);
		return EQUIPMENT + "/" + EQUIPMENT + "-read";
	}
	
	/*
	 * The performSearch method:
	 * 1. This method conditionally invokes specific findBy methods on the repository depending on the search field
	 * 2. This method returns the data matching the criteria
	 */
	
	private List<ArresterWithVariableResistance> performSearch(String searchField, String searchTerm) {
		try {
			if (searchField.equalsIgnoreCase("equipmentCode")) {
				return arresterWithVariableResistanceRepository.findByEquipmentCode(searchTerm);
			} else if (searchField.equalsIgnoreCase("shape")) {
				return arresterWithVariableResistanceRepository.findByShape(searchTerm);
			} else if (searchField.equalsIgnoreCase("material")) {
				return arresterWithVariableResistanceRepository.findByMaterial(searchTerm);
			} else if (searchField.equalsIgnoreCase("certification")) {
				return arresterWithVariableResistanceRepository.findByCertification(searchTerm);
			} else if (searchField.equalsIgnoreCase("color")) {
				return arresterWithVariableResistanceRepository.findByColor(searchTerm);
			} else if (searchField.equalsIgnoreCase("transportationPackage")) {
				return arresterWithVariableResistanceRepository.findByTransportationPackage(searchTerm);
			} else if (searchField.equalsIgnoreCase("specification")) {
				// from a string to an Integer
				return arresterWithVariableResistanceRepository.findBySpecification(searchTerm);
			} else if (searchField.equalsIgnoreCase("trademark")) {
				return arresterWithVariableResistanceRepository.findByTrademark(searchTerm);
			} else if (searchField.equalsIgnoreCase("origin")) {
				return arresterWithVariableResistanceRepository.findByOrigin(searchTerm);
			} else if (searchField.equalsIgnoreCase("equipmentSeries")) {
				return arresterWithVariableResistanceRepository.findByEquipmentSeries(searchTerm);
			} else if (searchField.equalsIgnoreCase("manufacturingYear")) {
				return arresterWithVariableResistanceRepository.findByManufacturingYear(Integer.parseInt(searchTerm));
			} else if (searchField.equalsIgnoreCase("pifYear")) {
				return arresterWithVariableResistanceRepository.findByPifYear(Integer.parseInt(searchTerm));
			} else if (searchField.equalsIgnoreCase("installationCompany")) {
				return arresterWithVariableResistanceRepository.findByInstallationCompany(searchTerm);
			} else if (searchField.equalsIgnoreCase("pifBulletin")) {
				return arresterWithVariableResistanceRepository.findByPifBulletin(searchTerm);
			} else if (searchField.equalsIgnoreCase("exploitationCenter")) {
				return arresterWithVariableResistanceRepository.findByExploitationCenter(searchTerm);
			} else if (searchField.equalsIgnoreCase("transformationCenter")) {
				return arresterWithVariableResistanceRepository.findByTransformationStation(searchTerm);
			} else if (searchField.equalsIgnoreCase("voltageLevel")) {
				return arresterWithVariableResistanceRepository.findByVoltageLevel(searchTerm);
			} else if (searchField.equalsIgnoreCase("assemblyCell")) {
				return arresterWithVariableResistanceRepository.findByAssemblyCell(searchTerm);
			} else if (searchField.equalsIgnoreCase("assemblyPhase")) {
				return arresterWithVariableResistanceRepository.findByAssemblyPhase(searchTerm);
			} else if (searchField.equalsIgnoreCase("accountingorWarehouseRegistrationCode")) {
				return arresterWithVariableResistanceRepository.findByAccountingorWarehouseRegistrationCode(searchTerm);
			} else if (searchField.equalsIgnoreCase("fixedAssetComponentNumber")) {
				return arresterWithVariableResistanceRepository.findByFixedAssetComponentNumber(searchTerm);				
			} else {
				return arresterWithVariableResistanceRepository.search(searchTerm);
			}	
		// The catch:	
		// 1. Helped the developer with the complexity
		// 2. All search terms are entered as Strings, but some findBy methods require numeric data such as Integer and Double
		// 3. In those cases, the Strings must be parsed into the appropriate numeric type
		// 4. However, not all Strings can be parsed into numeric numbers, and this specific catch deals with that possibility
		} catch (NumberFormatException e) {
			return arresterWithVariableResistanceRepository.search(searchTerm);
		}
	}
	
	/*
	 * The uploadFile method:
	 * 1. This endpoint stores binary large objects, for example pdf, inside the database
	 * 2. It uses the postMapping from an html form and a request parameter containing the contents of the specific file
	 * 3. The file is stored in the sql database using the repository
	 * !! Check the Equipment Manual for extra details!!
	 */
	
	@PostMapping("/uploadFile")
    public String uploadFile(@RequestParam("file") MultipartFile file, Model model) {
        EquipmentManual equipmentManual = new EquipmentManual();

        // setting up the entity of the equipment manual
        equipmentManual.setId(EQUIPMENT);
        equipmentManual.setOperatingManualFileType(file.getContentType());
        equipmentManual.setOperatingManualFileName(file.getOriginalFilename());
        
        try {
			//equipmentManual.setOperatingManual(file.getBytes());
        	
        	// create a blob object so that we can store anything bigger and 255 bytes
        	equipmentManual.setOperatingManual(sessionFactory.openSession().getLobHelper().createBlob(file.getBytes()));
			equipmentManualRepository.save(equipmentManual);
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
        
        return read(model); 
	}
	
	/*
	 * The downloadFile method/endpoint:
	 * 1. This endpoint allows the binary large object to be downloaded for a specific equipment type
	 * 2. Not every equipment has a specific manual, in which case the endpoint returns a 404 not found error using the code on line 210
	 */
	
	@GetMapping("/downloadFile")
    public ResponseEntity <Resource> downloadFile(HttpServletRequest request) {
        // Load file as Resource
        Optional<EquipmentManual> equipmentManual = equipmentManualRepository.findById(EQUIPMENT);

        if (equipmentManual.isPresent()) {
        	try {
        		// returns the manual if it exists line 201
				return ResponseEntity.ok()
				        .contentType(MediaType.parseMediaType(equipmentManual.get().getOperatingManualFileType()))
				        .header(HttpHeaders.CONTENT_DISPOSITION, "attachment; filename=\"" + equipmentManual.get().getOperatingManualFileName() + "\"")
				        .body(new ByteArrayResource(equipmentManual.get().getOperatingManual().getBytes(1, (int) equipmentManual.get().getOperatingManual().length())));
			} catch (SQLException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
        }
        //2. ***
        return ResponseEntity.notFound()
        		.build();
    }
	
	/*
	 * The create method: 
	 * 1. This endpoint presents the create html template 
	 */
	
	@GetMapping("/create")
	public String create(ArresterWithVariableResistance arresterWithVariableResistance, Model model)
	{
		model.addAttribute("equipment", EQUIPMENT);
		model.addAttribute("equipmentObject", new ArresterWithVariableResistance());
		return EQUIPMENT + "/" + EQUIPMENT + "-create";
	}
	
	/*
	 * The create-save method:
	 * 1. This endpoint processes the form from the equipment's html create template
	 */

	@PostMapping("/create-save")
	// @ModelAttribute("equipmentObject") connects the create-save method to the create html page of the equipment
	public String createSave(@Validated @ModelAttribute("equipmentObject") ArresterWithVariableResistance arresterWithVariableResistance, BindingResult result, Model model)
	{
		if(result.hasErrors())
		{
			// without line 144 we don't have the equipment object in the request, since we need it 
			model.addAttribute("equipmentObject", arresterWithVariableResistance);
			return EQUIPMENT + "/" + EQUIPMENT + "-create";
		}
		
		// update the time stamp before saving the record, which avoids concurrency issues within the application
		arresterWithVariableResistance.setUpdateTimeStamp(LocalDateTime.now());		
		arresterWithVariableResistanceRepository.save(arresterWithVariableResistance);
		// line 248 prevents double submissions, otherwise known as the prg problem, the post-redirect-get problem
		return "redirect:read";
	}
	
	/*
	 * The read method:
	 * 1. This endpoint actually loads all arresters
	 * 2. The method also returns the read html template
	 */
	
	@GetMapping("/read")
	public String read(Model model) 
	{
		model.addAttribute("equipment", EQUIPMENT);
		List<ArresterWithVariableResistance> arrestersWithVariableResistance = arresterWithVariableResistanceRepository.findAll();
		
		// same concept as in the search method
		// only set the model attribute if at least one row is found
		if (arrestersWithVariableResistance.size() != 0) 
		{
			System.out.println(arrestersWithVariableResistance); 
			model.addAttribute("equipmentObjects", arrestersWithVariableResistance);
		}
		// this is required to populate the drop-down for the search method
		model.addAttribute("searchFields", getHeadingsWithFields().entrySet());
		return EQUIPMENT + "/" + EQUIPMENT + "-read";
	}
	
	/*
	 * The edit endpoint/method:
	 * 1. This endpoint uses the path variable for the id of the equipment to allow editing
	 * 2. It loads the record for the specific id
	 * 3. It returns the update html template 
	 */
	
	@GetMapping("/edit/{id}")
	public String edit(@PathVariable("id") Long id, Model model) 
	{
		model.addAttribute("equipment", EQUIPMENT);
		// loading the record
		Optional<ArresterWithVariableResistance> arresterWithVariableResistance = arresterWithVariableResistanceRepository.findById(id);
	    // prepares the model for the template
		model.addAttribute("equipmentObject", arresterWithVariableResistance.get());
	    
		return EQUIPMENT + "/" + EQUIPMENT + "-update";
	}
	
	/*
	 * The update method:
	 * 1. This endpoint uses the path variable for the id of the equipment to allow updating
	 * 2. IMPORTANT: Prevents concurrent update conflicts by loading the equipment using the last update time stamp!!!!
	 * 3. If the last update time stamp does not match the last update time stamp in the form an error is displayed using line 311
	 * 4. IF the form has any errors, they will also be displayed
	 * 5. Otherwise, the time stamp is reset and the record is saved
	 */
	
	/*
	 * The concurrency issue addressed:
	 * 1. What is a concurrency issue? When 2 different users try to update the same data in the vicinity of the same time, but not the same instance in time
	 * 2. The application ensures that a record is only updated after the user has been presented with the freshest data from the database
	 * 3. If the user is viewing outdated data, the application will prevent any updates
	 * 4. ** The last updated time stamp is introduced in every entity as the vehicle to prevent concurrent updates **
	 */
	
	@PostMapping("/update/{id}")
	public String update(@PathVariable("id") Long id, @Validated @ModelAttribute("equipmentObject") ArresterWithVariableResistance arresterWithVariableResistance, BindingResult result, Model model) 
	{
	    // check to see that the user is updating using fresh data
		if (arresterWithVariableResistanceRepository.findByIdAndUpdateTimeStamp(id, arresterWithVariableResistance.getUpdateTimeStamp()).isEmpty()) {
			// if it's empty = not dealing with fresh data
			// reject allows 191 to have errors
			// each time the addError() function gets used, it means there is a global error
			result.addError(new ObjectError("global", "Another user has already updated the data"));
			// message is coming from line 190
			model.addAttribute("globalError", "Another user has already updated the data");
		}
		
	    if(result.hasErrors()) 
	    {
	    	arresterWithVariableResistance.setId(id);
			model.addAttribute("equipmentObject", arresterWithVariableResistance);
			return EQUIPMENT + "/" + EQUIPMENT + "-update";
	    }
	    // update the time stamp
	    arresterWithVariableResistance.setUpdateTimeStamp(LocalDateTime.now());
	    // save the record
	    arresterWithVariableResistanceRepository.save(arresterWithVariableResistance);
	    
		return "redirect:../read";
	}
	
	/*
	 * The delete method:
	 * 1. The endpoint deletes the record whose id matches the path variable
	 * 2. It returns the user to the read html template using a redirect to avoid the double submission problem
	 */
	
	@GetMapping("/delete/{id}")
	public String delete(@PathVariable("id") Long id, Model model) 
	{
		Optional<ArresterWithVariableResistance> arresterWithVariableResistance = arresterWithVariableResistanceRepository.findById(id);
		arresterWithVariableResistanceRepository.delete(arresterWithVariableResistance.get());
		return "redirect:../read";
	}	
	
	/*
	 * The CSV export method:
	 * 1. This method uses APACHE commons csv to export data in csv format compatible with excel
	 * 2. Using a response header, the browser stores the csv file in the user's download folder on the local machine
	 * 3. Using optional request parameters, the csv contents can be filtered by a search field and value
	 */
	
	@GetMapping("/exportToCSV")
	public void exportToCSV(@Param("searchTerm") String searchTerm, @Param("searchField") String searchField, HttpServletResponse response, Model model) throws IOException {
		response.setContentType("text/csv");
		//  how it gets saved
		response.addHeader("Content-Disposition", "attachment; filename = \"" + StringUtils.capitalize(EQUIPMENT) + "s.csv\"");

		// "attachment; filename = \"Separators.csv\"" is the header 
		List<ExportableToCSV> data = new ArrayList<>();
		if (searchField != null && searchTerm != null && !searchTerm.trim().equals("") ) {
			data.addAll(performSearch(searchField, searchTerm));
		} else {
			data.addAll(arresterWithVariableResistanceRepository.findAll()); // gets every record in the separator table and adds it to the list that gets exported
		}
		csvFileGenerator.write(getHeadings(), data, response.getWriter()); // write to the response object with the header
	}
	// controller handles the headings, entities handle the data
	
	/*
	 * The static getHeadings() method:
	 * 1. The method prepares a String array containing column heading descriptions for each attribute of the equipment
	 */
	
	private static String[] getHeadings() {
		String[] fields = new String[22];
		// fields at position zero
		fields[0] = "Equipment Code";
		fields[1] = "Shape";
		fields[2] = "Material";
		fields[3] = "Certification";
		fields[4] = "Color";
		fields[5] = "Transportation Package";
		fields[6] = "Specification";
		fields[7] = "Trademark";
		fields[8] = "Origin";
		fields[9] = "Production Capacity";
		fields[10] = "Equipment Series";
		fields[11] = "Manufacturing Year";
		fields[12] = "Pif Year";
		fields[13] = "Installation Company";
		fields[14] = "Pif Bulletin";
		fields[15] = "Exploitation Center";
		fields[16] = "Transformation Station";
		fields[17] = "Voltage Level";
		fields[18] = "Assembly Cell";
		fields[19] = "Assembly Phase";
		fields[20] = "Accounting or Warehouse Registration Code";
		fields[21] = "Fixed Asset Component Number";
		return fields;
	}
	
	/*
	 * The static getFieldNames() method:
	 * 1. This prepares the String array containing the attribute names for each attribute of the equipment
	 */
	
	private static String[] getFieldNames() {
		String[] fields = new String[22];
		// fields at position zero
		fields[0] = "equipmentCode";
		fields[1] = "shape";
		fields[2] = "material";
		fields[3] = "certification";
		fields[4] = "color";
		fields[5] = "transportationPackage";
		fields[6] = "specification";
		fields[7] = "tradeMark";
		fields[8] = "origin";
		fields[9] = "productionCapacity";
		fields[10] = "equipmentSeries";
		fields[11] = "manufacturingYear";
		fields[12] = "pifYear";
		fields[13] = "installationCompany";
		fields[14] = "pifBulletin";
		fields[15] = "exploitationCenter";
		fields[16] = "transformationStation";
		fields[17] = "voltageLevel";
		fields[18] = "assemblyCell";
		fields[19] = "assemblyPhase";
		fields[20] = "accountingorWarehouseRegistrationCode";
		fields[21] = "fixedAssetComponentNumber";
		return fields;
	}
	
	/*
	 * The static Map getHeadingsWithFields() method:
	 * 1. This prepares a key-value data structure (map) which associates descriptive column headings with their respective attribute names
	 * 2. It is used for the drop-down function in the search form of the equipment read html template
	 */
	
	private static Map<String, String> getHeadingsWithFields() {
		Map<String, String> map = new TreeMap<>();
		map.put("* Search All Fields", "*");
		String[] headings = getHeadings();
		String[] fieldNames = getFieldNames();
		for (int x = 0; x < headings.length; x++) {
			map.put(headings[x], fieldNames[x]);
		}
		
		return map;
	}

	/*
	 * The exportToPDF method:
	 * 1. This method uses the flying-saucer-pdf project to generate pdf file from a ThymeLeaf template
	 * 2. Using a response header, the browser will store the pdf file to the user's download folder on the local machine
	 */
	
	
	@GetMapping("/exportToPDF")
	public void exportToPDF(HttpServletResponse response, Model model) throws IOException {
		/* String outputFolder = "C:\\Users\\MY PC\\Downloads" + File.arrester + "Arresters.pdf";
		 OutputStream outputStream = new FileOutputStream(outputFolder);
		*/
		
		// getOutputStream() allows the PDF to be opened in the browser rather than a specific location on the server
		OutputStream outputStream = response.getOutputStream();

		response.addHeader("Content-Disposition", "attachment; filename = \"" + StringUtils.capitalize(EQUIPMENT) + "s.pdf\"");
		
	    ITextRenderer renderer = new ITextRenderer();
	    renderer.setDocumentFromString(parseThymeleafTemplate());
	    renderer.layout();
	    renderer.createPDF(outputStream);

	    outputStream.close();
	}
	
	/*
	 * The parseThymeleafTemplate() method:
	 * 1. This is a helper method to locate the pdf ThymeLeaf template and populate it with respective data
	 */
	
	private String parseThymeleafTemplate() {
	    ClassLoaderTemplateResolver templateResolver = new ClassLoaderTemplateResolver();
	    templateResolver.setSuffix(".html");
	    templateResolver.setTemplateMode(TemplateMode.HTML);

	    TemplateEngine templateEngine = new TemplateEngine();
	    templateEngine.setTemplateResolver(templateResolver);
	    
	    // model.addAttribute("equipment", "separator");
		List<ArresterWithVariableResistance> arresters = arresterWithVariableResistanceRepository.findAll();

	    Context context = new Context();
	    context.setVariable("equipmentObjects", arresters);

	    return templateEngine.process("templates/arresterWithVariableResistance/arresterWithVariableResistance-pdf", context);
	}
}