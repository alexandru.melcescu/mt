package ro.upt.ac.mtb.equipments.arresterwithvariableresistance;

import java.util.Arrays;
import java.util.stream.Stream;

import jakarta.persistence.Entity;
import ro.upt.ac.mtb.equipments.electricalequipment.ElectricalEquipment;
import ro.upt.ac.mtb.equipments.services.ExportableToCSV;

// Arrester with variable resistance

@Entity
public class ArresterWithVariableResistance extends ElectricalEquipment implements ExportableToCSV {
	
	// tech sheet
	private String equipmentCode;
	private String shape;
	private String material;
	private String certification;
	private String color;
	private String transportationPackage;
	private String specification;
	private String trademark;
	private String origin;
	private String productionCapacity;
	
	public ArresterWithVariableResistance() {
		
	}

	public String getEquipmentCode() {
		return equipmentCode;
	}

	public void setEquipmentCode(String equipmentCode) {
		this.equipmentCode = equipmentCode;
	}

	public String getShape() {
		return shape;
	}

	public void setShape(String shape) {
		this.shape = shape;
	}

	public String getMaterial() {
		return material;
	}

	public void setMaterial(String material) {
		this.material = material;
	}

	public String getCertification() {
		return certification;
	}

	public void setCertification(String certification) {
		this.certification = certification;
	}

	public String getColor() {
		return color;
	}

	public void setColor(String color) {
		this.color = color;
	}

	public String getTransportationPackage() {
		return transportationPackage;
	}

	public void setTransportationPackage(String transportationPackage) {
		this.transportationPackage = transportationPackage;
	}

	public String getSpecification() {
		return specification;
	}

	public void setSpecification(String specification) {
		this.specification = specification;
	}

	public String getTrademark() {
		return trademark;
	}

	public void setTrademark(String trademark) {
		this.trademark = trademark;
	}

	public String getOrigin() {
		return origin;
	}

	public void setOrigin(String origin) {
		this.origin = origin;
	}

	public String getProductionCapacity() {
		return productionCapacity;
	}

	public void setProductionCapacity(String productionCapacity) {
		this.productionCapacity = productionCapacity;
	}
	
	@Override
	public String[] getFields() {
		String[] fields = new String[10];
		// fields at position zero
		fields[0] = equipmentCode;
		fields[1] = shape;
		fields[2] = material;
		fields[3] = certification;
		fields[4] = color;
		fields[5] = transportationPackage;
//		fields[6] = specification == null ? "" : String.valueOf(specification);
		fields[6] = specification;
		fields[7] = trademark;
		fields[8] = origin;
		fields[9] = productionCapacity;
		// getFields() is defined by the ExportableToCSV interface by overriding the getFields() method
		// line 127 uses a stream to concatenate the specific equipment attributes with those found in the super class, which is the Electrical Equipment class
		return Stream.concat(Arrays.stream(fields), Arrays.stream(super.getFields()))
				.toArray(String[]::new);
	}

}