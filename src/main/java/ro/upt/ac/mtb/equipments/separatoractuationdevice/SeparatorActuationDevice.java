package ro.upt.ac.mtb.equipments.separatoractuationdevice;

import java.util.Arrays;
import java.util.stream.Stream;

import jakarta.persistence.Entity;
import ro.upt.ac.mtb.equipments.electricalequipment.ElectricalEquipment;
import ro.upt.ac.mtb.equipments.services.ExportableToCSV;

// Separator Actuation Device

@Entity
public class SeparatorActuationDevice extends ElectricalEquipment implements ExportableToCSV{
	
	// tech sheet
	private String deviceTypeForMainKnives;
	private String deviceTypeForGroundingKnife;
	private Integer voltageOperationForElectricMotor;
	private Integer limitOperationForElectricMotor;
	private Integer maximumPowerOperationForElectricMotor;
	private Integer voltageControlForMainKnives;
	private Integer limitControlForMainKnives;
	private Integer maximumPowerControlForMainKnives;
	private String modeOfAchievingTheLockingDeviceInPosition;
	private String additionalResistance;
	private String degreeOfProtection;
		
	public SeparatorActuationDevice() {
		
	}

	public String getDeviceTypeForMainKnives() {
		return deviceTypeForMainKnives;
	}

	public void setDeviceTypeForMainKnives(String deviceTypeForMainKnives) {
		this.deviceTypeForMainKnives = deviceTypeForMainKnives;
	}

	public String getDeviceTypeForGroundingKnife() {
		return deviceTypeForGroundingKnife;
	}

	public void setDeviceTypeForGroundingKnife(String deviceTypeForGroundingKnife) {
		this.deviceTypeForGroundingKnife = deviceTypeForGroundingKnife;
	}

	public Integer getVoltageOperationForElectricMotor() {
		return voltageOperationForElectricMotor;
	}

	public void setVoltageOperationForElectricMotor(Integer voltageOperationForElectricMotor) {
		this.voltageOperationForElectricMotor = voltageOperationForElectricMotor;
	}

	public Integer getLimitOperationForElectricMotor() {
		return limitOperationForElectricMotor;
	}

	public void setLimitOperationForElectricMotor(Integer limitOperationForElectricMotor) {
		this.limitOperationForElectricMotor = limitOperationForElectricMotor;
	}

	public Integer getMaximumPowerOperationForElectricMotor() {
		return maximumPowerOperationForElectricMotor;
	}

	public void setMaximumPowerOperationForElectricMotor(Integer maximumPowerOperationForElectricMotor) {
		this.maximumPowerOperationForElectricMotor = maximumPowerOperationForElectricMotor;
	}

	public Integer getVoltageControlForMainKnives() {
		return voltageControlForMainKnives;
	}

	public void setVoltageControlForMainKnives(Integer voltageControlForMainKnives) {
		this.voltageControlForMainKnives = voltageControlForMainKnives;
	}

	public Integer getLimitControlForMainKnives() {
		return limitControlForMainKnives;
	}

	public void setLimitControlForMainKnives(Integer limitControlForMainKnives) {
		this.limitControlForMainKnives = limitControlForMainKnives;
	}

	public Integer getMaximumPowerControlForMainKnives() {
		return maximumPowerControlForMainKnives;
	}

	public void setMaximumPowerControlForMainKnives(Integer maximumPowerControlForMainKnives) {
		this.maximumPowerControlForMainKnives = maximumPowerControlForMainKnives;
	}

	public String getModeOfAchievingTheLockingDeviceInPosition() {
		return modeOfAchievingTheLockingDeviceInPosition;
	}

	public void setModeOfAchievingTheLockingDeviceInPosition(String modeOfAchievingTheLockingDeviceInPosition) {
		this.modeOfAchievingTheLockingDeviceInPosition = modeOfAchievingTheLockingDeviceInPosition;
	}

	public String getAdditionalResistance() {
		return additionalResistance;
	}

	public void setAdditionalResistance(String additionalResistance) {
		this.additionalResistance = additionalResistance;
	}

	public String getDegreeOfProtection() {
		return degreeOfProtection;
	}

	public void setDegreeOfProtection(String degreeOfProtection) {
		this.degreeOfProtection = degreeOfProtection;
	}

	@Override
	public String[] getFields() {
		String[] fields = new String[11];
		// fields at position zero
		fields[0] = deviceTypeForMainKnives;
		fields[1] = deviceTypeForGroundingKnife;
		fields[2] = voltageOperationForElectricMotor == null ? "" : String.valueOf(voltageOperationForElectricMotor);
		fields[3] = limitOperationForElectricMotor == null ? "" : String.valueOf(limitOperationForElectricMotor);
		fields[4] = maximumPowerOperationForElectricMotor == null ? "" : String.valueOf(maximumPowerOperationForElectricMotor);
		fields[5] = voltageControlForMainKnives == null ? "" : String.valueOf(voltageControlForMainKnives);
		fields[6] = limitControlForMainKnives == null ? "" : String.valueOf(limitControlForMainKnives);
		fields[7] = maximumPowerControlForMainKnives == null ? "" : String.valueOf(maximumPowerControlForMainKnives);
		fields[8] = modeOfAchievingTheLockingDeviceInPosition;
		fields[9] = additionalResistance;
		fields[10] = degreeOfProtection;
		return Stream.concat(Arrays.stream(fields), Arrays.stream(super.getFields()))
				.toArray(String[]::new); 
	}	
}