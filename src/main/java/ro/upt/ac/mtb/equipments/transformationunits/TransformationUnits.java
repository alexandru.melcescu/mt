 package ro.upt.ac.mtb.equipments.transformationunits;

import java.util.Arrays;
import java.util.stream.Stream;

import jakarta.persistence.Entity;
import ro.upt.ac.mtb.equipments.electricalequipment.ElectricalEquipment;
import ro.upt.ac.mtb.equipments.services.ExportableToCSV;

// Transformation Units

@Entity
public class TransformationUnits extends ElectricalEquipment implements ExportableToCSV {

	//tech sheet
	private String minimumAirTemperatureValue;
	private String minimumAirTemperatureObservation;
	private String maximumAirTemperatureValue;
	private String maximumAirTemperatureObservation;
	private String maximumAltitudeValue;
	private String maximumAltitudeObservation;
	private String ratedVoltageOfMediumVoltageCircuitsValue;
	private String ratedVoltageOfMediumVoltageCircuitsObservation;
	
	public TransformationUnits() {
	}

	public String getMinimumAirTemperatureValue() {
		return minimumAirTemperatureValue;
	}

	public void setMinimumAirTemperatureValue(String minimumAirTemperatureValue) {
		this.minimumAirTemperatureValue = minimumAirTemperatureValue;
	}

	public String getMinimumAirTemperatureObservation() {
		return minimumAirTemperatureObservation;
	}

	public void setMinimumAirTemperatureObservation(String minimumAirTemperatureObservation) {
		this.minimumAirTemperatureObservation = minimumAirTemperatureObservation;
	}

	public String getMaximumAirTemperatureValue() {
		return maximumAirTemperatureValue;
	}

	public void setMaximumAirTemperatureValue(String maximumAirTemperatureValue) {
		this.maximumAirTemperatureValue = maximumAirTemperatureValue;
	}

	public String getMaximumAirTemperatureObservation() {
		return maximumAirTemperatureObservation;
	}

	public void setMaximumAirTemperatureObservation(String maximumAirTemperatureObservation) {
		this.maximumAirTemperatureObservation = maximumAirTemperatureObservation;
	}

	public String getMaximumAltitudeValue() {
		return maximumAltitudeValue;
	}

	public void setMaximumAltitudeValue(String maximumAltitudeValue) {
		this.maximumAltitudeValue = maximumAltitudeValue;
	}

	public String getMaximumAltitudeObservation() {
		return maximumAltitudeObservation;
	}

	public void setMaximumAltitudeObservation(String maximumAltitudeObservation) {
		this.maximumAltitudeObservation = maximumAltitudeObservation;
	}

	public String getRatedVoltageOfMediumVoltageCircuitsValue() {
		return ratedVoltageOfMediumVoltageCircuitsValue;
	}

	public void setRatedVoltageOfMediumVoltageCircuitsValue(String ratedVoltageOfMediumVoltageCircuitsValue) {
		this.ratedVoltageOfMediumVoltageCircuitsValue = ratedVoltageOfMediumVoltageCircuitsValue;
	}

	public String getRatedVoltageOfMediumVoltageCircuitsObservation() {
		return ratedVoltageOfMediumVoltageCircuitsObservation;
	}

	public void setRatedVoltageOfMediumVoltageCircuitsObservation(String ratedVoltageOfMediumVoltageCircuitsObservation) {
		this.ratedVoltageOfMediumVoltageCircuitsObservation = ratedVoltageOfMediumVoltageCircuitsObservation;
	}

	@Override
	public String[] getFields() {
		String[] fields = new String[8];
		// fields at position zero
		fields[0] = minimumAirTemperatureValue;
		fields[1] = minimumAirTemperatureObservation;
		fields[2] = maximumAirTemperatureValue;
		fields[3] = maximumAirTemperatureObservation;
		fields[4] = maximumAltitudeValue;
		fields[5] = maximumAltitudeObservation;
		fields[6] = ratedVoltageOfMediumVoltageCircuitsValue;
		fields[7] = ratedVoltageOfMediumVoltageCircuitsObservation;
		return Stream.concat(Arrays.stream(fields), Arrays.stream(super.getFields()))
				.toArray(String[]::new);	
	}
}