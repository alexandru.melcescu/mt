package ro.upt.ac.mtb.equipments.electricalequipment;

import jakarta.persistence.Column;
import jakarta.persistence.Entity;
import jakarta.persistence.Id;
import jakarta.persistence.Lob;
import java.sql.Blob;

@Entity
public class EquipmentManual {
	@Id
	private String id;
	
	@Lob
	// allows to change a tiny blob into either medium or long for larger files
	// specifically to the sql database, there is a specific setting that must be modified in order to support large file sizes
	// inside the ini file is where the change must be done manually and reflected in the database
	@Column(columnDefinition = "LONGBLOB")
	private Blob operatingManual;
	private String operatingManualFileName;
	private String operatingManualFileType;

	public Blob getOperatingManual() {
		return operatingManual;
	}

	public void setOperatingManual(Blob operatingManual) {
		this.operatingManual = operatingManual;
	}

	public String getOperatingManualFileName() {
		return operatingManualFileName;
	}

	public void setOperatingManualFileName(String operatingManualFileName) {
		this.operatingManualFileName = operatingManualFileName;
	}

	public String getOperatingManualFileType() {
		return operatingManualFileType;
	}

	public void setOperatingManualFileType(String operatingManualFileType) {
		this.operatingManualFileType = operatingManualFileType;
	}

	public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
	}
}