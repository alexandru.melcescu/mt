package ro.upt.ac.mtb.equipments.separatoractuationdevice;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.Optional;
import java.util.TreeMap;

import org.hibernate.SessionFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.io.ByteArrayResource;
import org.springframework.core.io.Resource;
import org.springframework.data.repository.query.Param;
import org.springframework.http.HttpHeaders;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.validation.ObjectError;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.multipart.MultipartFile;
import org.thymeleaf.TemplateEngine;
import org.thymeleaf.context.Context;
import org.thymeleaf.templatemode.TemplateMode;
import org.thymeleaf.templateresolver.ClassLoaderTemplateResolver;
import org.thymeleaf.util.StringUtils;
import org.xhtmlrenderer.pdf.ITextRenderer;

import jakarta.servlet.http.HttpServletRequest;
import jakarta.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.io.OutputStream;
import java.sql.SQLException;
import java.time.LocalDateTime;
import ro.upt.ac.mtb.equipments.electricalequipment.EquipmentManual;
import ro.upt.ac.mtb.equipments.electricalequipment.EquipmentManualRepository;
import ro.upt.ac.mtb.equipments.services.CSVFileGenerator;
import ro.upt.ac.mtb.equipments.services.ExportableToCSV;

@Controller
@RequestMapping("separatorActuationDevice")
public class SeparatorActuationDeviceController
{
	@Autowired
	SeparatorActuationDeviceRepository separatorActuationDeviceRepository;
	
	@Autowired
	EquipmentManualRepository equipmentManualRepository;
	
	@Autowired
	CSVFileGenerator csvFileGenerator;
	
	@Autowired
	SessionFactory sessionFactory;
	
	private static final String EQUIPMENT = "separatorActuationDevice";
	
	@PostMapping("/search")
	public String search(@Param("searchTerm") String searchTerm, @Param("searchField") String searchField, Model model) {
		//String searchTerm = (String) model.getAttribute("searchTerm");
		List<SeparatorActuationDevice> data = null;
		System.out.println(searchTerm + " " + searchField);
		
		data = performSearch(searchField, searchTerm);
		
		if (data.size() != 0) {
		    model.addAttribute("equipmentObjects", data);
		}
		model.addAttribute("equipment", EQUIPMENT);
		model.addAttribute("searchFields", getHeadingsWithFields().entrySet());
		model.addAttribute("search", Boolean.TRUE);
		model.addAttribute("searchField", searchField);
		model.addAttribute("searchTerm", searchTerm);
		return EQUIPMENT + "/" + EQUIPMENT + "-read";
	}
	
	private List<SeparatorActuationDevice> performSearch(String searchField, String searchTerm) {
		try {
			if (searchField.equalsIgnoreCase("deviceTypeForMainKnives")) {
				// from a string to an Integer
				return separatorActuationDeviceRepository.findByDeviceTypeForMainKnives(searchTerm);
			} else if (searchField.equalsIgnoreCase("deviceTypeForGroundingKnife")) {
				// from a string to an Integer
				return separatorActuationDeviceRepository.findByDeviceTypeForGroundingKnife(searchTerm);
			} else if (searchField.equalsIgnoreCase("voltageOperationForElectricMotor")) {
				// from a string to an Integer
				return separatorActuationDeviceRepository.findByVoltageOperationForElectricMotor(Integer.parseInt(searchTerm));
			} else if (searchField.equalsIgnoreCase("limitOperationForElectricMotor")) {
				// from a string to an Integer
				return separatorActuationDeviceRepository.findByLimitOperationForElectricMotor(Integer.parseInt(searchTerm));
			} else if (searchField.equalsIgnoreCase("maximumPowerOperationForElectricMotor")) {
				// from a string to an Integer
				return separatorActuationDeviceRepository.findByMaximumPowerOperationForElectricMotor(Integer.parseInt(searchTerm));
			} else if (searchField.equalsIgnoreCase("voltageControlForMainKnives")) {
				// from a string to an Integer
				return separatorActuationDeviceRepository.findByVoltageControlForMainKnives(Integer.parseInt(searchTerm));
			} else if (searchField.equalsIgnoreCase("limitControlForMainKnives")) {
				// from a string to an Integer
				return separatorActuationDeviceRepository.findByLimitControlForMainKnives(Integer.parseInt(searchTerm));
			} else if (searchField.equalsIgnoreCase("maximumPowerControlForMainKnives")) {
				// from a string to an Integer
				return separatorActuationDeviceRepository.findByMaximumPowerControlForMainKnives(Integer.parseInt(searchTerm));
			} else if (searchField.equalsIgnoreCase("modeOfAchievingTheLockingDeviceInPosition")) {
				return separatorActuationDeviceRepository.findByModeOfAchievingTheLockingDeviceInPosition(searchTerm);
			} else if (searchField.equalsIgnoreCase("additionalResistance")) {
				return separatorActuationDeviceRepository.findByAdditionalResistance(searchTerm);
			} else if (searchField.equalsIgnoreCase("degreeOfProtection")) {
				return separatorActuationDeviceRepository.findByDegreeOfProtection(searchTerm);
			} else if (searchField.equalsIgnoreCase("equipmentSeries")) {
				return separatorActuationDeviceRepository.findByEquipmentSeries(searchTerm);
			} else if (searchField.equalsIgnoreCase("manufacturingYear")) {
				return separatorActuationDeviceRepository.findByManufacturingYear(Integer.parseInt(searchTerm));
			} else if (searchField.equalsIgnoreCase("pifYear")) {
				return separatorActuationDeviceRepository.findByPifYear(Integer.parseInt(searchTerm));
			} else if (searchField.equalsIgnoreCase("installationCompany")) {
				return separatorActuationDeviceRepository.findByInstallationCompany(searchTerm);
			} else if (searchField.equalsIgnoreCase("pifBulletin")) {
				return separatorActuationDeviceRepository.findByPifBulletin(searchTerm);
			} else if (searchField.equalsIgnoreCase("exploitationCenter")) {
				return separatorActuationDeviceRepository.findByExploitationCenter(searchTerm);
			} else if (searchField.equalsIgnoreCase("transformationCenter")) {
				return separatorActuationDeviceRepository.findByTransformationStation(searchTerm);
			} else if (searchField.equalsIgnoreCase("voltageLevel")) {
				return separatorActuationDeviceRepository.findByVoltageLevel(searchTerm);
			} else if (searchField.equalsIgnoreCase("assemblyCell")) {
				return separatorActuationDeviceRepository.findByAssemblyCell(searchTerm);
			} else if (searchField.equalsIgnoreCase("assemblyPhase")) {
				return separatorActuationDeviceRepository.findByAssemblyPhase(searchTerm);
			} else if (searchField.equalsIgnoreCase("accountingorWarehouseRegistrationCode")) {
				return separatorActuationDeviceRepository.findByAccountingorWarehouseRegistrationCode(searchTerm);
			} else if (searchField.equalsIgnoreCase("fixedAssetComponentNumber")) {
				return separatorActuationDeviceRepository.findByFixedAssetComponentNumber(searchTerm);				
			} else {
				return separatorActuationDeviceRepository.search(searchTerm);
			}		
		} catch (NumberFormatException e) {
			return separatorActuationDeviceRepository.search(searchTerm);
		}
	}

	
	@PostMapping("/uploadFile")
    public String uploadFile(@RequestParam("file") MultipartFile file, Model model) {
        EquipmentManual equipmentManual = new EquipmentManual();

        // setting up the entity of the equipment manual
        equipmentManual.setId(EQUIPMENT);
        equipmentManual.setOperatingManualFileType(file.getContentType());
        equipmentManual.setOperatingManualFileName(file.getOriginalFilename());
        
        try {
			//equipmentManual.setOperatingManual(file.getBytes());
        	
        	// create a blob object so that we can store anything bigger and 255 bytes
        	equipmentManual.setOperatingManual(sessionFactory.openSession().getLobHelper().createBlob(file.getBytes()));
			equipmentManualRepository.save(equipmentManual);
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
        
        return read(model); 
	}
	
	@GetMapping("/downloadFile")
    public ResponseEntity <Resource> downloadFile(HttpServletRequest request) {
        // Load file as Resource
        Optional<EquipmentManual> equipmentManual = equipmentManualRepository.findById(EQUIPMENT);

        if (equipmentManual.isPresent()) {
        	try {
				return ResponseEntity.ok()
				        .contentType(MediaType.parseMediaType(equipmentManual.get().getOperatingManualFileType()))
				        .header(HttpHeaders.CONTENT_DISPOSITION, "attachment; filename=\"" + equipmentManual.get().getOperatingManualFileName() + "\"")
				        .body(new ByteArrayResource(equipmentManual.get().getOperatingManual().getBytes(1, (int) equipmentManual.get().getOperatingManual().length())));
			} catch (SQLException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
        }
        return ResponseEntity.notFound()
        		.build();
    }
	
	
	@GetMapping("/create")
	public String create(SeparatorActuationDevice separatorActuationDevice, Model model)
	{
		model.addAttribute("equipment", EQUIPMENT);
		model.addAttribute("equipmentObject", new SeparatorActuationDevice());
		return EQUIPMENT + "/" + EQUIPMENT + "-create";
	}

	@PostMapping("/create-save")
	// @ModelAttribute("equipmentObject") connects the create-save method to the create html page of the equipment
	public String createSave(@Validated @ModelAttribute("equipmentObject") SeparatorActuationDevice separatorActuationDevice, BindingResult result, Model model)
	{
		if(result.hasErrors())
		{
			// without line 144 we don't have the equipment object in the request, since we need it 
			model.addAttribute("equipmentObject", separatorActuationDevice);
			return EQUIPMENT + "/" + EQUIPMENT + "-create";
		}
		
		// update the time stamp before saving the record
	    separatorActuationDevice.setUpdateTimeStamp(LocalDateTime.now());		
		separatorActuationDeviceRepository.save(separatorActuationDevice);
		return "redirect:read";
	}
	
	@GetMapping("/read")
	public String read(Model model) 
	{
		model.addAttribute("equipment", EQUIPMENT);
		List<SeparatorActuationDevice> separatorActuationDevices = separatorActuationDeviceRepository.findAll();
		if (separatorActuationDevices.size() != 0) 
		{
			System.out.println(separatorActuationDevices); 
			model.addAttribute("equipmentObjects", separatorActuationDevices);
		}
		model.addAttribute("searchFields", getHeadingsWithFields().entrySet());
		return EQUIPMENT + "/" + EQUIPMENT + "-read";
	}
	
	@GetMapping("/edit/{id}")
	public String edit(@PathVariable("id") Long id, Model model) 
	{
		model.addAttribute("equipment", EQUIPMENT);
		Optional<SeparatorActuationDevice> separatorActuationDevice = separatorActuationDeviceRepository.findById(id);
	    model.addAttribute("equipmentObject", separatorActuationDevice.get());
	    
		return EQUIPMENT + "/" + EQUIPMENT + "-update";
	}
	
	@PostMapping("/update/{id}")
	public String update(@PathVariable("id") Long id, @Validated @ModelAttribute("equipmentObject") SeparatorActuationDevice separatorActuationDevice, BindingResult result, Model model) 
	{
	    // check to see that the user is updating using fresh data
		if (separatorActuationDeviceRepository.findByIdAndUpdateTimeStamp(id, separatorActuationDevice.getUpdateTimeStamp()).isEmpty()) {
			// if it's empty = not dealing with fresh data
			// reject allows 191 to have errors
			// each time the addError() function gets used, it means there is a global error
			result.addError(new ObjectError("global", "Another user has already updated the data"));
			// message is coming from line 190
			model.addAttribute("globalError", "Another user has already updated the data");
		}
		
	    if(result.hasErrors()) 
	    {
	    	separatorActuationDevice.setId(id);
			model.addAttribute("equipmentObject", separatorActuationDevice);
			return EQUIPMENT + "/" + EQUIPMENT + "-update";
	    }
	    // update the time stamp
	    separatorActuationDevice.setUpdateTimeStamp(LocalDateTime.now());
	    // save the record
	    separatorActuationDeviceRepository.save(separatorActuationDevice);
	    
		return "redirect:../read";
	}
	
	@GetMapping("/delete/{id}")
	public String delete(@PathVariable("id") Long id, Model model) 
	{
		Optional<SeparatorActuationDevice> separatorActuationDevice = separatorActuationDeviceRepository.findById(id);
	    
		separatorActuationDeviceRepository.delete(separatorActuationDevice.get());
		return "redirect:../read";
	}	
	
	@GetMapping("/exportToCSV")
	public void export(HttpServletResponse response, Model model) throws IOException {
		response.setContentType("text/csv");
		//  how it gets saved
		response.addHeader("Content-Disposition", "attachment; filename = \"" + StringUtils.capitalize(EQUIPMENT) + "s.csv\"");
		// "attachment; filename = \"Separators.csv\"" is the header 
		List<ExportableToCSV> data = new ArrayList<>();
		data.addAll(separatorActuationDeviceRepository.findAll()); // gets every record in the separator table and adds it to the list that gets exported
		csvFileGenerator.write(getHeadings(), data, response.getWriter()); // write to the response object with the header
	}
	// controller handles the headings, entities handle the data
	
	private static String[] getHeadings() {
		String[] fields = new String[23];
		// fields at position zero
		fields[0] = "Device Type For Main Knives";
		fields[1] = "Device Type For Grounding Knife";
		fields[2] = "Voltage Operation For Electric Motor";
		fields[3] = "Limit Operation For Electric Motor";
		fields[4] = "Maximum Power Operation For Electric Motor";
		fields[5] = "Voltage Control For Main Knives";
		fields[6] = "Limit Control For Main Knives";
		fields[7] = "Maximum Power Control For Main Knives";
		fields[8] = "Mode Of Achieving The Locking Device In Position";
		fields[9] = "Additional Resistance";
		fields[10] = "Degree Of Protection";
		fields[11] = "Equipment Series";
		fields[12] = "Manufacturing Year";
		fields[13] = "Pif Year";
		fields[14] = "Installation Company";
		fields[15] = "Pif Bulletin";
		fields[16] = "Exploitation Center";
		fields[17] = "Transformation Station";
		fields[18] = "Voltage Level";
		fields[19] = "Assembly Cell";
		fields[20] = "Assembly Phase";
		fields[21] = "Accounting or Warehouse Registration Code";
		fields[22] = "Fixed Asset Component Number";
		return fields;
	}
	
	private static String[] getFieldNames() {
		String[] fields = new String[23];
		// fields at position zero
		fields[0] = "deviceTypeForMainKnives";
		fields[1] = "deviceTypeForGroundingKnife";
		fields[2] = "voltageOperationForElectricMotor";
		fields[3] = "limitOperationForElectricMotor";
		fields[4] = "maximumPowerOperationForElectricMotor";
		fields[5] = "voltageControlForMainKnives";
		fields[6] = "limitControlForMainKnives";
		fields[7] = "maximumPowerControlForMainKnives";
		fields[8] = "modeOfAchievingTheLockingDeviceInPosition";
		fields[9] = "additionalResistance";
		fields[10] = "degreeOfProtection";
		fields[11] = "equipmentSeries";
		fields[12] = "manufacturingYear";
		fields[13] = "pifYear";
		fields[14] = "installationCompany";
		fields[15] = "pifBulletin";
		fields[16] = "exploitationCenter";
		fields[17] = "transformationStation";
		fields[18] = "voltageLevel";
		fields[19] = "assemblyCell";
		fields[20] = "assemblyPhase";
		fields[21] = "accountingorWarehouseRegistrationCode";
		fields[22] = "fixedAssetComponentNumber";
		return fields;
	}
	
	private static Map<String, String> getHeadingsWithFields() {
		Map<String, String> map = new TreeMap<>();
		map.put("* Search All Fields", "*");
		String[] headings = getHeadings();
		String[] fieldNames = getFieldNames();
		for (int x = 0; x < headings.length; x++) {
			map.put(headings[x], fieldNames[x]);
		}
		
		return map;
	}

	@GetMapping("/exportToPDF")
	public void exportToPDF(HttpServletResponse response, Model model) throws IOException {
		/* String outputFolder = "C:\\Users\\MY PC\\Downloads" + File.arrester + "Arresters.pdf";
		 OutputStream outputStream = new FileOutputStream(outputFolder);
		*/
		
		// getOutputStream() allows the PDF to be opened in the browser rather than a specific location on the server
		OutputStream outputStream = response.getOutputStream();

		response.addHeader("Content-Disposition", "attachment; filename = \"" + StringUtils.capitalize(EQUIPMENT) + "s.pdf\"");
		
	    ITextRenderer renderer = new ITextRenderer();
	    renderer.setDocumentFromString(parseThymeleafTemplate());
	    renderer.layout();
	    renderer.createPDF(outputStream);

	    outputStream.close();
	}
	
	
	private String parseThymeleafTemplate() {
	    ClassLoaderTemplateResolver templateResolver = new ClassLoaderTemplateResolver();
	    templateResolver.setSuffix(".html");
	    templateResolver.setTemplateMode(TemplateMode.HTML);

	    TemplateEngine templateEngine = new TemplateEngine();
	    templateEngine.setTemplateResolver(templateResolver);
	    
	    // model.addAttribute("equipment", "separator");
		List<SeparatorActuationDevice> separatorActuationDevices = separatorActuationDeviceRepository.findAll();

	    Context context = new Context();
	    context.setVariable("equipmentObjects", separatorActuationDevices);

	    return templateEngine.process("templates/separatorActuationDevice/separatorActuationDevice-pdf", context);
	}
}