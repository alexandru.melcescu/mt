package ro.upt.ac.mtb.equipments.arresterwithvariableresistance;

import java.util.List;
import java.util.Optional;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import java.time.LocalDateTime;

@Repository
public interface ArresterWithVariableResistanceRepository extends JpaRepository<ArresterWithVariableResistance,Long> 
{	
	Optional<ArresterWithVariableResistance> findById(Long id);

	/*
	 * These findBy methods are used for the performSearch() function and require an exact match
	 */
	List<ArresterWithVariableResistance> findByEquipmentCode(String equipmentCode);
	List<ArresterWithVariableResistance> findByShape(String shape);
	List<ArresterWithVariableResistance> findByMaterial(String material);
	List<ArresterWithVariableResistance> findByCertification(String certification);
	List<ArresterWithVariableResistance> findByColor(String color);
	List<ArresterWithVariableResistance> findByTransportationPackage(String transportationPackage);
	List<ArresterWithVariableResistance> findBySpecification(String specification);
	List<ArresterWithVariableResistance> findByTrademark(String trademark);
	List<ArresterWithVariableResistance> findByOrigin(String origin);
	List<ArresterWithVariableResistance> findByProductionCapacity(String productionCapacity);
	
	List<ArresterWithVariableResistance> findByEquipmentSeries(String equipmentSeries);
	List<ArresterWithVariableResistance> findByManufacturingYear(Integer manufacturingYear);
	List<ArresterWithVariableResistance> findByPifYear(Integer pifYear);
	List<ArresterWithVariableResistance> findByInstallationCompany(String installationCompany);
	List<ArresterWithVariableResistance> findByPifBulletin(String pifBulletin);
	List<ArresterWithVariableResistance> findByExploitationCenter(String exploitationCenter);
	List<ArresterWithVariableResistance> findByTransformationStation(String transformationStation);
	List<ArresterWithVariableResistance> findByVoltageLevel(String voltageLevel);
	List<ArresterWithVariableResistance> findByAssemblyCell(String assemblyCell);
	List<ArresterWithVariableResistance> findByAssemblyPhase(String assemblyPhase);
	List<ArresterWithVariableResistance> findByAccountingorWarehouseRegistrationCode(String accountingorWarehouseRegistrationCode);
	List<ArresterWithVariableResistance> findByFixedAssetComponentNumber(String fixedAssetComponentNumber);
	
	/*
	 * This annotation allows for a partial keyword search using all attributes of the equipment
	 */

	@Query("SELECT awvr FROM ArresterWithVariableResistance awvr"
			+ " WHERE awvr.equipmentCode LIKE %?1%"
			+ " OR awvr.shape LIKE %?1%"
			+ " OR awvr.material LIKE %?1%"
			+ " OR awvr.certification LIKE %?1%"
			+ " OR awvr.color LIKE %?1%"
			+ " OR awvr.transportationPackage LIKE %?1%"
			+ " OR awvr.specification LIKE %?1%"
			+ " OR awvr.trademark LIKE %?1%"
			+ " OR awvr.origin LIKE %?1%"
			+ " OR awvr.productionCapacity LIKE %?1%"
			
			+ " OR awvr.equipmentSeries LIKE %?1%"
			+ " OR awvr.manufacturingYear LIKE %?1%"
			+ " OR awvr.pifYear LIKE %?1%"
			+ " OR awvr.installationCompany LIKE %?1%"
			+ " OR awvr.pifBulletin LIKE %?1%"
			+ " OR awvr.exploitationCenter LIKE %?1%"
			+ " OR awvr.transformationStation LIKE %?1%"
			+ " OR awvr.voltageLevel LIKE %?1%"
			+ " OR awvr.assemblyCell LIKE %?1%"
			+ " OR awvr.assemblyPhase LIKE %?1%"
			+ " OR awvr.accountingorWarehouseRegistrationCode LIKE %?1%"
			+ " OR awvr.fixedAssetComponentNumber LIKE %?1%"
			)
	List<ArresterWithVariableResistance> search(String searchTerm);
	
	//returns an equipment object by id and by time stamp
	//used to prevent concurrent update collisions 
	Optional<ArresterWithVariableResistance> findByIdAndUpdateTimeStamp(Long id, LocalDateTime ts);
}
