package ro.upt.ac.mtb.equipments.currentmeasuringtransformer;

import java.util.List;
import java.util.Optional;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import java.time.LocalDateTime;

@Repository
public interface CurrentMeasuringTransformerRepository extends JpaRepository<CurrentMeasuringTransformer,Long> {
	Optional<CurrentMeasuringTransformer> findById(Long id);
	
	List<CurrentMeasuringTransformer> findByEquipmentCode(String equipmentCode);
	List<CurrentMeasuringTransformer> findByNominalVoltage(Double nominalVoltage);
	List<CurrentMeasuringTransformer> findByPrimaryNominalCurrent(Double primaryNominalCurrent);
	List<CurrentMeasuringTransformer> findBySecondaryNominalCurrent1(Double secondaryNominalCurrent1);
	List<CurrentMeasuringTransformer> findBySecondaryNominalCurrent2(Double secondaryNominalCurrent2);
	List<CurrentMeasuringTransformer> findBySecondaryNominalCurrent3(Double secondaryNominalCurrent3);
	List<CurrentMeasuringTransformer> findBySecondaryNominalCurrent4(Double secondaryNominalCurrent4);
	List<CurrentMeasuringTransformer> findBySecondaryNominalCurrent5(Double secondaryNominalCurrent5);
	List<CurrentMeasuringTransformer> findBySecondaryNominalCurrent6(Double secondaryNominalCurrent6);
	List<CurrentMeasuringTransformer> findBySecondaryPower1(Double secondaryPower1);
	List<CurrentMeasuringTransformer> findBySecondaryPower2(Double secondaryPower2);
	List<CurrentMeasuringTransformer> findBySecondaryPower3(Double secondaryPower3);
	List<CurrentMeasuringTransformer> findBySecondaryPower4(Double secondaryPower4);
	List<CurrentMeasuringTransformer> findBySecondaryPower5(Double secondaryPower5);
	List<CurrentMeasuringTransformer> findBySecondaryPower6(Double secondaryPower6);
	List<CurrentMeasuringTransformer> findBySecondaryPrecisionClass1(Double secondaryPrecisionClass1);
	List<CurrentMeasuringTransformer> findBySecondaryPrecisionClass2(Double secondaryPrecisionClass2);
	List<CurrentMeasuringTransformer> findBySecondaryPrecisionClass3(Double secondaryPrecisionClass3);
	List<CurrentMeasuringTransformer> findBySecondaryPrecisionClass4(Double secondaryPrecisionClass4);
	List<CurrentMeasuringTransformer> findBySecondaryPrecisionClass5(Double secondaryPrecisionClass5);
	List<CurrentMeasuringTransformer> findBySecondaryPrecisionClass6(Double secondaryPrecisionClass6);
	List<CurrentMeasuringTransformer> findByOperatingEnvironment(String operatingEnvironment);
	List<CurrentMeasuringTransformer> findByInsulationType(String insulationType);
	List<CurrentMeasuringTransformer> findByOperatingMode(String operatingMode);
	List<CurrentMeasuringTransformer> findByManufacturer(String manufacturer);

	List<CurrentMeasuringTransformer> findByEquipmentSeries(String equipmentSeries);
	List<CurrentMeasuringTransformer> findByManufacturingYear(Integer manufacturingYear);
	List<CurrentMeasuringTransformer> findByPifYear(Integer pifYear);
	List<CurrentMeasuringTransformer> findByInstallationCompany(String installationCompany);
	List<CurrentMeasuringTransformer> findByPifBulletin(String pifBulletin);
	List<CurrentMeasuringTransformer> findByExploitationCenter(String exploitationCenter);
	List<CurrentMeasuringTransformer> findByTransformationStation(String transformationStation);
	List<CurrentMeasuringTransformer> findByVoltageLevel(String voltageLevel);
	List<CurrentMeasuringTransformer> findByAssemblyCell(String assemblyCell);
	List<CurrentMeasuringTransformer> findByAssemblyPhase(String assemblyPhase);
	List<CurrentMeasuringTransformer> findByAccountingorWarehouseRegistrationCode(String accountingorWarehouseRegistrationCode);
	List<CurrentMeasuringTransformer> findByFixedAssetComponentNumber(String fixedAssetComponentNumber);
	
	@Query("SELECT cmt FROM CurrentMeasuringTransformer cmt"
			+ " WHERE cmt.equipmentCode LIKE %?1%"
			+ " OR cmt.nominalVoltage LIKE %?1%"
			+ " OR cmt.primaryNominalCurrent LIKE %?1%"
			+ " OR cmt.secondaryNominalCurrent1 LIKE %?1%"
			+ " OR cmt.secondaryNominalCurrent2 LIKE %?1%"
			+ " OR cmt.secondaryNominalCurrent3 LIKE %?1%"
			+ " OR cmt.secondaryNominalCurrent4 LIKE %?1%"
			+ " OR cmt.secondaryNominalCurrent5 LIKE %?1%"
			+ " OR cmt.secondaryNominalCurrent6 LIKE %?1%"
			+ " OR cmt.secondaryPower1 LIKE %?1%"
			+ " OR cmt.secondaryPower2 LIKE %?1%"
			+ " OR cmt.secondaryPower3 LIKE %?1%"
			+ " OR cmt.secondaryPower4 LIKE %?1%"
			+ " OR cmt.secondaryPower5 LIKE %?1%"
			+ " OR cmt.secondaryPower6 LIKE %?1%"
			+ " OR cmt.secondaryPrecisionClass1 LIKE %?1%"
			+ " OR cmt.secondaryPrecisionClass2 LIKE %?1%"
			+ " OR cmt.secondaryPrecisionClass3 LIKE %?1%"
			+ " OR cmt.secondaryPrecisionClass4 LIKE %?1%"
			+ " OR cmt.secondaryPrecisionClass5 LIKE %?1%"
			+ " OR cmt.secondaryPrecisionClass6 LIKE %?1%"
			+ " OR cmt.operatingEnvironment LIKE %?1%"
			+ " OR cmt.insulationType LIKE %?1%"
			+ " OR cmt.operatingMode LIKE %?1%"
			+ " OR cmt.manufacturer LIKE %?1%"
			
			+ " OR cmt.equipmentSeries LIKE %?1%"
			+ " OR cmt.manufacturingYear LIKE %?1%"
			+ " OR cmt.pifYear LIKE %?1%"
			+ " OR cmt.installationCompany LIKE %?1%"
			+ " OR cmt.pifBulletin LIKE %?1%"
			+ " OR cmt.exploitationCenter LIKE %?1%"
			+ " OR cmt.transformationStation LIKE %?1%"
			+ " OR cmt.voltageLevel LIKE %?1%"
			+ " OR cmt.assemblyCell LIKE %?1%"
			+ " OR cmt.assemblyPhase LIKE %?1%"
			+ " OR cmt.accountingorWarehouseRegistrationCode LIKE %?1%"
			+ " OR cmt.fixedAssetComponentNumber LIKE %?1%"
			)
	List<CurrentMeasuringTransformer> search(String searchTerm);
	
	//returns an equipment object by id and by time stamp
	Optional<CurrentMeasuringTransformer> findByIdAndUpdateTimeStamp(Long id, LocalDateTime ts);
}
