package ro.upt.ac.mtb.equipments.voltagemeasuringtransformer;

import java.util.List;
import java.util.Optional;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import java.time.LocalDateTime;

@Repository
public interface VoltageMeasuringTransformerRepository extends JpaRepository<VoltageMeasuringTransformer,Integer> {
	Optional<VoltageMeasuringTransformer> findById(Long id);
	// combination of SQL + hibernate = HQL Hibernate Query Language
	// ?1 refers to the argument provided 
	
	List<VoltageMeasuringTransformer> findByMaximumNetworkVoltageUnitOfMeasure(String maximumNetworkVoltageUnitOfMeasure);
	List<VoltageMeasuringTransformer> findByMaximumNetworkVoltageRequired(String maximumNetworkVoltageRequired);
	List<VoltageMeasuringTransformer> findByMaximumNetworkVoltageOffered(String maximumNetworkVoltageOffered);
	List<VoltageMeasuringTransformer> findByRatedVoltageOfThePrimaryWindingUnitOfMeasure(String ratedVoltageOfThePrimaryWindingUnitOfMeasure);
	List<VoltageMeasuringTransformer> findByRatedVoltageOfThePrimaryWindingRequired(String ratedVoltageOfThePrimaryWindingRequired);
	List<VoltageMeasuringTransformer> findByRatedVoltageOfThePrimaryWindingOffered(String ratedVoltageOfThePrimaryWindingOffered);
	
	List<VoltageMeasuringTransformer> findByEquipmentSeries(String equipmentSeries);
	List<VoltageMeasuringTransformer> findByManufacturingYear(Integer manufacturingYear);
	List<VoltageMeasuringTransformer> findByPifYear(Integer pifYear);
	List<VoltageMeasuringTransformer> findByInstallationCompany(String installationCompany);
	List<VoltageMeasuringTransformer> findByPifBulletin(String pifBulletin);
	List<VoltageMeasuringTransformer> findByExploitationCenter(String exploitationCenter);
	List<VoltageMeasuringTransformer> findByTransformationStation(String transformationStation);
	List<VoltageMeasuringTransformer> findByVoltageLevel(String voltageLevel);
	List<VoltageMeasuringTransformer> findByAssemblyCell(String assemblyCell);
	List<VoltageMeasuringTransformer> findByAssemblyPhase(String assemblyPhase);
	List<VoltageMeasuringTransformer> findByAccountingorWarehouseRegistrationCode(String accountingorWarehouseRegistrationCode);
	List<VoltageMeasuringTransformer> findByFixedAssetComponentNumber(String fixedAssetComponentNumber);

	
	@Query("SELECT vmt FROM VoltageMeasuringTransformer vmt"
			+ " WHERE vmt.maximumNetworkVoltageUnitOfMeasure LIKE %?1%"
			+ " OR vmt.maximumNetworkVoltageRequired LIKE %?1%"
			+ " OR vmt.maximumNetworkVoltageOffered LIKE %?1%"
			+ " OR vmt.ratedVoltageOfThePrimaryWindingUnitOfMeasure LIKE %?1%"
			+ " OR vmt.ratedVoltageOfThePrimaryWindingRequired LIKE %?1%"
			+ " OR vmt.ratedVoltageOfThePrimaryWindingOffered LIKE %?1%"
			
			+ " OR vmt.equipmentSeries LIKE %?1%"
			+ " OR vmt.manufacturingYear LIKE %?1%"
			+ " OR vmt.pifYear LIKE %?1%"
			+ " OR vmt.installationCompany LIKE %?1%"
			+ " OR vmt.pifBulletin LIKE %?1%"
			+ " OR vmt.exploitationCenter LIKE %?1%"
			+ " OR vmt.transformationStation LIKE %?1%"
			+ " OR vmt.voltageLevel LIKE %?1%"
			+ " OR vmt.assemblyCell LIKE %?1%"
			+ " OR vmt.assemblyPhase LIKE %?1%"
			+ " OR vmt.accountingorWarehouseRegistrationCode LIKE %?1%"
			+ " OR vmt.fixedAssetComponentNumber LIKE %?1%"
			)
	List<VoltageMeasuringTransformer> search(String searchTerm);
	
	//returns an equipment object by id and by time stamp
	Optional<VoltageMeasuringTransformer> findByIdAndUpdateTimeStamp(Long id, LocalDateTime ts);
}
