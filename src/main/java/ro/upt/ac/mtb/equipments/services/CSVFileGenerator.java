package ro.upt.ac.mtb.equipments.services;

import java.util.List;

import org.apache.commons.csv.CSVFormat;
import org.apache.commons.csv.CSVPrinter;
import org.springframework.stereotype.Component;

import java.io.IOException;
import java.io.Writer;

/*
 * This class only contains one method used to prepare equipment attributes in csv form
 * It's also used by the equipment measurements
 */

@Component
public class CSVFileGenerator {
	// write the csv file 
	public void write(String[] headings, List<ExportableToCSV> data, Writer writer) {
		// try with resources
		try(CSVPrinter printer = new CSVPrinter(writer, CSVFormat.DEFAULT.withNullString(""))) {
			printer.printRecord((Object[])headings);
			for (ExportableToCSV ex : data) {
				System.out.println(ex.getFields()[0]);
				System.out.println(ex.getFields()[1]);
				// goes through each object and print the fields in the order created
				// pass any array, but the Object[] array is more specific
				printer.printRecord((Object[])ex.getFields()); 			
			}
		} catch (IOException e) {
			e.printStackTrace();
		}
	}
}
