package ro.upt.ac.mtb.equipments.groundingknifeactuationdevice;

import java.util.List;
import java.util.Optional;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import java.time.LocalDateTime;

@Repository
public interface GroundingKnifeActuationDeviceRepository extends JpaRepository<GroundingKnifeActuationDevice,Long> {
	Optional<GroundingKnifeActuationDevice> findById(Long id);
	
	List<GroundingKnifeActuationDevice> findByBladeMaterial(String bladeMaterial);
	List<GroundingKnifeActuationDevice> findByHandleMaterial(String handleMaterial);
	List<GroundingKnifeActuationDevice> findByBladeLength(Double bladeLength);
	List<GroundingKnifeActuationDevice> findByBladeThickness(Integer bladeThickness);
	List<GroundingKnifeActuationDevice> findByHandleLength(Double handleLength);
	List<GroundingKnifeActuationDevice> findByCurrentCapacity(Double currentCapacity);
	List<GroundingKnifeActuationDevice> findByInsulationResistance(String insulationResistance);
	List<GroundingKnifeActuationDevice> findByOperatingTemperature(String operatingTemperature);
	List<GroundingKnifeActuationDevice> findByHumidityResistance(String humidityResistance);
	
	List<GroundingKnifeActuationDevice> findByEquipmentSeries(String equipmentSeries);
	List<GroundingKnifeActuationDevice> findByManufacturingYear(Integer manufacturingYear);
	List<GroundingKnifeActuationDevice> findByPifYear(Integer pifYear);
	List<GroundingKnifeActuationDevice> findByInstallationCompany(String installationCompany);
	List<GroundingKnifeActuationDevice> findByPifBulletin(String pifBulletin);
	List<GroundingKnifeActuationDevice> findByExploitationCenter(String exploitationCenter);
	List<GroundingKnifeActuationDevice> findByTransformationStation(String transformationStation);
	List<GroundingKnifeActuationDevice> findByVoltageLevel(String voltageLevel);
	List<GroundingKnifeActuationDevice> findByAssemblyCell(String assemblyCell);
	List<GroundingKnifeActuationDevice> findByAssemblyPhase(String assemblyPhase);
	List<GroundingKnifeActuationDevice> findByAccountingorWarehouseRegistrationCode(String accountingorWarehouseRegistrationCode);
	List<GroundingKnifeActuationDevice> findByFixedAssetComponentNumber(String fixedAssetComponentNumber);
	
	@Query("SELECT gkac FROM GroundingKnifeActuationDevice gkac"
			+ " WHERE gkac.bladeMaterial LIKE %?1%"
			+ " OR gkac.handleMaterial LIKE %?1%"
			+ " OR gkac.bladeLength LIKE %?1%"
			+ " OR gkac.bladeThickness LIKE %?1%"
			+ " OR gkac.handleLength LIKE %?1%"
			+ " OR gkac.currentCapacity LIKE %?1%"
			+ " OR gkac.insulationResistance LIKE %?1%"
			+ " OR gkac.operatingTemperature LIKE %?1%"
			+ " OR gkac.humidityResistance LIKE %?1%"
			
			+ " OR gkac.equipmentSeries LIKE %?1%"
			+ " OR gkac.manufacturingYear LIKE %?1%"
			+ " OR gkac.pifYear LIKE %?1%"
			+ " OR gkac.installationCompany LIKE %?1%"
			+ " OR gkac.pifBulletin LIKE %?1%"
			+ " OR gkac.exploitationCenter LIKE %?1%"
			+ " OR gkac.transformationStation LIKE %?1%"
			+ " OR gkac.voltageLevel LIKE %?1%"
			+ " OR gkac.assemblyCell LIKE %?1%"
			+ " OR gkac.assemblyPhase LIKE %?1%"
			+ " OR gkac.accountingorWarehouseRegistrationCode LIKE %?1%"
			+ " OR gkac.fixedAssetComponentNumber LIKE %?1%"
			)
	List<GroundingKnifeActuationDevice> search(String searchTerm);
	
	//returns an equipment object by id and by time stamp
	Optional<GroundingKnifeActuationDevice> findByIdAndUpdateTimeStamp(Long id, LocalDateTime ts);
}
