package ro.upt.ac.mtb.equipments.switchactuationdevice;

import java.util.List;
import java.util.Optional;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import java.time.LocalDateTime;

@Repository
public interface SwitchActuationDeviceRepository extends JpaRepository<SwitchActuationDevice,Integer> {
	Optional<SwitchActuationDevice> findById(Long id);
	
	List<SwitchActuationDevice> findByRatedVoltageUnitOfMeasure(String ratedVoltageUnitOfMeasure);
	List<SwitchActuationDevice> findByRatedVoltageValue(Integer ratedVoltageValue);
	List<SwitchActuationDevice> findByRatedVoltageDataPresentedByTheBinder(String ratedVoltageDataPresentedByTheBinder);
	List<SwitchActuationDevice> findByOperatingVoltageUnitOfMeasure(String operatingVoltageUnitOfMeasure);
	List<SwitchActuationDevice> findByOperatingVoltageValue(String operatingVoltageValue);
	List<SwitchActuationDevice> findByOperatingPresentedByTheBinder(String operatingPresentedByTheBinder);
	List<SwitchActuationDevice> findByRatedCurrentUnitOfMeasure(String ratedCurrentUnitOfMeasure);
	List<SwitchActuationDevice> findByRatedCurrentValue(Integer ratedCurrentValue);
	List<SwitchActuationDevice> findByRatedCurrentDataPresentedByTheBinder(String ratedCurrentDataPresentedByTheBinder);
	List<SwitchActuationDevice> findByWorkingDisconnectingCapacityUnitOfMeasure(String workingDisconnectingCapacityUnitOfMeasure);
	List<SwitchActuationDevice> findByWorkingDisconnectingCapacityValue(Integer workingDisconnectingCapacityValue);
	List<SwitchActuationDevice> findByWorkingDisconnectingCapacityDataPresentedByTheBinder(String workingDisconnectingCapacityDataPresentedByTheBinder);
	List<SwitchActuationDevice> findByMaximumDisconnectingCapacityUnitOfMeasure(String maximumDisconnectingCapacityUnitOfMeasure);
	List<SwitchActuationDevice> findByMaximumDisconnectingCapacityValue(Integer maximumDisconnectingCapacityValue);
	List<SwitchActuationDevice> findByMaximumDisconnectingCapacityDataPresentedByTheBinder(String maximumDisconnectingCapacityDataPresentedByTheBinder);
	List<SwitchActuationDevice> findByProtectionDegreeUnitOfMeasure(String protectionDegreeUnitOfMeasure);
	List<SwitchActuationDevice> findByProtectionDegreeValue(String protectionDegreeValue);
	List<SwitchActuationDevice> findByProtectionDegreeDataPresentedByTheBinder(String protectionDegreeDataPresentedByTheBinder);
	List<SwitchActuationDevice> findByAuxiliarySignalingContactsUnitOfMeasure(String auxiliarySignalingContactsUnitOfMeasure);
	List<SwitchActuationDevice> findByAuxiliarySignalingContactsValue(Integer auxiliarySignalingContactsValue);
	List<SwitchActuationDevice> findByAuxiliarySignalingContactsDataPresentedByTheBinder(String auxiliarySignalingContactsDataPresentedByTheBinder);
	
	List<SwitchActuationDevice> findByEquipmentSeries(String equipmentSeries);
	List<SwitchActuationDevice> findByManufacturingYear(Integer manufacturingYear);
	List<SwitchActuationDevice> findByPifYear(Integer pifYear);
	List<SwitchActuationDevice> findByInstallationCompany(String installationCompany);
	List<SwitchActuationDevice> findByPifBulletin(String pifBulletin);
	List<SwitchActuationDevice> findByExploitationCenter(String exploitationCenter);
	List<SwitchActuationDevice> findByTransformationStation(String transformationStation);
	List<SwitchActuationDevice> findByVoltageLevel(String voltageLevel);
	List<SwitchActuationDevice> findByAssemblyCell(String assemblyCell);
	List<SwitchActuationDevice> findByAssemblyPhase(String assemblyPhase);
	List<SwitchActuationDevice> findByAccountingorWarehouseRegistrationCode(String accountingorWarehouseRegistrationCode);
	List<SwitchActuationDevice> findByFixedAssetComponentNumber(String fixedAssetComponentNumber);
	
	@Query("SELECT swad FROM SwitchActuationDevice swad"
			+ " WHERE swad.ratedVoltageUnitOfMeasure LIKE %?1%"
			+ " OR swad.ratedVoltageValue LIKE %?1%"
			+ " OR swad.ratedVoltageDataPresentedByTheBinder LIKE %?1%"
			+ " OR swad.operatingVoltageUnitOfMeasure LIKE %?1%"
			+ " OR swad.operatingVoltageValue LIKE %?1%"
			+ " OR swad.operatingPresentedByTheBinder LIKE %?1%"
			+ " OR swad.ratedCurrentUnitOfMeasure LIKE %?1%"
			+ " OR swad.ratedCurrentValue LIKE %?1%"
			+ " OR swad.ratedCurrentDataPresentedByTheBinder LIKE %?1%"
			+ " OR swad.workingDisconnectingCapacityUnitOfMeasure LIKE %?1%"
			+ " OR swad.workingDisconnectingCapacityValue LIKE %?1%"
			+ " OR swad.workingDisconnectingCapacityDataPresentedByTheBinder LIKE %?1%"
			+ " OR swad.maximumDisconnectingCapacityUnitOfMeasure LIKE %?1%"
			+ " OR swad.maximumDisconnectingCapacityValue LIKE %?1%"
			+ " OR swad.maximumDisconnectingCapacityDataPresentedByTheBinder LIKE %?1%"
			+ " OR swad.protectionDegreeUnitOfMeasure LIKE %?1%"
			+ " OR swad.protectionDegreeValue LIKE %?1%"
			+ " OR swad.protectionDegreeDataPresentedByTheBinder LIKE %?1%"
			+ " OR swad.auxiliarySignalingContactsUnitOfMeasure LIKE %?1%"
			+ " OR swad.auxiliarySignalingContactsValue LIKE %?1%"
			+ " OR swad.auxiliarySignalingContactsDataPresentedByTheBinder LIKE %?1%"
			
			+ " OR swad.equipmentSeries LIKE %?1%"
			+ " OR swad.manufacturingYear LIKE %?1%"
			+ " OR swad.pifYear LIKE %?1%"
			+ " OR swad.installationCompany LIKE %?1%"
			+ " OR swad.pifBulletin LIKE %?1%"
			+ " OR swad.exploitationCenter LIKE %?1%"
			+ " OR swad.transformationStation LIKE %?1%"
			+ " OR swad.voltageLevel LIKE %?1%"
			+ " OR swad.assemblyCell LIKE %?1%"
			+ " OR swad.assemblyPhase LIKE %?1%"
			+ " OR swad.accountingorWarehouseRegistrationCode LIKE %?1%"
			+ " OR swad.fixedAssetComponentNumber LIKE %?1%"
			)
	List<SwitchActuationDevice> search(String searchTerm);
	
	//returns an equipment object by id and by time stamp
	Optional<SwitchActuationDevice> findByIdAndUpdateTimeStamp(Long id, LocalDateTime ts);
}