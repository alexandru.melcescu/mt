package ro.upt.ac.mtb.equipments.groundingknife;

import java.util.List;
import java.util.Optional;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import java.time.LocalDateTime;

@Repository
public interface GroundingKnifeRepository extends JpaRepository<GroundingKnife,Long> {
	Optional<GroundingKnife> findById(Long id);
	
	List<GroundingKnife> findByEquipmentCode(String equipmentCode);
	List<GroundingKnife> findByInstallationType(String installationType);
	List<GroundingKnife> findByStructureType(String structureType);
	List<GroundingKnife> findByCurrent(Integer current);
	List<GroundingKnife> findByVoltage(Integer voltage);
	List<GroundingKnife> findByFrequency(Integer frequency);
	List<GroundingKnife> findByPhaseDifference(String phaseDifference);
	List<GroundingKnife> findByRelatedShortTimeWithstandCurrent(String relatedShortTimeWithstandCurrent);
	List<GroundingKnife> findByAltitude(String altitude);
	List<GroundingKnife> findByTransportPackage(String transportPackage);
	List<GroundingKnife> findByTradeMark(String tradeMark);
	List<GroundingKnife> findByOrigin(String origin);
	List<GroundingKnife> findByProductionCapacity(String productionCapacity);
	
	List<GroundingKnife> findByEquipmentSeries(String equipmentSeries);
	List<GroundingKnife> findByManufacturingYear(Integer manufacturingYear);
	List<GroundingKnife> findByPifYear(Integer pifYear);
	List<GroundingKnife> findByInstallationCompany(String installationCompany);
	List<GroundingKnife> findByPifBulletin(String pifBulletin);
	List<GroundingKnife> findByExploitationCenter(String exploitationCenter);
	List<GroundingKnife> findByTransformationStation(String transformationStation);
	List<GroundingKnife> findByVoltageLevel(String voltageLevel);
	List<GroundingKnife> findByAssemblyCell(String assemblyCell);
	List<GroundingKnife> findByAssemblyPhase(String assemblyPhase);
	List<GroundingKnife> findByAccountingorWarehouseRegistrationCode(String accountingorWarehouseRegistrationCode);
	List<GroundingKnife> findByFixedAssetComponentNumber(String fixedAssetComponentNumber);
	
	@Query("SELECT gk FROM GroundingKnife gk"
			+ " WHERE gk.equipmentCode LIKE %?1%"
			+ " OR gk.installationType LIKE %?1%"
			+ " OR gk.structureType LIKE %?1%"
			+ " OR gk.current LIKE %?1%"
			+ " OR gk.voltage LIKE %?1%"
			+ " OR gk.frequency LIKE %?1%"
			+ " OR gk.phaseDifference LIKE %?1%"
			+ " OR gk.relatedShortTimeWithstandCurrent LIKE %?1%"
			+ " OR gk.altitude LIKE %?1%"
			+ " OR gk.transportPackage LIKE %?1%"
			+ " OR gk.tradeMark LIKE %?1%"
			+ " OR gk.origin LIKE %?1%"
			+ " OR gk.productionCapacity LIKE %?1%"
			
			+ " OR gk.equipmentSeries LIKE %?1%"
			+ " OR gk.manufacturingYear LIKE %?1%"
			+ " OR gk.pifYear LIKE %?1%"
			+ " OR gk.installationCompany LIKE %?1%"
			+ " OR gk.pifBulletin LIKE %?1%"
			+ " OR gk.exploitationCenter LIKE %?1%"
			+ " OR gk.transformationStation LIKE %?1%"
			+ " OR gk.voltageLevel LIKE %?1%"
			+ " OR gk.assemblyCell LIKE %?1%"
			+ " OR gk.assemblyPhase LIKE %?1%"
			+ " OR gk.accountingorWarehouseRegistrationCode LIKE %?1%"
			+ " OR gk.fixedAssetComponentNumber LIKE %?1%"
			)
	List<GroundingKnife> search(String searchTerm);
	
	//returns an equipment object by id and by time stamp
	Optional<GroundingKnife> findByIdAndUpdateTimeStamp(Long id, LocalDateTime ts);
}
