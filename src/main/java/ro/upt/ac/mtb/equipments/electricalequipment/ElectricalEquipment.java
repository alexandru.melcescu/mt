package ro.upt.ac.mtb.equipments.electricalequipment;

import org.springframework.format.annotation.DateTimeFormat;
import org.springframework.format.annotation.DateTimeFormat.ISO;

import jakarta.persistence.GeneratedValue;
import jakarta.persistence.GenerationType;
import jakarta.persistence.Id;
import jakarta.persistence.MappedSuperclass;
import java.time.LocalDateTime;
import ro.upt.ac.mtb.equipments.services.ExportableToCSV;

@MappedSuperclass
public class ElectricalEquipment implements ExportableToCSV
{
	@Id
    @GeneratedValue(strategy = GenerationType.AUTO)
	private Long id;
	
	
	// when was it the last time it was updated
	// iso format of time allows precision to the millisecond
	@DateTimeFormat(iso = ISO.DATE_TIME)
	private LocalDateTime updateTimeStamp;
	
	// manufacturing data
	private String equipmentSeries;
	// made the modification of int to Integer for manufacturingYear to avoid null issues
	private Integer manufacturingYear;

//	@Lob
//  private byte[] factoryBulletin;
//	private String factoryBulletinName;
//	private String factoryBulletinType;
	
	// assembly elements data
	private Integer pifYear;
	private String installationCompany;
	private String pifBulletin;
	
	// assembly place data
	private String exploitationCenter;
	private String transformationStation;
	private String voltageLevel;
	private String assemblyCell;
	private String assemblyPhase;
	
	// management data
	private String accountingorWarehouseRegistrationCode;
	private String fixedAssetComponentNumber;
	
	public ElectricalEquipment()
	{	
		updateTimeStamp = LocalDateTime.now();
	}

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}
	
	public LocalDateTime getUpdateTimeStamp() {
		return updateTimeStamp;
	}

	public void setUpdateTimeStamp(LocalDateTime updateTimeStamp) {
		this.updateTimeStamp = updateTimeStamp;
	}

	public String getEquipmentSeries() {
		return equipmentSeries;
	}

	public void setEquipmentSeries(String equipmentSeries) {
		this.equipmentSeries = equipmentSeries;
	}

	public Integer getManufacturingYear() {
		return manufacturingYear;
	}

	public void setManufacturingYear(Integer manufacturingYear) {
		this.manufacturingYear = manufacturingYear;
	}

	public Integer getPifYear() {
		return pifYear;
	}

	public void setPifYear(Integer pifYear) {
		this.pifYear = pifYear;
	}

	public String getInstallationCompany() {
		return installationCompany;
	}

	public void setInstallationCompany(String installationCompany) {
		this.installationCompany = installationCompany;
	}

	public String getPifBulletin() {
		return pifBulletin;
	}

	public void setPifBulletin(String pifBulletin) {
		this.pifBulletin = pifBulletin;
	}

	public String getExploitationCenter() {
		return exploitationCenter;
	}

	public void setExploitationCenter(String exploitationCenter) {
		this.exploitationCenter = exploitationCenter;
	}

	public String getTransformationStation() {
		return transformationStation;
	}

	public void setTransformationStation(String transformationStation) {
		this.transformationStation = transformationStation;
	}

	public String getVoltageLevel() {
		return voltageLevel;
	}

	public void setVoltageLevel(String voltageLevel) {
		this.voltageLevel = voltageLevel;
	}

	public String getAssemblyCell() {
		return assemblyCell;
	}

	public void setAssemblyCell(String assemblyCell) {
		this.assemblyCell = assemblyCell;
	}

	public String getAssemblyPhase() {
		return assemblyPhase;
	}

	public void setAssemblyPhase(String assemblyPhase) {
		this.assemblyPhase = assemblyPhase;
	}

	public String getAccountingorWarehouseRegistrationCode() {
		return accountingorWarehouseRegistrationCode;
	}

	public void setAccountingorWarehouseRegistrationCode(String accountingorWarehouseRegistrationCode) {
		this.accountingorWarehouseRegistrationCode = accountingorWarehouseRegistrationCode;
	}

	public String getFixedAssetComponentNumber() {
		return fixedAssetComponentNumber;
	}

	public void setFixedAssetComponentNumber(String fixedAssetComponentNumber) {
		this.fixedAssetComponentNumber = fixedAssetComponentNumber;
	}
	
	@Override
	public String[] getFields() {
		String[] fields = new String[12];
		// fields at position zero
		fields[0] = equipmentSeries;
		// valueOf must return the "null" string
		fields[1] = manufacturingYear == null ? "" : String.valueOf(manufacturingYear);
		fields[2] = pifYear == null ? "" : String.valueOf(pifYear);
		fields[3] = installationCompany;
		fields[4] = pifBulletin;
		fields[5] = exploitationCenter;
		fields[6] = transformationStation;
		fields[7] = voltageLevel;
		fields[8] = assemblyCell;
		fields[9] = assemblyPhase;
		fields[10] = accountingorWarehouseRegistrationCode;
		fields[11] = fixedAssetComponentNumber;
		return fields;
	}

//	public byte[] getFactoryBulletin() {
//		return factoryBulletin;
//	}
//
//	public void setFactoryBulletin(byte[] factoryBulletin) {
//		this.factoryBulletin = factoryBulletin;
//	}
//
//	public String getFactoryBulletinName() {
//		return factoryBulletinName;
//	}
//
//	public void setFactoryBulletinName(String factoryBulletinName) {
//		this.factoryBulletinName = factoryBulletinName;
//	}
//
//	public String getFactoryBulletinType() {
//		return factoryBulletinType;
//	}
//
//	public void setFactoryBulletinType(String factoryBulletinType) {
//		this.factoryBulletinType = factoryBulletinType;
//	}

	
}
