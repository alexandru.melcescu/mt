package ro.upt.ac.mtb.equipments.separator;

import java.util.List;
import java.util.Optional;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import java.time.LocalDateTime;

@Repository
public interface SeparatorRepository extends JpaRepository<Separator,Long> {
	Optional<Separator> findById(Long id);
	
	List<Separator> findByEquipmentCode(String equipmentCode);
	List<Separator> findByType(String type);
	List<Separator> findByManufacturer(String manufacturer);
	List<Separator>	findByNominalVoltage(Double nominalVoltage);
	List<Separator> findByPrimaryCurrent(Double primaryCurrent);
	List<Separator>	findByAmbientEnvironment(String ambientEnvironment);
	List<Separator>	findByInsulationType(String insulationType);
	List<Separator>	findByOperatingMode(String operatingMode);
	
	List<Separator> findByEquipmentSeries(String equipmentSeries);
	List<Separator> findByManufacturingYear(Integer manufacturingYear);
	List<Separator> findByPifYear(Integer pifYear);
	List<Separator> findByInstallationCompany(String installationCompany);
	List<Separator> findByPifBulletin(String pifBulletin);
	List<Separator> findByExploitationCenter(String exploitationCenter);
	List<Separator> findByTransformationStation(String transformationStation);
	List<Separator> findByVoltageLevel(String voltageLevel);
	List<Separator> findByAssemblyCell(String assemblyCell);
	List<Separator> findByAssemblyPhase(String assemblyPhase);
	List<Separator> findByAccountingorWarehouseRegistrationCode(String accountingorWarehouseRegistrationCode);
	List<Separator> findByFixedAssetComponentNumber(String fixedAssetComponentNumber);
	
	@Query("SELECT s FROM Separator s"
			+ " WHERE s.equipmentCode LIKE %?1%"
			+ " OR s.type LIKE %?1%"
			+ " OR s.manufacturer LIKE %?1%"
			+ " OR s.nominalVoltage LIKE %?1%"
			+ " OR s.primaryCurrent LIKE %?1%"
			+ " OR s.ambientEnvironment LIKE %?1%"
			+ " OR s.insulationType LIKE %?1%"
			+ " OR s.operatingMode LIKE %?1%"
			+ " OR s.lastMaintenanceDate LIKE %?1%"
			+ " OR s.nextScheduledMaintenanceDate LIKE %?1%"
			+ " OR s.status LIKE %?1%"
			+ " OR s.maintenancePerformedBy LIKE %?1%"
			+ " OR s.remarks LIKE %?1%"
			
			+ " OR s.equipmentSeries LIKE %?1%"
			+ " OR s.manufacturingYear LIKE %?1%"
			+ " OR s.pifYear LIKE %?1%"
			+ " OR s.installationCompany LIKE %?1%"
			+ " OR s.pifBulletin LIKE %?1%"
			+ " OR s.exploitationCenter LIKE %?1%"
			+ " OR s.transformationStation LIKE %?1%"
			+ " OR s.voltageLevel LIKE %?1%"
			+ " OR s.assemblyCell LIKE %?1%"
			+ " OR s.assemblyPhase LIKE %?1%"
			+ " OR s.accountingorWarehouseRegistrationCode LIKE %?1%"
			+ " OR s.fixedAssetComponentNumber LIKE %?1%"
			)
	List<Separator> search(String searchTerm);
	
	//returns an equipment object by id and by time stamp
	Optional<Separator> findByIdAndUpdateTimeStamp(Long id, LocalDateTime ts);
}
