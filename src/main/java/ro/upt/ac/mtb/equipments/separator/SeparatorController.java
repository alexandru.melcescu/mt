package ro.upt.ac.mtb.equipments.separator;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.Optional;
import java.util.TreeMap;

import org.hibernate.SessionFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.io.ByteArrayResource;
import org.springframework.core.io.Resource;
import org.springframework.data.repository.query.Param;
import org.springframework.http.HttpHeaders;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.validation.ObjectError;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.multipart.MultipartFile;
import org.thymeleaf.TemplateEngine;
import org.thymeleaf.context.Context;
import org.thymeleaf.templatemode.TemplateMode;
import org.thymeleaf.templateresolver.ClassLoaderTemplateResolver;
import org.thymeleaf.util.StringUtils;
import org.xhtmlrenderer.pdf.ITextRenderer;

import jakarta.servlet.http.HttpServletRequest;
import jakarta.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.io.OutputStream;
import java.sql.SQLException;
import java.time.LocalDateTime;
import ro.upt.ac.mtb.equipments.electricalequipment.EquipmentManual;
import ro.upt.ac.mtb.equipments.electricalequipment.EquipmentManualRepository;
import ro.upt.ac.mtb.equipments.services.CSVFileGenerator;
import ro.upt.ac.mtb.equipments.services.ExportableToCSV;

@Controller
@RequestMapping("separator")
public class SeparatorController
{
	@Autowired
	SeparatorRepository separatorRepository;
	
	@Autowired
	EquipmentManualRepository equipmentManualRepository;
	
	@Autowired
	CSVFileGenerator csvFileGenerator;
	
	@Autowired
	SessionFactory sessionFactory;
	
	private static final String EQUIPMENT = "separator";
	
	@PostMapping("/search")
	public String search(@Param("searchTerm") String searchTerm, @Param("searchField") String searchField, Model model) {
		//String searchTerm = (String) model.getAttribute("searchTerm");
		List<Separator> data = null;
		System.out.println(searchTerm + " " + searchField);
		
		data = performSearch(searchField, searchTerm);
		
		if (data.size() != 0) {
		    model.addAttribute("equipmentObjects", data);
		}
		model.addAttribute("equipment", EQUIPMENT);
		model.addAttribute("searchFields", getHeadingsWithFields().entrySet());
		model.addAttribute("search", Boolean.TRUE);
		model.addAttribute("searchField", searchField);
		model.addAttribute("searchTerm", searchTerm);
		return EQUIPMENT + "/" + EQUIPMENT + "-read";
	}
	
	private List<Separator> performSearch(String searchField, String searchTerm) {
		try {
			if (searchField.equalsIgnoreCase("equipmentCode")) {
				return separatorRepository.findByEquipmentCode(searchTerm);
			} else if (searchField.equalsIgnoreCase("type")) {
				return separatorRepository.findByType(searchTerm);
			} else if (searchField.equalsIgnoreCase("manufacturer")) {
				return separatorRepository.findByManufacturer(searchTerm);
			} else if (searchField.equalsIgnoreCase("nominalVoltage")) {
				return separatorRepository.findByNominalVoltage(Double.parseDouble(searchTerm));
			} else if (searchField.equalsIgnoreCase("primaryCurrent")) {
				return separatorRepository.findByPrimaryCurrent(Double.parseDouble(searchTerm));
			} else if (searchField.equalsIgnoreCase("ambientEnvironment")) {
				return separatorRepository.findByAmbientEnvironment(searchTerm);
			} else if (searchField.equalsIgnoreCase("insulationType")) {
				return separatorRepository.findByInsulationType(searchTerm);
			} else if (searchField.equalsIgnoreCase("operatingMode")) {
				return separatorRepository.findByOperatingMode(searchTerm);
			} else if (searchField.equalsIgnoreCase("equipmentSeries")) {
				return separatorRepository.findByEquipmentSeries(searchTerm);
			} else if (searchField.equalsIgnoreCase("manufacturingYear")) {
				return separatorRepository.findByManufacturingYear(Integer.parseInt(searchTerm));
			} else if (searchField.equalsIgnoreCase("pifYear")) {
				return separatorRepository.findByPifYear(Integer.parseInt(searchTerm));
			} else if (searchField.equalsIgnoreCase("installationCompany")) {
				return separatorRepository.findByInstallationCompany(searchTerm);
			} else if (searchField.equalsIgnoreCase("pifBulletin")) {
				return separatorRepository.findByPifBulletin(searchTerm);
			} else if (searchField.equalsIgnoreCase("exploitationCenter")) {
				return separatorRepository.findByExploitationCenter(searchTerm);
			} else if (searchField.equalsIgnoreCase("transformationCenter")) {
				return separatorRepository.findByTransformationStation(searchTerm);
			} else if (searchField.equalsIgnoreCase("voltageLevel")) {
				return separatorRepository.findByVoltageLevel(searchTerm);
			} else if (searchField.equalsIgnoreCase("assemblyCell")) {
				return separatorRepository.findByAssemblyCell(searchTerm);
			} else if (searchField.equalsIgnoreCase("assemblyPhase")) {
				return separatorRepository.findByAssemblyPhase(searchTerm);
			} else if (searchField.equalsIgnoreCase("accountingorWarehouseRegistrationCode")) {
				return separatorRepository.findByAccountingorWarehouseRegistrationCode(searchTerm);
			} else if (searchField.equalsIgnoreCase("fixedAssetComponentNumber")) {
				return separatorRepository.findByFixedAssetComponentNumber(searchTerm);				
			} else {
				return separatorRepository.search(searchTerm);
			}		
		} catch (NumberFormatException e) {
			return separatorRepository.search(searchTerm);
		}
	}

	
	@PostMapping("/uploadFile")
    public String uploadFile(@RequestParam("file") MultipartFile file, Model model) {
        EquipmentManual equipmentManual = new EquipmentManual();

        // setting up the entity of the equipment manual
        equipmentManual.setId(EQUIPMENT);
        equipmentManual.setOperatingManualFileType(file.getContentType());
        equipmentManual.setOperatingManualFileName(file.getOriginalFilename());
        
        try {
			//equipmentManual.setOperatingManual(file.getBytes());
        	
        	// create a blob object so that we can store anything bigger and 255 bytes
        	equipmentManual.setOperatingManual(sessionFactory.openSession().getLobHelper().createBlob(file.getBytes()));
			equipmentManualRepository.save(equipmentManual);
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
        
        return read(model); 
	}
	
	@GetMapping("/downloadFile")
    public ResponseEntity <Resource> downloadFile(HttpServletRequest request) {
        // Load file as Resource
        Optional<EquipmentManual> equipmentManual = equipmentManualRepository.findById(EQUIPMENT);

        if (equipmentManual.isPresent()) {
        	try {
				return ResponseEntity.ok()
				        .contentType(MediaType.parseMediaType(equipmentManual.get().getOperatingManualFileType()))
				        .header(HttpHeaders.CONTENT_DISPOSITION, "attachment; filename=\"" + equipmentManual.get().getOperatingManualFileName() + "\"")
				        .body(new ByteArrayResource(equipmentManual.get().getOperatingManual().getBytes(1, (int) equipmentManual.get().getOperatingManual().length())));
			} catch (SQLException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
        }
        return ResponseEntity.notFound()
        		.build();
    }
	
	@GetMapping("/create")
	public String create(Separator separator, Model model)
	{
		model.addAttribute("equipment", EQUIPMENT);
		model.addAttribute("equipmentObject", new Separator());
		return EQUIPMENT + "/" + EQUIPMENT + "-create";
	}

	@PostMapping("/create-save")
	// @ModelAttribute("equipmentObject") connects the create-save method to the create html page of the equipment
	public String createSave(@Validated @ModelAttribute("equipmentObject") Separator separator, BindingResult result, Model model)
	{
		if(result.hasErrors())
		{
			// without line 144 we don't have the equipment object in the request, since we need it 
			model.addAttribute("equipmentObject", separator);
			return EQUIPMENT + "/" + EQUIPMENT + "-create";
		}
		
		// update the time stamp before saving the record
	    separator.setUpdateTimeStamp(LocalDateTime.now());		
		separatorRepository.save(separator);
		return "redirect:read";
	}
	
	@GetMapping("/read")
	public String read(Model model) 
	{
		model.addAttribute("equipment", EQUIPMENT);
		List<Separator> separators = separatorRepository.findAll();
		if (separators.size() != 0) 
		{
			System.out.println(separators); 
			model.addAttribute("equipmentObjects", separators);
		}
		model.addAttribute("searchFields", getHeadingsWithFields().entrySet());
		return EQUIPMENT + "/" + EQUIPMENT + "-read";
	}
	
	@GetMapping("/edit/{id}")
	public String edit(@PathVariable("id") Long id, Model model) 
	{
		model.addAttribute("equipment", EQUIPMENT);
		Optional<Separator> separator = separatorRepository.findById(id);
	    model.addAttribute("equipmentObject", separator.get());
	    
		return EQUIPMENT + "/" + EQUIPMENT + "-update";
	}
	
	@PostMapping("/update/{id}")
	public String update(@PathVariable("id") Long id, @Validated @ModelAttribute("equipmentObject") Separator separator, BindingResult result, Model model) 
	{
	    // check to see that the user is updating using fresh data
		if (separatorRepository.findByIdAndUpdateTimeStamp(id, separator.getUpdateTimeStamp()).isEmpty()) {
			// if it's empty = not dealing with fresh data
			// reject allows 191 to have errors
			// each time the addError() function gets used, it means there is a global error
			result.addError(new ObjectError("global", "Another user has already updated the data"));
			// message is coming from line 190
			model.addAttribute("globalError", "Another user has already updated the data");
		}
		
	    if(result.hasErrors()) 
	    {
	    	separator.setId(id);
			model.addAttribute("equipmentObject", separator);
			return EQUIPMENT + "/" + EQUIPMENT + "-update";
	    }
	    // update the time stamp
	    separator.setUpdateTimeStamp(LocalDateTime.now());
	    // save the record
	    separatorRepository.save(separator);
	    
		return "redirect:../read";
	}
	
	@GetMapping("/delete/{id}")
	public String delete(@PathVariable("id") Long id, Model model) 
	{
		Optional<Separator> separator = separatorRepository.findById(id);
	    
		separatorRepository.delete(separator.get());
		return "redirect:../read";
	}	
	
	@GetMapping("/exportToCSV")
	public void export(HttpServletResponse response, Model model) throws IOException {
		response.setContentType("text/csv");
		//  how it gets saved
		response.addHeader("Content-Disposition", "attachment; filename = \"" + StringUtils.capitalize(EQUIPMENT) + "s.csv\"");
		// "attachment; filename = \"Separators.csv\"" is the header 
		List<ExportableToCSV> data = new ArrayList<>();
		data.addAll(separatorRepository.findAll()); // gets every record in the separator table and adds it to the list that gets exported
		csvFileGenerator.write(getHeadings(), data, response.getWriter()); // write to the response object with the header
	}
	// controller handles the headings, entities handle the data
	
	private static String[] getHeadings() {
		String[] fields = new String[25];
		// fields at position zero
		fields[0] = "Equipment Code";
		fields[1] = "Type";
		fields[2] = "Manufacturer";
		fields[3] = "Nominal Voltage";
		fields[4] = "Primary Current";
		fields[5] = "Ambient Environment";
		fields[6] = "Insulation Type";
		fields[7] = "Operating Mode";
		fields[8] = "Last Maintenance Date";
		fields[9] = "Next Scheduled Maintenance Date";
		fields[10] = "Status";
		fields[11] = "Maintenance Performed By";
		fields[12] = "Remarks";
		fields[13] = "Equipment Series";
		fields[14] = "Manufacturing Year";
		fields[15] = "Pif Year";
		fields[16] = "Installation Company";
		fields[17] = "Pif Bulletin";
		fields[18] = "Exploitation Center";
		fields[19] = "Transformation Station";
		fields[20] = "Voltage Level";
		fields[21] = "Assembly Cell";
		fields[22] = "Assembly Phase";
		fields[23] = "Accounting or Warehouse Registration Code";
		fields[24] = "Fixed Asset Component Number";
		return fields;
	}

	private static String[] getFieldNames() {
		String[] fields = new String[25];
		// fields at position zero
		fields[0] = "equipmentCode";
		fields[1] = "type";
		fields[2] = "manufacturer";
		fields[3] = "nominalVoltage";
		fields[4] = "primaryCurrent";
		fields[5] = "ambientEnvironment";
		fields[6] = "insulationType";
		fields[7] = "operatingMode";
		fields[8] = "lastMaintenanceDate";
		fields[9] = "nextScheduledMaintenanceDate";
		fields[10] = "status";
		fields[11] = "maintenancePerformedBy";
		fields[12] = "remarks";
		fields[13] = "equipmentSeries";
		fields[14] = "manufacturingYear";
		fields[15] = "pifYear";
		fields[16] = "installationCompany";
		fields[17] = "pifBulletin";
		fields[18] = "exploitationCenter";
		fields[19] = "transformationStation";
		fields[20] = "voltageLevel";
		fields[21] = "assemblyCell";
		fields[22] = "assemblyPhase";
		fields[23] = "accountingorWarehouseRegistrationCode";
		fields[24] = "fixedAssetComponentNumber";
		return fields;
	}
	
	private static Map<String, String> getHeadingsWithFields() {
		Map<String, String> map = new TreeMap<>();
		map.put("* Search All Fields", "*");
		String[] headings = getHeadings();
		String[] fieldNames = getFieldNames();
		for (int x = 0; x < headings.length; x++) {
			map.put(headings[x], fieldNames[x]);
		}
		
		return map;
	}
	
	@GetMapping("/exportToPDF")
	public void exportToPDF(HttpServletResponse response, Model model) throws IOException {
		/* String outputFolder = "C:\\Users\\MY PC\\Downloads" + File.arrester + "Arresters.pdf";
		 OutputStream outputStream = new FileOutputStream(outputFolder);
		*/
		
		// getOutputStream() allows the PDF to be opened in the browser rather than a specific location on the server
		OutputStream outputStream = response.getOutputStream();

		response.addHeader("Content-Disposition", "attachment; filename = \"" + StringUtils.capitalize(EQUIPMENT) + "s.pdf\"");
		
	    ITextRenderer renderer = new ITextRenderer();
	    renderer.setDocumentFromString(parseThymeleafTemplate());
	    renderer.layout();
	    renderer.createPDF(outputStream);

	    outputStream.close();
	}
	
	
	private String parseThymeleafTemplate() {
	    ClassLoaderTemplateResolver templateResolver = new ClassLoaderTemplateResolver();
	    templateResolver.setSuffix(".html");
	    templateResolver.setTemplateMode(TemplateMode.HTML);

	    TemplateEngine templateEngine = new TemplateEngine();
	    templateEngine.setTemplateResolver(templateResolver);
	    
	    // model.addAttribute("equipment", "separator");
		List<Separator> separators = separatorRepository.findAll();

	    Context context = new Context();
	    context.setVariable("equipmentObjects", separators);

	    return templateEngine.process("templates/separator/separator-pdf", context);
	}
}