package ro.upt.ac.mtb.equipments.switchactuationdevice;

import java.util.Arrays;
import java.util.stream.Stream;

import jakarta.persistence.Entity;
import ro.upt.ac.mtb.equipments.electricalequipment.ElectricalEquipment;
import ro.upt.ac.mtb.equipments.services.ExportableToCSV;

// Switch Actuation Device

@Entity
public class SwitchActuationDevice extends ElectricalEquipment implements ExportableToCSV {

	// tech sheet
	private String ratedVoltageUnitOfMeasure;
	private Integer ratedVoltageValue;
	private String ratedVoltageDataPresentedByTheBinder;
	private String operatingVoltageUnitOfMeasure;
	private String operatingVoltageValue;
	private String operatingPresentedByTheBinder;
	private String ratedCurrentUnitOfMeasure;
	private Integer ratedCurrentValue;
	private String ratedCurrentDataPresentedByTheBinder;
	private String workingDisconnectingCapacityUnitOfMeasure;
	private Integer workingDisconnectingCapacityValue;
	private String workingDisconnectingCapacityDataPresentedByTheBinder;
	private String maximumDisconnectingCapacityUnitOfMeasure;
	private Integer maximumDisconnectingCapacityValue;
	private String maximumDisconnectingCapacityDataPresentedByTheBinder;
	private String protectionDegreeUnitOfMeasure;
	private String protectionDegreeValue;
	private String protectionDegreeDataPresentedByTheBinder;
	private String auxiliarySignalingContactsUnitOfMeasure;
	private Integer auxiliarySignalingContactsValue;
	private String auxiliarySignalingContactsDataPresentedByTheBinder;
	
	public SwitchActuationDevice() {
	}

	public String getRatedVoltageUnitOfMeasure() {
		return ratedVoltageUnitOfMeasure;
	}

	public void setRatedVoltageUnitOfMeasure(String ratedVoltageUnitOfMeasure) {
		this.ratedVoltageUnitOfMeasure = ratedVoltageUnitOfMeasure;
	}

	public Integer getRatedVoltageValue() {
		return ratedVoltageValue;
	}

	public void setRatedVoltageValue(Integer ratedVoltageValue) {
		this.ratedVoltageValue = ratedVoltageValue;
	}

	public String getRatedVoltageDataPresentedByTheBinder() {
		return ratedVoltageDataPresentedByTheBinder;
	}

	public void setRatedVoltageDataPresentedByTheBinder(String ratedVoltageDataPresentedByTheBinder) {
		this.ratedVoltageDataPresentedByTheBinder = ratedVoltageDataPresentedByTheBinder;
	}

	public String getOperatingVoltageUnitOfMeasure() {
		return operatingVoltageUnitOfMeasure;
	}

	public void setOperatingVoltageUnitOfMeasure(String operatingVoltageUnitOfMeasure) {
		this.operatingVoltageUnitOfMeasure = operatingVoltageUnitOfMeasure;
	}

	public String getOperatingVoltageValue() {
		return operatingVoltageValue;
	}

	public void setOperatingVoltageValue(String operatingVoltageValue) {
		this.operatingVoltageValue = operatingVoltageValue;
	}

	public String getOperatingPresentedByTheBinder() {
		return operatingPresentedByTheBinder;
	}

	public void setOperatingPresentedByTheBinder(String operatingPresentedByTheBinder) {
		this.operatingPresentedByTheBinder = operatingPresentedByTheBinder;
	}

	public String getRatedCurrentUnitOfMeasure() {
		return ratedCurrentUnitOfMeasure;
	}

	public void setRatedCurrentUnitOfMeasure(String ratedCurrentUnitOfMeasure) {
		this.ratedCurrentUnitOfMeasure = ratedCurrentUnitOfMeasure;
	}

	public Integer getRatedCurrentValue() {
		return ratedCurrentValue;
	}

	public void setRatedCurrentValue(Integer ratedCurrentValue) {
		this.ratedCurrentValue = ratedCurrentValue;
	}

	public String getRatedCurrentDataPresentedByTheBinder() {
		return ratedCurrentDataPresentedByTheBinder;
	}

	public void setRatedCurrentDataPresentedByTheBinder(String ratedCurrentDataPresentedByTheBinder) {
		this.ratedCurrentDataPresentedByTheBinder = ratedCurrentDataPresentedByTheBinder;
	}

	public String getWorkingDisconnectingCapacityUnitOfMeasure() {
		return workingDisconnectingCapacityUnitOfMeasure;
	}

	public void setWorkingDisconnectingCapacityUnitOfMeasure(String workingDisconnectingCapacityUnitOfMeasure) {
		this.workingDisconnectingCapacityUnitOfMeasure = workingDisconnectingCapacityUnitOfMeasure;
	}

	public Integer getWorkingDisconnectingCapacityValue() {
		return workingDisconnectingCapacityValue;
	}

	public void setWorkingDisconnectingCapacityValue(Integer workingDisconnectingCapacityValue) {
		this.workingDisconnectingCapacityValue = workingDisconnectingCapacityValue;
	}

	public String getWorkingDisconnectingCapacityDataPresentedByTheBinder() {
		return workingDisconnectingCapacityDataPresentedByTheBinder;
	}

	public void setWorkingDisconnectingCapacityDataPresentedByTheBinder(
			String workingDisconnectingCapacityDataPresentedByTheBinder) {
		this.workingDisconnectingCapacityDataPresentedByTheBinder = workingDisconnectingCapacityDataPresentedByTheBinder;
	}

	public String getMaximumDisconnectingCapacityUnitOfMeasure() {
		return maximumDisconnectingCapacityUnitOfMeasure;
	}

	public void setMaximumDisconnectingCapacityUnitOfMeasure(String maximumDisconnectingCapacityUnitOfMeasure) {
		this.maximumDisconnectingCapacityUnitOfMeasure = maximumDisconnectingCapacityUnitOfMeasure;
	}

	public Integer getMaximumDisconnectingCapacityValue() {
		return maximumDisconnectingCapacityValue;
	}

	public void setMaximumDisconnectingCapacityValue(Integer maximumDisconnectingCapacityValue) {
		this.maximumDisconnectingCapacityValue = maximumDisconnectingCapacityValue;
	}

	public String getMaximumDisconnectingCapacityDataPresentedByTheBinder() {
		return maximumDisconnectingCapacityDataPresentedByTheBinder;
	}

	public void setMaximumDisconnectingCapacityDataPresentedByTheBinder(
			String maximumDisconnectingCapacityDataPresentedByTheBinder) {
		this.maximumDisconnectingCapacityDataPresentedByTheBinder = maximumDisconnectingCapacityDataPresentedByTheBinder;
	}

	public String getProtectionDegreeUnitOfMeasure() {
		return protectionDegreeUnitOfMeasure;
	}

	public void setProtectionDegreeUnitOfMeasure(String protectionDegreeUnitOfMeasure) {
		this.protectionDegreeUnitOfMeasure = protectionDegreeUnitOfMeasure;
	}

	public String getProtectionDegreeValue() {
		return protectionDegreeValue;
	}

	public void setProtectionDegreeValue(String protectionDegreeValue) {
		this.protectionDegreeValue = protectionDegreeValue;
	}

	public String getProtectionDegreeDataPresentedByTheBinder() {
		return protectionDegreeDataPresentedByTheBinder;
	}

	public void setProtectionDegreeDataPresentedByTheBinder(String protectionDegreeDataPresentedByTheBinder) {
		this.protectionDegreeDataPresentedByTheBinder = protectionDegreeDataPresentedByTheBinder;
	}

	public String getAuxiliarySignalingContactsUnitOfMeasure() {
		return auxiliarySignalingContactsUnitOfMeasure;
	}

	public void setAuxiliarySignalingContactsUnitOfMeasure(String auxiliarySignalingContactsUnitOfMeasure) {
		this.auxiliarySignalingContactsUnitOfMeasure = auxiliarySignalingContactsUnitOfMeasure;
	}

	public Integer getAuxiliarySignalingContactsValue() {
		return auxiliarySignalingContactsValue;
	}

	public void setAuxiliarySignalingContactsValue(Integer auxiliarySignalingContactsValue) {
		this.auxiliarySignalingContactsValue = auxiliarySignalingContactsValue;
	}

	public String getAuxiliarySignalingContactsDataPresentedByTheBinder() {
		return auxiliarySignalingContactsDataPresentedByTheBinder;
	}

	public void setAuxiliarySignalingContactsDataPresentedByTheBinder(
			String auxiliarySignalingContactsDataPresentedByTheBinder) {
		this.auxiliarySignalingContactsDataPresentedByTheBinder = auxiliarySignalingContactsDataPresentedByTheBinder;
	}
	
	@Override
	public String[] getFields() {
		String[] fields = new String[21];
		// fields at position zero
		fields[0] = ratedVoltageUnitOfMeasure;
		fields[1] = ratedVoltageValue == null ? "" : String.valueOf(ratedVoltageValue);
		fields[2] = ratedVoltageDataPresentedByTheBinder;
		fields[3] = operatingVoltageUnitOfMeasure;
		fields[4] = operatingVoltageValue;
		fields[5] = operatingPresentedByTheBinder;
		fields[6] = ratedCurrentUnitOfMeasure;
		fields[7] = ratedCurrentValue == null ? "" : String.valueOf(ratedCurrentValue);
		fields[8] = ratedCurrentDataPresentedByTheBinder;
		fields[9] = workingDisconnectingCapacityUnitOfMeasure;
		fields[10] = workingDisconnectingCapacityValue == null ? "" : String.valueOf(workingDisconnectingCapacityValue);
		fields[11] = workingDisconnectingCapacityDataPresentedByTheBinder;
		fields[12] = maximumDisconnectingCapacityUnitOfMeasure;
		fields[13] = maximumDisconnectingCapacityValue == null ? "" : String.valueOf(maximumDisconnectingCapacityValue);
		fields[14] = maximumDisconnectingCapacityDataPresentedByTheBinder;
		fields[15] = protectionDegreeUnitOfMeasure;
		fields[16] = protectionDegreeValue;
		fields[17] = protectionDegreeDataPresentedByTheBinder;
		fields[18] = auxiliarySignalingContactsUnitOfMeasure;
		fields[19] = auxiliarySignalingContactsValue == null ? "" : String.valueOf(auxiliarySignalingContactsValue);
		fields[20] = auxiliarySignalingContactsDataPresentedByTheBinder;
		return Stream.concat(Arrays.stream(fields), Arrays.stream(super.getFields()))
				.toArray(String[]::new);
	}
}