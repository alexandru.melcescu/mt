package ro.upt.ac.mtb.equipments.services;

/*
 * This interface allows equipments and their measurements to provide their data attributes for exporting to csv
 */

public interface ExportableToCSV {
	String[] getFields(); // fields that get exported
}
