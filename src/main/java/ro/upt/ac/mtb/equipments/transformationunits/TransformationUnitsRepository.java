package ro.upt.ac.mtb.equipments.transformationunits;

import java.util.List;
import java.util.Optional;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import java.time.LocalDateTime;

@Repository
public interface TransformationUnitsRepository extends JpaRepository<TransformationUnits,Integer> {
	Optional<TransformationUnits> findById(Long id);
	
	List<TransformationUnits> findByMinimumAirTemperatureValue(String minimumAirTemperatureValue);
	List<TransformationUnits> findByMinimumAirTemperatureObservation(String minimumAirTemperatureObservation);
	List<TransformationUnits> findByMaximumAirTemperatureValue(String maximumAirTemperatureValue);
	List<TransformationUnits> findByMaximumAirTemperatureObservation(String maximumAirTemperatureObservation);
	List<TransformationUnits> findByMaximumAltitudeValue(String maximumAltitudeValue);
	List<TransformationUnits> findByMaximumAltitudeObservation(String maximumAltitudeObservation);
	List<TransformationUnits> findByRatedVoltageOfMediumVoltageCircuitsValue(String ratedVoltageOfMediumVoltageCircuitsValue);
	List<TransformationUnits> findByRatedVoltageOfMediumVoltageCircuitsObservation(String ratedVoltageOfMediumVoltageCircuitsObservation);
	
	List<TransformationUnits> findByEquipmentSeries(String equipmentSeries);
	List<TransformationUnits> findByManufacturingYear(Integer manufacturingYear);
	List<TransformationUnits> findByPifYear(Integer pifYear);
	List<TransformationUnits> findByInstallationCompany(String installationCompany);
	List<TransformationUnits> findByPifBulletin(String pifBulletin);
	List<TransformationUnits> findByExploitationCenter(String exploitationCenter);
	List<TransformationUnits> findByTransformationStation(String transformationStation);
	List<TransformationUnits> findByVoltageLevel(String voltageLevel);
	List<TransformationUnits> findByAssemblyCell(String assemblyCell);
	List<TransformationUnits> findByAssemblyPhase(String assemblyPhase);
	List<TransformationUnits> findByAccountingorWarehouseRegistrationCode(String accountingorWarehouseRegistrationCode);
	List<TransformationUnits> findByFixedAssetComponentNumber(String fixedAssetComponentNumber);
	
	@Query("SELECT tu FROM TransformationUnits tu"
			+ " WHERE tu.minimumAirTemperatureValue LIKE %?1%"
			+ " OR tu.minimumAirTemperatureObservation LIKE %?1%"
			+ " OR tu.maximumAirTemperatureValue LIKE %?1%"
			+ " OR tu.maximumAirTemperatureObservation LIKE %?1%"
			+ " OR tu.maximumAltitudeValue LIKE %?1%"
			+ " OR tu.maximumAltitudeObservation LIKE %?1%"
			+ " OR tu.ratedVoltageOfMediumVoltageCircuitsValue LIKE %?1%"
			+ " OR tu.ratedVoltageOfMediumVoltageCircuitsObservation LIKE %?1%"
			
			+ " OR tu.equipmentSeries LIKE %?1%"
			+ " OR tu.manufacturingYear LIKE %?1%"
			+ " OR tu.pifYear LIKE %?1%"
			+ " OR tu.installationCompany LIKE %?1%"
			+ " OR tu.pifBulletin LIKE %?1%"
			+ " OR tu.exploitationCenter LIKE %?1%"
			+ " OR tu.transformationStation LIKE %?1%"
			+ " OR tu.voltageLevel LIKE %?1%"
			+ " OR tu.assemblyCell LIKE %?1%"
			+ " OR tu.assemblyPhase LIKE %?1%"
			+ " OR tu.accountingorWarehouseRegistrationCode LIKE %?1%"
			+ " OR tu.fixedAssetComponentNumber LIKE %?1%"
			)
	List<TransformationUnits> search(String searchTerm);
	
	//returns an equipment object by id and by time stamp
	Optional<TransformationUnits> findByIdAndUpdateTimeStamp(Long id, LocalDateTime ts);
}
