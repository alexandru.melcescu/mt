package ro.upt.ac.mtb.measurements.switchequipment;

import java.util.List;
import java.util.Optional;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import ro.upt.ac.mtb.equipments.switchequipment.SwitchEquipment;

@Repository
public interface SwitchEquipmentMeasurementRepository extends JpaRepository<SwitchEquipmentMeasurement,Long> {
	Optional<SwitchEquipmentMeasurement> findById(Long id);
	
	@Query("SELECT swm FROM SwitchEquipmentMeasurement swm"
			+ " WHERE swm.timeStamp LIKE %?1%"
			+ " OR swm.switchHeightOne LIKE %?1%"
			+ " OR swm.switchHeightTwo LIKE %?1%"
			+ " OR swm.switchBaseHeight LIKE %?1%"
			)
	List<SwitchEquipmentMeasurement> search(String searchTerm);

	List<SwitchEquipmentMeasurement> findBySwitchEquipmentOrderByTimeStamp(SwitchEquipment switchEquipment);
}
