package ro.upt.ac.mtb.measurements.switchactuationdevice;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.thymeleaf.TemplateEngine;
import org.thymeleaf.context.Context;
import org.thymeleaf.templatemode.TemplateMode;
import org.thymeleaf.templateresolver.ClassLoaderTemplateResolver;
import org.xhtmlrenderer.pdf.ITextRenderer;

import jakarta.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.io.OutputStream;
import ro.upt.ac.mtb.equipments.services.CSVFileGenerator;
import ro.upt.ac.mtb.equipments.services.ExportableToCSV;
import ro.upt.ac.mtb.equipments.switchactuationdevice.SwitchActuationDevice;
import ro.upt.ac.mtb.equipments.switchactuationdevice.SwitchActuationDeviceRepository;

@Controller
@RequestMapping("switchActuationDeviceMeasurement")
public class SwitchActuationDeviceMeasurementController
{
	@Autowired
	SwitchActuationDeviceRepository switchActuationDeviceRepository;

	@Autowired
	SwitchActuationDeviceMeasurementRepository switchActuationDeviceMeasurementRepository;
	
	@Autowired
	CSVFileGenerator csvFileGenerator;
	
	private static final String EQUIPMENT = "switchActuationDeviceMeasurement";
	
	@GetMapping("/graphWeight/{id}") 
	public String graphWeight(@PathVariable("id") Long id, Model model) {
		// load all the measurements for the id
		Optional<SwitchActuationDevice> switchActuationDevice = 
				switchActuationDeviceRepository.findById(id);
		
		List<SwitchActuationDeviceMeasurement> switchActuationDevices = 
				switchActuationDeviceMeasurementRepository.findBySwitchActuationDeviceOrderByTimeStamp(switchActuationDevice.get());
		
		// setting the measurement list in the model
		if(switchActuationDevices.size() != 0) {
			model.addAttribute("equipmentCode", switchActuationDevice.get().getEquipmentSeries());
			model.addAttribute("labels", switchActuationDevices.stream().map(m -> m.getTimeStamp()).collect(Collectors.toList()));
			model.addAttribute("data", switchActuationDevices.stream().map(m -> m.getTotalWidth()).collect(Collectors.toList()));
		}
		return EQUIPMENT + "/" + "graphWeight";
	}
	
	@PostMapping("/search")
	public String search(@Param("searchTerm") String searchTerm, Model model) 
	{
		//String searchTerm = (String) model.getAttribute("searchTerm");
		List<SwitchActuationDeviceMeasurement> data = switchActuationDeviceMeasurementRepository.search(searchTerm);
		if (data.size() != 0) {
		    model.addAttribute("equipmentObjects", data);
		}
		System.out.println(searchTerm);
		model.addAttribute("equipment", EQUIPMENT);
		model.addAttribute("search", Boolean.TRUE);
		return EQUIPMENT + "/" + EQUIPMENT + "-read";
	}
	
	@GetMapping("/create/{pid}")
	public String create(@PathVariable("pid") Long pid,
			SwitchActuationDeviceMeasurement switchActuationDeviceMeasurement, 
			Model model)
	{
		model.addAttribute("equipment", EQUIPMENT);
		model.addAttribute("pid", ""+pid.longValue());
		
		model.addAttribute("equipmentObject", switchActuationDeviceMeasurement);
		
		return EQUIPMENT + "/" + EQUIPMENT + "-create";
	}

	@PostMapping("/create-save/{pid}")
	public String createSave(@PathVariable("pid") Long pid,
			@Validated SwitchActuationDeviceMeasurement switchActuationDeviceMeasurement, 
			BindingResult result, Model model)
	{
		if(result.hasErrors())
		{
			System.out.println(result.toString());
			return EQUIPMENT + "/" + EQUIPMENT + "-create";
		} 
		
		//System.out.println(switchActuationDeviceMeasurement);
		//System.out.println("id "+switchActuationDeviceMeasurement.getId());
		//System.out.println("ts "+switchActuationDeviceMeasurement.getTimeStamp());
		//System.out.println("parent "+switchActuationDeviceMeasurement.getSwitchActuationDevice());
		
		// getting the parent equipment
		Optional<SwitchActuationDevice> switchActuationDevice = 
				switchActuationDeviceRepository.findById(pid);
		// setting the parent
		switchActuationDeviceMeasurement.setSwitchActuationDevice(switchActuationDevice.get());
		// saving the object
		switchActuationDeviceMeasurementRepository.save(switchActuationDeviceMeasurement);
				
	    return read(pid,model);
	}
	
	@GetMapping("/read/{pid}")
	public String read(@PathVariable("pid") Long pid,Model model) 
	{
		model.addAttribute("equipment", EQUIPMENT);
		model.addAttribute("pid", pid);
		
		// getting the parent equipment
		Optional<SwitchActuationDevice> switchActuationDevice = 
				switchActuationDeviceRepository.findById(pid);
		
		// getting the measurements of the equipment 
		List<SwitchActuationDeviceMeasurement> arrestersWithVariableResistance = 
				switchActuationDeviceMeasurementRepository.findBySwitchActuationDeviceOrderByTimeStamp(switchActuationDevice.get());
		
		// setting the measurement list in the model
		if(arrestersWithVariableResistance.size() != 0) 
		{
		    model.addAttribute("equipmentObjects", arrestersWithVariableResistance);
		}
		//System.out.println("hello");
		return EQUIPMENT + "/" + EQUIPMENT + "-read";
	}
	
	@GetMapping("/edit/{id}")
	public String edit(@PathVariable("id") Long id, Model model) 
	{
		model.addAttribute("equipment", EQUIPMENT); 
		Optional<SwitchActuationDeviceMeasurement> switchActuationDeviceMeasurement = 
				switchActuationDeviceMeasurementRepository.findById(id);
	    model.addAttribute("equipmentObject", switchActuationDeviceMeasurement.get());
	    
		return EQUIPMENT + "/" + EQUIPMENT + "-update";
	}
	
	@PostMapping("/update/{id}")
	// removed the pid from the path variable because you have to load the measurement so that you can get 
	// its parent so that you can set the parent before the update because the parent can't be null
	public String update(@PathVariable("id") Long id, 
			@Validated SwitchActuationDeviceMeasurement switchActuationDeviceMeasurement, BindingResult result, Model model) 
	{
		Optional<SwitchActuationDeviceMeasurement> switchActuationDeviceMeasurementOld = 
				switchActuationDeviceMeasurementRepository.findById(id);
		
		long pid = 0L;
		// defensive code to prevent 500 error
		if (switchActuationDeviceMeasurementOld.isPresent()) {
			// load the parent object
			pid = switchActuationDeviceMeasurementOld.get().getSwitchActuationDevice().getId();
			// fetched the pre-updated measurement and get the parent id
			// set the parent object before the update otherwise you get field is not nullable
			switchActuationDeviceMeasurement.setSwitchActuationDevice(switchActuationDeviceMeasurementOld.get().getSwitchActuationDevice());
		}
		if(result.hasErrors()) {
	    	switchActuationDeviceMeasurement.setId(id);
			return EQUIPMENT + "/" + EQUIPMENT + "-update";
	    }
	    
	    switchActuationDeviceMeasurementRepository.save(switchActuationDeviceMeasurement);
	    
//	    return read(switchActuationDeviceMeasurementOld.get().getSwitchActuationDevice().getId(),model);
	    return read(pid,model);
	}
	
	@GetMapping("/delete/{id}")
	public String delete(@PathVariable("id") Long id, Model model) 
	{
		Optional<SwitchActuationDeviceMeasurement> switchActuationDeviceMeasurement = 
				switchActuationDeviceMeasurementRepository.findById(id);
		
		Long pid = null;
		
		if(switchActuationDeviceMeasurement.isPresent())
		{
			pid = switchActuationDeviceMeasurement.get().getSwitchActuationDevice().getId();
			switchActuationDeviceMeasurementRepository.delete(switchActuationDeviceMeasurement.get());
		}
	    return read(pid, model);
	}	
	
	@GetMapping("/exportToCSV")
	public void export(HttpServletResponse response, Model model) throws IOException {
		response.setContentType("text/csv");
		//  how it gets saved
		response.addHeader("Content-Disposition", "attachment; filename = \"SwitchActuationDeviceMeasurements.csv\"");
		// "attachment; filename = \"Separators.csv\"" is the header 
		List<ExportableToCSV> data = new ArrayList<>();
		data.addAll(switchActuationDeviceMeasurementRepository.findAll()); // gets every record in the separator table and adds it to the list that gets exported
		csvFileGenerator.write(getHeadings(), data, response.getWriter()); // write to the response object with the header
	}
	// controller handles the headings, entities handle the data
	
	private static String[] getHeadings() {
		String[] fields = new String[5];
		// fields at position zero
		fields[0] = "Id";
		fields[1] = "Parent Id";
		fields[2] = "Time Stamp";
		fields[3] = "Fourty Percent of the Length of the Bottom of the Switch";
		fields[3] = "Width One";
		fields[4] = "Width Two";
		fields[3] = "Initial Temperature";
		fields[4] = "Measured Temperature";
		return fields;
	}

	@GetMapping("/exportToPDF")
	public void exportToPDF(HttpServletResponse response, Model model) throws IOException {
		/* String outputFolder = "C:\\Users\\MY PC\\Downloads" + File.arrester + "Arresters.pdf";
		 OutputStream outputStream = new FileOutputStream(outputFolder);
		*/
		
		// getOutputStream() allows the PDF to be opened in the browser rather than a specific location on the server
		OutputStream outputStream = response.getOutputStream();

		response.addHeader("Content-Disposition", "attachment; filename = \"SwitchActuationDeviceMeasurements.pdf\"");
		
	    ITextRenderer renderer = new ITextRenderer();
	    renderer.setDocumentFromString(parseThymeleafTemplate());
	    renderer.layout();
	    renderer.createPDF(outputStream);

	    outputStream.close();
	}
	
	
	private String parseThymeleafTemplate() {
	    ClassLoaderTemplateResolver templateResolver = new ClassLoaderTemplateResolver();
	    templateResolver.setSuffix(".html");
	    templateResolver.setTemplateMode(TemplateMode.HTML);

	    TemplateEngine templateEngine = new TemplateEngine();
	    templateEngine.setTemplateResolver(templateResolver);
	    
	    // model.addAttribute("equipment", "separator");
		List<SwitchActuationDeviceMeasurement> arresters = switchActuationDeviceMeasurementRepository.findAll();

	    Context context = new Context();
	    context.setVariable("equipmentObjects", arresters);

	    return templateEngine.process("templates/switchActuationDeviceMeasurement/switchActuationDeviceMeasurement-pdf", context);
	}
}