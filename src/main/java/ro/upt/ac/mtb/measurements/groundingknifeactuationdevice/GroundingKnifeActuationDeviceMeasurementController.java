package ro.upt.ac.mtb.measurements.groundingknifeactuationdevice;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.thymeleaf.TemplateEngine;
import org.thymeleaf.context.Context;
import org.thymeleaf.templatemode.TemplateMode;
import org.thymeleaf.templateresolver.ClassLoaderTemplateResolver;
import org.xhtmlrenderer.pdf.ITextRenderer;

import jakarta.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.io.OutputStream;
import ro.upt.ac.mtb.equipments.groundingknifeactuationdevice.GroundingKnifeActuationDevice;
import ro.upt.ac.mtb.equipments.groundingknifeactuationdevice.GroundingKnifeActuationDeviceRepository;
import ro.upt.ac.mtb.equipments.services.CSVFileGenerator;
import ro.upt.ac.mtb.equipments.services.ExportableToCSV;

@Controller
@RequestMapping("groundingKnifeActuationDeviceMeasurement")
public class GroundingKnifeActuationDeviceMeasurementController
{
	@Autowired
	GroundingKnifeActuationDeviceRepository groundingKnifeActuationDeviceRepository;

	@Autowired
	GroundingKnifeActuationDeviceMeasurementRepository groundingKnifeActuationDeviceMeasurementRepository;
	
	@Autowired
	CSVFileGenerator csvFileGenerator;
	
	private static final String EQUIPMENT = "groundingKnifeActuationDeviceMeasurement";
	
	@GetMapping("/graphMeasuredTemperature/{id}") 
	public String graphMeasuredTemperature(@PathVariable("id") Long id, Model model) {
		// load all the measurements for the id
		Optional<GroundingKnifeActuationDevice> groundingKnifeActuationDevice = 
				groundingKnifeActuationDeviceRepository.findById(id);
		
		List<GroundingKnifeActuationDeviceMeasurement> groundingKnifeActuationDevices = 
				groundingKnifeActuationDeviceMeasurementRepository.findByGroundingKnifeActuationDeviceOrderByTimeStamp(groundingKnifeActuationDevice.get());
		
		// setting the measurement list in the model
		if(groundingKnifeActuationDevices.size() != 0) {
			model.addAttribute("equipmentCode", groundingKnifeActuationDevice.get().getEquipmentSeries());
			model.addAttribute("labels", groundingKnifeActuationDevices.stream().map(m -> m.getTimeStamp()).collect(Collectors.toList()));
			model.addAttribute("data", groundingKnifeActuationDevices.stream().map(m -> m.getMeasuredTemperature()).collect(Collectors.toList()));
		}
		return EQUIPMENT + "/" + "graphMeasuredTemperature";
	}
	
	@PostMapping("/search")
	public String search(@Param("searchTerm") String searchTerm, Model model) 
	{
		//String searchTerm = (String) model.getAttribute("searchTerm");
		List<GroundingKnifeActuationDeviceMeasurement> data = groundingKnifeActuationDeviceMeasurementRepository.search(searchTerm);
		if (data.size() != 0) {
		    model.addAttribute("equipmentObjects", data);
		}
		System.out.println(searchTerm);
		model.addAttribute("equipment", EQUIPMENT);
		model.addAttribute("search", Boolean.TRUE);
		return EQUIPMENT + "/" + EQUIPMENT + "-read";
	}
	
	@GetMapping("/create/{pid}")
	public String create(@PathVariable("pid") Long pid,
			GroundingKnifeActuationDeviceMeasurement groundingKnifeActuationDeviceMeasurement, 
			Model model)
	{
		model.addAttribute("equipment", EQUIPMENT);
		model.addAttribute("pid", ""+pid.longValue());
		
		model.addAttribute("equipmentObject", groundingKnifeActuationDeviceMeasurement);
		
		return EQUIPMENT + "/" + EQUIPMENT + "-create";
	}

	@PostMapping("/create-save/{pid}")
	public String createSave(@PathVariable("pid") Long pid,
			@Validated GroundingKnifeActuationDeviceMeasurement groundingKnifeActuationDeviceMeasurement, 
			BindingResult result, Model model)
	{
		if(result.hasErrors())
		{
			System.out.println(result.toString());
			return EQUIPMENT + "/" + EQUIPMENT + "-create";
		} 
		
		//System.out.println(groundingKnifeActuationDeviceMeasurement);
		//System.out.println("id "+groundingKnifeActuationDeviceMeasurement.getId());
		//System.out.println("ts "+groundingKnifeActuationDeviceMeasurement.getTimeStamp());
		//System.out.println("parent "+groundingKnifeActuationDeviceMeasurement.getGroundingKnifeActuationDevice());
		
		// getting the parent equipment
		Optional<GroundingKnifeActuationDevice> groundingKnifeActuationDevice = 
				groundingKnifeActuationDeviceRepository.findById(pid);
		// setting the parent
		groundingKnifeActuationDeviceMeasurement.setGroundingKnifeActuationDevice(groundingKnifeActuationDevice.get());
		// saving the object
		groundingKnifeActuationDeviceMeasurementRepository.save(groundingKnifeActuationDeviceMeasurement);
				
	    return read(pid,model);
	}
	
	@GetMapping("/read/{pid}")
	public String read(@PathVariable("pid") Long pid,Model model) 
	{
		model.addAttribute("equipment", EQUIPMENT);
		model.addAttribute("pid", pid);
		
		// getting the parent equipment
		Optional<GroundingKnifeActuationDevice> groundingKnifeActuationDevice = 
				groundingKnifeActuationDeviceRepository.findById(pid);
		
		// getting the measurements of the equipment 
		List<GroundingKnifeActuationDeviceMeasurement> groundingKnifeActuationDevices = 
				groundingKnifeActuationDeviceMeasurementRepository.findByGroundingKnifeActuationDeviceOrderByTimeStamp(groundingKnifeActuationDevice.get());
		
		// setting the measurement list in the model
		if(groundingKnifeActuationDevices.size() != 0) 
		{
		    model.addAttribute("equipmentObjects", groundingKnifeActuationDevices);
		}
		//System.out.println("hello");
		return EQUIPMENT + "/" + EQUIPMENT + "-read";
	}
	
	@GetMapping("/edit/{id}")
	public String edit(@PathVariable("id") Long id, Model model) 
	{
		model.addAttribute("equipment", EQUIPMENT); 
		Optional<GroundingKnifeActuationDeviceMeasurement> groundingKnifeActuationDeviceMeasurement = 
				groundingKnifeActuationDeviceMeasurementRepository.findById(id);
	    model.addAttribute("equipmentObject", groundingKnifeActuationDeviceMeasurement.get());
	    
		return EQUIPMENT + "/" + EQUIPMENT + "-update";
	}
	
	@PostMapping("/update/{id}")
	// removed the pid from the path variable because you have to load the measurement so that you can get 
	// its parent so that you can set the parent before the update because the parent can't be null
	public String update(@PathVariable("id") Long id, 
			@Validated GroundingKnifeActuationDeviceMeasurement groundingKnifeActuationDeviceMeasurement, BindingResult result, Model model) 
	{
		Optional<GroundingKnifeActuationDeviceMeasurement> groundingKnifeActuationDeviceMeasurementOld = 
				groundingKnifeActuationDeviceMeasurementRepository.findById(id);
		
		long pid = 0L;
		// defensive code to prevent 500 error
		if (groundingKnifeActuationDeviceMeasurementOld.isPresent()) {
			// load the parent object
			pid = groundingKnifeActuationDeviceMeasurementOld.get().getGroundingKnifeActuationDevice().getId();
			// fetched the pre-updated measurement and get the parent id
			// set the parent object before the update otherwise you get field is not nullable
			groundingKnifeActuationDeviceMeasurement.setGroundingKnifeActuationDevice(groundingKnifeActuationDeviceMeasurementOld.get().getGroundingKnifeActuationDevice());
		}
		if(result.hasErrors()) {
	    	groundingKnifeActuationDeviceMeasurement.setId(id);
			return EQUIPMENT + "/" + EQUIPMENT + "-update";
	    }
	    
	    groundingKnifeActuationDeviceMeasurementRepository.save(groundingKnifeActuationDeviceMeasurement);
	    
//	    return read(groundingKnifeActuationDeviceMeasurementOld.get().getGroundingKnifeActuationDevice().getId(),model);
	    return read(pid,model);
	}
	
	@GetMapping("/delete/{id}")
	public String delete(@PathVariable("id") Long id, Model model) 
	{
		Optional<GroundingKnifeActuationDeviceMeasurement> groundingKnifeActuationDeviceMeasurement = 
				groundingKnifeActuationDeviceMeasurementRepository.findById(id);
		
		Long pid = null;
		
		if(groundingKnifeActuationDeviceMeasurement.isPresent())
		{
			pid = groundingKnifeActuationDeviceMeasurement.get().getGroundingKnifeActuationDevice().getId();
			groundingKnifeActuationDeviceMeasurementRepository.delete(groundingKnifeActuationDeviceMeasurement.get());
		}
	    return read(pid, model);
	}	
	
	@GetMapping("/exportToCSV")
	public void export(HttpServletResponse response, Model model) throws IOException {
		response.setContentType("text/csv");
		//  how it gets saved
		response.addHeader("Content-Disposition", "attachment; filename = \"GroundingKnifeActuationDeviceMeasurements.csv\"");
		// "attachment; filename = \"Separators.csv\"" is the header 
		List<ExportableToCSV> data = new ArrayList<>();
		data.addAll(groundingKnifeActuationDeviceMeasurementRepository.findAll()); // gets every record in the separator table and adds it to the list that gets exported
		csvFileGenerator.write(getHeadings(), data, response.getWriter()); // write to the response object with the header
	}
	// controller handles the headings, entities handle the data
	
//	private static String[] getHeadings() {
//		String[] fields = new String[12];
//		// fields at position zero
//		fields[0] = "id";
//		fields[1] = "timeStamp";
//		fields[2] = "equipmentCode";
//		fields[3] = "shape";
//		fields[4] = "material";
//		fields[5] = "certification";
//		fields[6] = "color";
//		fields[7] = "transportationPackage";
//		fields[8] = "specification";
//		fields[9] = "tradeMark";
//		fields[10] = "origin";
//		fields[11] = "productionCapacity";
//		return fields;
//	}
	private static String[] getHeadings() {
		String[] fields = new String[5];
		// fields at position zero
		fields[0] = "Id";
		fields[1] = "Parent Id";
		fields[2] = "Time Stamp";
		fields[3] = "Initial Temperature";
		fields[4] = "Measured Temperature";
		return fields;
	}

	@GetMapping("/exportToPDF")
	public void exportToPDF(HttpServletResponse response, Model model) throws IOException {
		/* String outputFolder = "C:\\Users\\MY PC\\Downloads" + File.arrester + "Arresters.pdf";
		 OutputStream outputStream = new FileOutputStream(outputFolder);
		*/
		
		// getOutputStream() allows the PDF to be opened in the browser rather than a specific location on the server
		OutputStream outputStream = response.getOutputStream();

		response.addHeader("Content-Disposition", "attachment; filename = \"GroundingKnifeActuationDeviceMeasurements.pdf\"");
		
	    ITextRenderer renderer = new ITextRenderer();
	    renderer.setDocumentFromString(parseThymeleafTemplate());
	    renderer.layout();
	    renderer.createPDF(outputStream);

	    outputStream.close();
	}
	
	
	private String parseThymeleafTemplate() {
	    ClassLoaderTemplateResolver templateResolver = new ClassLoaderTemplateResolver();
	    templateResolver.setSuffix(".html");
	    templateResolver.setTemplateMode(TemplateMode.HTML);

	    TemplateEngine templateEngine = new TemplateEngine();
	    templateEngine.setTemplateResolver(templateResolver);
	    
	    // model.addAttribute("equipment", "separator");
		List<GroundingKnifeActuationDeviceMeasurement> groundingKnifeActuationDevices = groundingKnifeActuationDeviceMeasurementRepository.findAll();

	    Context context = new Context();
	    context.setVariable("equipmentObjects", groundingKnifeActuationDevices);

	    return templateEngine.process("templates/groundingKnifeActuationDeviceMeasurement/groundingKnifeActuationDeviceMeasurement-pdf", context);
	}
}