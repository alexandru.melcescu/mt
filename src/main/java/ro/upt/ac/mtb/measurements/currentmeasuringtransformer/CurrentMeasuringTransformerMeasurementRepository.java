package ro.upt.ac.mtb.measurements.currentmeasuringtransformer;

import java.util.List;
import java.util.Optional;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import ro.upt.ac.mtb.equipments.currentmeasuringtransformer.CurrentMeasuringTransformer;

@Repository
public interface CurrentMeasuringTransformerMeasurementRepository extends JpaRepository<CurrentMeasuringTransformerMeasurement,Long> {
	Optional<CurrentMeasuringTransformerMeasurement> findById(Long id);
	
	@Query("SELECT cmtm FROM CurrentMeasuringTransformerMeasurement cmtm"
			+ " WHERE cmtm.timeStamp LIKE %?1%"
			+ " OR cmtm.currentMeasuringTransformerHeightOne LIKE %?1%"
			+ " OR cmtm.currentMeasuringTransformerHeightTwo LIKE %?1%"
			)
	List<CurrentMeasuringTransformerMeasurement> search(String searchTerm);
	List<CurrentMeasuringTransformerMeasurement> findByCurrentMeasuringTransformerOrderByTimeStamp(CurrentMeasuringTransformer currentMeasuringTransformer);
}
