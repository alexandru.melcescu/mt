package ro.upt.ac.mtb.measurements.separator;

import java.util.List;
import java.util.Optional;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import ro.upt.ac.mtb.equipments.separator.Separator;

@Repository
public interface SeparatorMeasurementRepository extends JpaRepository<SeparatorMeasurement,Long> {
	Optional<SeparatorMeasurement> findById(Long id);
	
	@Query("SELECT sm FROM SeparatorMeasurement sm"
			+ " WHERE sm.timeStamp LIKE %?1%"
			+ " OR sm.heightOne LIKE %?1%"
			+ " OR sm.heightTwo LIKE %?1%"
			+ " OR sm.width LIKE %?1%"
			)
	List<SeparatorMeasurement> search(String searchTerm);
	List<SeparatorMeasurement> findBySeparatorOrderByTimeStamp(Separator separator);	

}
