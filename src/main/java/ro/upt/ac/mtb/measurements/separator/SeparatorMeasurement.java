package ro.upt.ac.mtb.measurements.separator;

import java.util.Arrays;
import java.util.stream.Stream;

import org.hibernate.annotations.OnDelete;
import org.hibernate.annotations.OnDeleteAction;
import org.springframework.format.annotation.DateTimeFormat;

import jakarta.persistence.Entity;
import jakarta.persistence.FetchType;
import jakarta.persistence.GeneratedValue;
import jakarta.persistence.GenerationType;
import jakarta.persistence.Id;
import jakarta.persistence.JoinColumn;
import jakarta.persistence.ManyToOne;
import java.time.LocalDateTime;
import ro.upt.ac.mtb.equipments.separator.Separator;
import ro.upt.ac.mtb.equipments.services.ExportableToCSV;

@Entity
public class SeparatorMeasurement implements ExportableToCSV
{
	@Id
    @GeneratedValue(strategy = GenerationType.AUTO)
	private Long id;
	
	//relationship between the arrester with variable resistance measurement table and the arrester with variable resistance table
	@ManyToOne(fetch = FetchType.LAZY, optional = false)
	@JoinColumn(nullable = false)
	@OnDelete(action = OnDeleteAction.CASCADE)	
	private Separator separator;
		
	// TODO
	// shows the current date and time stamp
	@DateTimeFormat(pattern = "yyyy-MM-dd'T'HH:mm")
	private LocalDateTime timeStamp;
	// private Date timeStamp;
	// no way to use java.util.Date for the specific date and time without using deprecated methods or without using 
	// a local date to get a long value
	
	private Double heightOne;
	private Double heightTwo;
	private Double width;
	private Integer initialTemperature;
	private Integer measuredTemperature;
	
	public SeparatorMeasurement()
	{		
		timeStamp = LocalDateTime.now();
	}

	public Long getId() 
	{
		return id;
	}

	public void setId(Long id) 
	{
		this.id = id;
	}

	public Separator getSeparator() 
	{
		return separator;
	}

	public void setSeparator(Separator separator) 
	{
		this.separator = separator;
	}

	public LocalDateTime getTimeStamp() 
	{
		return timeStamp;
	}

	public void setTimeStamp(LocalDateTime timeStamp) 
	{
		this.timeStamp = timeStamp;
	}	
	
	public Double getHeightOne() {
		return heightOne;
	}

	public void setHeightOne(Double heightOne) {
		this.heightOne = heightOne;
	}

	public Double getHeightTwo() {
		return heightTwo;
	}

	public void setHeightTwo(Double heightTwo) {
		this.heightTwo = heightTwo;
	}

	public Double getWidth() {
		return width;
	}

	public void setWidth(Double width) {
		this.width = width;
	}
	
	public Double getTotalHeight() {
		double total =  (getHeightOne() == null ? 0 : getHeightOne()) + 
				(getHeightTwo() == null ? 0 : getHeightTwo());
		if (total == 0) {
			return null;
		} 
		
		return total;
	}

	public Integer getInitialTemperature() {
		return initialTemperature;
	}

	public void setInitialTemperature(Integer initialTemperature) {
		this.initialTemperature = initialTemperature;
	}

	public Integer getMeasuredTemperature() {
		return measuredTemperature;
	}

	public void setMeasuredTemperature(Integer measuredTemperature) {
		this.measuredTemperature = measuredTemperature;
	}

	@Override
	public String[] getFields() {
		String[] fields = new String[8];
		// fields at position zero
		fields[0] = String.valueOf(id);
		fields[1] = separator.getId().toString();
		fields[2] = timeStamp.toString();
		fields[3] = String.valueOf(heightOne);
		fields[4] = String.valueOf(heightOne);
		fields[5] = String.valueOf(width);
		fields[6] = String.valueOf(initialTemperature);
		fields[7] = String.valueOf(measuredTemperature);
		// concatenates 2 Arrays using Streams from arrester and measurement and returns the combined array
		return Stream.concat(Arrays.stream(fields), Arrays.stream(separator.getFields()))
				.toArray(String[]::new);
	}
}
