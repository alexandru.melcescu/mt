package ro.upt.ac.mtb.measurements.compensationcoil;

import java.util.List;
import java.util.Optional;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import ro.upt.ac.mtb.equipments.compensationcoil.CompensationCoil;

@Repository
public interface CompensationCoilMeasurementRepository extends JpaRepository<CompensationCoilMeasurement,Long> {
	Optional<CompensationCoilMeasurement> findById(Long id);
	
	@Query("SELECT ccm FROM CompensationCoilMeasurement ccm"
			+ " WHERE ccm.timeStamp LIKE %?1%"
			+ "OR ccm.cableDimension LIKE %?1%"
			+ "OR ccm.cableLength LIKE %?1%"
			)
	List<CompensationCoilMeasurement> search(String searchTerm);
	
	List<CompensationCoilMeasurement> findByCompensationCoilOrderByTimeStamp(CompensationCoil compensationCoil);	
}




