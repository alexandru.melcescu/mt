package ro.upt.ac.mtb.measurements.voltagemeasuringtransformer;

import java.util.Arrays;
import java.util.stream.Stream;

import org.hibernate.annotations.OnDelete;
import org.hibernate.annotations.OnDeleteAction;
import org.springframework.format.annotation.DateTimeFormat;

import jakarta.persistence.Entity;
import jakarta.persistence.FetchType;
import jakarta.persistence.GeneratedValue;
import jakarta.persistence.GenerationType;
import jakarta.persistence.Id;
import jakarta.persistence.JoinColumn;
import jakarta.persistence.ManyToOne;
import java.time.LocalDateTime;
import ro.upt.ac.mtb.equipments.electricalequipment.ElectricalEquipment;
import ro.upt.ac.mtb.equipments.services.ExportableToCSV;
import ro.upt.ac.mtb.equipments.voltagemeasuringtransformer.VoltageMeasuringTransformer;

@Entity
public class VoltageMeasuringTransformerMeasurement extends ElectricalEquipment implements ExportableToCSV
{
	@Id
    @GeneratedValue(strategy = GenerationType.AUTO)
	private Long id;
	
	//relationship between the arrester with variable resistance measurement table and the arrester with variable resistance table
	@ManyToOne(fetch = FetchType.LAZY, optional = false)
	@JoinColumn(nullable = false)
	@OnDelete(action = OnDeleteAction.CASCADE)	
	private VoltageMeasuringTransformer voltageMeasuringTransformer;
		
	// TODO
	// shows the current date and time stamp
	@DateTimeFormat(pattern = "yyyy-MM-dd'T'HH:mm")
	private LocalDateTime timeStamp;
	// private Date timeStamp;
	// no way to use java.util.Date for the specific date and time without using deprecated methods or without using 
	// a local date to get a long value
	
	private Integer voltageMeasuringTransformerHeightOne;
	private Integer voltageMeasuringTransformerHeightTwo;
	
	private Integer initialTemperature;
	private Integer measuredTemperature;
	
	public VoltageMeasuringTransformerMeasurement()
	{		
		timeStamp = LocalDateTime.now();
	}

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public VoltageMeasuringTransformer getVoltageMeasuringTransformer() {
		return voltageMeasuringTransformer;
	}

	public void setVoltageMeasuringTransformer(VoltageMeasuringTransformer voltageMeasuringTransformer) {
		this.voltageMeasuringTransformer = voltageMeasuringTransformer;
	}

	public LocalDateTime getTimeStamp() {
		return timeStamp;
	}

	public void setTimeStamp(LocalDateTime timeStamp) {
		this.timeStamp = timeStamp;
	}
	
	public Integer getVoltageMeasuringTransformerHeightOne() {
		return voltageMeasuringTransformerHeightOne;
	}

	public void setVoltageMeasuringTransformerHeightOne(Integer voltageMeasuringTransformerHeightOne) {
		this.voltageMeasuringTransformerHeightOne = voltageMeasuringTransformerHeightOne;
	}

	public Integer getVoltageMeasuringTransformerHeightTwo() {
		return voltageMeasuringTransformerHeightTwo;
	}

	public void setVoltageMeasuringTransformerHeightTwo(Integer voltageMeasuringTransformerHeightTwo) {
		this.voltageMeasuringTransformerHeightTwo = voltageMeasuringTransformerHeightTwo;
	}

	public Integer getTotalVoltageMeasuringTransformerHeight() {
		int total =  (getVoltageMeasuringTransformerHeightOne() == null ? 0 : getVoltageMeasuringTransformerHeightOne()) + 
				(getVoltageMeasuringTransformerHeightTwo() == null ? 0 : getVoltageMeasuringTransformerHeightTwo());
		
		if (total == 0) {
			return null;
		} 
		
		return total;
	}
	
	public Integer getInitialTemperature() {
		return initialTemperature;
	}

	public void setInitialTemperature(Integer initialTemperature) {
		this.initialTemperature = initialTemperature;
	}

	public Integer getMeasuredTemperature() {
		return measuredTemperature;
	}

	public void setMeasuredTemperature(Integer measuredTemperature) {
		this.measuredTemperature = measuredTemperature;
	}

	@Override
	public String[] getFields() {
		String[] fields = new String[7];
		// fields at position zero
		fields[0] = String.valueOf(id);
		fields[1] = voltageMeasuringTransformer.getId().toString();
		fields[2] = timeStamp.toString();
		fields[3] = String.valueOf(voltageMeasuringTransformerHeightOne);
		fields[4] = String.valueOf(voltageMeasuringTransformerHeightTwo);
		fields[5] = String.valueOf(initialTemperature);
		fields[6] = String.valueOf(measuredTemperature);
		// concatenates 2 Arrays using Streams from arrester and measurement and returns the combined array
		return Stream.concat(Arrays.stream(fields), Arrays.stream(voltageMeasuringTransformer.getFields()))
				.toArray(String[]::new);
	}
}
