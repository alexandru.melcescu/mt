package ro.upt.ac.mtb.measurements.mixedmeasuringtransformer;

import java.util.List;
import java.util.Optional;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import ro.upt.ac.mtb.equipments.mixedmeasuringtransformer.MixedMeasuringTransformer;

@Repository
public interface MixedMeasuringTransformerMeasurementRepository extends JpaRepository<MixedMeasuringTransformerMeasurement,Long> {
	Optional<MixedMeasuringTransformerMeasurement> findById(Long id);
	
	@Query("SELECT mmtm FROM MixedMeasuringTransformerMeasurement mmtm"
			+ " WHERE mmtm.timeStamp LIKE %?1%"
			+ " OR mmtm.primaryTerminalMaterial LIKE %?1%"
			+ " OR mmtm.minimumCreepageDistance LIKE %?1%"
			+ " OR mmtm.gravityCenter LIKE %?1%"
			+ " OR mmtm.insulationMaterial LIKE %?1%"
			)
	List<MixedMeasuringTransformerMeasurement> search(String searchTerm);
	List<MixedMeasuringTransformerMeasurement> findByMixedMeasuringTransformerOrderByTimeStamp(MixedMeasuringTransformer mixedMeasuringTransformer);	

}
