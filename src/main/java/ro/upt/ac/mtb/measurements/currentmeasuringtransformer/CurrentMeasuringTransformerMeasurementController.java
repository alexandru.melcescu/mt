package ro.upt.ac.mtb.measurements.currentmeasuringtransformer;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.thymeleaf.TemplateEngine;
import org.thymeleaf.context.Context;
import org.thymeleaf.templatemode.TemplateMode;
import org.thymeleaf.templateresolver.ClassLoaderTemplateResolver;
import org.xhtmlrenderer.pdf.ITextRenderer;

import jakarta.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.io.OutputStream;
import ro.upt.ac.mtb.equipments.currentmeasuringtransformer.CurrentMeasuringTransformer;
import ro.upt.ac.mtb.equipments.currentmeasuringtransformer.CurrentMeasuringTransformerRepository;
import ro.upt.ac.mtb.equipments.services.CSVFileGenerator;
import ro.upt.ac.mtb.equipments.services.ExportableToCSV;

@Controller
@RequestMapping("currentMeasuringTransformerMeasurement")
public class CurrentMeasuringTransformerMeasurementController
{
	@Autowired
	CurrentMeasuringTransformerRepository currentMeasuringTransformerRepository;

	@Autowired
	CurrentMeasuringTransformerMeasurementRepository currentMeasuringTransformerMeasurementRepository;
	
	@Autowired
	CSVFileGenerator csvFileGenerator;
	
	private static final String EQUIPMENT = "currentMeasuringTransformerMeasurement";
	
	@GetMapping("/graphHeight/{id}") 
	public String graphHeight(@PathVariable("id") Long id, Model model) {
		// load all the measurements for the id
		Optional<CurrentMeasuringTransformer> currentMeasuringTransformer = 
				currentMeasuringTransformerRepository.findById(id);
		
		List<CurrentMeasuringTransformerMeasurement> currentMeasuringTransformers = 
				currentMeasuringTransformerMeasurementRepository.findByCurrentMeasuringTransformerOrderByTimeStamp(currentMeasuringTransformer.get());
		
		// setting the measurement list in the model
		if(currentMeasuringTransformers.size() != 0) {
			model.addAttribute("equipmentCode", currentMeasuringTransformer.get().getEquipmentCode());
			model.addAttribute("labels", currentMeasuringTransformers.stream().map(m -> m.getTimeStamp()).collect(Collectors.toList()));
			model.addAttribute("data", currentMeasuringTransformers.stream().map(m -> m.getTotalHeight()).collect(Collectors.toList()));
		}
		return EQUIPMENT + "/" + "graphHeight";
	}
	
	@PostMapping("/search")
	public String search(@Param("searchTerm") String searchTerm, Model model) 
	{
		//String searchTerm = (String) model.getAttribute("searchTerm");
		List<CurrentMeasuringTransformerMeasurement> data = currentMeasuringTransformerMeasurementRepository.search(searchTerm);
		if (data.size() != 0) {
		    model.addAttribute("equipmentObjects", data);
		}
		System.out.println(searchTerm);
		model.addAttribute("equipment", EQUIPMENT);
		model.addAttribute("search", Boolean.TRUE);
		return EQUIPMENT + "/" + EQUIPMENT + "-read";
	}
	
	@GetMapping("/create/{pid}")
	public String create(@PathVariable("pid") Long pid,
			CurrentMeasuringTransformerMeasurement currentMeasuringTransformerMeasurement, 
			Model model)
	{
		model.addAttribute("equipment", EQUIPMENT);
		model.addAttribute("pid", ""+pid.longValue());
		
		model.addAttribute("equipmentObject", currentMeasuringTransformerMeasurement);
		
		return EQUIPMENT + "/" + EQUIPMENT + "-create";
	}

	@PostMapping("/create-save/{pid}")
	public String createSave(@PathVariable("pid") Long pid,
			@Validated CurrentMeasuringTransformerMeasurement currentMeasuringTransformerMeasurement, 
			BindingResult result, Model model)
	{
		if(result.hasErrors())
		{
			System.out.println(result.toString());
			return EQUIPMENT + "/" + EQUIPMENT + "-create";
		} 
		
		//System.out.println(currentMeasuringTransformerMeasurement);
		//System.out.println("id "+currentMeasuringTransformerMeasurement.getId());
		//System.out.println("ts "+currentMeasuringTransformerMeasurement.getTimeStamp());
		//System.out.println("parent "+currentMeasuringTransformerMeasurement.getCurrentMeasuringTransformer());
		
		// getting the parent equipment
		Optional<CurrentMeasuringTransformer> currentMeasuringTransformer = 
				currentMeasuringTransformerRepository.findById(pid);
		// setting the parent
		currentMeasuringTransformerMeasurement.setCurrentMeasuringTransformer(currentMeasuringTransformer.get());
		// saving the object
		currentMeasuringTransformerMeasurementRepository.save(currentMeasuringTransformerMeasurement);
				
	    return read(pid,model);
	}
	
	@GetMapping("/read/{pid}")
	public String read(@PathVariable("pid") Long pid,Model model) 
	{
		model.addAttribute("equipment", EQUIPMENT);
		model.addAttribute("pid", pid);
		
		// getting the parent equipment
		Optional<CurrentMeasuringTransformer> currentMeasuringTransformer = 
				currentMeasuringTransformerRepository.findById(pid);
		
		// getting the measurements of the equipment 
		List<CurrentMeasuringTransformerMeasurement> currentMeasuringTransformers = 
				currentMeasuringTransformerMeasurementRepository.findByCurrentMeasuringTransformerOrderByTimeStamp(currentMeasuringTransformer.get());
		
		// setting the measurement list in the model
		if(currentMeasuringTransformers.size() != 0) 
		{
		    model.addAttribute("equipmentObjects", currentMeasuringTransformers);
		}
		System.out.println("XXXX " + currentMeasuringTransformers);
		//System.out.println("hello");
		return EQUIPMENT + "/" + EQUIPMENT + "-read";
	}
	
	@GetMapping("/edit/{id}")
	public String edit(@PathVariable("id") Long id, Model model) 
	{
		model.addAttribute("equipment", EQUIPMENT); 
		Optional<CurrentMeasuringTransformerMeasurement> currentMeasuringTransformerMeasurement = 
				currentMeasuringTransformerMeasurementRepository.findById(id);
	    model.addAttribute("equipmentObject", currentMeasuringTransformerMeasurement.get());
	    
		return EQUIPMENT + "/" + EQUIPMENT + "-update";
	}
	
	@PostMapping("/update/{id}")
	// removed the pid from the path variable because you have to load the measurement so that you can get 
	// its parent so that you can set the parent before the update because the parent can't be null
	public String update(@PathVariable("id") Long id, 
			@Validated CurrentMeasuringTransformerMeasurement currentMeasuringTransformerMeasurement, BindingResult result, Model model) 
	{
		Optional<CurrentMeasuringTransformerMeasurement> currentMeasuringTransformerMeasurementOld = 
				currentMeasuringTransformerMeasurementRepository.findById(id);
		
		long pid = 0L;
		// defensive code to prevent 500 error
		if (currentMeasuringTransformerMeasurementOld.isPresent()) {
			// load the parent object
			pid = currentMeasuringTransformerMeasurementOld.get().getCurrentMeasuringTransformer().getId();
			// fetched the pre-updated measurement and get the parent id
			// set the parent object before the update otherwise you get field is not nullable
			currentMeasuringTransformerMeasurement.setCurrentMeasuringTransformer(currentMeasuringTransformerMeasurementOld.get().getCurrentMeasuringTransformer());
		}
		if(result.hasErrors()) {
	    	currentMeasuringTransformerMeasurement.setId(id);
			return EQUIPMENT + "/" + EQUIPMENT + "-update";
	    }
	    
	    currentMeasuringTransformerMeasurementRepository.save(currentMeasuringTransformerMeasurement);
	    
//	    return read(currentMeasuringTransformerMeasurementOld.get().getCurrentMeasuringTransformer().getId(),model);
	    return read(pid,model);
	}
	
	@GetMapping("/delete/{id}")
	public String delete(@PathVariable("id") Long id, Model model) 
	{
		Optional<CurrentMeasuringTransformerMeasurement> currentMeasuringTransformerMeasurement = 
				currentMeasuringTransformerMeasurementRepository.findById(id);
		
		Long pid = null;
		
		if(currentMeasuringTransformerMeasurement.isPresent())
		{
			pid = currentMeasuringTransformerMeasurement.get().getCurrentMeasuringTransformer().getId();
			currentMeasuringTransformerMeasurementRepository.delete(currentMeasuringTransformerMeasurement.get());
		}
	    return read(pid, model);
	}	
	
	@GetMapping("/exportToCSV")
	public void export(HttpServletResponse response, Model model) throws IOException {
		response.setContentType("text/csv");
		//  how it gets saved
		response.addHeader("Content-Disposition", "attachment; filename = \"CurrentMeasuringTransformerMeasurements.csv\"");
		// "attachment; filename = \"Separators.csv\"" is the header 
		List<ExportableToCSV> data = new ArrayList<>();
		data.addAll(currentMeasuringTransformerMeasurementRepository.findAll()); // gets every record in the separator table and adds it to the list that gets exported
		csvFileGenerator.write(getHeadings(), data, response.getWriter()); // write to the response object with the header
	}
	// controller handles the headings, entities handle the data
	
	private static String[] getHeadings() {
		String[] fields = new String[8];
		// fields at position zero
		fields[0] = "Id";
		fields[1] = "Parent Id";
		fields[2] = "Time Stamp";
		fields[3] = "Current Measuring Transformer Height One";
		fields[4] = "Current Measuring Transformer Height Two";
		fields[5] = "Total Height";
		fields[6] = "Initial Temperature";
		fields[7] = "Measured Temperature";
		return fields;
	}

	@GetMapping("/exportToPDF")
	public void exportToPDF(HttpServletResponse response, Model model) throws IOException {
		/* String outputFolder = "C:\\Users\\MY PC\\Downloads" + File.arrester + "Arresters.pdf";
		 OutputStream outputStream = new FileOutputStream(outputFolder);
		*/
		
		// getOutputStream() allows the PDF to be opened in the browser rather than a specific location on the server
		OutputStream outputStream = response.getOutputStream();

		response.addHeader("Content-Disposition", "attachment; filename = \"CurrentMeasuringTransformerMeasurements.pdf\"");
		
	    ITextRenderer renderer = new ITextRenderer();
	    renderer.setDocumentFromString(parseThymeleafTemplate());
	    renderer.layout();
	    renderer.createPDF(outputStream);

	    outputStream.close();
	}
	
	
	private String parseThymeleafTemplate() {
	    ClassLoaderTemplateResolver templateResolver = new ClassLoaderTemplateResolver();
	    templateResolver.setSuffix(".html");
	    templateResolver.setTemplateMode(TemplateMode.HTML);

	    TemplateEngine templateEngine = new TemplateEngine();
	    templateEngine.setTemplateResolver(templateResolver);
	    
	    // model.addAttribute("equipment", "separator");
		List<CurrentMeasuringTransformerMeasurement> currentMeasuringTransformers = currentMeasuringTransformerMeasurementRepository.findAll();

	    Context context = new Context();
	    context.setVariable("equipmentObjects", currentMeasuringTransformers);

	    return templateEngine.process("templates/currentMeasuringTransformerMeasurement/currentMeasuringTransformerMeasurement-pdf", context);
	}
}