package ro.upt.ac.mtb.measurements.mixedmeasuringtransformer;

import java.util.Arrays;
import java.util.stream.Stream;

import org.hibernate.annotations.OnDelete;
import org.hibernate.annotations.OnDeleteAction;
import org.springframework.format.annotation.DateTimeFormat;

import jakarta.persistence.Entity;
import jakarta.persistence.FetchType;
import jakarta.persistence.GeneratedValue;
import jakarta.persistence.GenerationType;
import jakarta.persistence.Id;
import jakarta.persistence.JoinColumn;
import jakarta.persistence.ManyToOne;
import java.time.LocalDateTime;
import ro.upt.ac.mtb.equipments.mixedmeasuringtransformer.MixedMeasuringTransformer;
import ro.upt.ac.mtb.equipments.services.ExportableToCSV;

@Entity
public class MixedMeasuringTransformerMeasurement implements ExportableToCSV
{
	@Id
    @GeneratedValue(strategy = GenerationType.AUTO)
	private Long id;
	
	//relationship between the arrester with variable resistance measurement table and the arrester with variable resistance table
	@ManyToOne(fetch = FetchType.LAZY, optional = false)
	@JoinColumn(nullable = false)
	@OnDelete(action = OnDeleteAction.CASCADE)	
	private MixedMeasuringTransformer mixedMeasuringTransformer;
		
	// TODO
	// shows the current date and time stamp
	@DateTimeFormat(pattern = "yyyy-MM-dd'T'HH:mm")
	private LocalDateTime timeStamp;
	// private Date timeStamp;
	// no way to use java.util.Date for the specific date and time without using deprecated methods or without using 
	// a local date to get a long value
	
	private String primaryTerminalMaterial;
	private Integer minimumCreepageDistance;
	private Integer gravityCenter;
	private String insulationMaterial;
	private Integer initialTemperature;
	private Integer measuredTemperature;
	
	public MixedMeasuringTransformerMeasurement()
	{		
		timeStamp = LocalDateTime.now();
	}

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public MixedMeasuringTransformer getMixedMeasuringTransformer() {
		return mixedMeasuringTransformer;
	}

	public void setMixedMeasuringTransformer(MixedMeasuringTransformer mixedMeasuringTransformer) {
		this.mixedMeasuringTransformer = mixedMeasuringTransformer;
	}

	public LocalDateTime getTimeStamp() {
		return timeStamp;
	}

	public void setTimeStamp(LocalDateTime timeStamp) {
		this.timeStamp = timeStamp;
	}
	
	public String getPrimaryTerminalMaterial() {
		return primaryTerminalMaterial;
	}

	public void setPrimaryTerminalMaterial(String primaryTerminalMaterial) {
		this.primaryTerminalMaterial = primaryTerminalMaterial;
	}

	public Integer getMinimumCreepageDistance() {
		return minimumCreepageDistance;
	}

	public void setMinimumCreepageDistance(Integer minimumCreepageDistance) {
		this.minimumCreepageDistance = minimumCreepageDistance;
	}

	public Integer getGravityCenter() {
		return gravityCenter;
	}

	public void setGravityCenter(Integer gravityCenter) {
		this.gravityCenter = gravityCenter;
	}

	public String getInsulationMaterial() {
		return insulationMaterial;
	}

	public void setInsulationMaterial(String insulationMaterial) {
		this.insulationMaterial = insulationMaterial;
	}
	
	public Integer getInitialTemperature() {
		return initialTemperature;
	}

	public void setInitialTemperature(Integer initialTemperature) {
		this.initialTemperature = initialTemperature;
	}

	public Integer getMeasuredTemperature() {
		return measuredTemperature;
	}

	public void setMeasuredTemperature(Integer measuredTemperature) {
		this.measuredTemperature = measuredTemperature;
	}

	@Override
	public String[] getFields() {
		String[] fields = new String[7];
		// fields at position zero
		fields[0] = String.valueOf(id);
		fields[1] = mixedMeasuringTransformer.getId().toString();
		fields[2] = timeStamp.toString();
		fields[3] = primaryTerminalMaterial;
		fields[4] = String.valueOf(minimumCreepageDistance);
		fields[5] = String.valueOf(gravityCenter);
		fields[6] = insulationMaterial;
		fields[7] = String.valueOf(initialTemperature);
		fields[8] = String.valueOf(measuredTemperature);
		// concatenates 2 Arrays using Streams from arrester and measurement and returns the combined array
		return Stream.concat(Arrays.stream(fields), Arrays.stream(mixedMeasuringTransformer.getFields()))
				.toArray(String[]::new);
	}
}
