package ro.upt.ac.mtb.measurements.separator;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.thymeleaf.TemplateEngine;
import org.thymeleaf.context.Context;
import org.thymeleaf.templatemode.TemplateMode;
import org.thymeleaf.templateresolver.ClassLoaderTemplateResolver;
import org.xhtmlrenderer.pdf.ITextRenderer;

import jakarta.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.io.OutputStream;
import java.time.format.DateTimeFormatter;
import ro.upt.ac.mtb.equipments.separator.Separator;
import ro.upt.ac.mtb.equipments.separator.SeparatorRepository;
import ro.upt.ac.mtb.equipments.services.CSVFileGenerator;
import ro.upt.ac.mtb.equipments.services.ExportableToCSV;

@Controller
@RequestMapping("separatorMeasurement")
public class SeparatorMeasurementController
{
	@Autowired
	SeparatorRepository separatorRepository;

	@Autowired
	SeparatorMeasurementRepository separatorMeasurementRepository;
	
	@Autowired
	CSVFileGenerator csvFileGenerator;
	
	private static final String EQUIPMENT = "separatorMeasurement";
	
	@GetMapping("/graphHeight/{id}") 
	public String graphHeight(@PathVariable("id") Long id, Model model) {
		// load all the measurements for the id
		Optional<Separator> separator = 
				separatorRepository.findById(id);
		
		List<SeparatorMeasurement> separators = 
				separatorMeasurementRepository.findBySeparatorOrderByTimeStamp(separator.get());
		
		// setting the measurement list in the model
		if(separators.size() != 0) {
			model.addAttribute("equipmentCode", separator.get().getEquipmentCode());
//			model.addAttribute("labels", arrestersWithVariableResistance.stream().map(m -> m.getTimeStamp()).collect(Collectors.toList()));
//			model.addAttribute("data", arrestersWithVariableResistance.stream().map(m -> m.getTotalHeightInMillimeters()).collect(Collectors.toList()));
			// modifications for a bubble graph
			List<Map<String, Object>> data = new ArrayList<>();
//			int x = 1;
			for(SeparatorMeasurement m : separators) {
				Map<String, Object> record = new HashMap<>();
				int date = 0;
				try {
					DateTimeFormatter formatter = DateTimeFormatter.ofPattern("yyyyMMdd");
					String text = m.getTimeStamp().format(formatter);
					date = Integer.parseInt(text);
				} catch (NumberFormatException e) {
					e.printStackTrace();
				}
				record.put("x", date);
//				record.put("x", x++);
				record.put("y", m.getTotalHeight());
				record.put("r", m.getMeasuredTemperature());
				data.add(record);
			}
			model.addAttribute("data", data);
		}
		return EQUIPMENT + "/" + "graphHeight";
	}
	
	
	@PostMapping("/search")
	public String search(@Param("searchTerm") String searchTerm, Model model) 
	{
		//String searchTerm = (String) model.getAttribute("searchTerm");
		List<SeparatorMeasurement> data = separatorMeasurementRepository.search(searchTerm);
		if (data.size() != 0) {
		    model.addAttribute("equipmentObjects", data);
		}
		System.out.println(searchTerm);
		model.addAttribute("equipment", EQUIPMENT);
		model.addAttribute("search", Boolean.TRUE);
		return EQUIPMENT + "/" + EQUIPMENT + "-read";
	}
	
	@GetMapping("/create/{pid}")
	public String create(@PathVariable("pid") Long pid,
			SeparatorMeasurement separatorMeasurement, 
			Model model)
	{
		model.addAttribute("equipment", EQUIPMENT);
		model.addAttribute("pid", ""+pid.longValue());
		
		model.addAttribute("equipmentObject", separatorMeasurement);
		
		return EQUIPMENT + "/" + EQUIPMENT + "-create";
	}

	@PostMapping("/create-save/{pid}")
	public String createSave(@PathVariable("pid") Long pid,
			@Validated SeparatorMeasurement separatorMeasurement, 
			BindingResult result, Model model)
	{
		if(result.hasErrors())
		{
			System.out.println(result.toString());
			return EQUIPMENT + "/" + EQUIPMENT + "-create";
		} 
		
		//System.out.println(separatorMeasurement);
		//System.out.println("id "+separatorMeasurement.getId());
		//System.out.println("ts "+separatorMeasurement.getTimeStamp());
		//System.out.println("parent "+separatorMeasurement.getSeparator());
		
		// getting the parent equipment
		Optional<Separator> separator = 
				separatorRepository.findById(pid);
		// setting the parent
		separatorMeasurement.setSeparator(separator.get());
		// saving the object
		separatorMeasurementRepository.save(separatorMeasurement);
				
	    return read(pid,model);
	}
	
	@GetMapping("/read/{pid}")
	public String read(@PathVariable("pid") Long pid,Model model) 
	{
		model.addAttribute("equipment", EQUIPMENT);
		model.addAttribute("pid", pid);
		
		// getting the parent equipment
		Optional<Separator> separator = 
				separatorRepository.findById(pid);
		
		// getting the measurements of the equipment 
		List<SeparatorMeasurement> separators = 
				separatorMeasurementRepository.findBySeparatorOrderByTimeStamp(separator.get());
		
		// setting the measurement list in the model
		if(separators.size() != 0) 
		{
		    model.addAttribute("equipmentObjects", separators);
		}
		//System.out.println("hello");
		return EQUIPMENT + "/" + EQUIPMENT + "-read";
	}
	
	@GetMapping("/edit/{id}")
	public String edit(@PathVariable("id") Long id, Model model) 
	{
		model.addAttribute("equipment", EQUIPMENT); 
		Optional<SeparatorMeasurement> separatorMeasurement = 
				separatorMeasurementRepository.findById(id);
	    model.addAttribute("equipmentObject", separatorMeasurement.get());
	    
		return EQUIPMENT + "/" + EQUIPMENT + "-update";
	}
	
	@PostMapping("/update/{id}")
	// removed the pid from the path variable because you have to load the measurement so that you can get 
	// its parent so that you can set the parent before the update because the parent can't be null
	public String update(@PathVariable("id") Long id, 
			@Validated SeparatorMeasurement separatorMeasurement, BindingResult result, Model model) 
	{
		Optional<SeparatorMeasurement> separatorMeasurementOld = 
				separatorMeasurementRepository.findById(id);
		
		long pid = 0L;
		// defensive code to prevent 500 error
		if (separatorMeasurementOld.isPresent()) {
			// load the parent object
			pid = separatorMeasurementOld.get().getSeparator().getId();
			// fetched the pre-updated measurement and get the parent id
			// set the parent object before the update otherwise you get field is not nullable
			separatorMeasurement.setSeparator(separatorMeasurementOld.get().getSeparator());
		}
		if(result.hasErrors()) {
	    	separatorMeasurement.setId(id);
			return EQUIPMENT + "/" + EQUIPMENT + "-update";
	    }
	    
	    separatorMeasurementRepository.save(separatorMeasurement);
	    
//	    return read(separatorMeasurementOld.get().getSeparator().getId(),model);
	    return read(pid,model);
	}
	
	@GetMapping("/delete/{id}")
	public String delete(@PathVariable("id") Long id, Model model) 
	{
		Optional<SeparatorMeasurement> separatorMeasurement = 
				separatorMeasurementRepository.findById(id);
		
		Long pid = null;
		
		if(separatorMeasurement.isPresent())
		{
			pid = separatorMeasurement.get().getSeparator().getId();
			separatorMeasurementRepository.delete(separatorMeasurement.get());
		}
	    return read(pid, model);
	}	
	
	@GetMapping("/exportToCSV")
	public void export(HttpServletResponse response, Model model) throws IOException {
		response.setContentType("text/csv");
		//  how it gets saved
		response.addHeader("Content-Disposition", "attachment; filename = \"SeparatorMeasurements.csv\"");
		// "attachment; filename = \"Separators.csv\"" is the header 
		List<ExportableToCSV> data = new ArrayList<>();
		data.addAll(separatorMeasurementRepository.findAll()); // gets every record in the separator table and adds it to the list that gets exported
		csvFileGenerator.write(getHeadings(), data, response.getWriter()); // write to the response object with the header
	}
	// controller handles the headings, entities handle the data
	
	private static String[] getHeadings() {
		String[] fields = new String[9];
		// fields at position zero
		fields[0] = "Id";
		fields[1] = "Parent Id";
		fields[2] = "Time Stamp";
		fields[3] = "Height One";
		fields[4] = "Height Two";
		fields[5] = "Total Height";
		fields[6] = "Width";
		fields[7] = "Initial Temperature";
		fields[8] = "Measured Temperature";
		return fields;
	}

	@GetMapping("/exportToPDF")
	public void exportToPDF(HttpServletResponse response, Model model) throws IOException {
		/* String outputFolder = "C:\\Users\\MY PC\\Downloads" + File.arrester + "Arresters.pdf";
		 OutputStream outputStream = new FileOutputStream(outputFolder);
		*/
		
		// getOutputStream() allows the PDF to be opened in the browser rather than a specific location on the server
		OutputStream outputStream = response.getOutputStream();

		response.addHeader("Content-Disposition", "attachment; filename = \"SeparatorMeasurements.pdf\"");
		
	    ITextRenderer renderer = new ITextRenderer();
	    renderer.setDocumentFromString(parseThymeleafTemplate());
	    renderer.layout();
	    renderer.createPDF(outputStream);

	    outputStream.close();
	}
	
	
	private String parseThymeleafTemplate() {
	    ClassLoaderTemplateResolver templateResolver = new ClassLoaderTemplateResolver();
	    templateResolver.setSuffix(".html");
	    templateResolver.setTemplateMode(TemplateMode.HTML);

	    TemplateEngine templateEngine = new TemplateEngine();
	    templateEngine.setTemplateResolver(templateResolver);
	    
	    // model.addAttribute("equipment", "separator");
		List<SeparatorMeasurement> separators = separatorMeasurementRepository.findAll();

	    Context context = new Context();
	    context.setVariable("equipmentObjects", separators);

	    return templateEngine.process("templates/separatorMeasurement/separatorMeasurement-pdf", context);
	}
}