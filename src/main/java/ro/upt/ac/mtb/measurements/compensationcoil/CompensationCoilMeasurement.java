package ro.upt.ac.mtb.measurements.compensationcoil;

import java.util.Arrays;
import java.util.stream.Stream;

import org.hibernate.annotations.OnDelete;
import org.hibernate.annotations.OnDeleteAction;
import org.springframework.format.annotation.DateTimeFormat;

import jakarta.persistence.Entity;
import jakarta.persistence.FetchType;
import jakarta.persistence.GeneratedValue;
import jakarta.persistence.GenerationType;
import jakarta.persistence.Id;
import jakarta.persistence.JoinColumn;
import jakarta.persistence.ManyToOne;
import java.time.LocalDateTime;
import ro.upt.ac.mtb.equipments.compensationcoil.CompensationCoil;
import ro.upt.ac.mtb.equipments.electricalequipment.ElectricalEquipment;
import ro.upt.ac.mtb.equipments.services.ExportableToCSV;

@Entity
public class CompensationCoilMeasurement extends ElectricalEquipment implements ExportableToCSV
{
	@Id
    @GeneratedValue(strategy = GenerationType.AUTO)
	private Long id;
	
	//relationship between the compensation coil measurement table and the compensation coil table
	@ManyToOne(fetch = FetchType.LAZY, optional = false)
	@JoinColumn(nullable = false)
	@OnDelete(action = OnDeleteAction.CASCADE)	
	private CompensationCoil compensationCoil;
	
	// TODO
	// shows the current date and time stamp
	@DateTimeFormat(pattern = "yyyy-MM-dd'T'HH:mm")
	private LocalDateTime timeStamp;
	
	private Integer cableDimension;
	private Integer cableLength;
	
	private Integer initialTemperature;
	private Integer measuredTemperature;
	
	// private Date timeStamp;
	// no way to use java.util.Date for the specific date and time without using deprecated methods or without using 
	// a local date to get a long value
	
	public CompensationCoilMeasurement()
	{		
		timeStamp = LocalDateTime.now();
	}

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public CompensationCoil getCompensationCoil() {
		return compensationCoil;
	}

	public void setCompensationCoil(CompensationCoil compensationCoil) {
		this.compensationCoil = compensationCoil;
	}

	public LocalDateTime getTimeStamp() {
		return timeStamp;
	}

	public void setTimeStamp(LocalDateTime timeStamp) {
		this.timeStamp = timeStamp;
	}
	
	public Integer getCableDimension() {
		return cableDimension;
	}

	public void setCableDimension(Integer cableDimension) {
		this.cableDimension = cableDimension;
	}

	public Integer getCableLength() {
		return cableLength;
	}

	public void setCableLength(Integer cableLength) {
		this.cableLength = cableLength;
	}

	public Integer getInitialTemperature() {
		return initialTemperature;
	}

	public void setInitialTemperature(Integer initialTemperature) {
		this.initialTemperature = initialTemperature;
	}

	public Integer getMeasuredTemperature() {
		return measuredTemperature;
	}

	public void setMeasuredTemperature(Integer measuredTemperature) {
		this.measuredTemperature = measuredTemperature;
	}

	@Override
	public String[] getFields() {
		String[] fields = new String[7];
		// fields at position zero
		fields[0] = String.valueOf(id);
		fields[1] = compensationCoil.getId().toString();
		fields[2] = timeStamp.toString();
		fields[3] = String.valueOf(cableDimension);
		fields[4] = String.valueOf(cableLength);
		fields[5] = String.valueOf(initialTemperature);
		fields[6] = String.valueOf(measuredTemperature);
		// concatenates 2 Arrays using Streams from arrester and measurement and returns the combined array
		return Stream.concat(Arrays.stream(fields), Arrays.stream(compensationCoil.getFields()))
				.toArray(String[]::new);
	}
}
