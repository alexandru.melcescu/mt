package ro.upt.ac.mtb.measurements.arresterwithvariableresistance;

import java.util.Arrays;
import java.util.stream.Stream;

import org.hibernate.annotations.OnDelete;
import org.hibernate.annotations.OnDeleteAction;
import org.springframework.format.annotation.DateTimeFormat;

import jakarta.persistence.Entity;
import jakarta.persistence.FetchType;
import jakarta.persistence.GeneratedValue;
import jakarta.persistence.GenerationType;
import jakarta.persistence.Id;
import jakarta.persistence.JoinColumn;
import jakarta.persistence.ManyToOne;
import java.time.LocalDateTime;
import ro.upt.ac.mtb.equipments.arresterwithvariableresistance.ArresterWithVariableResistance;
import ro.upt.ac.mtb.equipments.electricalequipment.ElectricalEquipment;
import ro.upt.ac.mtb.equipments.services.ExportableToCSV;

@Entity
public class ArresterWithVariableResistanceMeasurement extends ElectricalEquipment implements ExportableToCSV
{
	@Id
    @GeneratedValue(strategy = GenerationType.AUTO)
	private Long id;
	
	//relationship between the arrester with variable resistance measurement table and the arrester with variable resistance table
	@ManyToOne(fetch = FetchType.LAZY, optional = false)
	@JoinColumn(nullable = false)
	@OnDelete(action = OnDeleteAction.CASCADE)	
	private ArresterWithVariableResistance arresterWithVariableResistance;
	
	// TODO
	// shows the current date and time stamp
	@DateTimeFormat(pattern = "yyyy-MM-dd'T'HH:mm:ss")
	private LocalDateTime timeStamp;
	
	private Integer heightOneInMillimeters;
	private Integer heightTwoInMillimeters;
	private Integer heightThreeInMillimeters;
	private Integer heightFourInMillimeters;
		
	private Integer weightInKilograms;
	
	private Integer creepageDistanceInMillimeters;
	
	private Integer initialTemperature;
	private Integer measuredTemperature;
	// private Date timeStamp;
	// no way to use java.util.Date for the specific date and time without using deprecated methods or without using 
	// a local date to get a long value
	
	public ArresterWithVariableResistanceMeasurement()
	{
		timeStamp = LocalDateTime.now();
	}

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public ArresterWithVariableResistance getArresterWithVariableResistance() {
		return arresterWithVariableResistance;
	}

	public void setArresterWithVariableResistance(ArresterWithVariableResistance arresterWithVariableResistance) {
		this.arresterWithVariableResistance = arresterWithVariableResistance;
	}

	public LocalDateTime getTimeStamp() {
		return timeStamp;
	}

	public void setTimeStamp(LocalDateTime timeStamp) {
		this.timeStamp = timeStamp;
	}	

	public Integer getHeightOneInMillimeters() {
		return heightOneInMillimeters;
	}

	public void setHeightOneInMillimeters(Integer heightOneInMillimeters) {
		this.heightOneInMillimeters = heightOneInMillimeters;
	}

	public Integer getHeightTwoInMillimeters() {
		return heightTwoInMillimeters;
	}

	public void setHeightTwoInMillimeters(Integer heightTwoInMillimeters) {
		this.heightTwoInMillimeters = heightTwoInMillimeters;
	}

	public Integer getHeightThreeInMillimeters() {
		return heightThreeInMillimeters;
	}

	public void setHeightThreeInMillimeters(Integer heightThreeInMillimeters) {
		this.heightThreeInMillimeters = heightThreeInMillimeters;
	}

	public Integer getHeightFourInMillimeters() {
		return heightFourInMillimeters;
	}

	public void setHeightFourInMillimeters(Integer heightFourInMillimeters) {
		this.heightFourInMillimeters = heightFourInMillimeters;
	}

	public Integer getTotalHeightInMillimeters() {
		int total =  (getHeightOneInMillimeters() == null ? 0 : getHeightOneInMillimeters()) + 
				(getHeightTwoInMillimeters() == null ? 0 : getHeightTwoInMillimeters()) + 
				(getHeightThreeInMillimeters() == null ? 0 : getHeightThreeInMillimeters()) + 
				(getHeightFourInMillimeters() == null ? 0 : getHeightFourInMillimeters());
		
		if (total == 0) {
			return null;
		} 
		
		return total;
	}

	public Integer getWeightInKilograms() {
		return weightInKilograms;
	}

	public void setWeightInKilograms(Integer weightInKilograms) {
		this.weightInKilograms = weightInKilograms;
	}
	
	public Integer getCreepageDistanceInMillimeters() {
		return creepageDistanceInMillimeters;
	}

	public void setCreepageDistanceInMillimeters(Integer creepageDistanceInMillimeters) {
		this.creepageDistanceInMillimeters = creepageDistanceInMillimeters;
	}

	public Integer getInitialTemperature() {
		return initialTemperature;
	}

	public void setInitialTemperature(Integer initialTemperature) {
		this.initialTemperature = initialTemperature;
	}

	public Integer getMeasuredTemperature() {
		return measuredTemperature;
	}

	public void setMeasuredTemperature(Integer measuredTemperature) {
		this.measuredTemperature = measuredTemperature;
	}

	@Override
	public String toString() {
		return "ArresterWithVariableResistanceMeasurement [id=" + id + ", timeStamp=" + timeStamp + "]";
	}

	@Override
	public String[] getFields() {
		String[] fields = new String[11];
		// fields at position zero
		fields[0] = String.valueOf(id);
		fields[1] = arresterWithVariableResistance.getId().toString();
		fields[2] = timeStamp.toString();
		fields[3] = String.valueOf(heightOneInMillimeters);
		fields[4] = String.valueOf(heightTwoInMillimeters);
		fields[5] = String.valueOf(heightThreeInMillimeters);
		fields[6] = String.valueOf(heightFourInMillimeters);
		fields[7] = String.valueOf(weightInKilograms);
		fields[8] = String.valueOf(creepageDistanceInMillimeters);
		fields[9] = String.valueOf(initialTemperature);
		fields[10] = String.valueOf(measuredTemperature);
		// concatenates 2 Arrays using Streams from arrester and measurement and returns the combined array
		return Stream.concat(Arrays.stream(fields), Arrays.stream(arresterWithVariableResistance.getFields()))
				.toArray(String[]::new);
//		return fields;
	}
}