package ro.upt.ac.mtb.measurements.groundingknifeactuationdevice;

import java.util.List;
import java.util.Optional;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import ro.upt.ac.mtb.equipments.groundingknifeactuationdevice.GroundingKnifeActuationDevice;

@Repository
public interface GroundingKnifeActuationDeviceMeasurementRepository extends JpaRepository<GroundingKnifeActuationDeviceMeasurement,Integer> {
	Optional<GroundingKnifeActuationDeviceMeasurement> findById(Long id);
	
	@Query("SELECT gkadm FROM GroundingKnifeActuationDeviceMeasurement gkadm"
			+ " WHERE gkadm.timeStamp LIKE %?1%"
			)
	List<GroundingKnifeActuationDeviceMeasurement> search(String searchTerm);

	List<GroundingKnifeActuationDeviceMeasurement> findByGroundingKnifeActuationDeviceOrderByTimeStamp(
			GroundingKnifeActuationDevice groundingKnifeActuationDevice);
}
