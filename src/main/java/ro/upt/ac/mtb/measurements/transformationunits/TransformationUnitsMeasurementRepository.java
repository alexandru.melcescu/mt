package ro.upt.ac.mtb.measurements.transformationunits;

import java.util.List;
import java.util.Optional;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import ro.upt.ac.mtb.equipments.transformationunits.TransformationUnits;

@Repository
public interface TransformationUnitsMeasurementRepository extends JpaRepository<TransformationUnitsMeasurement,Integer> {
	Optional<TransformationUnitsMeasurement> findById(Long id);
	
	@Query("SELECT tum FROM TransformationUnitsMeasurement tum"
			+ " WHERE tum.timeStamp LIKE %?1%"
			)
	List<TransformationUnitsMeasurement> search(String searchTerm);

	List<TransformationUnitsMeasurement> findByTransformationUnitsOrderByTimeStamp(TransformationUnits transformationUnits);
}
