package ro.upt.ac.mtb.measurements.currentmeasuringtransformer;

import java.util.Arrays;
import java.util.stream.Stream;

import org.hibernate.annotations.OnDelete;
import org.hibernate.annotations.OnDeleteAction;
import org.springframework.format.annotation.DateTimeFormat;

import jakarta.persistence.Entity;
import jakarta.persistence.FetchType;
import jakarta.persistence.GeneratedValue;
import jakarta.persistence.GenerationType;
import jakarta.persistence.Id;
import jakarta.persistence.JoinColumn;
import jakarta.persistence.ManyToOne;
import java.time.LocalDateTime;
import ro.upt.ac.mtb.equipments.currentmeasuringtransformer.CurrentMeasuringTransformer;
import ro.upt.ac.mtb.equipments.services.ExportableToCSV;

@Entity
public class CurrentMeasuringTransformerMeasurement implements ExportableToCSV
{
	@Id
    @GeneratedValue(strategy = GenerationType.AUTO)
	private Long id;
	
	//relationship between the current measuring transformer measurement table and the current measuring transformer table
	@ManyToOne(fetch = FetchType.LAZY, optional = false)
	@JoinColumn(nullable = false)
	@OnDelete(action = OnDeleteAction.CASCADE)	
	private CurrentMeasuringTransformer currentMeasuringTransformer;
		
	// TODO
	// shows the current date and time stamp
	@DateTimeFormat(pattern = "yyyy-MM-dd'T'HH:mm")
	private LocalDateTime timeStamp;
	// private Date timeStamp;
	// no way to use java.util.Date for the specific date and time without using deprecated methods or without using 
	// a local date to get a long value
	
	private Integer currentMeasuringTransformerHeightOne;
	private Integer currentMeasuringTransformerHeightTwo;
	
	private Integer initialTemperature;
	private Integer measuredTemperature;
	
	public CurrentMeasuringTransformerMeasurement()
	{		
		timeStamp = LocalDateTime.now();
	}

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public CurrentMeasuringTransformer getCurrentMeasuringTransformer() {
		return currentMeasuringTransformer;
	}

	public void setCurrentMeasuringTransformer(CurrentMeasuringTransformer currentMeasuringTransformer) {
		this.currentMeasuringTransformer = currentMeasuringTransformer;
	}

	public LocalDateTime getTimeStamp() {
		return timeStamp;
	}

	public void setTimeStamp(LocalDateTime timeStamp) {
		this.timeStamp = timeStamp;
	}
	
	public Integer getCurrentMeasuringTransformerHeightOne() {
		return currentMeasuringTransformerHeightOne;
	}

	public void setCurrentMeasuringTransformerHeightOne(Integer currentMeasuringTransformerHeightOne) {
		this.currentMeasuringTransformerHeightOne = currentMeasuringTransformerHeightOne;
	}

	public Integer getCurrentMeasuringTransformerHeightTwo() {
		return currentMeasuringTransformerHeightTwo;
	}

	public void setCurrentMeasuringTransformerHeightTwo(Integer currentMeasuringTransformerHeightTwo) {
		this.currentMeasuringTransformerHeightTwo = currentMeasuringTransformerHeightTwo;
	}

	public Integer getTotalHeight() {
		int total =  (getCurrentMeasuringTransformerHeightOne() == null ? 0 : getCurrentMeasuringTransformerHeightOne()) + 
				(getCurrentMeasuringTransformerHeightTwo() == null ? 0 : getCurrentMeasuringTransformerHeightTwo());
		if (total == 0) {
			return null;
		} 
		
		return total;
	}
	
	public Integer getInitialTemperature() {
		return initialTemperature;
	}

	public void setInitialTemperature(Integer initialTemperature) {
		this.initialTemperature = initialTemperature;
	}

	public Integer getMeasuredTemperature() {
		return measuredTemperature;
	}

	public void setMeasuredTemperature(Integer measuredTemperature) {
		this.measuredTemperature = measuredTemperature;
	}

	@Override
	public String[] getFields() {
		String[] fields = new String[7];
		// fields at position zero
		fields[0] = String.valueOf(id);
		fields[1] = currentMeasuringTransformer.getId().toString();
		fields[2] = timeStamp.toString();
		fields[3] = String.valueOf(currentMeasuringTransformerHeightOne);
		fields[4] = String.valueOf(currentMeasuringTransformerHeightTwo);
		fields[5] = String.valueOf(initialTemperature);
		fields[6] = String.valueOf(measuredTemperature);
		// concatenates 2 Arrays using Streams from arrester and measurement and returns the combined array
		return Stream.concat(Arrays.stream(fields), Arrays.stream(currentMeasuringTransformer.getFields()))
				.toArray(String[]::new);
	}
}