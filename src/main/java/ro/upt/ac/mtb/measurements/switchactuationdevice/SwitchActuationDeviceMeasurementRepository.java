package ro.upt.ac.mtb.measurements.switchactuationdevice;

import java.util.List;
import java.util.Optional;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import ro.upt.ac.mtb.equipments.switchactuationdevice.SwitchActuationDevice;

@Repository
public interface SwitchActuationDeviceMeasurementRepository extends JpaRepository<SwitchActuationDeviceMeasurement, Long> {
	Optional<SwitchActuationDeviceMeasurement> findById(Long id);
	
	@Query("SELECT swadm FROM SwitchActuationDeviceMeasurement swadm"
			+ " WHERE swadm.timeStamp LIKE %?1%"
			+ "OR swadm.fourtyPercentOfTheLengthOfTheBottomOfTheSwitch LIKE %?1%"
			+ "OR swadm.widthOne LIKE %?1%"
			+ "OR swadm.widthTwo LIKE %?1%"
			)
	List<SwitchActuationDeviceMeasurement> search(String searchTerm);

	List<SwitchActuationDeviceMeasurement> findBySwitchActuationDeviceOrderByTimeStamp(SwitchActuationDevice switchActuationDevice);
}
