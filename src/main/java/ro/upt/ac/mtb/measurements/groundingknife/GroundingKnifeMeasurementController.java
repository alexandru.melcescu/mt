package ro.upt.ac.mtb.measurements.groundingknife;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.thymeleaf.TemplateEngine;
import org.thymeleaf.context.Context;
import org.thymeleaf.templatemode.TemplateMode;
import org.thymeleaf.templateresolver.ClassLoaderTemplateResolver;
import org.xhtmlrenderer.pdf.ITextRenderer;

import jakarta.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.io.OutputStream;
import ro.upt.ac.mtb.equipments.groundingknife.GroundingKnife;
import ro.upt.ac.mtb.equipments.groundingknife.GroundingKnifeRepository;
import ro.upt.ac.mtb.equipments.services.CSVFileGenerator;
import ro.upt.ac.mtb.equipments.services.ExportableToCSV;

@Controller
@RequestMapping("groundingKnifeMeasurement")
public class GroundingKnifeMeasurementController
{
	@Autowired
	GroundingKnifeRepository groundingKnifeRepository;

	@Autowired
	GroundingKnifeMeasurementRepository groundingKnifeMeasurementRepository;
	
	@Autowired
	CSVFileGenerator csvFileGenerator;
	
	private static final String EQUIPMENT = "groundingKnifeMeasurement";
	
	@GetMapping("/graphRadii/{id}") 
	public String graphRadii(@PathVariable("id") Long id, Model model) {
		// load all the measurements for the id
		Optional<GroundingKnife> groundingKnife = 
				groundingKnifeRepository.findById(id);
		
		List<GroundingKnifeMeasurement> groundingKnives = 
				groundingKnifeMeasurementRepository.findByGroundingKnifeOrderByTimeStamp(groundingKnife.get());
		
		// setting the measurement list in the model
		if(groundingKnives.size() != 0) {
			model.addAttribute("equipmentCode", groundingKnife.get().getEquipmentCode());
			model.addAttribute("labels", groundingKnives.stream().map(m -> m.getTimeStamp()).collect(Collectors.toList()));
			model.addAttribute("data", groundingKnives.stream().map(m -> m.getrRadius()).collect(Collectors.toList()));
		}
		return EQUIPMENT + "/" + "graphRadii";
	}
	
	@PostMapping("/search")
	public String search(@Param("searchTerm") String searchTerm, Model model) 
	{
		//String searchTerm = (String) model.getAttribute("searchTerm");
		List<GroundingKnifeMeasurement> data = groundingKnifeMeasurementRepository.search(searchTerm);
		if (data.size() != 0) {
		    model.addAttribute("equipmentObjects", data);
		}
		System.out.println(searchTerm);
		model.addAttribute("equipment", EQUIPMENT);
		model.addAttribute("search", Boolean.TRUE);
		return EQUIPMENT + "/" + EQUIPMENT + "-read";
	}
	
	@GetMapping("/create/{pid}")
	public String create(@PathVariable("pid") Long pid,
			GroundingKnifeMeasurement groundingKnifeMeasurement, 
			Model model)
	{
		model.addAttribute("equipment", EQUIPMENT);
		model.addAttribute("pid", ""+pid.longValue());
		
		model.addAttribute("equipmentObject", groundingKnifeMeasurement);
		
		return EQUIPMENT + "/" + EQUIPMENT + "-create";
	}

	@PostMapping("/create-save/{pid}")
	public String createSave(@PathVariable("pid") Long pid,
			@Validated GroundingKnifeMeasurement groundingKnifeMeasurement, 
			BindingResult result, Model model)
	{
		if(result.hasErrors())
		{
			System.out.println(result.toString());
			return EQUIPMENT + "/" + EQUIPMENT + "-create";
		} 
		
		//System.out.println(groundingKnifeMeasurement);
		//System.out.println("id "+groundingKnifeMeasurement.getId());
		//System.out.println("ts "+groundingKnifeMeasurement.getTimeStamp());
		//System.out.println("parent "+groundingKnifeMeasurement.getGroundingKnife());
		
		// getting the parent equipment
		Optional<GroundingKnife> groundingKnife = 
				groundingKnifeRepository.findById(pid);
		// setting the parent
		groundingKnifeMeasurement.setGroundingKnife(groundingKnife.get());
		// saving the object
		groundingKnifeMeasurementRepository.save(groundingKnifeMeasurement);
				
	    return read(pid,model);
	}
	
	@GetMapping("/read/{pid}")
	public String read(@PathVariable("pid") Long pid,Model model) 
	{
		model.addAttribute("equipment", EQUIPMENT);
		model.addAttribute("pid", pid);
		
		// getting the parent equipment
		Optional<GroundingKnife> groundingKnife = 
				groundingKnifeRepository.findById(pid);
		
		// getting the measurements of the equipment 
		List<GroundingKnifeMeasurement> groundingKnives = 
				groundingKnifeMeasurementRepository.findByGroundingKnifeOrderByTimeStamp(groundingKnife.get());
		
		// setting the measurement list in the model
		if(groundingKnives.size() != 0) 
		{
		    model.addAttribute("equipmentObjects", groundingKnives);
		}
		//System.out.println("hello");
		return EQUIPMENT + "/" + EQUIPMENT + "-read";
	}
	
	@GetMapping("/edit/{id}")
	public String edit(@PathVariable("id") Long id, Model model) 
	{
		model.addAttribute("equipment", EQUIPMENT); 
		Optional<GroundingKnifeMeasurement> groundingKnifeMeasurement = 
				groundingKnifeMeasurementRepository.findById(id);
	    model.addAttribute("equipmentObject", groundingKnifeMeasurement.get());
	    
		return EQUIPMENT + "/" + EQUIPMENT + "-update";
	}
	
	@PostMapping("/update/{id}")
	// removed the pid from the path variable because you have to load the measurement so that you can get 
	// its parent so that you can set the parent before the update because the parent can't be null
	public String update(@PathVariable("id") Long id, 
			@Validated GroundingKnifeMeasurement groundingKnifeMeasurement, BindingResult result, Model model) 
	{
		Optional<GroundingKnifeMeasurement> groundingKnifeMeasurementOld = 
				groundingKnifeMeasurementRepository.findById(id);
		
		long pid = 0L;
		// defensive code to prevent 500 error
		if (groundingKnifeMeasurementOld.isPresent()) {
			// load the parent object
			pid = groundingKnifeMeasurementOld.get().getGroundingKnife().getId();
			// fetched the pre-updated measurement and get the parent id
			// set the parent object before the update otherwise you get field is not nullable
			groundingKnifeMeasurement.setGroundingKnife(groundingKnifeMeasurementOld.get().getGroundingKnife());
		}
		if(result.hasErrors()) {
	    	groundingKnifeMeasurement.setId(id);
			return EQUIPMENT + "/" + EQUIPMENT + "-update";
	    }
	    
	    groundingKnifeMeasurementRepository.save(groundingKnifeMeasurement);
	    
//	    return read(groundingKnifeMeasurementOld.get().getGroundingKnife().getId(),model);
	    return read(pid,model);
	}
	
	@GetMapping("/delete/{id}")
	public String delete(@PathVariable("id") Long id, Model model) 
	{
		Optional<GroundingKnifeMeasurement> groundingKnifeMeasurement = 
				groundingKnifeMeasurementRepository.findById(id);
		
		Long pid = null;
		
		if(groundingKnifeMeasurement.isPresent())
		{
			pid = groundingKnifeMeasurement.get().getGroundingKnife().getId();
			groundingKnifeMeasurementRepository.delete(groundingKnifeMeasurement.get());
		}
	    return read(pid, model);
	}	
	
	@GetMapping("/exportToCSV")
	public void export(HttpServletResponse response, Model model) throws IOException {
		response.setContentType("text/csv");
		//  how it gets saved
		response.addHeader("Content-Disposition", "attachment; filename = \"GroundingKnifeMeasurements.csv\"");
		// "attachment; filename = \"Separators.csv\"" is the header 
		List<ExportableToCSV> data = new ArrayList<>();
		data.addAll(groundingKnifeMeasurementRepository.findAll()); // gets every record in the separator table and adds it to the list that gets exported
		csvFileGenerator.write(getHeadings(), data, response.getWriter()); // write to the response object with the header
	}
	// controller handles the headings, entities handle the data
	
	private static String[] getHeadings() {
		String[] fields = new String[6];
		// fields at position zero
		fields[0] = "Id";
		fields[1] = "Parent Id";
		fields[2] = "Time Stamp";
		fields[3] = "R Radius";
		fields[4] = "Initial Temperature";
		fields[5] = "Measured Temperature";
		return fields;
	}

	@GetMapping("/exportToPDF")
	public void exportToPDF(HttpServletResponse response, Model model) throws IOException {
		/* String outputFolder = "C:\\Users\\MY PC\\Downloads" + File.arrester + "Arresters.pdf";
		 OutputStream outputStream = new FileOutputStream(outputFolder);
		*/
		
		// getOutputStream() allows the PDF to be opened in the browser rather than a specific location on the server
		OutputStream outputStream = response.getOutputStream();

		response.addHeader("Content-Disposition", "attachment; filename = \"GroundingKnifeMeasurements.pdf\"");
		
	    ITextRenderer renderer = new ITextRenderer();
	    renderer.setDocumentFromString(parseThymeleafTemplate());
	    renderer.layout();
	    renderer.createPDF(outputStream);

	    outputStream.close();
	}
	
	
	private String parseThymeleafTemplate() {
	    ClassLoaderTemplateResolver templateResolver = new ClassLoaderTemplateResolver();
	    templateResolver.setSuffix(".html");
	    templateResolver.setTemplateMode(TemplateMode.HTML);

	    TemplateEngine templateEngine = new TemplateEngine();
	    templateEngine.setTemplateResolver(templateResolver);
	    
	    // model.addAttribute("equipment", "separator");
		List<GroundingKnifeMeasurement> groundingKnives = groundingKnifeMeasurementRepository.findAll();

	    Context context = new Context();
	    context.setVariable("equipmentObjects", groundingKnives);

	    return templateEngine.process("templates/groundingKnifeMeasurement/groundingKnifeMeasurement-pdf", context);
	}
}