package ro.upt.ac.mtb.measurements.switchequipment;

import java.util.Arrays;
import java.util.stream.Stream;

import org.hibernate.annotations.OnDelete;
import org.hibernate.annotations.OnDeleteAction;
import org.springframework.format.annotation.DateTimeFormat;

import jakarta.persistence.Entity;
import jakarta.persistence.FetchType;
import jakarta.persistence.GeneratedValue;
import jakarta.persistence.GenerationType;
import jakarta.persistence.Id;
import jakarta.persistence.JoinColumn;
import jakarta.persistence.ManyToOne;
import java.time.LocalDateTime;
import ro.upt.ac.mtb.equipments.services.ExportableToCSV;
import ro.upt.ac.mtb.equipments.switchequipment.SwitchEquipment;

@Entity
public class SwitchEquipmentMeasurement implements ExportableToCSV
{
	@Id
    @GeneratedValue(strategy = GenerationType.AUTO)
	private Long id;
	
	//relationship between the arrester with variable resistance measurement table and the arrester with variable resistance table
	@ManyToOne(fetch = FetchType.LAZY, optional = false)
	@JoinColumn(nullable = false)
	@OnDelete(action = OnDeleteAction.CASCADE)	
	private SwitchEquipment switchEquipment;
		
	// TODO
	// shows the current date and time stamp
	@DateTimeFormat(pattern = "yyyy-MM-dd'T'HH:mm:ss")
	private LocalDateTime timeStamp;
	// private Date timeStamp;
	// no way to use java.util.Date for the specific date and time without using deprecated methods or without using 
	// a local date to get a long value
	
	private Integer switchHeightOne;
	private Integer switchHeightTwo;
	private Integer switchBaseHeight;
	
	private Integer initialTemperature;
	private Integer measuredTemperature;
	
	public SwitchEquipmentMeasurement()
	{		
		timeStamp = LocalDateTime.now();
	}

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public SwitchEquipment getSwitchEquipment() {
		return switchEquipment;
	}

	public void setSwitchEquipment(SwitchEquipment switchEquipment) {
		this.switchEquipment = switchEquipment;
	}

	public LocalDateTime getTimeStamp() {
		return timeStamp;
	}

	public void setTimeStamp(LocalDateTime timeStamp) {
		this.timeStamp = timeStamp;
	}
	
	public Integer getSwitchHeightOne() {
		return switchHeightOne;
	}

	public void setSwitchHeightOne(Integer switchHeightOne) {
		this.switchHeightOne = switchHeightOne;
	}

	public Integer getSwitchHeightTwo() {
		return switchHeightTwo;
	}

	public void setSwitchHeightTwo(Integer switchHeightTwo) {
		this.switchHeightTwo = switchHeightTwo;
	}

	public Integer getSwitchBaseHeight() {
		return switchBaseHeight;
	}

	public void setSwitchBaseHeight(Integer switchBaseHeight) {
		this.switchBaseHeight = switchBaseHeight;
	}
	
	public Integer getTotalSwitchHeight() {
		int total =  (getSwitchHeightOne() == null ? 0 : getSwitchHeightOne()) + 
				(getSwitchHeightTwo() == null ? 0 : getSwitchHeightTwo()) + 
				(getSwitchBaseHeight() == null ? 0 : getSwitchBaseHeight());
		if (total == 0) {
			return null;
		} 
		
		return total;
	}

	public Integer getInitialTemperature() {
		return initialTemperature;
	}

	public void setInitialTemperature(Integer initialTemperature) {
		this.initialTemperature = initialTemperature;
	}

	public Integer getMeasuredTemperature() {
		return measuredTemperature;
	}

	public void setMeasuredTemperature(Integer measuredTemperature) {
		this.measuredTemperature = measuredTemperature;
	}

	@Override
	public String[] getFields() {
		String[] fields = new String[8];
		// fields at position zero
		fields[0] = String.valueOf(id);
		fields[1] = switchEquipment.getId().toString();
		fields[2] = timeStamp.toString();
		fields[3] = String.valueOf(switchHeightOne);
		fields[4] = String.valueOf(switchHeightTwo);
		fields[5] = String.valueOf(switchBaseHeight);
		fields[6] = String.valueOf(initialTemperature);
		fields[7] = String.valueOf(measuredTemperature);
		// concatenates 2 Arrays using Streams from arrester and measurement and returns the combined array
		return Stream.concat(Arrays.stream(fields), Arrays.stream(switchEquipment.getFields()))
				.toArray(String[]::new);
	}
}
