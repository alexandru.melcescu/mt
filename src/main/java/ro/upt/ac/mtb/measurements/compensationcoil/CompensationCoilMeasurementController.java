package ro.upt.ac.mtb.measurements.compensationcoil;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.thymeleaf.TemplateEngine;
import org.thymeleaf.context.Context;
import org.thymeleaf.templatemode.TemplateMode;
import org.thymeleaf.templateresolver.ClassLoaderTemplateResolver;
import org.xhtmlrenderer.pdf.ITextRenderer;

import jakarta.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.io.OutputStream;
import ro.upt.ac.mtb.equipments.compensationcoil.CompensationCoil;
import ro.upt.ac.mtb.equipments.compensationcoil.CompensationCoilRepository;
import ro.upt.ac.mtb.equipments.services.CSVFileGenerator;
import ro.upt.ac.mtb.equipments.services.ExportableToCSV;

@Controller
@RequestMapping("compensationCoilMeasurement")
public class CompensationCoilMeasurementController
{
	@Autowired
	CompensationCoilRepository compensationCoilRepository;

	@Autowired
	CompensationCoilMeasurementRepository compensationCoilMeasurementRepository;
	
	@Autowired
	CSVFileGenerator csvFileGenerator;
	
	private static final String EQUIPMENT = "compensationCoilMeasurement";
	
	@GetMapping("/graphDimensionAndHeight/{id}") 
	public String graphDimensionAndHeight(@PathVariable("id") Long id, Model model) {
		// load all the measurements for the id
		Optional<CompensationCoil> compensationCoil = 
				compensationCoilRepository.findById(id);
		
		List<CompensationCoilMeasurement> compensationCoils = 
				compensationCoilMeasurementRepository.findByCompensationCoilOrderByTimeStamp(compensationCoil.get());
		
		// setting the measurement list in the model
		if(compensationCoils.size() != 0) {
			model.addAttribute("equipmentCode", compensationCoil.get().getArticleNumber());
			model.addAttribute("labels", compensationCoils.stream().map(m -> m.getTimeStamp()).collect(Collectors.toList()));
			model.addAttribute("data", compensationCoils.stream().map(m -> m.getCableDimension()).collect(Collectors.toList()));
			model.addAttribute("data", compensationCoils.stream().map(m -> m.getCableLength()).collect(Collectors.toList()));
		}
		return EQUIPMENT + "/" + "graphDimensionAndHeight";
	}
	
	@PostMapping("/search")
	public String search(@Param("searchTerm") String searchTerm, Model model) 
	{
		//String searchTerm = (String) model.getAttribute("searchTerm");
		List<CompensationCoilMeasurement> data = compensationCoilMeasurementRepository.search(searchTerm);
		if (data.size() != 0) {
		    model.addAttribute("equipmentObjects", data);
		}
		System.out.println(searchTerm);
		model.addAttribute("equipment", EQUIPMENT);
		model.addAttribute("search", Boolean.TRUE);
		return EQUIPMENT + "/" + EQUIPMENT + "-read";
	}
	
	@GetMapping("/create/{pid}")
	public String create(@PathVariable("pid") Long pid,
			CompensationCoilMeasurement compensationCoilMeasurement, 
			Model model)
	{
		model.addAttribute("equipment", EQUIPMENT);
		model.addAttribute("pid", ""+pid.longValue());
		
		model.addAttribute("equipmentObject", compensationCoilMeasurement);
		
		return EQUIPMENT + "/" + EQUIPMENT + "-create";
	}

	@PostMapping("/create-save/{pid}")
	public String createSave(@PathVariable("pid") Long pid,
			@Validated CompensationCoilMeasurement compensationCoilMeasurement, 
			BindingResult result, Model model)
	{
		if(result.hasErrors())
		{
			System.out.println(result.toString());
			return EQUIPMENT + "/" + EQUIPMENT + "-create";
		} 
		
		//System.out.println(CompensationCoilMeasurement);
		//System.out.println("id "+CompensationCoilMeasurement.getId());
		//System.out.println("ts "+CompensationCoilMeasurement.getTimeStamp());
		//System.out.println("parent "+CompensationCoilMeasurement.getCompensationCoil());
		
		// getting the parent equipment
		Optional<CompensationCoil> compensationCoil = 
				compensationCoilRepository.findById(pid);
		// setting the parent
		compensationCoilMeasurement.setCompensationCoil(compensationCoil.get());
		// saving the object
		compensationCoilMeasurementRepository.save(compensationCoilMeasurement);
				
	    return read(pid,model);
	}
	
	@GetMapping("/read/{pid}")
	public String read(@PathVariable("pid") Long pid,Model model) 
	{
		model.addAttribute("equipment", EQUIPMENT);
		model.addAttribute("pid", pid);
		
		// getting the parent equipment
		Optional<CompensationCoil> compensationCoil = 
				compensationCoilRepository.findById(pid);
		
		// getting the measurements of the equipment 
		List<CompensationCoilMeasurement> compensationCoils = 
				compensationCoilMeasurementRepository.findByCompensationCoilOrderByTimeStamp(compensationCoil.get());
		
		// setting the measurement list in the model
		if(compensationCoils.size() != 0) 
		{
		    model.addAttribute("equipmentObjects", compensationCoils);
		}
		//System.out.println("hello");
		return EQUIPMENT + "/" + EQUIPMENT + "-read";
	}
	
	@GetMapping("/edit/{id}")
	public String edit(@PathVariable("id") Long id, Model model) 
	{
		model.addAttribute("equipment", EQUIPMENT); 
		Optional<CompensationCoilMeasurement> compensationCoilMeasurement = 
				compensationCoilMeasurementRepository.findById(id);
	    model.addAttribute("equipmentObject", compensationCoilMeasurement.get());
	    
		return EQUIPMENT + "/" + EQUIPMENT + "-update";
	}
	
	@PostMapping("/update/{id}")
	// removed the pid from the path variable because you have to load the measurement so that you can get 
	// its parent so that you can set the parent before the update because the parent can't be null
	public String update(@PathVariable("id") Long id, 
			@Validated CompensationCoilMeasurement compensationCoilMeasurement, BindingResult result, Model model) 
	{
		Optional<CompensationCoilMeasurement> compensationCoilMeasurementOld = 
				compensationCoilMeasurementRepository.findById(id);
		
		long pid = 0L;
		// defensive code to prevent 500 error
		if (compensationCoilMeasurementOld.isPresent()) {
			// load the parent object
			pid = compensationCoilMeasurementOld.get().getCompensationCoil().getId();
			// fetched the pre-updated measurement and get the parent id
			// set the parent object before the update otherwise you get field is not nullable
			compensationCoilMeasurement.setCompensationCoil(compensationCoilMeasurementOld.get().getCompensationCoil());
		}
		if(result.hasErrors()) {
	    	compensationCoilMeasurement.setId(id);
			return EQUIPMENT + "/" + EQUIPMENT + "-update";
	    }
	    
	    compensationCoilMeasurementRepository.save(compensationCoilMeasurement);
	    
//	    return read(CompensationCoilMeasurementOld.get().getCompensationCoil().getId(),model);
	    return read(pid,model);
	}
	
	@GetMapping("/delete/{id}")
	public String delete(@PathVariable("id") Long id, Model model) 
	{
		Optional<CompensationCoilMeasurement> compensationCoilMeasurement = 
				compensationCoilMeasurementRepository.findById(id);
		
		Long pid = null;
		
		if(compensationCoilMeasurement.isPresent())
		{
			pid = compensationCoilMeasurement.get().getCompensationCoil().getId();
			compensationCoilMeasurementRepository.delete(compensationCoilMeasurement.get());
		}
	    return read(pid, model);
	}	
	
	@GetMapping("/exportToCSV")
	public void exportToCSV(HttpServletResponse response, Model model) throws IOException {
		response.setContentType("text/csv");
		//  how it gets saved
		response.addHeader("Content-Disposition", "attachment; filename = \"CompensationCoilMeasurements.csv\"");
		// "attachment; filename = \"Separators.csv\"" is the header 
		List<ExportableToCSV> data = new ArrayList<>();
		data.addAll(compensationCoilMeasurementRepository.findAll()); // gets every record in the separator table and adds it to the list that gets exported
		csvFileGenerator.write(getHeadings(), data, response.getWriter()); // write to the response object with the header
	}
	// controller handles the headings, entities handle the data

	private static String[] getHeadings() {
		String[] fields = new String[7];
		// fields at position zero
		fields[0] = "Id";
		fields[1] = "Parent Id";
		fields[2] = "Time Stamp";
		fields[3] = "Cable Dimension";
		fields[4] = "Cable Length";
		fields[5] = "Initial Temperature";
		fields[6] = "Measured Temperature";
		return fields;
	}

	@GetMapping("/exportToPDF")
	public void exportToPDF(HttpServletResponse response, Model model) throws IOException {
		/* String outputFolder = "C:\\Users\\MY PC\\Downloads" + File.arrester + "Arresters.pdf";
		 OutputStream outputStream = new FileOutputStream(outputFolder);
		*/
		
		// getOutputStream() allows the PDF to be opened in the browser rather than a specific location on the server
		OutputStream outputStream = response.getOutputStream();

		response.addHeader("Content-Disposition", "attachment; filename = \"CompensationCoilMeasurements.pdf\"");
		
	    ITextRenderer renderer = new ITextRenderer();
	    renderer.setDocumentFromString(parseThymeleafTemplate());
	    renderer.layout();
	    renderer.createPDF(outputStream);

	    outputStream.close();
	}
	
	
	private String parseThymeleafTemplate() {
	    ClassLoaderTemplateResolver templateResolver = new ClassLoaderTemplateResolver();
	    templateResolver.setSuffix(".html");
	    templateResolver.setTemplateMode(TemplateMode.HTML);

	    TemplateEngine templateEngine = new TemplateEngine();
	    templateEngine.setTemplateResolver(templateResolver);
	    
	    // model.addAttribute("equipment", "separator");
		List<CompensationCoilMeasurement> compensationCoils = compensationCoilMeasurementRepository.findAll();

	    Context context = new Context();
	    context.setVariable("equipmentObjects", compensationCoils);

	    return templateEngine.process("templates/compensationCoilMeasurement/compensationCoilMeasurement-pdf", context);
	}
}
