package ro.upt.ac.mtb.measurements.separatoractuationdevice;

import java.util.List;
import java.util.Optional;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import ro.upt.ac.mtb.equipments.separatoractuationdevice.SeparatorActuationDevice;

@Repository
public interface SeparatorActuationDeviceMeasurementRepository extends JpaRepository<SeparatorActuationDeviceMeasurement,Integer> {
	Optional<SeparatorActuationDeviceMeasurement> findById(Long id);
	
	@Query("SELECT sadm FROM SeparatorActuationDeviceMeasurement sadm"
			+ " WHERE sadm.timeStamp LIKE %?1%"
			)
	List<SeparatorActuationDeviceMeasurement> search(String searchTerm);

	List<SeparatorActuationDeviceMeasurement> findBySeparatorActuationDeviceOrderByTimeStamp(SeparatorActuationDevice 
			separatorActuationDevice);
}
