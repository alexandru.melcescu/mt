package ro.upt.ac.mtb.measurements.switchactuationdevice;

import java.util.Arrays;
import java.util.stream.Stream;

import org.hibernate.annotations.OnDelete;
import org.hibernate.annotations.OnDeleteAction;
import org.springframework.format.annotation.DateTimeFormat;

import jakarta.persistence.Entity;
import jakarta.persistence.FetchType;
import jakarta.persistence.GeneratedValue;
import jakarta.persistence.GenerationType;
import jakarta.persistence.Id;
import jakarta.persistence.JoinColumn;
import jakarta.persistence.ManyToOne;
import java.time.LocalDateTime;
import ro.upt.ac.mtb.equipments.services.ExportableToCSV;
import ro.upt.ac.mtb.equipments.switchactuationdevice.SwitchActuationDevice;

@Entity
public class SwitchActuationDeviceMeasurement implements ExportableToCSV
{
	@Id
    @GeneratedValue(strategy = GenerationType.AUTO)
	private Long id;
	
	//relationship between the arrester with variable resistance measurement table and the arrester with variable resistance table
	@ManyToOne(fetch = FetchType.LAZY, optional = false)
	@JoinColumn(nullable = false)
	@OnDelete(action = OnDeleteAction.CASCADE)	
	private SwitchActuationDevice switchActuationDevice;
		
	// TODO
	// shows the current date and time stamp
	@DateTimeFormat(pattern = "yyyy-MM-dd'T'HH:mm")
	private LocalDateTime timeStamp;
	// private Date timeStamp;
	// no way to use java.util.Date for the specific date and time without using deprecated methods or without using 
	// a local date to get a long value
	
	private Double fourtyPercentOfTheLengthOfTheBottomOfTheSwitch;
	private Double widthOne;
	private Double widthTwo;
	
	private Integer initialTemperature;
	private Integer measuredTemperature;
	
	public SwitchActuationDeviceMeasurement()
	{		
		timeStamp = LocalDateTime.now();
	}

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public SwitchActuationDevice getSwitchActuationDevice() {
		return switchActuationDevice;
	}

	public void setSwitchActuationDevice(SwitchActuationDevice switchActuationDevice) {
		this.switchActuationDevice = switchActuationDevice;
	}

	public LocalDateTime getTimeStamp() {
		return timeStamp;
	}

	public void setTimeStamp(LocalDateTime timeStamp) {
		this.timeStamp = timeStamp;
	}
	
	public Double getFourtyPercentOfTheLengthOfTheBottomOfTheSwitch() {
		return fourtyPercentOfTheLengthOfTheBottomOfTheSwitch;
	}

	public void setFourtyPercentOfTheLengthOfTheBottomOfTheSwitch(Double fourtyPercentOfTheLengthOfTheBottomOfTheSwitch) {
		this.fourtyPercentOfTheLengthOfTheBottomOfTheSwitch = fourtyPercentOfTheLengthOfTheBottomOfTheSwitch;
	}
	
	public Double getWidthOne() {
		return widthOne;
	}

	public void setWidthOne(Double widthOne) {
		this.widthOne = widthOne;
	}

	public Double getWidthTwo() {
		return widthTwo;
	}

	public void setWidthTwo(Double widthTwo) {
		this.widthTwo = widthTwo;
	}

	public Double getTotalWidth() {
		double total =  (getWidthOne() == null ? 0 : getWidthOne()) + 
				(getWidthTwo() == null ? 0 : getWidthTwo());
		if (total == 0) {
			return null;
		} 
		
		return total;
	}
	
	public Integer getInitialTemperature() {
		return initialTemperature;
	}

	public void setInitialTemperature(Integer initialTemperature) {
		this.initialTemperature = initialTemperature;
	}

	public Integer getMeasuredTemperature() {
		return measuredTemperature;
	}

	public void setMeasuredTemperature(Integer measuredTemperature) {
		this.measuredTemperature = measuredTemperature;
	}

	@Override
	public String[] getFields() {
		String[] fields = new String[7];
		// fields at position zero
		fields[0] = String.valueOf(id);
		fields[1] = switchActuationDevice.getId().toString();
		fields[2] = timeStamp.toString();
		fields[3] = String.valueOf(fourtyPercentOfTheLengthOfTheBottomOfTheSwitch);
		fields[3] = String.valueOf(widthOne);
		fields[4] = String.valueOf(widthTwo);
		fields[5] = String.valueOf(initialTemperature);
		fields[6] = String.valueOf(measuredTemperature);
		// concatenates 2 Arrays using Streams from arrester and measurement and returns the combined array
		return Stream.concat(Arrays.stream(fields), Arrays.stream(switchActuationDevice.getFields()))
				.toArray(String[]::new);
	}
}
