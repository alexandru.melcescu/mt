package ro.upt.ac.mtb.measurements.mixedmeasuringtransformer;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.thymeleaf.TemplateEngine;
import org.thymeleaf.context.Context;
import org.thymeleaf.templatemode.TemplateMode;
import org.thymeleaf.templateresolver.ClassLoaderTemplateResolver;
import org.xhtmlrenderer.pdf.ITextRenderer;

import jakarta.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.io.OutputStream;
import ro.upt.ac.mtb.equipments.mixedmeasuringtransformer.MixedMeasuringTransformer;
import ro.upt.ac.mtb.equipments.mixedmeasuringtransformer.MixedMeasuringTransformerRepository;
import ro.upt.ac.mtb.equipments.services.CSVFileGenerator;
import ro.upt.ac.mtb.equipments.services.ExportableToCSV;

@Controller
@RequestMapping("mixedMeasuringTransformerMeasurement")
public class MixedMeasuringTransformerMeasurementController
{
	@Autowired
	MixedMeasuringTransformerRepository mixedMeasuringTransformerRepository;

	@Autowired
	MixedMeasuringTransformerMeasurementRepository mixedMeasuringTransformerMeasurementRepository;
	
	@Autowired
	CSVFileGenerator csvFileGenerator;
	
	private static final String EQUIPMENT = "mixedMeasuringTransformerMeasurement";
	
	@GetMapping("/graphDistanceAndCenter/{id}") 
	public String graphDistanceAndCenter(@PathVariable("id") Long id, Model model) {
		// load all the measurements for the id
		Optional<MixedMeasuringTransformer> mixedMeasuringTransformer = 
				mixedMeasuringTransformerRepository.findById(id);
		
		List<MixedMeasuringTransformerMeasurement> mixedMeasuringTransformers = 
				mixedMeasuringTransformerMeasurementRepository.findByMixedMeasuringTransformerOrderByTimeStamp(mixedMeasuringTransformer.get());
		
		// setting the measurement list in the model
		if(mixedMeasuringTransformers.size() != 0) {
			model.addAttribute("equipmentCode", mixedMeasuringTransformer.get().getEquipmentSeries());
			model.addAttribute("labels", mixedMeasuringTransformers.stream().map(m -> m.getTimeStamp()).collect(Collectors.toList()));
			model.addAttribute("distanceData", mixedMeasuringTransformers.stream().map(m -> m.getMinimumCreepageDistance()).collect(Collectors.toList()));
			model.addAttribute("centerData", mixedMeasuringTransformers.stream().map(m -> m.getGravityCenter()).collect(Collectors.toList()));
		}
		return EQUIPMENT + "/" + "graphDistanceAndCenter";
	}
	
	@PostMapping("/search")
	public String search(@Param("searchTerm") String searchTerm, Model model) 
	{
		//String searchTerm = (String) model.getAttribute("searchTerm");
		List<MixedMeasuringTransformerMeasurement> data = mixedMeasuringTransformerMeasurementRepository.search(searchTerm);
		if (data.size() != 0) {
		    model.addAttribute("equipmentObjects", data);
		}
		System.out.println(searchTerm);
		model.addAttribute("equipment", EQUIPMENT);
		model.addAttribute("search", Boolean.TRUE);
		return EQUIPMENT + "/" + EQUIPMENT + "-read";
	}
	
	@GetMapping("/create/{pid}")
	public String create(@PathVariable("pid") Long pid,
			MixedMeasuringTransformerMeasurement mixedMeasuringTransformerMeasurement, 
			Model model)
	{
		model.addAttribute("equipment", EQUIPMENT);
		model.addAttribute("pid", ""+pid.longValue());
		
		model.addAttribute("equipmentObject", mixedMeasuringTransformerMeasurement);
		
		return EQUIPMENT + "/" + EQUIPMENT + "-create";
	}

	@PostMapping("/create-save/{pid}")
	public String createSave(@PathVariable("pid") Long pid,
			@Validated MixedMeasuringTransformerMeasurement mixedMeasuringTransformerMeasurement, 
			BindingResult result, Model model)
	{
		if(result.hasErrors())
		{
			System.out.println(result.toString());
			return EQUIPMENT + "/" + EQUIPMENT + "-create";
		} 
		
		//System.out.println(mixedMeasuringTransformerMeasurement);
		//System.out.println("id "+mixedMeasuringTransformerMeasurement.getId());
		//System.out.println("ts "+mixedMeasuringTransformerMeasurement.getTimeStamp());
		//System.out.println("parent "+mixedMeasuringTransformerMeasurement.getMixedMeasuringTransformer());
		
		// getting the parent equipment
		Optional<MixedMeasuringTransformer> mixedMeasuringTransformer = 
				mixedMeasuringTransformerRepository.findById(pid);
		// setting the parent
		mixedMeasuringTransformerMeasurement.setMixedMeasuringTransformer(mixedMeasuringTransformer.get());
		// saving the object
		mixedMeasuringTransformerMeasurementRepository.save(mixedMeasuringTransformerMeasurement);
				
	    return read(pid,model);
	}
	
	@GetMapping("/read/{pid}")
	public String read(@PathVariable("pid") Long pid,Model model) 
	{
		model.addAttribute("equipment", EQUIPMENT);
		model.addAttribute("pid", pid);
		
		// getting the parent equipment
		Optional<MixedMeasuringTransformer> mixedMeasuringTransformer = 
				mixedMeasuringTransformerRepository.findById(pid);
		
		// getting the measurements of the equipment 
		List<MixedMeasuringTransformerMeasurement> mixedMeasuringTransformers = 
				mixedMeasuringTransformerMeasurementRepository.findByMixedMeasuringTransformerOrderByTimeStamp(mixedMeasuringTransformer.get());
		
		// setting the measurement list in the model
		if(mixedMeasuringTransformers.size() != 0) 
		{
		    model.addAttribute("equipmentObjects", mixedMeasuringTransformers);
		}
		//System.out.println("hello");
		return EQUIPMENT + "/" + EQUIPMENT + "-read";
	}
	
	@GetMapping("/edit/{id}")
	public String edit(@PathVariable("id") Long id, Model model) 
	{
		model.addAttribute("equipment", EQUIPMENT); 
		Optional<MixedMeasuringTransformerMeasurement> mixedMeasuringTransformerMeasurement = 
				mixedMeasuringTransformerMeasurementRepository.findById(id);
	    model.addAttribute("equipmentObject", mixedMeasuringTransformerMeasurement.get());
	    
		return EQUIPMENT + "/" + EQUIPMENT + "-update";
	}
	
	@PostMapping("/update/{id}")
	// removed the pid from the path variable because you have to load the measurement so that you can get 
	// its parent so that you can set the parent before the update because the parent can't be null
	public String update(@PathVariable("id") Long id, 
			@Validated MixedMeasuringTransformerMeasurement mixedMeasuringTransformerMeasurement, BindingResult result, Model model) 
	{
		Optional<MixedMeasuringTransformerMeasurement> mixedMeasuringTransformerMeasurementOld = 
				mixedMeasuringTransformerMeasurementRepository.findById(id);
		
		long pid = 0L;
		// defensive code to prevent 500 error
		if (mixedMeasuringTransformerMeasurementOld.isPresent()) {
			// load the parent object
			pid = mixedMeasuringTransformerMeasurementOld.get().getMixedMeasuringTransformer().getId();
			// fetched the pre-updated measurement and get the parent id
			// set the parent object before the update otherwise you get field is not nullable
			mixedMeasuringTransformerMeasurement.setMixedMeasuringTransformer(mixedMeasuringTransformerMeasurementOld.get().getMixedMeasuringTransformer());
		}
		if(result.hasErrors()) {
	    	mixedMeasuringTransformerMeasurement.setId(id);
			return EQUIPMENT + "/" + EQUIPMENT + "-update";
	    }
	    
	    mixedMeasuringTransformerMeasurementRepository.save(mixedMeasuringTransformerMeasurement);
	    
//	    return read(mixedMeasuringTransformerMeasurementOld.get().getMixedMeasuringTransformer().getId(),model);
	    return read(pid,model);
	}
	
	@GetMapping("/delete/{id}")
	public String delete(@PathVariable("id") Long id, Model model) 
	{
		Optional<MixedMeasuringTransformerMeasurement> mixedMeasuringTransformerMeasurement = 
				mixedMeasuringTransformerMeasurementRepository.findById(id);
		
		Long pid = null;
		
		if(mixedMeasuringTransformerMeasurement.isPresent())
		{
			pid = mixedMeasuringTransformerMeasurement.get().getMixedMeasuringTransformer().getId();
			mixedMeasuringTransformerMeasurementRepository.delete(mixedMeasuringTransformerMeasurement.get());
		}
	    return read(pid, model);
	}	
	
	@GetMapping("/exportToCSV")
	public void export(HttpServletResponse response, Model model) throws IOException {
		response.setContentType("text/csv");
		//  how it gets saved
		response.addHeader("Content-Disposition", "attachment; filename = \"MixedMeasuringTransformerMeasurements.csv\"");
		// "attachment; filename = \"Separators.csv\"" is the header 
		List<ExportableToCSV> data = new ArrayList<>();
		data.addAll(mixedMeasuringTransformerMeasurementRepository.findAll()); // gets every record in the separator table and adds it to the list that gets exported
		csvFileGenerator.write(getHeadings(), data, response.getWriter()); // write to the response object with the header
	}
	// controller handles the headings, entities handle the data
	
	private static String[] getHeadings() {
		String[] fields = new String[9];
		// fields at position zero
		fields[0] = "Id";
		fields[1] = "Parent Id";
		fields[2] = "Time Stamp";
		fields[3] = "Primary Terminal Material";
		fields[4] = "Minimum Creepage Distance";
		fields[5] = "Gravity Center";
		fields[6] = "Insulation Material";
		fields[7] = "Initial Temperature";
		fields[8] = "Measured Temperature";
		return fields;
	}

	@GetMapping("/exportToPDF")
	public void exportToPDF(HttpServletResponse response, Model model) throws IOException {
		/* String outputFolder = "C:\\Users\\MY PC\\Downloads" + File.arrester + "Arresters.pdf";
		 OutputStream outputStream = new FileOutputStream(outputFolder);
		*/
		
		// getOutputStream() allows the PDF to be opened in the browser rather than a specific location on the server
		OutputStream outputStream = response.getOutputStream();

		response.addHeader("Content-Disposition", "attachment; filename = \"MixedMeasuringTransformerMeasurements.pdf\"");
		
	    ITextRenderer renderer = new ITextRenderer();
	    renderer.setDocumentFromString(parseThymeleafTemplate());
	    renderer.layout();
	    renderer.createPDF(outputStream);

	    outputStream.close();
	}
	
	
	private String parseThymeleafTemplate() {
	    ClassLoaderTemplateResolver templateResolver = new ClassLoaderTemplateResolver();
	    templateResolver.setSuffix(".html");
	    templateResolver.setTemplateMode(TemplateMode.HTML);

	    TemplateEngine templateEngine = new TemplateEngine();
	    templateEngine.setTemplateResolver(templateResolver);
	    
	    // model.addAttribute("equipment", "separator");
		List<MixedMeasuringTransformerMeasurement> mixedMeasuringTransformers = mixedMeasuringTransformerMeasurementRepository.findAll();

	    Context context = new Context();
	    context.setVariable("equipmentObjects", mixedMeasuringTransformers);

	    return templateEngine.process("templates/mixedMeasuringTransformerMeasurement/mixedMeasuringTransformerMeasurement-pdf", context);
	}
}