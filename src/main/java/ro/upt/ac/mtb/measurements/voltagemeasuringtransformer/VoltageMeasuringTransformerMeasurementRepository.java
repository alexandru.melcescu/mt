package ro.upt.ac.mtb.measurements.voltagemeasuringtransformer;

import java.util.List;
import java.util.Optional;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import ro.upt.ac.mtb.equipments.voltagemeasuringtransformer.VoltageMeasuringTransformer;

@Repository
public interface VoltageMeasuringTransformerMeasurementRepository extends JpaRepository<VoltageMeasuringTransformerMeasurement,Long> {
	Optional<VoltageMeasuringTransformerMeasurement> findById(Long id);
	
	@Query("SELECT vmtm FROM VoltageMeasuringTransformerMeasurement vmtm"
			+ " WHERE vmtm.timeStamp LIKE %?1%"
			+ " OR vmtm.voltageMeasuringTransformerHeightOne LIKE %?1%"
			+ " OR vmtm.voltageMeasuringTransformerHeightTwo LIKE %?1%"
			)
	List<VoltageMeasuringTransformerMeasurement> search(String searchTerm);

	List<VoltageMeasuringTransformerMeasurement> findByVoltageMeasuringTransformerOrderByTimeStamp(
			VoltageMeasuringTransformer voltageMeasuringTransformer);

}
