package ro.upt.ac.mtb.measurements.arresterwithvariableresistance;

import java.util.List;
import java.util.Optional;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import ro.upt.ac.mtb.equipments.arresterwithvariableresistance.ArresterWithVariableResistance;

@Repository
public interface ArresterWithVariableResistanceMeasurementRepository extends JpaRepository<ArresterWithVariableResistanceMeasurement,Long> 
{
	Optional<ArresterWithVariableResistanceMeasurement> findById(Long id);
	
	@Query("SELECT awvrm FROM ArresterWithVariableResistanceMeasurement awvrm"
			+ " WHERE awvrm.timeStamp LIKE %?1%"
			+ " OR awvrm.heightOneInMillimeters LIKE %?1%"
			+ " OR awvrm.heightTwoInMillimeters LIKE %?1%"
			+ " OR awvrm.heightThreeInMillimeters LIKE %?1%"
			+ " OR awvrm.weightInKilograms LIKE %?1%"
			+ " OR awvrm.creepageDistanceInMillimeters LIKE %?1%"
			)
	List<ArresterWithVariableResistanceMeasurement> search(String searchTerm);
	
	List<ArresterWithVariableResistanceMeasurement> findByArresterWithVariableResistanceOrderByTimeStamp(ArresterWithVariableResistance arresterWithVariableResistance);	
}

