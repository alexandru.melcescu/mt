package ro.upt.ac.mtb.measurements.separatoractuationdevice;

import java.util.Arrays;
import java.util.stream.Stream;

import org.hibernate.annotations.OnDelete;
import org.hibernate.annotations.OnDeleteAction;
import org.springframework.format.annotation.DateTimeFormat;

import jakarta.persistence.Entity;
import jakarta.persistence.FetchType;
import jakarta.persistence.GeneratedValue;
import jakarta.persistence.GenerationType;
import jakarta.persistence.Id;
import jakarta.persistence.JoinColumn;
import jakarta.persistence.ManyToOne;
import java.time.LocalDateTime;
import ro.upt.ac.mtb.equipments.separatoractuationdevice.SeparatorActuationDevice;
import ro.upt.ac.mtb.equipments.services.ExportableToCSV;

@Entity
public class SeparatorActuationDeviceMeasurement implements ExportableToCSV
{
	@Id
    @GeneratedValue(strategy = GenerationType.AUTO)
	private Long id;
	
	//relationship between the arrester with variable resistance measurement table and the arrester with variable resistance table
	@ManyToOne(fetch = FetchType.LAZY, optional = false)
	@JoinColumn(nullable = false)
	@OnDelete(action = OnDeleteAction.CASCADE)	
	private SeparatorActuationDevice separatorActuationDevice;
		
	// TODO
	// shows the current date and time stamp
	@DateTimeFormat(pattern = "yyyy-MM-dd'T'HH:mm")
	private LocalDateTime timeStamp;
	// private Date timeStamp;
	// no way to use java.util.Date for the specific date and time without using deprecated methods or without using 
	// a local date to get a long value
	
	private Integer initialTemperature;
	private Integer measuredTemperature;
	
	public SeparatorActuationDeviceMeasurement()
	{		
		timeStamp = LocalDateTime.now();
	}

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public SeparatorActuationDevice getSeparatorActuationDevice() {
		return separatorActuationDevice;
	}

	public void setSeparatorActuationDevice(SeparatorActuationDevice separatorActuationDevice) {
		this.separatorActuationDevice = separatorActuationDevice;
	}

	public LocalDateTime getTimeStamp() {
		return timeStamp;
	}

	public void setTimeStamp(LocalDateTime timeStamp) {
		this.timeStamp = timeStamp;
	}	
	
	public Integer getInitialTemperature() {
		return initialTemperature;
	}

	public void setInitialTemperature(Integer initialTemperature) {
		this.initialTemperature = initialTemperature;
	}

	public Integer getMeasuredTemperature() {
		return measuredTemperature;
	}

	public void setMeasuredTemperature(Integer measuredTemperature) {
		this.measuredTemperature = measuredTemperature;
	}
	
	@Override
	public String[] getFields() {
		String[] fields = new String[5];
		// fields at position zero
		fields[0] = String.valueOf(id);
		fields[1] = separatorActuationDevice.getId().toString();
		fields[2] = timeStamp.toString();
		fields[3] = String.valueOf(initialTemperature);
		fields[4] = String.valueOf(measuredTemperature);
		// concatenates 2 Arrays using Streams from arrester and measurement and returns the combined array
		return Stream.concat(Arrays.stream(fields), Arrays.stream(separatorActuationDevice.getFields()))
				.toArray(String[]::new);
	}
}
