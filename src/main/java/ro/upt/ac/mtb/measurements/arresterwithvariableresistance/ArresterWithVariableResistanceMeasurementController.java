package ro.upt.ac.mtb.measurements.arresterwithvariableresistance;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.thymeleaf.TemplateEngine;
import org.thymeleaf.context.Context;
import org.thymeleaf.templatemode.TemplateMode;
import org.thymeleaf.templateresolver.ClassLoaderTemplateResolver;
import org.xhtmlrenderer.pdf.ITextRenderer;

import jakarta.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.io.OutputStream;
import java.time.format.DateTimeFormatter;
import ro.upt.ac.mtb.equipments.arresterwithvariableresistance.ArresterWithVariableResistance;
import ro.upt.ac.mtb.equipments.arresterwithvariableresistance.ArresterWithVariableResistanceRepository;
import ro.upt.ac.mtb.equipments.services.CSVFileGenerator;
import ro.upt.ac.mtb.equipments.services.ExportableToCSV;

@Controller
@RequestMapping("arresterWithVariableResistanceMeasurement")
public class ArresterWithVariableResistanceMeasurementController
{
	@Autowired
	ArresterWithVariableResistanceRepository arresterWithVariableResistanceRepository;

	@Autowired
	ArresterWithVariableResistanceMeasurementRepository arresterWithVariableResistanceMeasurementRepository;
	
	@Autowired
	CSVFileGenerator csvFileGenerator;
	
	private static final String EQUIPMENT = "arresterWithVariableResistanceMeasurement";
	
	@GetMapping("/graphHeight/{id}") 
	public String graphHeight(@PathVariable("id") Long id, Model model) {
		// load all the measurements for the id
		Optional<ArresterWithVariableResistance> arresterWithVariableResistance = 
				arresterWithVariableResistanceRepository.findById(id);
		
		List<ArresterWithVariableResistanceMeasurement> arrestersWithVariableResistance = 
				arresterWithVariableResistanceMeasurementRepository.findByArresterWithVariableResistanceOrderByTimeStamp(arresterWithVariableResistance.get());
		
		// setting the measurement list in the model
		if(arrestersWithVariableResistance.size() != 0) {
			model.addAttribute("equipmentCode", arresterWithVariableResistance.get().getEquipmentCode());
//			model.addAttribute("labels", arrestersWithVariableResistance.stream().map(m -> m.getTimeStamp()).collect(Collectors.toList()));
//			model.addAttribute("data", arrestersWithVariableResistance.stream().map(m -> m.getTotalHeightInMillimeters()).collect(Collectors.toList()));
			// modifications for a bubble graph
			List<Map<String, Object>> data = new ArrayList<>();
//			int x = 1;
			for(ArresterWithVariableResistanceMeasurement m : arrestersWithVariableResistance) {
				Map<String, Object> record = new HashMap<>();
				int date = 0;
				try {
					DateTimeFormatter formatter = DateTimeFormatter.ofPattern("yyyyMMdd");
					String text = m.getTimeStamp().format(formatter);
					date = Integer.parseInt(text);
				} catch (NumberFormatException e) {
					e.printStackTrace();
				}
				record.put("x", date);
//				record.put("x", x++);
				record.put("y", m.getTotalHeightInMillimeters());
				record.put("r", m.getMeasuredTemperature());
				data.add(record);
			}
			model.addAttribute("data", data);
		}
		return EQUIPMENT + "/" + "graphHeight";
	}
	
	@PostMapping("/search")
	public String search(@Param("searchTerm") String searchTerm, Model model) 
	{
		//String searchTerm = (String) model.getAttribute("searchTerm");
		List<ArresterWithVariableResistanceMeasurement> data = arresterWithVariableResistanceMeasurementRepository.search(searchTerm);
		if (data.size() != 0) {
		    model.addAttribute("equipmentObjects", data);
		}
		System.out.println(searchTerm);
		model.addAttribute("equipment", EQUIPMENT);
		model.addAttribute("search", Boolean.TRUE);
		return EQUIPMENT + "/" + EQUIPMENT + "-read";
	}
	
	@GetMapping("/create/{pid}")
	public String create(@PathVariable("pid") Long pid,
			ArresterWithVariableResistanceMeasurement arresterWithVariableResistanceMeasurement, 
			Model model)
	{
		model.addAttribute("equipment", EQUIPMENT);
		model.addAttribute("pid", ""+pid.longValue());
		
		model.addAttribute("equipmentObject", arresterWithVariableResistanceMeasurement);
		
		return EQUIPMENT + "/" + EQUIPMENT + "-create";
	}

	@PostMapping("/create-save/{pid}")
	public String createSave(@PathVariable("pid") Long pid,
			@Validated ArresterWithVariableResistanceMeasurement arresterWithVariableResistanceMeasurement, 
			BindingResult result, Model model)
	{
		if(result.hasErrors())
		{
			System.out.println(result.toString());
			return EQUIPMENT + "/" + EQUIPMENT + "-create";
		} 
		
		//System.out.println(arresterWithVariableResistanceMeasurement);
		//System.out.println("id "+arresterWithVariableResistanceMeasurement.getId());
		//System.out.println("ts "+arresterWithVariableResistanceMeasurement.getTimeStamp());
		//System.out.println("parent "+arresterWithVariableResistanceMeasurement.getArresterWithVariableResistance());
		
		// getting the parent equipment
		Optional<ArresterWithVariableResistance> arresterWithVariableResistance = 
				arresterWithVariableResistanceRepository.findById(pid);
		// setting the parent
		arresterWithVariableResistanceMeasurement.setArresterWithVariableResistance(arresterWithVariableResistance.get());
		// saving the object
		arresterWithVariableResistanceMeasurementRepository.save(arresterWithVariableResistanceMeasurement);
				
	    return read(pid,model);
	}
	
	@GetMapping("/read/{pid}")
	public String read(@PathVariable("pid") Long pid,Model model) 
	{
		model.addAttribute("equipment", EQUIPMENT);
		model.addAttribute("pid", pid);
		
		// getting the parent equipment
		Optional<ArresterWithVariableResistance> arresterWithVariableResistance = 
				arresterWithVariableResistanceRepository.findById(pid);
		
		// getting the measurements of the equipment 
		List<ArresterWithVariableResistanceMeasurement> arrestersWithVariableResistance = 
				arresterWithVariableResistanceMeasurementRepository.findByArresterWithVariableResistanceOrderByTimeStamp(arresterWithVariableResistance.get());
		
		// setting the measurement list in the model
		if(arrestersWithVariableResistance.size() != 0) 
		{
		    model.addAttribute("equipmentObjects", arrestersWithVariableResistance);
		}
		//System.out.println("hello");
		return EQUIPMENT + "/" + EQUIPMENT + "-read";
	}
	
	@GetMapping("/edit/{id}")
	public String edit(@PathVariable("id") Long id, Model model) 
	{
		model.addAttribute("equipment", EQUIPMENT); 
		Optional<ArresterWithVariableResistanceMeasurement> arresterWithVariableResistanceMeasurement = 
				arresterWithVariableResistanceMeasurementRepository.findById(id);
	    model.addAttribute("equipmentObject", arresterWithVariableResistanceMeasurement.get());
	    
		return EQUIPMENT + "/" + EQUIPMENT + "-update";
	}
	
	@PostMapping("/update/{id}")
	// removed the pid from the path variable because you have to load the measurement so that you can get 
	// its parent so that you can set the parent before the update because the parent can't be null
	public String update(@PathVariable("id") Long id, 
			@Validated ArresterWithVariableResistanceMeasurement arresterWithVariableResistanceMeasurement, BindingResult result, Model model) 
	{
		Optional<ArresterWithVariableResistanceMeasurement> arresterWithVariableResistanceMeasurementOld = 
				arresterWithVariableResistanceMeasurementRepository.findById(id);
		
		long pid = 0L;
		// defensive code to prevent 500 error
		if (arresterWithVariableResistanceMeasurementOld.isPresent()) {
			// load the parent object
			pid = arresterWithVariableResistanceMeasurementOld.get().getArresterWithVariableResistance().getId();
			// fetched the pre-updated measurement and get the parent id
			// set the parent object before the update otherwise you get field is not nullable
			arresterWithVariableResistanceMeasurement.setArresterWithVariableResistance(arresterWithVariableResistanceMeasurementOld.get().getArresterWithVariableResistance());
		}
		if(result.hasErrors()) {
	    	arresterWithVariableResistanceMeasurement.setId(id);
			return EQUIPMENT + "/" + EQUIPMENT + "-update";
	    }
	    
	    arresterWithVariableResistanceMeasurementRepository.save(arresterWithVariableResistanceMeasurement);
	    
//	    return read(arresterWithVariableResistanceMeasurementOld.get().getArresterWithVariableResistance().getId(),model);
	    return read(pid,model);
	}
	
	@GetMapping("/delete/{id}")
	public String delete(@PathVariable("id") Long id, Model model) 
	{
		Optional<ArresterWithVariableResistanceMeasurement> arresterWithVariableResistanceMeasurement = 
				arresterWithVariableResistanceMeasurementRepository.findById(id);
		
		Long pid = null;
		
		if(arresterWithVariableResistanceMeasurement.isPresent())
		{
			pid = arresterWithVariableResistanceMeasurement.get().getArresterWithVariableResistance().getId();
			arresterWithVariableResistanceMeasurementRepository.delete(arresterWithVariableResistanceMeasurement.get());
		}
	    return read(pid, model);
	}	
	
	@GetMapping("/exportToCSV")
	public void exportToCSV(HttpServletResponse response, Model model) throws IOException {
		response.setContentType("text/csv");
		//  how it gets saved
		response.addHeader("Content-Disposition", "attachment; filename = \"ArrestersWithVariableResistanceMeasurements.csv\"");
		// "attachment; filename = \"Separators.csv\"" is the header 
		List<ExportableToCSV> data = new ArrayList<>();
		data.addAll(arresterWithVariableResistanceMeasurementRepository.findAll()); // gets every record in the separator table and adds it to the list that gets exported
		System.out.println(data);
		csvFileGenerator.write(getHeadings(), data, response.getWriter()); // write to the response object with the header
	}
	// controller handles the headings, entities handle the data
	
	private static String[] getHeadings() {
		String[] fields = new String[12];
		// fields at position zero
		fields[0] = "Id";
		fields[1] = "Parent Id";
		fields[2] = "Time Stamp";
		fields[3] = "Height One in Millimeters";
		fields[4] = "Height Two in Millimeters";
		fields[5] = "Height Three in Millimeters";
		fields[6] = "Height Four in Millimeters";
		fields[7] = "Total Height in Millimeters";
		fields[8] = "Weight in Kilograms";
		fields[9] = "Creepage Distance in Millimeters";
		fields[10] = "Initial Temperature";
		fields[11] = "Measured Temperature";
		return fields;
	}

	@GetMapping("/exportToPDF")
	public void exportToPDF(HttpServletResponse response, Model model) throws IOException {
		/* String outputFolder = "C:\\Users\\MY PC\\Downloads" + File.arrester + "Arresters.pdf";
		 OutputStream outputStream = new FileOutputStream(outputFolder);
		*/
		
		// getOutputStream() allows the PDF to be opened in the browser rather than a specific location on the server
		OutputStream outputStream = response.getOutputStream();

		response.addHeader("Content-Disposition", "attachment; filename = \"ArresterMeasurements.pdf\"");
		
	    ITextRenderer renderer = new ITextRenderer();
	    renderer.setDocumentFromString(parseThymeleafTemplate());
	    renderer.layout();
	    renderer.createPDF(outputStream);

	    outputStream.close();
	}
	
	
	private String parseThymeleafTemplate() {
	    ClassLoaderTemplateResolver templateResolver = new ClassLoaderTemplateResolver();
	    templateResolver.setSuffix(".html");
	    templateResolver.setTemplateMode(TemplateMode.HTML);

	    TemplateEngine templateEngine = new TemplateEngine();
	    templateEngine.setTemplateResolver(templateResolver);
	    
	    // model.addAttribute("equipment", "separator");
		List<ArresterWithVariableResistanceMeasurement> arresters = arresterWithVariableResistanceMeasurementRepository.findAll();

	    Context context = new Context();
	    context.setVariable("equipmentObjects", arresters);

	    return templateEngine.process("templates/arresterWithVariableResistanceMeasurement/arresterWithVariableResistanceMeasurement-pdf", context);
	}
}
