package ro.upt.ac.mtb.measurements.switchequipment;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.thymeleaf.TemplateEngine;
import org.thymeleaf.context.Context;
import org.thymeleaf.templatemode.TemplateMode;
import org.thymeleaf.templateresolver.ClassLoaderTemplateResolver;
import org.xhtmlrenderer.pdf.ITextRenderer;

import jakarta.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.io.OutputStream;
import ro.upt.ac.mtb.equipments.services.CSVFileGenerator;
import ro.upt.ac.mtb.equipments.services.ExportableToCSV;
import ro.upt.ac.mtb.equipments.switchequipment.SwitchEquipment;
import ro.upt.ac.mtb.equipments.switchequipment.SwitchEquipmentRepository;

@Controller
@RequestMapping("switchMeasurement")
public class SwitchEquipmentMeasurementController
{
	@Autowired
	SwitchEquipmentRepository switchRepository;

	@Autowired
	SwitchEquipmentMeasurementRepository switchMeasurementRepository;
	
	@Autowired
	CSVFileGenerator csvFileGenerator;
	
	private static final String EQUIPMENT = "switchMeasurement";
	
	@GetMapping("/graphHeight/{id}") 
	public String graphHeight(@PathVariable("id") Long id, Model model) {
		// load all the measurements for the id
		Optional<SwitchEquipment> switchEquipment = 
				switchRepository.findById(id);
		
		List<SwitchEquipmentMeasurement> switches = 
				switchMeasurementRepository.findBySwitchEquipmentOrderByTimeStamp(switchEquipment.get());
		
		// setting the measurement list in the model
		if(switches.size() != 0) {
			model.addAttribute("equipmentCode", switchEquipment.get().getEquipmentSeries());
			model.addAttribute("labels", switches.stream().map(m -> m.getTimeStamp()).collect(Collectors.toList()));
			model.addAttribute("data", switches.stream().map(m -> m.getTotalSwitchHeight()).collect(Collectors.toList()));
		}
		return EQUIPMENT + "/" + "graphHeight";
	}
	
	@PostMapping("/search")
	public String search(@Param("searchTerm") String searchTerm, Model model) 
	{
		//String searchTerm = (String) model.getAttribute("searchTerm");
		List<SwitchEquipmentMeasurement> data = switchMeasurementRepository.search(searchTerm);
		if (data.size() != 0) {
		    model.addAttribute("equipmentObjects", data);
		}
		System.out.println(searchTerm);
		model.addAttribute("equipment", EQUIPMENT);
		model.addAttribute("search", Boolean.TRUE);
		return EQUIPMENT + "/" + EQUIPMENT + "-read";
	}
	
	@GetMapping("/create/{pid}")
	public String create(@PathVariable("pid") Long pid,
			SwitchEquipmentMeasurement switchMeasurement, 
			Model model)
	{
		model.addAttribute("equipment", EQUIPMENT);
		model.addAttribute("pid", ""+pid.longValue());
		
		model.addAttribute("equipmentObject", switchMeasurement);
		
		return EQUIPMENT + "/" + EQUIPMENT + "-create";
	}

	@PostMapping("/create-save/{pid}")
	public String createSave(@PathVariable("pid") Long pid,
			@Validated SwitchEquipmentMeasurement switchMeasurement, 
			BindingResult result, Model model)
	{
		if(result.hasErrors())
		{
			System.out.println(result.toString());
			return EQUIPMENT + "/" + EQUIPMENT + "-create";
		} 
		
		//System.out.println(switchMeasurement);
		//System.out.println("id "+switchMeasurement.getId());
		//System.out.println("ts "+switchMeasurement.getTimeStamp());
		//System.out.println("parent "+switchMeasurement.getSwitchEquipment());
		
		// getting the parent equipment
		Optional<SwitchEquipment> switchEquipment = 
				switchRepository.findById(pid);
		// setting the parent
		switchMeasurement.setSwitchEquipment(switchEquipment.get());
		// saving the object
		switchMeasurementRepository.save(switchMeasurement);
				
	    return read(pid,model);
	}
	
	@GetMapping("/read/{pid}")
	public String read(@PathVariable("pid") Long pid,Model model) 
	{
		model.addAttribute("equipment", EQUIPMENT);
		model.addAttribute("pid", pid);
		
		// getting the parent equipment
		Optional<SwitchEquipment> switchEquipment = 
				switchRepository.findById(pid);
		
		// getting the measurements of the equipment 
		List<SwitchEquipmentMeasurement> switches = 
				switchMeasurementRepository.findBySwitchEquipmentOrderByTimeStamp(switchEquipment.get());
		
		// setting the measurement list in the model
		if(switches.size() != 0) 
		{
		    model.addAttribute("equipmentObjects", switches);
		}
		//System.out.println("hello");
		return EQUIPMENT + "/" + EQUIPMENT + "-read";
	}
	
	@GetMapping("/edit/{id}")
	public String edit(@PathVariable("id") Long id, Model model) 
	{
		model.addAttribute("equipment", EQUIPMENT); 
		Optional<SwitchEquipmentMeasurement> switchMeasurement = 
				switchMeasurementRepository.findById(id);
	    model.addAttribute("equipmentObject", switchMeasurement.get());
	    
		return EQUIPMENT + "/" + EQUIPMENT + "-update";
	}
	
	@PostMapping("/update/{id}")
	// removed the pid from the path variable because you have to load the measurement so that you can get 
	// its parent so that you can set the parent before the update because the parent can't be null
	public String update(@PathVariable("id") Long id, 
			@Validated SwitchEquipmentMeasurement switchMeasurement, BindingResult result, Model model) 
	{
		Optional<SwitchEquipmentMeasurement> switchMeasurementOld = 
				switchMeasurementRepository.findById(id);
		
		long pid = 0L;
		// defensive code to prevent 500 error
		if (switchMeasurementOld.isPresent()) {
			// load the parent object
			pid = switchMeasurementOld.get().getSwitchEquipment().getId();
			// fetched the pre-updated measurement and get the parent id
			// set the parent object before the update otherwise you get field is not nullable
			switchMeasurement.setSwitchEquipment(switchMeasurementOld.get().getSwitchEquipment());
		}
		if(result.hasErrors()) {
	    	switchMeasurement.setId(id);
			return EQUIPMENT + "/" + EQUIPMENT + "-update";
	    }
	    
	    switchMeasurementRepository.save(switchMeasurement);
	    
//	    return read(switchMeasurementOld.get().getSwitchEquipment().getId(),model);
	    return read(pid,model);
	}
	
	@GetMapping("/delete/{id}")
	public String delete(@PathVariable("id") Long id, Model model) 
	{
		Optional<SwitchEquipmentMeasurement> switchMeasurement = 
				switchMeasurementRepository.findById(id);
		
		Long pid = null;
		
		if(switchMeasurement.isPresent())
		{
			pid = switchMeasurement.get().getSwitchEquipment().getId();
			switchMeasurementRepository.delete(switchMeasurement.get());
		}
	    return read(pid, model);
	}	
	
	@GetMapping("/exportToCSV")
	public void exportToCSV(HttpServletResponse response, Model model) throws IOException {
		response.setContentType("text/csv");
		//  how it gets saved
		response.addHeader("Content-Disposition", "attachment; filename = \"SwitchMeasurements.csv\"");
		// "attachment; filename = \"Separators.csv\"" is the header 
		List<ExportableToCSV> data = new ArrayList<>();
		data.addAll(switchMeasurementRepository.findAll()); // gets every record in the separator table and adds it to the list that gets exported
		csvFileGenerator.write(getHeadings(), data, response.getWriter()); // write to the response object with the header
	}
	// controller handles the headings, entities handle the data
	
	private static String[] getHeadings() {
		String[] fields = new String[9];
		// fields at position zero
		fields[0] = "Id";
		fields[1] = "Parent Id";
		fields[2] = "Time Stamp";
		fields[3] = "Switch Height One";
		fields[4] = "Switch Height Two";
		fields[5] = "Switch Base Height";
		fields[6] = "Total Switch Height";
		fields[7] = "Initial Temperature";
		fields[8] = "Measured Temperature";
		return fields;
	}

	@GetMapping("/exportToPDF")
	public void exportToPDF(HttpServletResponse response, Model model) throws IOException {
		/* String outputFolder = "C:\\Users\\MY PC\\Downloads" + File.arrester + "Arresters.pdf";
		 OutputStream outputStream = new FileOutputStream(outputFolder);
		*/
		
		// getOutputStream() allows the PDF to be opened in the browser rather than a specific location on the server
		OutputStream outputStream = response.getOutputStream();

		response.addHeader("Content-Disposition", "attachment; filename = \"SwitchMeasurements.pdf\"");
		
	    ITextRenderer renderer = new ITextRenderer();
	    renderer.setDocumentFromString(parseThymeleafTemplate());
	    renderer.layout();
	    renderer.createPDF(outputStream);

	    outputStream.close();
	}
	
	
	private String parseThymeleafTemplate() {
	    ClassLoaderTemplateResolver templateResolver = new ClassLoaderTemplateResolver();
	    templateResolver.setSuffix(".html");
	    templateResolver.setTemplateMode(TemplateMode.HTML);

	    TemplateEngine templateEngine = new TemplateEngine();
	    templateEngine.setTemplateResolver(templateResolver);
	    
	    // model.addAttribute("equipment", "separator");
		List<SwitchEquipmentMeasurement> switches = switchMeasurementRepository.findAll();

	    Context context = new Context();
	    context.setVariable("equipmentObjects", switches);

	    return templateEngine.process("templates/switchMeasurement/switchMeasurement-pdf", context);
	}
}