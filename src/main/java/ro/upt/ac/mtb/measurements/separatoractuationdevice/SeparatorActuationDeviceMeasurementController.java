package ro.upt.ac.mtb.measurements.separatoractuationdevice;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.thymeleaf.TemplateEngine;
import org.thymeleaf.context.Context;
import org.thymeleaf.templatemode.TemplateMode;
import org.thymeleaf.templateresolver.ClassLoaderTemplateResolver;
import org.xhtmlrenderer.pdf.ITextRenderer;

import jakarta.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.io.OutputStream;
import ro.upt.ac.mtb.equipments.separatoractuationdevice.SeparatorActuationDevice;
import ro.upt.ac.mtb.equipments.separatoractuationdevice.SeparatorActuationDeviceRepository;
import ro.upt.ac.mtb.equipments.services.CSVFileGenerator;
import ro.upt.ac.mtb.equipments.services.ExportableToCSV;

@Controller
@RequestMapping("separatorActuationDeviceMeasurement")
public class SeparatorActuationDeviceMeasurementController
{
	@Autowired
	SeparatorActuationDeviceRepository separatorActuationDeviceRepository;

	@Autowired
	SeparatorActuationDeviceMeasurementRepository separatorActuationDeviceMeasurementRepository;
	
	@Autowired
	CSVFileGenerator csvFileGenerator;
	
	private static final String EQUIPMENT = "separatorActuationDeviceMeasurement";
	
	@GetMapping("/graphMeasuredTemperature/{id}") 
	public String graphMeasuredTemperature(@PathVariable("id") Long id, Model model) {
		// load all the measurements for the id
		Optional<SeparatorActuationDevice> separatorActuationDevice = 
				separatorActuationDeviceRepository.findById(id);
		
		List<SeparatorActuationDeviceMeasurement> separatorActuationDevices = 
				separatorActuationDeviceMeasurementRepository.findBySeparatorActuationDeviceOrderByTimeStamp(separatorActuationDevice.get());
		
		// setting the measurement list in the model
		if(separatorActuationDevices.size() != 0) {
			model.addAttribute("equipmentSeries", separatorActuationDevice.get().getEquipmentSeries());
			model.addAttribute("labels", separatorActuationDevices.stream().map(m -> m.getTimeStamp()).collect(Collectors.toList()));
			model.addAttribute("data", separatorActuationDevices.stream().map(m -> m.getMeasuredTemperature()).collect(Collectors.toList()));
		}
		return EQUIPMENT + "/" + "graphMeasuredTemperature";
	}
	
	@PostMapping("/search")
	public String search(@Param("searchTerm") String searchTerm, Model model) 
	{
		//String searchTerm = (String) model.getAttribute("searchTerm");
		List<SeparatorActuationDeviceMeasurement> data = separatorActuationDeviceMeasurementRepository.search(searchTerm);
		if (data.size() != 0) {
		    model.addAttribute("equipmentObjects", data);
		}
		System.out.println(searchTerm);
		model.addAttribute("equipment", EQUIPMENT);
		model.addAttribute("search", Boolean.TRUE);
		return EQUIPMENT + "/" + EQUIPMENT + "-read";
	}
	
	@GetMapping("/create/{pid}")
	public String create(@PathVariable("pid") Long pid,
			SeparatorActuationDeviceMeasurement separatorActuationDeviceMeasurement, 
			Model model)
	{
		model.addAttribute("equipment", EQUIPMENT);
		model.addAttribute("pid", ""+pid.longValue());
		
		model.addAttribute("equipmentObject", separatorActuationDeviceMeasurement);
		
		return EQUIPMENT + "/" + EQUIPMENT + "-create";
	}

	@PostMapping("/create-save/{pid}")
	public String createSave(@PathVariable("pid") Long pid,
			@Validated SeparatorActuationDeviceMeasurement separatorActuationDeviceMeasurement, 
			BindingResult result, Model model)
	{
		if(result.hasErrors())
		{
			System.out.println(result.toString());
			return EQUIPMENT + "/" + EQUIPMENT + "-create";
		} 
		
		//System.out.println(separatorActuationDeviceMeasurement);
		//System.out.println("id "+separatorActuationDeviceMeasurement.getId());
		//System.out.println("ts "+separatorActuationDeviceMeasurement.getTimeStamp());
		//System.out.println("parent "+separatorActuationDeviceMeasurement.getSeparatorActuationDevice());
		
		// getting the parent equipment
		Optional<SeparatorActuationDevice> separatorActuationDevice = 
				separatorActuationDeviceRepository.findById(pid);
		// setting the parent
		separatorActuationDeviceMeasurement.setSeparatorActuationDevice(separatorActuationDevice.get());
		// saving the object
		separatorActuationDeviceMeasurementRepository.save(separatorActuationDeviceMeasurement);
				
	    return read(pid,model);
	}
	
	@GetMapping("/read/{pid}")
	public String read(@PathVariable("pid") Long pid,Model model) 
	{
		model.addAttribute("equipment", EQUIPMENT);
		model.addAttribute("pid", pid);
		
		// getting the parent equipment
		Optional<SeparatorActuationDevice> separatorActuationDevice = 
				separatorActuationDeviceRepository.findById(pid);
		
		// getting the measurements of the equipment 
		List<SeparatorActuationDeviceMeasurement> separatorActuationDevices = 
				separatorActuationDeviceMeasurementRepository.findBySeparatorActuationDeviceOrderByTimeStamp(separatorActuationDevice.get());
		
		// setting the measurement list in the model
		if(separatorActuationDevices.size() != 0) 
		{
		    model.addAttribute("equipmentObjects", separatorActuationDevices);
		}
		//System.out.println("hello");
		return EQUIPMENT + "/" + EQUIPMENT + "-read";
	}
	
	@GetMapping("/edit/{id}")
	public String edit(@PathVariable("id") Long id, Model model) 
	{
		model.addAttribute("equipment", EQUIPMENT); 
		Optional<SeparatorActuationDeviceMeasurement> separatorActuationDeviceMeasurement = 
				separatorActuationDeviceMeasurementRepository.findById(id);
	    model.addAttribute("equipmentObject", separatorActuationDeviceMeasurement.get());
	    
		return EQUIPMENT + "/" + EQUIPMENT + "-update";
	}
	
	@PostMapping("/update/{id}")
	// removed the pid from the path variable because you have to load the measurement so that you can get 
	// its parent so that you can set the parent before the update because the parent can't be null
	public String update(@PathVariable("id") Long id, 
			@Validated SeparatorActuationDeviceMeasurement separatorActuationDeviceMeasurement, BindingResult result, Model model) 
	{
		Optional<SeparatorActuationDeviceMeasurement> separatorActuationDeviceMeasurementOld = 
				separatorActuationDeviceMeasurementRepository.findById(id);
		
		long pid = 0L;
		// defensive code to prevent 500 error
		if (separatorActuationDeviceMeasurementOld.isPresent()) {
			// load the parent object
			pid = separatorActuationDeviceMeasurementOld.get().getSeparatorActuationDevice().getId();
			// fetched the pre-updated measurement and get the parent id
			// set the parent object before the update otherwise you get field is not nullable
			separatorActuationDeviceMeasurement.setSeparatorActuationDevice(separatorActuationDeviceMeasurementOld.get().getSeparatorActuationDevice());
		}
		if(result.hasErrors()) {
	    	separatorActuationDeviceMeasurement.setId(id);
			return EQUIPMENT + "/" + EQUIPMENT + "-update";
	    }
	    
	    separatorActuationDeviceMeasurementRepository.save(separatorActuationDeviceMeasurement);
	    
//	    return read(separatorActuationDeviceMeasurementOld.get().getSeparatorActuationDevice().getId(),model);
	    return read(pid,model);
	}
	
	@GetMapping("/delete/{id}")
	public String delete(@PathVariable("id") Long id, Model model) 
	{
		Optional<SeparatorActuationDeviceMeasurement> separatorActuationDeviceMeasurement = 
				separatorActuationDeviceMeasurementRepository.findById(id);
		
		Long pid = null;
		
		if(separatorActuationDeviceMeasurement.isPresent())
		{
			pid = separatorActuationDeviceMeasurement.get().getSeparatorActuationDevice().getId();
			separatorActuationDeviceMeasurementRepository.delete(separatorActuationDeviceMeasurement.get());
		}
	    return read(pid, model);
	}	
	
	@GetMapping("/exportToCSV")
	public void export(HttpServletResponse response, Model model) throws IOException {
		response.setContentType("text/csv");
		//  how it gets saved
		response.addHeader("Content-Disposition", "attachment; filename = \"SeparatorActuationDeviceMeasurements.csv\"");
		// "attachment; filename = \"Separators.csv\"" is the header 
		List<ExportableToCSV> data = new ArrayList<>();
		data.addAll(separatorActuationDeviceMeasurementRepository.findAll()); // gets every record in the separator table and adds it to the list that gets exported
		csvFileGenerator.write(getHeadings(), data, response.getWriter()); // write to the response object with the header
	}
	// controller handles the headings, entities handle the data
	
	private static String[] getHeadings() {
		String[] fields = new String[5];
		// fields at position zero
		fields[0] = "Id";
		fields[1] = "Parent Id";
		fields[2] = "Time Stamp";
		fields[3] = "Initial Temperature";
		fields[4] = "Measured Temperature";
		return fields;
	}

	@GetMapping("/exportToPDF")
	public void exportToPDF(HttpServletResponse response, Model model) throws IOException {
		/* String outputFolder = "C:\\Users\\MY PC\\Downloads" + File.arrester + "Arresters.pdf";
		 OutputStream outputStream = new FileOutputStream(outputFolder);
		*/
		
		// getOutputStream() allows the PDF to be opened in the browser rather than a specific location on the server
		OutputStream outputStream = response.getOutputStream();

		response.addHeader("Content-Disposition", "attachment; filename = \"SeparatorActuationDeviceMeasurements.pdf\"");
		
	    ITextRenderer renderer = new ITextRenderer();
	    renderer.setDocumentFromString(parseThymeleafTemplate());
	    renderer.layout();
	    renderer.createPDF(outputStream);

	    outputStream.close();
	}
	
	
	private String parseThymeleafTemplate() {
	    ClassLoaderTemplateResolver templateResolver = new ClassLoaderTemplateResolver();
	    templateResolver.setSuffix(".html");
	    templateResolver.setTemplateMode(TemplateMode.HTML);

	    TemplateEngine templateEngine = new TemplateEngine();
	    templateEngine.setTemplateResolver(templateResolver);
	    
	    // model.addAttribute("equipment", "separator");
		List<SeparatorActuationDeviceMeasurement> separatorActuationDevices = separatorActuationDeviceMeasurementRepository.findAll();

	    Context context = new Context();
	    context.setVariable("equipmentObjects", separatorActuationDevices);

	    return templateEngine.process("templates/separatorActuationDeviceMeasurement/separatorActuationDeviceMeasurement-pdf", context);
	}
}