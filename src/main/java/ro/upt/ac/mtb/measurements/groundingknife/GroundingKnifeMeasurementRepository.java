package ro.upt.ac.mtb.measurements.groundingknife;

import java.util.List;
import java.util.Optional;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import ro.upt.ac.mtb.equipments.groundingknife.GroundingKnife;

@Repository
public interface GroundingKnifeMeasurementRepository extends JpaRepository<GroundingKnifeMeasurement,Long> {
	Optional<GroundingKnifeMeasurement> findById(Long id);
	
	@Query("SELECT gkm FROM GroundingKnifeMeasurement gkm"
			+ " WHERE gkm.timeStamp LIKE %?1%"
			+ " OR gkm.rRadius LIKE %?1%"
			)
	List<GroundingKnifeMeasurement> search(String searchTerm);
	
	List<GroundingKnifeMeasurement> findByGroundingKnifeOrderByTimeStamp(GroundingKnife groundingKnife);	

}
