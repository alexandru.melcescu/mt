package ro.upt.ac.mtb.measurements.transformationunits;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.thymeleaf.TemplateEngine;
import org.thymeleaf.context.Context;
import org.thymeleaf.templatemode.TemplateMode;
import org.thymeleaf.templateresolver.ClassLoaderTemplateResolver;
import org.xhtmlrenderer.pdf.ITextRenderer;

import jakarta.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.io.OutputStream;
import ro.upt.ac.mtb.equipments.services.CSVFileGenerator;
import ro.upt.ac.mtb.equipments.services.ExportableToCSV;
import ro.upt.ac.mtb.equipments.transformationunits.TransformationUnits;
import ro.upt.ac.mtb.equipments.transformationunits.TransformationUnitsRepository;

@Controller
@RequestMapping("transformationUnitsMeasurement")
public class TransformationUnitsMeasurementController
{
	@Autowired
	TransformationUnitsRepository transformationUnitsRepository;

	@Autowired
	TransformationUnitsMeasurementRepository transformationUnitsMeasurementRepository;
	
	@Autowired
	CSVFileGenerator csvFileGenerator;
	
	private static final String EQUIPMENT = "transformationUnitsMeasurement";
	
	@GetMapping("/graphMeasuredTemperature/{id}") 
	public String graphMeasuredTemperature(@PathVariable("id") Long id, Model model) {
		// load all the measurements for the id
		Optional<TransformationUnits> transformationUnits = 
				transformationUnitsRepository.findById(id);
		
		List<TransformationUnitsMeasurement> transformationUnitss = 
				transformationUnitsMeasurementRepository.findByTransformationUnitsOrderByTimeStamp(transformationUnits.get());
		
		// setting the measurement list in the model
		if(transformationUnitss.size() != 0) {
			model.addAttribute("equipmentCode", transformationUnits.get().getEquipmentSeries());
			model.addAttribute("labels", transformationUnitss.stream().map(m -> m.getTimeStamp()).collect(Collectors.toList()));
			model.addAttribute("data", transformationUnitss.stream().map(m -> m.getMeasuredTemperature()).collect(Collectors.toList()));
		}
		return EQUIPMENT + "/" + "graphMeasuredTemperature";
	}
	
	@PostMapping("/search")
	public String search(@Param("searchTerm") String searchTerm, Model model) 
	{
		//String searchTerm = (String) model.getAttribute("searchTerm");
		List<TransformationUnitsMeasurement> data = transformationUnitsMeasurementRepository.search(searchTerm);
		if (data.size() != 0) {
		    model.addAttribute("equipmentObjects", data);
		}
		System.out.println(searchTerm);
		model.addAttribute("equipment", EQUIPMENT);
		model.addAttribute("search", Boolean.TRUE);
		return EQUIPMENT + "/" + EQUIPMENT + "-read";
	}
	
	@GetMapping("/create/{pid}")
	public String create(@PathVariable("pid") Long pid,
			TransformationUnitsMeasurement transformationUnitsMeasurement, 
			Model model)
	{
		model.addAttribute("equipment", EQUIPMENT);
		model.addAttribute("pid", ""+pid.longValue());
		
		model.addAttribute("equipmentObject", transformationUnitsMeasurement);
		
		return EQUIPMENT + "/" + EQUIPMENT + "-create";
	}

	@PostMapping("/create-save/{pid}")
	public String createSave(@PathVariable("pid") Long pid,
			@Validated TransformationUnitsMeasurement transformationUnitsMeasurement, 
			BindingResult result, Model model)
	{
		if(result.hasErrors())
		{
			System.out.println(result.toString());
			return EQUIPMENT + "/" + EQUIPMENT + "-create";
		} 
		
		//System.out.println(transformationUnitsMeasurement);
		//System.out.println("id "+transformationUnitsMeasurement.getId());
		//System.out.println("ts "+transformationUnitsMeasurement.getTimeStamp());
		//System.out.println("parent "+transformationUnitsMeasurement.getTransformationUnits());
		
		// getting the parent equipment
		Optional<TransformationUnits> transformationUnits = 
				transformationUnitsRepository.findById(pid);
		// setting the parent
		transformationUnitsMeasurement.setTransformationUnits(transformationUnits.get());
		// saving the object
		transformationUnitsMeasurementRepository.save(transformationUnitsMeasurement);
				
	    return read(pid,model);
	}
	
	@GetMapping("/read/{pid}")
	public String read(@PathVariable("pid") Long pid, Model model) 
	{
		model.addAttribute("equipment", EQUIPMENT);
		model.addAttribute("pid", pid);
		
		// getting the parent equipment
		Optional<TransformationUnits> transformationUnits = 
				transformationUnitsRepository.findById(pid);
		
		// getting the measurements of the equipment 
		List<TransformationUnitsMeasurement> transformationUnitEquipments = 
				transformationUnitsMeasurementRepository.findByTransformationUnitsOrderByTimeStamp(transformationUnits.get());
		
		// setting the measurement list in the model
		if(transformationUnitEquipments.size() != 0) 
		{
		    model.addAttribute("equipmentObjects", transformationUnitEquipments);
		}
		//System.out.println("hello");
		return EQUIPMENT + "/" + EQUIPMENT + "-read";
	}
	
	@GetMapping("/edit/{id}")
	public String edit(@PathVariable("id") Long id, Model model) 
	{
		model.addAttribute("equipment", EQUIPMENT); 
		Optional<TransformationUnitsMeasurement> transformationUnitsMeasurement = 
				transformationUnitsMeasurementRepository.findById(id);
	    model.addAttribute("equipmentObject", transformationUnitsMeasurement.get());
	    
		return EQUIPMENT + "/" + EQUIPMENT + "-update";
	}
	
	@PostMapping("/update/{id}")
	// removed the pid from the path variable because you have to load the measurement so that you can get 
	// its parent so that you can set the parent before the update because the parent can't be null
	public String update(@PathVariable("id") Long id, 
			@Validated TransformationUnitsMeasurement transformationUnitsMeasurement, BindingResult result, Model model) 
	{
		Optional<TransformationUnitsMeasurement> transformationUnitsMeasurementOld = 
				transformationUnitsMeasurementRepository.findById(id);
		
		long pid = 0L;
		// defensive code to prevent 500 error
		if (transformationUnitsMeasurementOld.isPresent()) {
			// load the parent object
			pid = transformationUnitsMeasurementOld.get().getTransformationUnits().getId();
			// fetched the pre-updated measurement and get the parent id
			// set the parent object before the update otherwise you get field is not nullable
			transformationUnitsMeasurement.setTransformationUnits(transformationUnitsMeasurementOld.get().getTransformationUnits());
		}
		if(result.hasErrors()) {
	    	transformationUnitsMeasurement.setId(id);
			return EQUIPMENT + "/" + EQUIPMENT + "-update";
	    }
	    
	    transformationUnitsMeasurementRepository.save(transformationUnitsMeasurement);
	    
//	    return read(transformationUnitsMeasurementOld.get().getTransformationUnits().getId(),model);
	    return read(pid,model);
	}
	
	@GetMapping("/delete/{id}")
	public String delete(@PathVariable("id") Long id, Model model) 
	{
		Optional<TransformationUnitsMeasurement> transformationUnitsMeasurement = 
				transformationUnitsMeasurementRepository.findById(id);
		
		Long pid = null;
		
		if(transformationUnitsMeasurement.isPresent())
		{
			pid = transformationUnitsMeasurement.get().getTransformationUnits().getId();
//			System.out.println(pid);
			transformationUnitsMeasurementRepository.delete(transformationUnitsMeasurement.get());
		}
	    return read(pid, model);
	}	
	
	@GetMapping("/exportToCSV")
	public void export(HttpServletResponse response, Model model) throws IOException {
		response.setContentType("text/csv");
		//  how it gets saved
		response.addHeader("Content-Disposition", "attachment; filename = \"TransformationUnitsMeasurements.csv\"");
		// "attachment; filename = \"Separators.csv\"" is the header 
		List<ExportableToCSV> data = new ArrayList<>();
		data.addAll(transformationUnitsMeasurementRepository.findAll()); // gets every record in the separator table and adds it to the list that gets exported
		csvFileGenerator.write(getHeadings(), data, response.getWriter()); // write to the response object with the header
	}
	// controller handles the headings, entities handle the data

	private static String[] getHeadings() {
		String[] fields = new String[5];
		// fields at position zero
		fields[0] = "Id";
		fields[1] = "Parent Id";
		fields[2] = "Time Stamp";
		fields[3] = "Initial Temperature";
		fields[4] = "Measured Temperature";
		return fields;
	}

	@GetMapping("/exportToPDF")
	public void exportToPDF(HttpServletResponse response, Model model) throws IOException {
		/* String outputFolder = "C:\\Users\\MY PC\\Downloads" + File.arrester + "Arresters.pdf";
		 OutputStream outputStream = new FileOutputStream(outputFolder);
		*/
		
		// getOutputStream() allows the PDF to be opened in the browser rather than a specific location on the server
		OutputStream outputStream = response.getOutputStream();

		response.addHeader("Content-Disposition", "attachment; filename = \"TransformationUnitsMeasurements.pdf\"");
		
	    ITextRenderer renderer = new ITextRenderer();
	    renderer.setDocumentFromString(parseThymeleafTemplate());
	    renderer.layout();
	    renderer.createPDF(outputStream);

	    outputStream.close();
	}
	
	
	private String parseThymeleafTemplate() {
	    ClassLoaderTemplateResolver templateResolver = new ClassLoaderTemplateResolver();
	    templateResolver.setSuffix(".html");
	    templateResolver.setTemplateMode(TemplateMode.HTML);

	    TemplateEngine templateEngine = new TemplateEngine();
	    templateEngine.setTemplateResolver(templateResolver);
	    
	    // model.addAttribute("equipment", "separator");
		List<TransformationUnitsMeasurement> arresters = transformationUnitsMeasurementRepository.findAll();

	    Context context = new Context();
	    context.setVariable("equipmentObjects", arresters);

	    return templateEngine.process("templates/transformationUnitsMeasurement/transformationUnitsMeasurement-pdf", context);
	}
}