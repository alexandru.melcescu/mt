package ro.upt.ac.mtb;

import org.hibernate.SessionFactory;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.Bean;
import org.springframework.core.io.ClassPathResource;
import org.springframework.web.servlet.config.annotation.EnableWebMvc;

import java.io.IOException;
import java.io.InputStream;
import java.time.LocalDate;
import java.time.LocalDateTime;
import ro.upt.ac.mtb.equipments.arresterwithvariableresistance.ArresterWithVariableResistance;
import ro.upt.ac.mtb.equipments.arresterwithvariableresistance.ArresterWithVariableResistanceRepository;
import ro.upt.ac.mtb.equipments.compensationcoil.CompensationCoil;
import ro.upt.ac.mtb.equipments.compensationcoil.CompensationCoilRepository;
import ro.upt.ac.mtb.equipments.currentmeasuringtransformer.CurrentMeasuringTransformer;
import ro.upt.ac.mtb.equipments.currentmeasuringtransformer.CurrentMeasuringTransformerRepository;
import ro.upt.ac.mtb.equipments.electricalequipment.EquipmentManual;
import ro.upt.ac.mtb.equipments.electricalequipment.EquipmentManualRepository;
import ro.upt.ac.mtb.equipments.groundingknife.GroundingKnife;
import ro.upt.ac.mtb.equipments.groundingknife.GroundingKnifeRepository;
import ro.upt.ac.mtb.equipments.groundingknifeactuationdevice.GroundingKnifeActuationDevice;
import ro.upt.ac.mtb.equipments.groundingknifeactuationdevice.GroundingKnifeActuationDeviceRepository;
import ro.upt.ac.mtb.equipments.mixedmeasuringtransformer.MixedMeasuringTransformer;
import ro.upt.ac.mtb.equipments.mixedmeasuringtransformer.MixedMeasuringTransformerRepository;
import ro.upt.ac.mtb.equipments.separator.Separator;
import ro.upt.ac.mtb.equipments.separator.SeparatorRepository;
import ro.upt.ac.mtb.equipments.separatoractuationdevice.SeparatorActuationDevice;
import ro.upt.ac.mtb.equipments.separatoractuationdevice.SeparatorActuationDeviceRepository;
import ro.upt.ac.mtb.equipments.switchactuationdevice.SwitchActuationDevice;
import ro.upt.ac.mtb.equipments.switchactuationdevice.SwitchActuationDeviceRepository;
import ro.upt.ac.mtb.equipments.switchequipment.SwitchEquipment;
import ro.upt.ac.mtb.equipments.switchequipment.SwitchEquipmentRepository;
import ro.upt.ac.mtb.equipments.transformationunits.TransformationUnits;
import ro.upt.ac.mtb.equipments.transformationunits.TransformationUnitsRepository;
import ro.upt.ac.mtb.equipments.voltagemeasuringtransformer.VoltageMeasuringTransformer;
import ro.upt.ac.mtb.equipments.voltagemeasuringtransformer.VoltageMeasuringTransformerRepository;
import ro.upt.ac.mtb.measurements.arresterwithvariableresistance.ArresterWithVariableResistanceMeasurement;
import ro.upt.ac.mtb.measurements.arresterwithvariableresistance.ArresterWithVariableResistanceMeasurementRepository;
import ro.upt.ac.mtb.measurements.compensationcoil.CompensationCoilMeasurement;
import ro.upt.ac.mtb.measurements.compensationcoil.CompensationCoilMeasurementRepository;
import ro.upt.ac.mtb.measurements.currentmeasuringtransformer.CurrentMeasuringTransformerMeasurement;
import ro.upt.ac.mtb.measurements.currentmeasuringtransformer.CurrentMeasuringTransformerMeasurementRepository;
import ro.upt.ac.mtb.measurements.groundingknife.GroundingKnifeMeasurement;
import ro.upt.ac.mtb.measurements.groundingknife.GroundingKnifeMeasurementRepository;
import ro.upt.ac.mtb.measurements.groundingknifeactuationdevice.GroundingKnifeActuationDeviceMeasurement;
import ro.upt.ac.mtb.measurements.groundingknifeactuationdevice.GroundingKnifeActuationDeviceMeasurementRepository;
import ro.upt.ac.mtb.measurements.mixedmeasuringtransformer.MixedMeasuringTransformerMeasurement;
import ro.upt.ac.mtb.measurements.mixedmeasuringtransformer.MixedMeasuringTransformerMeasurementRepository;
import ro.upt.ac.mtb.measurements.separator.SeparatorMeasurement;
import ro.upt.ac.mtb.measurements.separator.SeparatorMeasurementRepository;
import ro.upt.ac.mtb.measurements.separatoractuationdevice.SeparatorActuationDeviceMeasurement;
import ro.upt.ac.mtb.measurements.separatoractuationdevice.SeparatorActuationDeviceMeasurementRepository;
import ro.upt.ac.mtb.measurements.switchactuationdevice.SwitchActuationDeviceMeasurement;
import ro.upt.ac.mtb.measurements.switchactuationdevice.SwitchActuationDeviceMeasurementRepository;
import ro.upt.ac.mtb.measurements.switchequipment.SwitchEquipmentMeasurement;
import ro.upt.ac.mtb.measurements.switchequipment.SwitchEquipmentMeasurementRepository;
import ro.upt.ac.mtb.measurements.transformationunits.TransformationUnitsMeasurement;
import ro.upt.ac.mtb.measurements.transformationunits.TransformationUnitsMeasurementRepository;
import ro.upt.ac.mtb.measurements.voltagemeasuringtransformer.VoltageMeasuringTransformerMeasurement;
import ro.upt.ac.mtb.measurements.voltagemeasuringtransformer.VoltageMeasuringTransformerMeasurementRepository;
import ro.upt.ac.mtb.users.User;
import ro.upt.ac.mtb.users.UserRepository;

@EnableWebMvc
@SpringBootApplication
public class Application 
{	
	@Autowired
	SessionFactory sessionFactory;
	
	private static final Logger log = LoggerFactory.getLogger(Application.class);

	public static void main(String[] args) 
	{
		SpringApplication.run(Application.class, args);
	}
	
	/*
	@Bean
	public CommandLineRunner loadDataTc(TCRepository tcRepository) 
	{
	    return (args) -> {	
			log.info("starting tcs initialization...");
			
			TC tc1=new TC();
	        tc1.setCodEchipament("tc alfa");
	        tc1.setTensiuneNominala(220.11);
	        
	        tc1.setCurentNominalPrimar(16.10);
	        
	        tc1.setCurentNominalSecundar1(16.10);
	        tc1.setCurentNominalSecundar2(16.10);
	        tc1.setCurentNominalSecundar3(16.10);
	        tc1.setCurentNominalSecundar4(16.10);
	        tc1.setCurentNominalSecundar5(16.10);
	        tc1.setCurentNominalSecundar6(16.10);
	        
	        tc1.setPutereSecundara1(100.0);
	        tc1.setPutereSecundara2(100.0);
	        tc1.setPutereSecundara3(100.0);
	        tc1.setPutereSecundara4(100.0);
	        tc1.setPutereSecundara5(100.0);
	        tc1.setPutereSecundara6(100.0);

	        tc1.setClasaPrecizieSecundara1(.5);
	        tc1.setClasaPrecizieSecundara2(.5);
	        tc1.setClasaPrecizieSecundara3(.5);
	        tc1.setClasaPrecizieSecundara4(.5);
	        tc1.setClasaPrecizieSecundara5(.5);
	        tc1.setClasaPrecizieSecundara6(.5);	        
	        
	        tc1.setMediuAmbiantDeFunctionare("interior");
	        tc1.setTipIzolatie("tip izolatie 1");
	        tc1.setModDeFunctionare("etans");
	        tc1.setFabricant("Bosch");
	        tc1.setCaleManualFabricant("e:/home/cipak/temp");
	        tc1.setCaleInstructiuniDeExploatare("e:/home/cipak/temp");        
	        tcRepository.save(tc1);
	
	        TC tc2=new TC();
	        tc2.setCodEchipament("tc beta");
	        tc2.setTensiuneNominala(220.22);
	        tc2.setCurentNominalPrimar(16.22);
	        
	        tc2.setCurentNominalSecundar1(16.10);
	        tc2.setCurentNominalSecundar2(16.10);
	        tc2.setCurentNominalSecundar3(16.10);
	        tc2.setCurentNominalSecundar4(16.10);
	        tc2.setCurentNominalSecundar5(16.10);
	        tc2.setCurentNominalSecundar6(16.10);
	        
	        tc2.setPutereSecundara1(100.0);
	        tc2.setPutereSecundara2(100.0);
	        tc2.setPutereSecundara3(100.0);
	        tc2.setPutereSecundara4(100.0);
	        tc2.setPutereSecundara5(100.0);
	        tc2.setPutereSecundara6(100.0);

	        tc2.setClasaPrecizieSecundara1(.5);
	        tc2.setClasaPrecizieSecundara2(.5);
	        tc2.setClasaPrecizieSecundara3(.5);
	        tc2.setClasaPrecizieSecundara4(.5);
	        tc2.setClasaPrecizieSecundara5(.5);
	        tc2.setClasaPrecizieSecundara6(.5);
	        
	        tc2.setMediuAmbiantDeFunctionare("exterior");
	        tc2.setTipIzolatie("tip izolatie 2");
	        tc2.setModDeFunctionare("respiratie libera");
	        tc2.setFabricant("Schneider");
	        tc2.setCaleManualFabricant("e:/home/cipak/temp");
	        tc2.setCaleInstructiuniDeExploatare("e:/home/cipak/temp");
	        tcRepository.save(tc2);
	
	        TC tc3=new TC();
	        tc3.setCodEchipament("tc gama");
	        tc3.setTensiuneNominala(220.33);
	        tc3.setCurentNominalPrimar(16.33);

	        tc3.setCurentNominalSecundar1(16.10);
	        tc3.setCurentNominalSecundar2(16.10);
	        tc3.setCurentNominalSecundar3(16.10);
	        tc3.setCurentNominalSecundar4(16.10);
	        tc3.setCurentNominalSecundar5(16.10);
	        tc3.setCurentNominalSecundar6(16.10);
	        
	        tc3.setPutereSecundara1(100.0);
	        tc3.setPutereSecundara2(100.0);
	        tc3.setPutereSecundara3(100.0);
	        tc3.setPutereSecundara4(100.0);
	        tc3.setPutereSecundara5(100.0);
	        tc3.setPutereSecundara6(100.0);

	        tc3.setClasaPrecizieSecundara1(.5);
	        tc3.setClasaPrecizieSecundara2(.5);
	        tc3.setClasaPrecizieSecundara3(.5);
	        tc3.setClasaPrecizieSecundara4(.5);
	        tc3.setClasaPrecizieSecundara5(.5);
	        tc3.setClasaPrecizieSecundara6(.5);
	        
	        tc3.setMediuAmbiantDeFunctionare("exterior");
	        tc3.setTipIzolatie("tip izolatie 3");
	        tc3.setModDeFunctionare("respiratie libera");
	        tc3.setFabricant("Schrack");
	        tc3.setCaleManualFabricant("e:/home/cipak/temp");
	        tc3.setCaleInstructiuniDeExploatare("e:/home/cipak/temp");
	        tcRepository.save(tc3);
			
			log.info("ending tcs initialization...");
	    };
	}
	

	@Bean
	public CommandLineRunner loadDataUser(UserRepository userRepository) 
	{
	    return (args) -> {	
			log.info("starting users initialization...");
	        User u1=new User("ciprian","chirila","123456");
	        userRepository.save(u1);
	        
	        User u2=new User("oana","chirila","123456");
	        userRepository.save(u2);
	        
	        User u3=new User("doru","vatau","123456");
	        userRepository.save(u3);
	        
			log.info("ending users initialization...");
	    };
	}
	*/
	
	/*
	@Bean
	public CommandLineRunner loadDataUser(SwitchRepository switchRepository)
	{
	    return (args) -> {	
			log.info("starting switches initialization...");
			
	        Switch s1=new Switch();
	        s1.setEquipmentCode("1234");
	        s1.setNominalCurrent(100);
	        switchRepository.save(s1);
	        
	        Switch s2=new Switch();
	        s2.setEquipmentCode("1235");
	        s2.setNominalCurrent(200);
	        switchRepository.save(s2);
	        
			log.info("ending switces initialization...");
	    };
	}
	
	@Bean
	public CommandLineRunner loadSeparators(SeparatorRepository separatorRepository,SeparatorMeasurementRepository separatorMeasurementRepository)
	{
	    return (args) -> {	
			log.info("starting separators initialization...");
			
	        Separator s1=new Separator();
	        s1.setEquipmentCode("1111");
	        s1.setNominalVoltage(100);
	        separatorRepository.save(s1);
	        
	        Separator s2=new Separator();
	        s2.setEquipmentCode("2222");
	        s2.setNominalVoltage(200);
	        separatorRepository.save(s2);
	        
	        SeparatorMeasurement sm1=new SeparatorMeasurement();
	        sm1.setSeparator(s1);
	        sm1.setTimeStamp(LocalDate.of(2024, Month.JULY, 20));
	        separatorMeasurementRepository.save(sm1);
	        
	        SeparatorMeasurement sm2=new SeparatorMeasurement();
	        sm2.setSeparator(s1);
	        sm2.setTimeStamp(LocalDate.of(2024, Month.JULY, 22));
	        separatorMeasurementRepository.save(sm2);

	        SeparatorMeasurement sm3=new SeparatorMeasurement();
	        sm3.setSeparator(s2);
	        sm3.setTimeStamp(LocalDate.of(2024, Month.JULY, 24));
	        separatorMeasurementRepository.save(sm3);
	        
	        SeparatorMeasurement sm4=new SeparatorMeasurement();
	        sm4.setSeparator(s2);
	        sm4.setTimeStamp(LocalDate.of(2024, Month.JULY, 26));
	        // modified this code above sm4.setTimeStamp(new Date(2024,07,26));	      
	        // there is a java.sql.date class used by JDBC
	        separatorMeasurementRepository.save(sm4);
	        
			log.info("ending separators initialization...");
	    };
	}
	*/
	
	@Bean
	public CommandLineRunner loadDataUser(UserRepository userRepository) 
	{
	    return (args) -> {	
			log.info("starting users initialization...");
	        User u1=new User("ciprian","chirila","123456");
	        userRepository.save(u1);
	        
	        User u2=new User("oana","chirila","123456");
	        userRepository.save(u2);
	        
	        User u3=new User("doru","vatau","123456");
	        userRepository.save(u3);
	        
			log.info("ending users initialization...");
	    };
	}
	
	// the resources folder must include the equipmentManuals folder in order for the manuals to be preloaded and to avoid fileNotFound error (line 311 class path)
	// add the rest of the code for the remaining equipments in the Application.java class and modify the read-template to match either the coil or arrester
	@Bean 
	public CommandLineRunner loadEquipmentManual(EquipmentManualRepository equipmentManualRepository) {
		return (args) -> {
			EquipmentManual arresterWithVariableResistanceEquipmentManual = new EquipmentManual();

	        // setting up the entity of the equipment manual
			arresterWithVariableResistanceEquipmentManual.setId("arresterWithVariableResistance");
			arresterWithVariableResistanceEquipmentManual.setOperatingManualFileType("application/pdf");
			arresterWithVariableResistanceEquipmentManual.setOperatingManualFileName("ArresterWithVariableResistanceEquipmentManual.pdf");
	        
        	ClassPathResource arresterWithVariableResistanceResource = new ClassPathResource("/equipmentManuals/arresterWithVariableResistance/ArresterWithVariableResistanceEquipmentManual.pdf", Application.class);
	        
	        try (InputStream inputStream = arresterWithVariableResistanceResource.getInputStream()) {
	        	arresterWithVariableResistanceEquipmentManual.setOperatingManual(sessionFactory.openSession().getLobHelper().createBlob(inputStream.readAllBytes()));
				equipmentManualRepository.save(arresterWithVariableResistanceEquipmentManual);
			} catch (IOException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
	        
			EquipmentManual compensationCoilEquipmentManual = new EquipmentManual();

	        // setting up the entity of the equipment manual
	        compensationCoilEquipmentManual.setId("compensationCoil");
	        compensationCoilEquipmentManual.setOperatingManualFileType("application/pdf");
	        compensationCoilEquipmentManual.setOperatingManualFileName("CompensationCoilEquipmentManual.pdf");
	        
        	ClassPathResource compensationCoilResource = new ClassPathResource("/equipmentManuals/compensationCoil/CompensationCoilEquipmentManual.pdf", Application.class);
	        
	        try (InputStream inputStream = compensationCoilResource.getInputStream()) {
	        	//InputStream inputStream = new FileInputStream(new File("equipmentManuals/compensationCoil/CompensationCoilEquipmentManual.pdf"));
	        	//InputStream inputStream = new FileInputStream(ResourceUtils.getFile("classpath:/equipmentManuals/compensationCoil/CompensationCoilEquipmentManual.pdf"));
	        		        	
	        	//equipmentManual.setOperatingManual(file.getBytes());
	        	
	        	// create a blob object so that we can store anything bigger and 255 bytes
	        	compensationCoilEquipmentManual.setOperatingManual(sessionFactory.openSession().getLobHelper().createBlob(inputStream.readAllBytes()));
				equipmentManualRepository.save(compensationCoilEquipmentManual);
			} catch (IOException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
	        
	        EquipmentManual currentMeasuringTransformerEquipmentManual = new EquipmentManual();

	        // setting up the entity of the equipment manual
	        currentMeasuringTransformerEquipmentManual.setId("currentMeasuringTransformer");
	        currentMeasuringTransformerEquipmentManual.setOperatingManualFileType("application/pdf");
	        currentMeasuringTransformerEquipmentManual.setOperatingManualFileName("CurrentMeasuringTransformerEquipmentManual.pdf");
	        
        	ClassPathResource currentMeasuringTransformerResource = new ClassPathResource("/equipmentManuals/currentMeasuringTransformer/CurrentMeasuringTransformerEquipmentManual.pdf", Application.class);
	        
	        try (InputStream inputStream = currentMeasuringTransformerResource.getInputStream()) {
	        	//InputStream inputStream = new FileInputStream(new File("equipmentManuals/compensationCoil/CompensationCoilEquipmentManual.pdf"));
	        	//InputStream inputStream = new FileInputStream(ResourceUtils.getFile("classpath:/equipmentManuals/compensationCoil/CompensationCoilEquipmentManual.pdf"));
	        		        	
	        	//equipmentManual.setOperatingManual(file.getBytes());
	        	
	        	// create a blob object so that we can store anything bigger and 255 bytes
	        	currentMeasuringTransformerEquipmentManual.setOperatingManual(sessionFactory.openSession().getLobHelper().createBlob(inputStream.readAllBytes()));
				equipmentManualRepository.save(currentMeasuringTransformerEquipmentManual);
			} catch (IOException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
	        
	        EquipmentManual groundingKnifeEquipmentManual = new EquipmentManual();

	        // setting up the entity of the equipment manual
	        groundingKnifeEquipmentManual.setId("groundingKnife");
	        groundingKnifeEquipmentManual.setOperatingManualFileType("application/pdf");
	        groundingKnifeEquipmentManual.setOperatingManualFileName("GroundingKnifeEquipmentManual.pdf");
	        
        	ClassPathResource groundingKnifeResource = new ClassPathResource("/equipmentManuals/groundingKnife/GroundingKnifeEquipmentManual.pdf", Application.class);
	        
	        try (InputStream inputStream = groundingKnifeResource.getInputStream()) {
	        	//InputStream inputStream = new FileInputStream(new File("equipmentManuals/compensationCoil/CompensationCoilEquipmentManual.pdf"));
	        	//InputStream inputStream = new FileInputStream(ResourceUtils.getFile("classpath:/equipmentManuals/compensationCoil/CompensationCoilEquipmentManual.pdf"));
	        		        	
	        	//equipmentManual.setOperatingManual(file.getBytes());
	        	
	        	// create a blob object so that we can store anything bigger and 255 bytes
	        	groundingKnifeEquipmentManual.setOperatingManual(sessionFactory.openSession().getLobHelper().createBlob(inputStream.readAllBytes()));
				equipmentManualRepository.save(groundingKnifeEquipmentManual);
			} catch (IOException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
	        
	        EquipmentManual groundingKnifeActuationDeviceEquipmentManual = new EquipmentManual();

	        // setting up the entity of the equipment manual
	        groundingKnifeActuationDeviceEquipmentManual.setId("groundingKnifeActuationDevice");
	        groundingKnifeActuationDeviceEquipmentManual.setOperatingManualFileType("application/pdf");
	        groundingKnifeActuationDeviceEquipmentManual.setOperatingManualFileName("GroundingKnifeActuationDeviceEquipmentManual.pdf");
	        
        	ClassPathResource groundingKnifeActuationDeviceResource = new ClassPathResource("/equipmentManuals/groundingKnifeActuationDevice/GroundingKnifeActuationDeviceEquipmentManual.pdf", Application.class);
	        
	        try (InputStream inputStream = groundingKnifeActuationDeviceResource.getInputStream()) {
	        	//InputStream inputStream = new FileInputStream(new File("equipmentManuals/compensationCoil/CompensationCoilEquipmentManual.pdf"));
	        	//InputStream inputStream = new FileInputStream(ResourceUtils.getFile("classpath:/equipmentManuals/compensationCoil/CompensationCoilEquipmentManual.pdf"));
	        		        	
	        	//equipmentManual.setOperatingManual(file.getBytes());
	        	
	        	// create a blob object so that we can store anything bigger and 255 bytes
	        	groundingKnifeActuationDeviceEquipmentManual.setOperatingManual(sessionFactory.openSession().getLobHelper().createBlob(inputStream.readAllBytes()));
				equipmentManualRepository.save(groundingKnifeActuationDeviceEquipmentManual);
			} catch (IOException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
	        
	        EquipmentManual mixedMeasuringTransformerDeviceEquipmentManual = new EquipmentManual();

	        // setting up the entity of the equipment manual
	        mixedMeasuringTransformerDeviceEquipmentManual.setId("mixedMeasuringTransformer");
	        mixedMeasuringTransformerDeviceEquipmentManual.setOperatingManualFileType("application/pdf");
	        mixedMeasuringTransformerDeviceEquipmentManual.setOperatingManualFileName("MixedMeasuringTransformerEquipmentManual.pdf");
	        
        	ClassPathResource mixedMeasuringTransformerResource = new ClassPathResource("/equipmentManuals/mixedMeasuringTransformer/MixedMeasuringTransformerEquipmentManual.pdf", Application.class);
	        
	        try (InputStream inputStream = mixedMeasuringTransformerResource.getInputStream()) {
	        	//InputStream inputStream = new FileInputStream(new File("equipmentManuals/compensationCoil/CompensationCoilEquipmentManual.pdf"));
	        	//InputStream inputStream = new FileInputStream(ResourceUtils.getFile("classpath:/equipmentManuals/compensationCoil/CompensationCoilEquipmentManual.pdf"));
	        		        	
	        	//equipmentManual.setOperatingManual(file.getBytes());
	        	
	        	// create a blob object so that we can store anything bigger and 255 bytes
	        	mixedMeasuringTransformerDeviceEquipmentManual.setOperatingManual(sessionFactory.openSession().getLobHelper().createBlob(inputStream.readAllBytes()));
				equipmentManualRepository.save(mixedMeasuringTransformerDeviceEquipmentManual);
			} catch (IOException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
	        
	        EquipmentManual separatorEquipmentManual = new EquipmentManual();

	        // setting up the entity of the equipment manual
	        separatorEquipmentManual.setId("separator");
	        separatorEquipmentManual.setOperatingManualFileType("application/pdf");
	        separatorEquipmentManual.setOperatingManualFileName("SeparatorEquipmentManual.pdf");
	        
        	ClassPathResource separatorResource = new ClassPathResource("/equipmentManuals/separator/SeparatorEquipmentManual.pdf", Application.class);
	        
	        try (InputStream inputStream = separatorResource.getInputStream()) {
	        	//InputStream inputStream = new FileInputStream(new File("equipmentManuals/compensationCoil/CompensationCoilEquipmentManual.pdf"));
	        	//InputStream inputStream = new FileInputStream(ResourceUtils.getFile("classpath:/equipmentManuals/compensationCoil/CompensationCoilEquipmentManual.pdf"));
	        		        	
	        	//equipmentManual.setOperatingManual(file.getBytes());
	        	
	        	// create a blob object so that we can store anything bigger and 255 bytes
	        	separatorEquipmentManual.setOperatingManual(sessionFactory.openSession().getLobHelper().createBlob(inputStream.readAllBytes()));
				equipmentManualRepository.save(separatorEquipmentManual);
			} catch (IOException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
	        
	        EquipmentManual separatorActuationDeviceEquipmentManual = new EquipmentManual();

	        // setting up the entity of the equipment manual
	        separatorActuationDeviceEquipmentManual.setId("separatorActuationDevice");
	        separatorActuationDeviceEquipmentManual.setOperatingManualFileType("application/pdf");
	        separatorActuationDeviceEquipmentManual.setOperatingManualFileName("SeparatorActuationDeviceEquipmentManual.pdf");
	        
        	ClassPathResource separatorActuationDeviceResource = new ClassPathResource("/equipmentManuals/separatorActuationDevice/SeparatorActuationDeviceEquipmentManual.pdf", Application.class);
	        
	        try (InputStream inputStream = separatorActuationDeviceResource.getInputStream()) {
	        	//InputStream inputStream = new FileInputStream(new File("equipmentManuals/compensationCoil/CompensationCoilEquipmentManual.pdf"));
	        	//InputStream inputStream = new FileInputStream(ResourceUtils.getFile("classpath:/equipmentManuals/compensationCoil/CompensationCoilEquipmentManual.pdf"));
	        		        	
	        	//equipmentManual.setOperatingManual(file.getBytes());
	        	
	        	// create a blob object so that we can store anything bigger and 255 bytes
	        	separatorActuationDeviceEquipmentManual.setOperatingManual(sessionFactory.openSession().getLobHelper().createBlob(inputStream.readAllBytes()));
				equipmentManualRepository.save(separatorActuationDeviceEquipmentManual);
			} catch (IOException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
	        
	        EquipmentManual switchEquipmentManual = new EquipmentManual();

	        // setting up the entity of the equipment manual
	        switchEquipmentManual.setId("switch");
	        switchEquipmentManual.setOperatingManualFileType("application/pdf");
	        switchEquipmentManual.setOperatingManualFileName("SwitchEquipmentManual.pdf");
	        
        	ClassPathResource switchResource = new ClassPathResource("/equipmentManuals/switchEquipment/SwitchEquipmentManual.pdf", Application.class);
	        
	        try (InputStream inputStream = switchResource.getInputStream()) {
	        	//InputStream inputStream = new FileInputStream(new File("equipmentManuals/compensationCoil/CompensationCoilEquipmentManual.pdf"));
	        	//InputStream inputStream = new FileInputStream(ResourceUtils.getFile("classpath:/equipmentManuals/compensationCoil/CompensationCoilEquipmentManual.pdf"));
	        		        	
	        	//equipmentManual.setOperatingManual(file.getBytes());
	        	
	        	// create a blob object so that we can store anything bigger and 255 bytes
	        	switchEquipmentManual.setOperatingManual(sessionFactory.openSession().getLobHelper().createBlob(inputStream.readAllBytes()));
				equipmentManualRepository.save(switchEquipmentManual);
			} catch (IOException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
	        
	        EquipmentManual switchActuationDeviceEquipmentManual = new EquipmentManual();

	        // setting up the entity of the equipment manual
	        switchActuationDeviceEquipmentManual.setId("switchActuationDevice");
	        switchActuationDeviceEquipmentManual.setOperatingManualFileType("application/pdf");
	        switchActuationDeviceEquipmentManual.setOperatingManualFileName("SwitchActuationDeviceEquipmentManual.pdf");
	        
        	ClassPathResource switchActuationDeviceResource = new ClassPathResource("/equipmentManuals/switchActuationDevice/SwitchEquipmentManual.pdf", Application.class);
	        
	        try (InputStream inputStream = switchActuationDeviceResource.getInputStream()) {
	        	//InputStream inputStream = new FileInputStream(new File("equipmentManuals/compensationCoil/CompensationCoilEquipmentManual.pdf"));
	        	//InputStream inputStream = new FileInputStream(ResourceUtils.getFile("classpath:/equipmentManuals/compensationCoil/CompensationCoilEquipmentManual.pdf"));
	        		        	
	        	//equipmentManual.setOperatingManual(file.getBytes());
	        	
	        	// create a blob object so that we can store anything bigger and 255 bytes
	        	switchActuationDeviceEquipmentManual.setOperatingManual(sessionFactory.openSession().getLobHelper().createBlob(inputStream.readAllBytes()));
				equipmentManualRepository.save(switchActuationDeviceEquipmentManual);
			} catch (IOException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
	        
	        EquipmentManual voltageMeasuringTransformerEquipmentManual = new EquipmentManual();

	        // setting up the entity of the equipment manual
	        voltageMeasuringTransformerEquipmentManual.setId("voltageMeasuringTransformer");
	        voltageMeasuringTransformerEquipmentManual.setOperatingManualFileType("application/pdf");
	        voltageMeasuringTransformerEquipmentManual.setOperatingManualFileName("VoltageMeasuringTransformerEquipmentManual.pdf");
	        
        	ClassPathResource voltageMeasuringTransformerResource = new ClassPathResource("/equipmentManuals/voltageMeasuringTransformer/VoltageMeasuringTransformerEquipmentManual.pdf", Application.class);
	        
	        try (InputStream inputStream = voltageMeasuringTransformerResource.getInputStream()) {
	        	//InputStream inputStream = new FileInputStream(new File("equipmentManuals/compensationCoil/CompensationCoilEquipmentManual.pdf"));
	        	//InputStream inputStream = new FileInputStream(ResourceUtils.getFile("classpath:/equipmentManuals/compensationCoil/CompensationCoilEquipmentManual.pdf"));
	        		        	
	        	//equipmentManual.setOperatingManual(file.getBytes());
	        	
	        	// create a blob object so that we can store anything bigger and 255 bytes
	        	voltageMeasuringTransformerEquipmentManual.setOperatingManual(sessionFactory.openSession().getLobHelper().createBlob(inputStream.readAllBytes()));
				equipmentManualRepository.save(voltageMeasuringTransformerEquipmentManual);
			} catch (IOException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
	        
	        EquipmentManual transformationUnitsEquipmentManual = new EquipmentManual();

	        // setting up the entity of the equipment manual
	        transformationUnitsEquipmentManual.setId("transformationUnits");
	        transformationUnitsEquipmentManual.setOperatingManualFileType("application/pdf");
	        transformationUnitsEquipmentManual.setOperatingManualFileName("TransformationUnitsEquipmentManual.pdf");
	        
        	ClassPathResource transformationUnitsResource = new ClassPathResource("/equipmentManuals/transformationUnits/TransformationUnitsEquipmentManual.pdf", Application.class);
	        
	        try (InputStream inputStream = transformationUnitsResource.getInputStream()) {
	        	//InputStream inputStream = new FileInputStream(new File("equipmentManuals/compensationCoil/CompensationCoilEquipmentManual.pdf"));
	        	//InputStream inputStream = new FileInputStream(ResourceUtils.getFile("classpath:/equipmentManuals/compensationCoil/CompensationCoilEquipmentManual.pdf"));
	        		        	
	        	//equipmentManual.setOperatingManual(file.getBytes());
	        	
	        	// create a blob object so that we can store anything bigger and 255 bytes
	        	transformationUnitsEquipmentManual.setOperatingManual(sessionFactory.openSession().getLobHelper().createBlob(inputStream.readAllBytes()));
				equipmentManualRepository.save(transformationUnitsEquipmentManual);
			} catch (IOException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		};
	}
	
	@Bean
	public CommandLineRunner loadSeparators(SeparatorRepository separatorRepository,SeparatorMeasurementRepository separatorMeasurementRepository)
	{
	    return (args) -> {	
			log.info("starting separators initialization...");
			
	        Separator s1=new Separator();
	        s1.setEquipmentCode("SEP-2024-01");
	        s1.setType("High-Voltage Separator");
	        s1.setManufacturer("Siemens AG");
	        s1.setNominalVoltage(100.0);
	        s1.setPrimaryCurrent(630.0);
	        s1.setAmbientEnvironment("Outdoor");
	        s1.setInsulationType("SF6 Gas-Insulated");
	        s1.setOperatingMode("Motor-Operated");
	        s1.setLastMaintenanceDate(LocalDate.of(2024, 1, 15));
	        s1.setNextScheduledMaintenanceDate(LocalDate.of(2025, 1, 15));
	        s1.setStatus("In Operation");
	        s1.setMaintenancePerformedBy("ABC Electrial Services");
	        s1.setRemarks("No issues detected during the last maintenance");
	        s1.setEquipmentSeries("SEP-HV-2024");
	        s1.setManufacturingYear(2023);
	        s1.setPifYear(2024);
	        s1.setInstallationCompany("Global Tech Installations Ltd.");
	        s1.setPifBulletin("");
	        s1.setExploitationCenter("North-Western Grid Operations");
	        s1.setTransformationStation("Greenfield Substation");
	        s1.setVoltageLevel("110 kV");
	        s1.setAssemblyCell("Cell A3");
	        s1.setAssemblyPhase("Phase 1");
	        s1.setAccountingorWarehouseRegistrationCode("REG-SEP-2024-001");
	        s1.setFixedAssetComponentNumber("FM-0012457");
	        separatorRepository.save(s1);
	        
	        Separator s2=new Separator();
	        s2.setEquipmentCode("SEP-2024-02");
	        s2.setType("Medium-Voltage Separator");
	        s2.setManufacturer("ABB Group");
	        s2.setNominalVoltage(33.0);
	        s2.setPrimaryCurrent(400.0);
	        s2.setAmbientEnvironment("Indoor");
	        s2.setInsulationType("Air-Insulated");
	        s2.setOperatingMode("Manual-Operated");
	        s2.setLastMaintenanceDate(LocalDate.of(2023, 10, 05));
	        s2.setNextScheduledMaintenanceDate(LocalDate.of(2024, 10, 05));
	        s2.setStatus("Scheduled For Maintenance");
	        s2.setMaintenancePerformedBy("XYZ Electrial Maintenance Ltd.");
	        s2.setRemarks("Minor wear observed on contacts, replacement planned during next maintenance");
	        s2.setEquipmentSeries("SEP-MV-2024");
	        s2.setManufacturingYear(2022);
	        s2.setPifYear(2023);
	        s2.setInstallationCompany("Power Grid Installations Inc.");
	        s2.setPifBulletin("");
	        s2.setExploitationCenter("Eastern Region Operations");
	        s2.setTransformationStation("Sunrise Substation");
	        s2.setVoltageLevel("33 kV");
	        s2.setAssemblyCell("Cell B2");
	        s2.setAssemblyPhase("Phase 2");
	        s2.setAccountingorWarehouseRegistrationCode("REG-SEP-2024-002");
	        s2.setFixedAssetComponentNumber("FM-0023489");
	        separatorRepository.save(s2);
	        
	        SeparatorMeasurement sm1=new SeparatorMeasurement();
	        sm1.setSeparator(s1);
	        sm1.setHeightOne(2.5);
	        sm1.setHeightTwo(4.0);
	        sm1.setWidth(1.2);
	        sm1.setInitialTemperature(50);
	        sm1.setMeasuredTemperature(53);
	        //sm1.setTimeStamp(LocalDate.of(2024, Month.JULY, 20));
	        separatorMeasurementRepository.save(sm1);
	        
	        SeparatorMeasurement sm1a=new SeparatorMeasurement();
	        sm1a.setSeparator(s1);
	        sm1a.setHeightOne(3.2);
	        sm1a.setHeightTwo(5.3);
	        sm1a.setWidth(1.5);
	        sm1a.setInitialTemperature(46);
	        sm1a.setMeasuredTemperature(43);
	        sm1a.setTimeStamp(LocalDateTime.of(2023, 04, 22, 12, 35));	        
	        separatorMeasurementRepository.save(sm1a);
	        
	        SeparatorMeasurement sm1b=new SeparatorMeasurement();
	        sm1b.setSeparator(s1);
	        sm1b.setHeightOne(4.7);
	        sm1b.setHeightTwo(3.5);
	        sm1b.setWidth(1.7);
	        sm1b.setInitialTemperature(74);
	        sm1b.setMeasuredTemperature(73);
	        sm1b.setTimeStamp(LocalDateTime.of(2023, 07, 21, 16, 47));	        
	        separatorMeasurementRepository.save(sm1b);
	        
	        SeparatorMeasurement sm2=new SeparatorMeasurement();
	        sm2.setSeparator(s2);
	        sm2.setHeightOne(2.1);
	        sm2.setHeightTwo(3.6);
	        sm2.setWidth(1.0);
	        sm2.setInitialTemperature(43);
	        sm2.setMeasuredTemperature(44);
	        separatorMeasurementRepository.save(sm2);
	        
			log.info("ending separators initialization...");
			
	    };
	}
	
	@Bean
	public CommandLineRunner loadArresters(ArresterWithVariableResistanceRepository arresterWithVariableResistanceRepository, ArresterWithVariableResistanceMeasurementRepository arresterWithVariableResistanceMeasurementRepository)
	{
	    return (args) -> {	
			log.info("starting arresters initialization...");
				        
			
			ArresterWithVariableResistance awvr1 = new ArresterWithVariableResistance();
			awvr1.setEquipmentCode("AVR-2024-03");
			awvr1.setShape("Cylindrical");
			awvr1.setMaterial("High-Grade Ceramic with Zinc Oxide Varistor");
			awvr1.setCertification("IEC 60099-4 Certified");
			awvr1.setColor("Gray");
			awvr1.setTransportationPackage("Wooden Crate with Foam Padding");
			awvr1.setSpecification("10 kA, 110 kV Rated Voltage");
			awvr1.setTrademark("SurgeProtect™");
			awvr1.setOrigin("Germany");
			awvr1.setProductionCapacity("500 Units/Year");
			awvr1.setEquipmentSeries("AVR-VR-2024");
	        awvr1.setManufacturingYear(2023);
	        awvr1.setPifYear(2024);
	        awvr1.setInstallationCompany("EuroTech Installations GmbH");
	        awvr1.setPifBulletin("");
	        awvr1.setExploitationCenter("Southern Grid Operations");
	        awvr1.setTransformationStation("Riverbank Substation");
	        awvr1.setVoltageLevel("110 kV");
	        awvr1.setAssemblyCell("Cell C5");
	        awvr1.setAssemblyPhase("Phase 3");
	        awvr1.setAccountingorWarehouseRegistrationCode("REG-AVR-2024-003");
	        awvr1.setFixedAssetComponentNumber("FM-0035678");
			arresterWithVariableResistanceRepository.save(awvr1);
			
			ArresterWithVariableResistance awvr2 = new ArresterWithVariableResistance();
			awvr2.setEquipmentCode("AVR-2024-04");
			awvr2.setShape("Rectangular");
			awvr2.setMaterial("Polymer Composite with Metal Oxide Varistor");
			awvr2.setCertification("ISO 9001:2015 Certified");
			awvr2.setColor("Black");
			awvr2.setTransportationPackage("Reinforced Steel Case with Protective Foam");
			awvr2.setSpecification("8 kA, 132 kV Rated Voltage");
			awvr2.setTrademark("SafeGuard™");
			awvr2.setOrigin("Japan");
			awvr2.setProductionCapacity("300 Units/Year");
			awvr2.setEquipmentSeries("AVR-PRO-2024");
	        awvr2.setManufacturingYear(2022);
	        awvr2.setPifYear(2023);
	        awvr2.setInstallationCompany("Nippon Electric Installation Co., Ltd.");
	        awvr2.setPifBulletin("");
	        awvr2.setExploitationCenter("Central Grid Operations");
	        awvr2.setTransformationStation("Highland Substation");
	        awvr2.setVoltageLevel("132 kV");
	        awvr2.setAssemblyCell("Cell D7");
	        awvr2.setAssemblyPhase("Phase 4");
	        awvr2.setAccountingorWarehouseRegistrationCode("REG-AVR-2024-004");
	        awvr2.setFixedAssetComponentNumber("FM-0046721");
			arresterWithVariableResistanceRepository.save(awvr2);
			
			ArresterWithVariableResistance awvr3 = new ArresterWithVariableResistance();
			awvr3.setEquipmentCode("AVR-2024-05");
			awvr3.setShape("Conical");
			awvr3.setMaterial("Silicone Rubber with Metal Oxide Varistor");
			awvr3.setCertification("ANSI/IEEE C62.11 Certified");
			awvr3.setColor("Red");
			awvr3.setTransportationPackage("Shock-Resistant Plastic Container");
			awvr3.setSpecification("15 kA, 66 kV Rated Voltage");
			awvr3.setTrademark("VoltGuard™");
			awvr3.setOrigin("United States");
			awvr3.setProductionCapacity("700 Units/Year");
			awvr3.setEquipmentSeries("AVR-ELITE-2024");
	        awvr3.setManufacturingYear(2021);
	        awvr3.setPifYear(2022);
	        awvr3.setInstallationCompany("US Power Installations LLC");
	        awvr3.setPifBulletin("");
	        awvr3.setExploitationCenter("Western Grid Operations");
	        awvr3.setTransformationStation("Mountain View Substation");
	        awvr3.setVoltageLevel("66 kV");
	        awvr3.setAssemblyCell("Cell E2");
	        awvr3.setAssemblyPhase("Phase 1");
	        awvr3.setAccountingorWarehouseRegistrationCode("REG-AVR-2024-005");
	        awvr3.setFixedAssetComponentNumber("FM-0057893");
			arresterWithVariableResistanceRepository.save(awvr3);

			ArresterWithVariableResistanceMeasurement awvrm1 = new ArresterWithVariableResistanceMeasurement();
			awvrm1.setHeightOneInMillimeters(1113);
			awvrm1.setHeightTwoInMillimeters(1169);
			awvrm1.setHeightThreeInMillimeters(1215);
			awvrm1.setHeightFourInMillimeters(1057);
			awvrm1.setWeightInKilograms(67);
			awvrm1.setCreepageDistanceInMillimeters(7600);
			awvrm1.setArresterWithVariableResistance(awvr1);
			awvrm1.setInitialTemperature(60);
			awvrm1.setMeasuredTemperature(59);
			arresterWithVariableResistanceMeasurementRepository.save(awvrm1);

			ArresterWithVariableResistanceMeasurement awvrm1a = new ArresterWithVariableResistanceMeasurement();
			awvrm1a.setHeightOneInMillimeters(2312);
			awvrm1a.setHeightTwoInMillimeters(1232);
			awvrm1a.setHeightThreeInMillimeters(4324);
			awvrm1a.setHeightFourInMillimeters(2313);
			awvrm1a.setWeightInKilograms(34);
			awvrm1a.setCreepageDistanceInMillimeters(5400);
			awvrm1a.setArresterWithVariableResistance(awvr1);
			awvrm1a.setInitialTemperature(50);
			awvrm1a.setMeasuredTemperature(49);
			awvrm1a.setTimeStamp(LocalDateTime.of(2023, 01, 12, 13, 46));	        
			arresterWithVariableResistanceMeasurementRepository.save(awvrm1a);
			
			ArresterWithVariableResistanceMeasurement awvrm1b = new ArresterWithVariableResistanceMeasurement();
			awvrm1b.setHeightOneInMillimeters(1200);
			awvrm1b.setHeightTwoInMillimeters(1132);
			awvrm1b.setHeightThreeInMillimeters(1234);
			awvrm1b.setHeightFourInMillimeters(1132);
			awvrm1b.setWeightInKilograms(89);
			awvrm1b.setCreepageDistanceInMillimeters(5400);
			awvrm1b.setArresterWithVariableResistance(awvr1);
			awvrm1b.setInitialTemperature(50);
			awvrm1b.setMeasuredTemperature(49);
			awvrm1b.setTimeStamp(LocalDateTime.of(2023, 05, 25, 18, 34));	        
			arresterWithVariableResistanceMeasurementRepository.save(awvrm1b);
			
			ArresterWithVariableResistanceMeasurement awvrm1c = new ArresterWithVariableResistanceMeasurement();
			awvrm1c.setHeightOneInMillimeters(2100);
			awvrm1c.setHeightTwoInMillimeters(2233);
			awvrm1c.setHeightThreeInMillimeters(2131);
			awvrm1c.setHeightFourInMillimeters(1233);
			awvrm1c.setWeightInKilograms(47);
			awvrm1c.setCreepageDistanceInMillimeters(6730);
			awvrm1c.setArresterWithVariableResistance(awvr1);
			awvrm1c.setInitialTemperature(85);
			awvrm1c.setMeasuredTemperature(83);
			awvrm1c.setTimeStamp(LocalDateTime.of(2023, 07, 15, 16, 28));	
			arresterWithVariableResistanceMeasurementRepository.save(awvrm1c);

			ArresterWithVariableResistanceMeasurement awvrm2 = new ArresterWithVariableResistanceMeasurement();
			awvrm2.setHeightOneInMillimeters(1000);
			awvrm2.setHeightTwoInMillimeters(1123);
			awvrm2.setHeightThreeInMillimeters(1266);
			awvrm2.setHeightFourInMillimeters(1532);
			awvrm2.setWeightInKilograms(130);
			awvrm2.setCreepageDistanceInMillimeters(1200);
			awvrm2.setArresterWithVariableResistance(awvr2);
			awvrm2.setInitialTemperature(96);
			awvrm2.setMeasuredTemperature(94);
			arresterWithVariableResistanceMeasurementRepository.save(awvrm2);
			
			ArresterWithVariableResistanceMeasurement awvrm3 = new ArresterWithVariableResistanceMeasurement();
			awvrm3.setHeightOneInMillimeters(2605);
			awvrm3.setHeightTwoInMillimeters(2760);
			awvrm3.setHeightThreeInMillimeters(1233);
			awvrm3.setHeightFourInMillimeters(1764);
			awvrm3.setWeightInKilograms(93);
			awvrm3.setCreepageDistanceInMillimeters(4300);
			awvrm3.setArresterWithVariableResistance(awvr3);
			awvrm3.setInitialTemperature(85);
			awvrm3.setMeasuredTemperature(83);
			arresterWithVariableResistanceMeasurementRepository.save(awvrm3);

			log.info("ending arresters initialization...");

	    };
	}
	
	@Bean
	public CommandLineRunner loadCompensationCoils(CompensationCoilRepository compensationCoilRepository, CompensationCoilMeasurementRepository compensationCoilMeasurementRepository)
	{
	    return (args) -> {	
			log.info("starting compensation coils initialization...");
				        
			
			CompensationCoil cc1 = new CompensationCoil();
			cc1.setArticleNumber("0001-2024-06");
			cc1.setProductDesign("Toroidal Wound Coil with Air Core");
			cc1.setUsableWith("110 kV Transmission Systems");
			
			cc1.setEquipmentSeries("CC-PRO-2024");
			cc1.setManufacturingYear(2023);
			cc1.setPifYear(2024);
			cc1.setInstallationCompany("Electromontaj");
			cc1.setPifBulletin("");
			cc1.setExploitationCenter("Seiler");
			cc1.setTransformationStation("Pine Valley Substation");
			cc1.setVoltageLevel("110 kV");
			cc1.setAssemblyCell("Cell F4");
			cc1.setAssemblyPhase("Phase 2");
			cc1.setAccountingorWarehouseRegistrationCode("REG-CC-2024-006");
			cc1.setFixedAssetComponentNumber("FM-0068945");
			
			compensationCoilRepository.save(cc1);
			
			CompensationCoil cc2 = new CompensationCoil();
			cc2.setArticleNumber("0002-2024-06");
			cc2.setProductDesign("Laminated Core with Copper Windings");
			cc2.setUsableWith("132 kV Distribution Systems");
			
			cc2.setEquipmentSeries("CC-ELITE-2024");
			cc2.setManufacturingYear(2022);
			cc2.setPifYear(2023);
			cc2.setInstallationCompany("Electromontaj");
			cc2.setPifBulletin("");
			cc2.setExploitationCenter("Fratelli");
			cc2.setTransformationStation("Meadowbrook Substation");
			cc2.setVoltageLevel("132 kV");
			cc2.setAssemblyCell("Cell G3");
			cc2.setAssemblyPhase("Phase 3");
			cc2.setAccountingorWarehouseRegistrationCode("REG-CC-2024-007");
			cc2.setFixedAssetComponentNumber("FM-0079862");
			compensationCoilRepository.save(cc2);
			
			CompensationCoil cc3 = new CompensationCoil();
			cc3.setArticleNumber("0003-2024-06");
			cc3.setProductDesign("Spiral Wound Coil with Ferrite Core");
			cc3.setUsableWith("220 kV Transmission Systems");
			
			cc3.setEquipmentSeries("CC-MAX-2024");
			cc3.setManufacturingYear(2021);
			cc3.setPifYear(2022);
			cc3.setInstallationCompany("Electromontaj");
			cc3.setPifBulletin("");
			cc3.setExploitationCenter("Bucuresti");
			cc3.setTransformationStation("Lakeside Substation");
			cc3.setVoltageLevel("220 kV");
			cc3.setAssemblyCell("Cell H1");
			cc3.setAssemblyPhase("Phase 1");
			cc3.setAccountingorWarehouseRegistrationCode("REG-CC-2024-008");
			cc3.setFixedAssetComponentNumber("FM-0081347");
			
			compensationCoilRepository.save(cc3);

			CompensationCoilMeasurement ccm1 = new CompensationCoilMeasurement();
			ccm1.setCableDimension(23);
			ccm1.setCableLength(100);
			ccm1.setCompensationCoil(cc1);
			ccm1.setInitialTemperature(57);
			ccm1.setMeasuredTemperature(55);
			compensationCoilMeasurementRepository.save(ccm1);

			CompensationCoilMeasurement ccm1a = new CompensationCoilMeasurement();
			ccm1a.setCableDimension(10);
			ccm1a.setCableLength(90);
			ccm1a.setCompensationCoil(cc1);
			ccm1a.setInitialTemperature(63);
			ccm1a.setMeasuredTemperature(61);
			compensationCoilMeasurementRepository.save(ccm1a);

			CompensationCoilMeasurement ccm2 = new CompensationCoilMeasurement();
			ccm2.setCableDimension(15);
			ccm2.setCableLength(70);
			ccm2.setCompensationCoil(cc2);
			ccm2.setInitialTemperature(76);
			ccm2.setMeasuredTemperature(74);
			compensationCoilMeasurementRepository.save(ccm2);
			
			CompensationCoilMeasurement ccm3 = new CompensationCoilMeasurement();
			ccm3.setCableDimension(13);
			ccm3.setCableLength(59);
			ccm3.setCompensationCoil(cc3);
			ccm3.setInitialTemperature(84);
			ccm3.setMeasuredTemperature(82);
			compensationCoilMeasurementRepository.save(ccm3);

			log.info("ending compensation coils initialization...");

	    };
	}
	
	@Bean
	public CommandLineRunner loadCurrentMeasuringTransformers(CurrentMeasuringTransformerRepository currentMeasuringTransformerRepository, CurrentMeasuringTransformerMeasurementRepository currentMeasuringTransformerMeasurementRepository)
	{
	    return (args) -> {	
			log.info("starting current measuring transformers initialization...");
				        
			
			CurrentMeasuringTransformer cmt1 = new CurrentMeasuringTransformer();
			cmt1.setEquipmentCode("CMT-2024-004");
			cmt1.setNominalVoltage(33.0);
			cmt1.setPrimaryNominalCurrent(1000.0);
			cmt1.setSecondaryNominalCurrent1(5.0);
			cmt1.setSecondaryNominalCurrent2(10.0);
			cmt1.setSecondaryNominalCurrent3(15.0);
			cmt1.setSecondaryNominalCurrent4(20.0);
			cmt1.setSecondaryNominalCurrent5(25.0);
			cmt1.setSecondaryNominalCurrent6(30.0);
			cmt1.setSecondaryPower1(1.0);
			cmt1.setSecondaryPower2(2.5);
			cmt1.setSecondaryPower3(5.0);
			cmt1.setSecondaryPower4(7.5);
			cmt1.setSecondaryPower5(10.0);
			cmt1.setSecondaryPower6(15.0);
			cmt1.setSecondaryPrecisionClass1(0.2);
			cmt1.setSecondaryPrecisionClass2(0.5);
			cmt1.setSecondaryPrecisionClass3(1.0);
			cmt1.setSecondaryPrecisionClass4(1.5);
			cmt1.setSecondaryPrecisionClass5(2.0);
			cmt1.setSecondaryPrecisionClass6(3.0);
			cmt1.setOperatingEnvironment("Outdoor, Temperature Range -30°C to +70°C");
			cmt1.setInsulationType("SF6 Gas Insulated");
			cmt1.setOperatingMode("Remote Control with Manual Override");
			cmt1.setManufacturer("Siemens AG");
			
			cmt1.setEquipmentSeries("CMT-ELITE-2024");
			cmt1.setManufacturingYear(2023);
			cmt1.setPifYear(2024);
			cmt1.setInstallationCompany("HighPower Installations Ltd.");
			cmt1.setPifBulletin("");
			cmt1.setExploitationCenter("Eastern Grid Operations");
			cmt1.setTransformationStation("Maplewood Substation");
			cmt1.setVoltageLevel("33 kV");
			cmt1.setAssemblyCell("Cell D3");
			cmt1.setAssemblyPhase("Phase 1");
			cmt1.setAccountingorWarehouseRegistrationCode("REG-CMT-2024-004");
			cmt1.setFixedAssetComponentNumber("FM-0045679");
			currentMeasuringTransformerRepository.save(cmt1);
			
			CurrentMeasuringTransformer cmt2 = new CurrentMeasuringTransformer();
			cmt2.setEquipmentCode("CMT-2024-005");
			cmt2.setNominalVoltage(11.0);
			cmt2.setPrimaryNominalCurrent(800.0);
			cmt2.setSecondaryNominalCurrent1(1.0);
			cmt2.setSecondaryNominalCurrent2(5.0);
			cmt2.setSecondaryNominalCurrent3(10.0);
			cmt2.setSecondaryNominalCurrent4(15.0);
			cmt2.setSecondaryNominalCurrent5(20.0);
			cmt2.setSecondaryNominalCurrent6(25.0);
			cmt2.setSecondaryPower1(2.0);
			cmt2.setSecondaryPower2(4.0);
			cmt2.setSecondaryPower3(6.0);
			cmt2.setSecondaryPower4(8.0);
			cmt2.setSecondaryPower5(10.0);
			cmt2.setSecondaryPower6(12.0);
			cmt2.setSecondaryPrecisionClass1(0.1);
			cmt2.setSecondaryPrecisionClass2(0.2);
			cmt2.setSecondaryPrecisionClass3(0.5);
			cmt2.setSecondaryPrecisionClass4(1.0);
			cmt2.setSecondaryPrecisionClass5(1.5);
			cmt2.setSecondaryPrecisionClass6(2.0);
			cmt2.setOperatingEnvironment("Indoor, Temperature Range -10°C to +60°C");
			cmt2.setInsulationType("Air Insulated");
			cmt2.setOperatingMode("Automatic with Manual Backup");
			cmt2.setManufacturer("ABB Group");
			
			cmt2.setEquipmentSeries("CMT-PRO-2024");
			cmt2.setManufacturingYear(2022);
			cmt2.setPifYear(2023);
			cmt2.setInstallationCompany("SafeInstallations Ltd.");
			cmt2.setPifBulletin("");
			cmt2.setExploitationCenter("Western Grid Operations");
			cmt2.setTransformationStation("Pinehill Substation");
			cmt2.setVoltageLevel("11 kV");
			cmt2.setAssemblyCell("Cell B1");
			cmt2.setAssemblyPhase("Phase 2");
			cmt2.setAccountingorWarehouseRegistrationCode("REG-CMT-2024-005");
			cmt2.setFixedAssetComponentNumber("FM-0056789");
			currentMeasuringTransformerRepository.save(cmt2);
			
			CurrentMeasuringTransformer cmt3 = new CurrentMeasuringTransformer();
			cmt3.setEquipmentCode("CMT-2024-006");
			cmt3.setNominalVoltage(6.6);
			cmt3.setPrimaryNominalCurrent(1200.0);
			cmt3.setSecondaryNominalCurrent1(2.0);
			cmt3.setSecondaryNominalCurrent2(4.0);
			cmt3.setSecondaryNominalCurrent3(6.0);
			cmt3.setSecondaryNominalCurrent4(8.0);
			cmt3.setSecondaryNominalCurrent5(10.0);
			cmt3.setSecondaryNominalCurrent6(12.0);
			cmt3.setSecondaryPower1(3.0);
			cmt3.setSecondaryPower2(6.0);
			cmt3.setSecondaryPower3(9.0);
			cmt3.setSecondaryPower4(12.0);
			cmt3.setSecondaryPower5(15.0);
			cmt3.setSecondaryPower6(18.0);
			cmt3.setSecondaryPrecisionClass1(0.5);
			cmt3.setSecondaryPrecisionClass2(1.0);
			cmt3.setSecondaryPrecisionClass3(1.5);
			cmt3.setSecondaryPrecisionClass4(2.0);
			cmt3.setSecondaryPrecisionClass5(2.5);
			cmt3.setSecondaryPrecisionClass6(3.0);
			cmt3.setOperatingEnvironment("Outdoor, Temperature Range -20°C to +50°C");
			cmt3.setInsulationType("Oil Insulated");
			cmt3.setOperatingMode("Manual with Remote Monitoring");
			cmt3.setManufacturer("Schneider Electric");
			
			cmt3.setEquipmentSeries("CMT-ADVANCE-2024");
			cmt3.setManufacturingYear(2023);
			cmt3.setPifYear(2024);
			cmt3.setInstallationCompany("PowerLine Installations Ltd.");
			cmt3.setPifBulletin("");
			cmt3.setExploitationCenter("Southern Grid Operations");
			cmt3.setTransformationStation("Redwood Substation");
			cmt3.setVoltageLevel("6.6 kV");
			cmt3.setAssemblyCell("Cell C4");
			cmt3.setAssemblyPhase("Phase 3");
			cmt3.setAccountingorWarehouseRegistrationCode("REG-CMT-2024-006");
			cmt3.setFixedAssetComponentNumber("FM-0067890");
			currentMeasuringTransformerRepository.save(cmt3);

			CurrentMeasuringTransformerMeasurement cmtm1 = new CurrentMeasuringTransformerMeasurement();
			cmtm1.setCurrentMeasuringTransformerHeightOne(5);
			cmtm1.setCurrentMeasuringTransformerHeightTwo(7);
			cmtm1.setInitialTemperature(40);
			cmtm1.setMeasuredTemperature(38);
			cmtm1.setCurrentMeasuringTransformer(cmt1);
			currentMeasuringTransformerMeasurementRepository.save(cmtm1);

			CurrentMeasuringTransformerMeasurement cmtm1a = new CurrentMeasuringTransformerMeasurement();
			cmtm1a.setCurrentMeasuringTransformerHeightOne(10);
			cmtm1a.setCurrentMeasuringTransformerHeightTwo(4);
			cmtm1a.setInitialTemperature(50);
			cmtm1a.setMeasuredTemperature(47);
			cmtm1a.setCurrentMeasuringTransformer(cmt1);
			currentMeasuringTransformerMeasurementRepository.save(cmtm1a);

			CurrentMeasuringTransformerMeasurement cmtm2 = new CurrentMeasuringTransformerMeasurement();
			cmtm2.setCurrentMeasuringTransformerHeightOne(23);
			cmtm2.setCurrentMeasuringTransformerHeightTwo(7);
			cmtm2.setInitialTemperature(76);
			cmtm2.setMeasuredTemperature(74);
			cmtm2.setCurrentMeasuringTransformer(cmt2);
			currentMeasuringTransformerMeasurementRepository.save(cmtm2);
			
			CurrentMeasuringTransformerMeasurement cmtm3 = new CurrentMeasuringTransformerMeasurement();
			cmtm3.setCurrentMeasuringTransformerHeightOne(15);
			cmtm3.setCurrentMeasuringTransformerHeightTwo(8);
			cmtm3.setInitialTemperature(95);
			cmtm3.setMeasuredTemperature(93);
			cmtm3.setCurrentMeasuringTransformer(cmt3);
			currentMeasuringTransformerMeasurementRepository.save(cmtm3);

			log.info("ending current measuring transformers initialization...");

	    };
	}
	
	@Bean
	public CommandLineRunner loadGroundingKnives(GroundingKnifeRepository groundingKnifeRepository,GroundingKnifeMeasurementRepository groundingKnifeMeasurementRepository)
	{
	    return (args) -> {	
			log.info("starting grounding knives initialization...");
				        
			
			GroundingKnife gk1 = new GroundingKnife();
			gk1.setEquipmentCode("GK-2024-001");
			gk1.setInstallationType("Outdoor, Fixed Installation");
			gk1.setStructureType("Double Break Isolator");
			gk1.setCurrent(630);
			gk1.setVoltage(33);
			gk1.setFrequency(50);
			gk1.setPhaseDifference("120 degrees");
			gk1.setRelatedShortTimeWithstandCurrent("25 kA for 3 seconds");
			gk1.setAltitude("Up to 2000 meters above sea level");
			gk1.setTransportPackage("Wooden Crate with Shock-Absorbing Foam");
			gk1.setTradeMark("GroundSafe™");
			gk1.setOrigin("Germany");
			gk1.setProductionCapacity("500 Units/Year");
			gk1.setEquipmentSeries("GK-SAFE-2024");
			gk1.setManufacturingYear(2023);
			gk1.setPifYear(2024);
			gk1.setInstallationCompany("SecureLine Electrical Installations Ltd.");
			gk1.setPifBulletin("");
			gk1.setExploitationCenter("Central Grid Operations");
			gk1.setTransformationStation("Greenfield Substation");
			gk1.setVoltageLevel("33 kV");
			gk1.setAssemblyCell("Cell D5");
			gk1.setAssemblyPhase("Phase 2");
			gk1.setAccountingorWarehouseRegistrationCode("REG-GK-2024-001");
			gk1.setFixedAssetComponentNumber("FM-0012345");
			groundingKnifeRepository.save(gk1);
			
			GroundingKnife gk2 = new GroundingKnife();
			gk2.setEquipmentCode("GK-2024-002");
			gk2.setInstallationType("Indoor, Modular Installation");
			gk2.setStructureType("Single Break Isolator");
			gk2.setCurrent(800);
			gk2.setVoltage(66);
			gk2.setFrequency(60);
			gk2.setPhaseDifference("90 degrees");
			gk2.setRelatedShortTimeWithstandCurrent("20 kA for 1 second");
			gk2.setAltitude("Up to 1500 meters above sea level");
			gk2.setTransportPackage("Reinforced Steel Case with Vibration Dampening");
			gk2.setTradeMark("PowerGuard™");
			gk2.setOrigin("United States");
			gk2.setProductionCapacity("300 Units/Year");
			gk2.setEquipmentSeries("GK-PRO-2024");
			gk2.setManufacturingYear(2022);
			gk2.setPifYear(2023);
			gk2.setInstallationCompany("HighTech Electrical Installations Inc.");
			gk2.setPifBulletin("");
			gk2.setExploitationCenter("Northern Grid Operations");
			gk2.setTransformationStation("Mountain Ridge Substation");
			gk2.setVoltageLevel("66 kV");
			gk2.setAssemblyCell("Cell C3");
			gk2.setAssemblyPhase("Phase 1");
			gk2.setAccountingorWarehouseRegistrationCode("REG-GK-2024-002");
			gk2.setFixedAssetComponentNumber("FM-0024567");
			groundingKnifeRepository.save(gk2);
			
			GroundingKnife gk3 = new GroundingKnife();
			gk3.setEquipmentCode("GK-2024-003");
			gk3.setInstallationType("Outdoor, Pole-Mounted Installation");
			gk3.setStructureType("Vertical Break Isolator");
			gk3.setCurrent(1200);
			gk3.setVoltage(110);
			gk3.setFrequency(50);
			gk3.setPhaseDifference("180 degrees");
			gk3.setRelatedShortTimeWithstandCurrent("31.5 kA for 3 seconds");
			gk3.setAltitude("Up to 1000 meters above sea level");
			gk3.setTransportPackage("Heavy-Duty Wooden Crate with Moisture Protection");
			gk3.setTradeMark("SafeSwitch™");
			gk3.setOrigin("Canada");
			gk3.setProductionCapacity("200 Units/Year");
			gk3.setEquipmentSeries("GK-ELITE-2024");
			gk3.setManufacturingYear(2023);
			gk3.setPifYear(2024);
			gk3.setInstallationCompany("PowerLine Installations Ltd.");
			gk3.setPifBulletin("");
			gk3.setExploitationCenter("Western Grid Operations");
			gk3.setTransformationStation("Valley View Substation");
			gk3.setVoltageLevel("110 kV");
			gk3.setAssemblyCell("Cell A1");
			gk3.setAssemblyPhase("Phase 4");
			gk3.setAccountingorWarehouseRegistrationCode("REG-GK-2024-003");
			gk3.setFixedAssetComponentNumber("FM-0036789");
			groundingKnifeRepository.save(gk3);

			GroundingKnifeMeasurement gkm1 = new GroundingKnifeMeasurement();
			gkm1.setrRadius(150);
			gkm1.setGroundingKnife(gk1);
			gkm1.setInitialTemperature(45);
			gkm1.setMeasuredTemperature(43);
			groundingKnifeMeasurementRepository.save(gkm1);

			GroundingKnifeMeasurement gkm1a = new GroundingKnifeMeasurement();
			gkm1a.setrRadius(200);
			gkm1a.setGroundingKnife(gk1);
			gkm1a.setInitialTemperature(50);
			gkm1a.setMeasuredTemperature(48);
			groundingKnifeMeasurementRepository.save(gkm1a);

			GroundingKnifeMeasurement gkm2 = new GroundingKnifeMeasurement();
			gkm2.setrRadius(175);
			gkm2.setGroundingKnife(gk2);
			gkm2.setInitialTemperature(67);
			gkm2.setMeasuredTemperature(66);
			groundingKnifeMeasurementRepository.save(gkm2);
			
			GroundingKnifeMeasurement gkm3 = new GroundingKnifeMeasurement();
			gkm3.setrRadius(160);
			gkm3.setGroundingKnife(gk3);
			gkm3.setInitialTemperature(88);
			gkm3.setMeasuredTemperature(87);
			groundingKnifeMeasurementRepository.save(gkm3);

			log.info("ending grounding knives initialization...");

	    };
	}
	
	@Bean
	public CommandLineRunner loadTransformationUnits(TransformationUnitsRepository transformationUnitsRepository,TransformationUnitsMeasurementRepository transformationUnitsMeasurementRepository)
	{
	    return (args) -> {	
			log.info("starting transformation units initialization...");
				        
			
			TransformationUnits tu1 = new TransformationUnits();
			tu1.setMinimumAirTemperatureValue("-25°C");
			tu1.setMinimumAirTemperatureObservation("Suitable for extreme cold environments");
			tu1.setMaximumAirTemperatureValue("40°C");
			tu1.setMaximumAirTemperatureObservation("Operates efficiently up to high temperatures");
			tu1.setMaximumAltitudeValue("1000 meters above sea level");
			tu1.setMaximumAltitudeObservation("Designed for standard altitudes without derating");
			tu1.setRatedVoltageOfMediumVoltageCircuitsValue("20 kV");
			tu1.setRatedVoltageOfMediumVoltageCircuitsObservation("Designed for medium voltage distribution networks");
			tu1.setEquipmentSeries("TU-ADVANCE-2024");
			tu1.setManufacturingYear(2023);
			tu1.setPifYear(2024);
			tu1.setInstallationCompany("Electra Installations Ltd.");
			tu1.setPifBulletin("");
			tu1.setExploitationCenter("Eastern Grid Operations");
			tu1.setTransformationStation("Granite Peak Substation");
			tu1.setVoltageLevel("20 kV");
			tu1.setAssemblyCell("Cell D4");
			tu1.setAssemblyPhase("Phase 2");
			tu1.setAccountingorWarehouseRegistrationCode("REG-TU-2024-009");
			tu1.setFixedAssetComponentNumber("FM-0092871");
			transformationUnitsRepository.save(tu1);
			
			TransformationUnits tu2 = new TransformationUnits();
			tu2.setMinimumAirTemperatureValue("-30°C");
			tu2.setMinimumAirTemperatureObservation("Suitable for arctic and sub-arctic climates");
			tu2.setMaximumAirTemperatureValue("50°C");
			tu2.setMaximumAirTemperatureObservation("Capable of operating in desert environments");
			tu2.setMaximumAltitudeValue("1500 meters above sea level");
			tu2.setMaximumAltitudeObservation("Requires no performance derating at specified altitude");
			tu2.setRatedVoltageOfMediumVoltageCircuitsValue("33 kV");
			tu2.setRatedVoltageOfMediumVoltageCircuitsObservation("Optimized for high-efficiency power distribution");
			tu2.setEquipmentSeries("TU-PRO-2024");
			tu2.setManufacturingYear(2022);
			tu2.setPifYear(2023);
			tu2.setInstallationCompany("Global Energy Solutions");
			tu2.setPifBulletin("");
			tu2.setExploitationCenter("Northern Grid Operations");
			tu2.setTransformationStation("Silver Lake Substation");
			tu2.setVoltageLevel("33 kV");
			tu2.setAssemblyCell("Cell E5");
			tu2.setAssemblyPhase("Phase 3");
			tu2.setAccountingorWarehouseRegistrationCode("REG-TU-2024-010");
			tu2.setFixedAssetComponentNumber("FM-0103459");
			transformationUnitsRepository.save(tu2);
			
			TransformationUnits tu3 = new TransformationUnits();
			tu3.setMinimumAirTemperatureValue("-20°C");
			tu3.setMinimumAirTemperatureObservation("Designed for temperate climates with occasional cold spells");
			tu3.setMaximumAirTemperatureValue("45°C");
			tu3.setMaximumAirTemperatureObservation("Suitable for hot and humid environments");
			tu3.setMaximumAltitudeValue("2000 meters above sea level");
			tu3.setMaximumAltitudeObservation("Capable of operating efficiently in high-altitude regions");
			tu3.setRatedVoltageOfMediumVoltageCircuitsValue("11 kV");
			tu3.setRatedVoltageOfMediumVoltageCircuitsObservation("Ideal for industrial and urban power distribution");
			tu3.setEquipmentSeries("TU-ELITE-2024");
			tu3.setManufacturingYear(2021);
			tu3.setPifYear(2022);
			tu3.setInstallationCompany("HighTech Electrical Contractors");
			tu3.setPifBulletin("");
			tu3.setExploitationCenter("Southern Grid Operations");
			tu3.setTransformationStation("Riverside Substation");
			tu3.setVoltageLevel("11 kV");
			tu3.setAssemblyCell("Cell B3");
			tu3.setAssemblyPhase("Phase 1");
			tu3.setAccountingorWarehouseRegistrationCode("REG-TU-2024-011");
			tu3.setFixedAssetComponentNumber("FM-0114523");
			transformationUnitsRepository.save(tu3);

			TransformationUnitsMeasurement tum1 = new TransformationUnitsMeasurement();
			tum1.setTransformationUnits(tu1);
			tum1.setInitialTemperature(36);
			tum1.setMeasuredTemperature(35);
			transformationUnitsMeasurementRepository.save(tum1);

			TransformationUnitsMeasurement tum1a = new TransformationUnitsMeasurement();
			tum1a.setTransformationUnits(tu1);
			tum1a.setInitialTemperature(45);
			tum1a.setMeasuredTemperature(43);
			transformationUnitsMeasurementRepository.save(tum1a);
			
			TransformationUnitsMeasurement tum1b = new TransformationUnitsMeasurement();
			tum1b.setTransformationUnits(tu1);
			tum1b.setInitialTemperature(57);
			tum1b.setMeasuredTemperature(55);
			transformationUnitsMeasurementRepository.save(tum1b);
			
			TransformationUnitsMeasurement tum1c = new TransformationUnitsMeasurement();
			tum1c.setTransformationUnits(tu1);
			tum1c.setInitialTemperature(35);
			tum1c.setMeasuredTemperature(33);
			transformationUnitsMeasurementRepository.save(tum1c);

			TransformationUnitsMeasurement tum2 = new TransformationUnitsMeasurement();
			tum2.setTransformationUnits(tu2);
			tum2.setInitialTemperature(83);
			tum2.setMeasuredTemperature(80);
			transformationUnitsMeasurementRepository.save(tum2);
			
			TransformationUnitsMeasurement tum3 = new TransformationUnitsMeasurement();
			tum3.setTransformationUnits(tu3);
			tum3.setInitialTemperature(74);
			tum3.setMeasuredTemperature(72);
			transformationUnitsMeasurementRepository.save(tum3);

			log.info("ending transformation units initialization...");

	    };
	}
	
	@Bean
	public CommandLineRunner loadGroundingKnifeActuationDevice(GroundingKnifeActuationDeviceRepository groundingKnifeActuationDeviceRepository,GroundingKnifeActuationDeviceMeasurementRepository groundingKnifeActuationDeviceMeasurementRepository)
	{
	    return (args) -> {	
			log.info("starting grounding knife actuation device initialization...");
				        
			
			GroundingKnifeActuationDevice gkad1 = new GroundingKnifeActuationDevice();
			gkad1.setBladeMaterial("Copper Alloy");
			gkad1.setHandleMaterial("High-Density Rubber");
			gkad1.setBladeLength(300.0);
			gkad1.setBladeThickness(6);
			gkad1.setHandleLength(180.0);
			gkad1.setCurrentCapacity(15.0);
			gkad1.setInsulationResistance("200 MΩ");
			gkad1.setOperatingTemperature("-30°C to 80°C");
			gkad1.setHumidityResistance("98% RH");
			gkad1.setEquipmentSeries("GKA-SECURE-2024");
			gkad1.setManufacturingYear(2023);
			gkad1.setPifYear(2024);
			gkad1.setInstallationCompany("SafeGround Installations Ltd.");
			gkad1.setPifBulletin("");
			gkad1.setExploitationCenter("Western Grid Operations");
			gkad1.setTransformationStation("Cedar Ridge Substation");
			gkad1.setVoltageLevel("35 kV");
			gkad1.setAssemblyCell("Cell D2");
			gkad1.setAssemblyPhase("Phase 3");
			gkad1.setAccountingorWarehouseRegistrationCode("REG-GKA-2024-019");
			gkad1.setFixedAssetComponentNumber("FM-0198763");
			groundingKnifeActuationDeviceRepository.save(gkad1);
			
			GroundingKnifeActuationDevice gkad2 = new GroundingKnifeActuationDevice();
			gkad2.setBladeMaterial("Stainless Steel");
			gkad2.setHandleMaterial("Thermoplastic Elastomer (TPE)");
			gkad2.setBladeLength(250.0);
			gkad2.setBladeThickness(4);
			gkad2.setHandleLength(200.0);
			gkad2.setCurrentCapacity(20.0);
			gkad2.setInsulationResistance("150 MΩ");
			gkad2.setOperatingTemperature("-40°C to 75°C");
			gkad2.setHumidityResistance("90% RH");
			gkad2.setEquipmentSeries("GKA-PROTECT-2024");
			gkad2.setManufacturingYear(2022);
			gkad2.setPifYear(2023);
			gkad2.setInstallationCompany("GroundTech Solutions Inc.");
			gkad2.setPifBulletin("");
			gkad2.setExploitationCenter("Eastern Grid Operations");
			gkad2.setTransformationStation("Maple Grove Substation");
			gkad2.setVoltageLevel("25 kV");
			gkad2.setAssemblyCell("Cell A4");
			gkad2.setAssemblyPhase("Phase 2");
			gkad2.setAccountingorWarehouseRegistrationCode("REG-GKA-2024-020");
			gkad2.setFixedAssetComponentNumber("FM-0209845");
			groundingKnifeActuationDeviceRepository.save(gkad2);
			
			GroundingKnifeActuationDevice gkad3 = new GroundingKnifeActuationDevice();
			gkad3.setBladeMaterial("Bronze Alloy");
			gkad3.setHandleMaterial("Vulcanized Rubber");
			gkad3.setBladeLength(275.0);
			gkad3.setBladeThickness(5);
			gkad3.setHandleLength(190.0);
			gkad3.setCurrentCapacity(25.0);
			gkad3.setInsulationResistance("180 MΩ");
			gkad3.setOperatingTemperature("-35°C to 85°C");
			gkad3.setHumidityResistance("92% RH");
			gkad3.setEquipmentSeries("GKA-SHIELD-2024");
			gkad3.setManufacturingYear(2023);
			gkad3.setPifYear(2024);
			gkad3.setInstallationCompany("SafeGround Electrical Installations Ltd.");
			gkad3.setPifBulletin("");
			gkad3.setExploitationCenter("Northern Grid Operations");
			gkad3.setTransformationStation("Birchwood Substation");
			gkad3.setVoltageLevel("30 kV");
			gkad3.setAssemblyCell("Cell C2");
			gkad3.setAssemblyPhase("Phase 3");
			gkad3.setAccountingorWarehouseRegistrationCode("REG-GKA-2024-021");
			gkad3.setFixedAssetComponentNumber("FM-0215732");
			groundingKnifeActuationDeviceRepository.save(gkad3);

			GroundingKnifeActuationDeviceMeasurement gkadm1 = new GroundingKnifeActuationDeviceMeasurement();
			gkadm1.setGroundingKnifeActuationDevice(gkad1);
			gkadm1.setInitialTemperature(53);
			gkadm1.setMeasuredTemperature(53);
			groundingKnifeActuationDeviceMeasurementRepository.save(gkadm1);

			GroundingKnifeActuationDeviceMeasurement gkadm1a = new GroundingKnifeActuationDeviceMeasurement();
			gkadm1a.setGroundingKnifeActuationDevice(gkad1);
			gkadm1a.setInitialTemperature(47);
			gkadm1a.setMeasuredTemperature(45);
			groundingKnifeActuationDeviceMeasurementRepository.save(gkadm1a);

			GroundingKnifeActuationDeviceMeasurement gkadm2 = new GroundingKnifeActuationDeviceMeasurement();
			gkadm2.setGroundingKnifeActuationDevice(gkad2);
			gkadm2.setInitialTemperature(73);
			gkadm2.setMeasuredTemperature(71);
			groundingKnifeActuationDeviceMeasurementRepository.save(gkadm2);
			
			GroundingKnifeActuationDeviceMeasurement gkadm3 = new GroundingKnifeActuationDeviceMeasurement();
			gkadm3.setGroundingKnifeActuationDevice(gkad3);
			gkadm3.setInitialTemperature(84);
			gkadm3.setMeasuredTemperature(83);
			groundingKnifeActuationDeviceMeasurementRepository.save(gkadm3);

			log.info("ending grounding knife actuation device initialization...");

	    };
	}

	@Bean
	public CommandLineRunner loadMixedMeasuringTransformer(MixedMeasuringTransformerRepository mixedMeasuringTransformerRepository,MixedMeasuringTransformerMeasurementRepository mixedMeasuringTransformerMeasurementRepository)
	{
	    return (args) -> {	
			log.info("starting mixed measuring transformer initialization...");
				        
			
			MixedMeasuringTransformer mmt1 = new MixedMeasuringTransformer();
			mmt1.setDesigningPurpose("Combined measurement of current and voltage in high-voltage systems");
			mmt1.setPrimaryCurrent(600);
			mmt1.setSecondaryCurrent(5);
			mmt1.setBurden(30);
			mmt1.setAccuracyClass(0.2);
			mmt1.setEquipmentSeries("MMT-PRO-2024");
			mmt1.setManufacturingYear(2023);
			mmt1.setPifYear(2024);
			mmt1.setInstallationCompany("ElectroMech Solutions Ltd.");
			mmt1.setPifBulletin("");
			mmt1.setExploitationCenter("Western Grid Operations");
			mmt1.setTransformationStation("Eagle Ridge Substation");
			mmt1.setVoltageLevel("110 kV");
			mmt1.setAssemblyCell("Cell A2");
			mmt1.setAssemblyPhase("Phase 1");
			mmt1.setAccountingorWarehouseRegistrationCode("REG-MMT-2024-012");
			mmt1.setFixedAssetComponentNumber("FM-0125678");
			mixedMeasuringTransformerRepository.save(mmt1);
			
			MixedMeasuringTransformer mmt2 = new MixedMeasuringTransformer();
			mmt2.setDesigningPurpose("Accurate metering and protection in medium-voltage networks");
			mmt2.setPrimaryCurrent(800);
			mmt2.setSecondaryCurrent(1);
			mmt2.setBurden(25);
			mmt2.setAccuracyClass(0.5);
			mmt2.setEquipmentSeries("MMT-ELITE-2024");
			mmt2.setManufacturingYear(2022);
			mmt2.setPifYear(2023);
			mmt2.setInstallationCompany("PowerGrid Installations Inc.");
			mmt2.setPifBulletin("");
			mmt2.setExploitationCenter("Central Grid Operations");
			mmt2.setTransformationStation("Pinewood Substation");
			mmt2.setVoltageLevel("66 kV");
			mmt2.setAssemblyCell("Cell C4");
			mmt2.setAssemblyPhase("Phase 2");
			mmt2.setAccountingorWarehouseRegistrationCode("REG-MMT-2024-013");
			mmt2.setFixedAssetComponentNumber("FM-0137890");
			mixedMeasuringTransformerRepository.save(mmt2);
			
			MixedMeasuringTransformer mmt3 = new MixedMeasuringTransformer();
			mmt3.setDesigningPurpose("Dual-function measurement for current and voltage in industrial power systems");
			mmt3.setPrimaryCurrent(1000);
			mmt3.setSecondaryCurrent(5);
			mmt3.setBurden(20);
			mmt3.setAccuracyClass(0.1);
			mmt3.setEquipmentSeries("MMT-ADVANCE-2024");
			mmt3.setManufacturingYear(2021);
			mmt3.setPifYear(2022);
			mmt3.setInstallationCompany("Industrial Electrical Contractors Ltd.");
			mmt3.setPifBulletin("");
			mmt3.setExploitationCenter("Northern Grid Operations");
			mmt3.setTransformationStation("Maple Valley Substation");
			mmt3.setVoltageLevel("33 kV");
			mmt3.setAssemblyCell("Cell B1");
			mmt3.setAssemblyPhase("Phase 3");
			mmt3.setAccountingorWarehouseRegistrationCode("REG-MMT-2024-014");
			mmt3.setFixedAssetComponentNumber("FM-0142345");
			mixedMeasuringTransformerRepository.save(mmt3);

			MixedMeasuringTransformerMeasurement mmtm1 = new MixedMeasuringTransformerMeasurement();
			mmtm1.setPrimaryTerminalMaterial("Copper Alloy");
			mmtm1.setMinimumCreepageDistance(2);
			mmtm1.setGravityCenter(1);
			mmtm1.setInsulationMaterial("Epoxy Resin");
			mmtm1.setMixedMeasuringTransformer(mmt1);
			mmtm1.setInitialTemperature(76);
			mmtm1.setMeasuredTemperature(73);
			mixedMeasuringTransformerMeasurementRepository.save(mmtm1);

			MixedMeasuringTransformerMeasurement mmtm1a = new MixedMeasuringTransformerMeasurement();
			mmtm1a.setPrimaryTerminalMaterial("Aluminum");
			mmtm1a.setMinimumCreepageDistance(3);
			mmtm1a.setGravityCenter(2);
			mmtm1a.setInsulationMaterial("Silicone Rubber");
			mmtm1a.setMixedMeasuringTransformer(mmt1);
			mmtm1a.setInitialTemperature(85);
			mmtm1a.setMeasuredTemperature(83);
			mixedMeasuringTransformerMeasurementRepository.save(mmtm1a);
			
			MixedMeasuringTransformerMeasurement mmtm2 = new MixedMeasuringTransformerMeasurement();
			mmtm2.setPrimaryTerminalMaterial("Silver-Plated Copper");
			mmtm2.setMinimumCreepageDistance(400);
			mmtm2.setGravityCenter(3);
			mmtm2.setInsulationMaterial("Porcelain");
			mmtm2.setMixedMeasuringTransformer(mmt2);
			mmtm2.setInitialTemperature(50);
			mmtm2.setMeasuredTemperature(48);
			mixedMeasuringTransformerMeasurementRepository.save(mmtm2);
			
			MixedMeasuringTransformerMeasurement mmtm3 = new MixedMeasuringTransformerMeasurement();
			mmtm3.setPrimaryTerminalMaterial("Brass");
			mmtm3.setMinimumCreepageDistance(600);
			mmtm3.setGravityCenter(4);
			mmtm3.setInsulationMaterial("Polyurethane");
			mmtm3.setMixedMeasuringTransformer(mmt3);
			mmtm3.setInitialTemperature(45);
			mmtm3.setMeasuredTemperature(43);
			mixedMeasuringTransformerMeasurementRepository.save(mmtm3);

			log.info("ending mixed measuring transformer initialization...");

	    };
	}

	@Bean
	public CommandLineRunner loadSeparatorActuationDevice(SeparatorActuationDeviceRepository separatorActuationDeviceRepository,SeparatorActuationDeviceMeasurementRepository separatorActuationDeviceMeasurementRepository)
	{
	    return (args) -> {	
			log.info("starting separator actuation device initialization...");
				        
			
			SeparatorActuationDevice sad1 = new SeparatorActuationDevice();
			sad1.setDeviceTypeForMainKnives("Rotary Actuator");
			sad1.setDeviceTypeForGroundingKnife("Linear Actuator");
			sad1.setVoltageOperationForElectricMotor(230);
			sad1.setLimitOperationForElectricMotor(50);
			sad1.setMaximumPowerOperationForElectricMotor(1);
			sad1.setVoltageControlForMainKnives(110);
			sad1.setLimitControlForMainKnives(60);
			sad1.setMaximumPowerControlForMainKnives(2);
			sad1.setModeOfAchievingTheLockingDeviceInPosition("Mechanical Lock");
			sad1.setAdditionalResistance("10 Ω");
			sad1.setDegreeOfProtection("IP54");
			sad1.setEquipmentSeries("SAD-PRO-2024");
			sad1.setManufacturingYear(2023);
			sad1.setPifYear(2024);
			sad1.setInstallationCompany("ElectroInstallations Ltd.");
			sad1.setPifBulletin("");
			sad1.setExploitationCenter("Eastern Grid Operations");
			sad1.setTransformationStation("Oakridge Substation");
			sad1.setVoltageLevel("110 kV");
			sad1.setAssemblyCell("Cell A3");
			sad1.setAssemblyPhase("Phase 2");
			sad1.setAccountingorWarehouseRegistrationCode("REG-SAD-2024-001");
			sad1.setFixedAssetComponentNumber("FM-0019786");
			separatorActuationDeviceRepository.save(sad1);
			
			SeparatorActuationDevice sad2 = new SeparatorActuationDevice();
			sad2.setDeviceTypeForMainKnives("Pneumatic Actuator");
			sad2.setDeviceTypeForGroundingKnife("Hydraulic Actuator");
			sad2.setVoltageOperationForElectricMotor(400);
			sad2.setLimitOperationForElectricMotor(40);
			sad2.setMaximumPowerOperationForElectricMotor(3);
			sad2.setVoltageControlForMainKnives(24);
			sad2.setLimitControlForMainKnives(30);
			sad2.setMaximumPowerControlForMainKnives(1);
			sad2.setModeOfAchievingTheLockingDeviceInPosition("Electromagnetic Lock");
			sad2.setAdditionalResistance("5 Ω");
			sad2.setDegreeOfProtection("IP65");
			sad2.setEquipmentSeries("SAD-ELITE-2024");
			sad2.setManufacturingYear(2022);
			sad2.setPifYear(2023);
			sad2.setInstallationCompany("PowerTech Installations Inc.");
			sad2.setPifBulletin("");
			sad2.setExploitationCenter("Northern Grid Operations");
			sad2.setTransformationStation("Riverbend Substation");
			sad2.setVoltageLevel("66 kV");
			sad2.setAssemblyCell("Cell B5");
			sad2.setAssemblyPhase("Phase 3");
			sad2.setAccountingorWarehouseRegistrationCode("REG-SAD-2024-002");
			sad2.setFixedAssetComponentNumber("FM-0023467");
			separatorActuationDeviceRepository.save(sad2);
			
			SeparatorActuationDevice sad3 = new SeparatorActuationDevice();
			sad3.setDeviceTypeForMainKnives("Electric Motor Actuator");
			sad3.setDeviceTypeForGroundingKnife("Manual Lever Actuator");
			sad3.setVoltageOperationForElectricMotor(380);
			sad3.setLimitOperationForElectricMotor(60);
			sad3.setMaximumPowerOperationForElectricMotor(4);
			sad3.setVoltageControlForMainKnives(48);
			sad3.setLimitControlForMainKnives(20);
			sad3.setMaximumPowerControlForMainKnives(1);
			sad3.setModeOfAchievingTheLockingDeviceInPosition("Spring-Loaded Lock");
			sad3.setAdditionalResistance("8 Ω");
			sad3.setDegreeOfProtection("IP67");
			sad3.setEquipmentSeries("SAD-ADVANCE-2024");
			sad3.setManufacturingYear(2023);
			sad3.setPifYear(2024);
			sad3.setInstallationCompany("SafeLine Installations Ltd.");
			sad3.setPifBulletin("");
			sad3.setExploitationCenter("Southern Grid Operations");
			sad3.setTransformationStation("Silverwood Substation");
			sad3.setVoltageLevel("33 kV");
			sad3.setAssemblyCell("Cell D4");
			sad3.setAssemblyPhase("Phase 1");
			sad3.setAccountingorWarehouseRegistrationCode("REG-SAD-2024-003");
			sad3.setFixedAssetComponentNumber("FM-0031298");
			separatorActuationDeviceRepository.save(sad3);

			SeparatorActuationDeviceMeasurement sadm1 = new SeparatorActuationDeviceMeasurement();
			sadm1.setSeparatorActuationDevice(sad1);
			sadm1.setInitialTemperature(70);
			sadm1.setMeasuredTemperature(75);
			separatorActuationDeviceMeasurementRepository.save(sadm1);

			SeparatorActuationDeviceMeasurement sadm1a = new SeparatorActuationDeviceMeasurement();
			sadm1a.setSeparatorActuationDevice(sad1);
			sadm1a.setInitialTemperature(57);
			sadm1a.setMeasuredTemperature(55);
			separatorActuationDeviceMeasurementRepository.save(sadm1a);

			SeparatorActuationDeviceMeasurement sadm2 = new SeparatorActuationDeviceMeasurement();
			sadm2.setSeparatorActuationDevice(sad2);
			sadm2.setInitialTemperature(80);
			sadm2.setMeasuredTemperature(78);
			separatorActuationDeviceMeasurementRepository.save(sadm2);
			
			SeparatorActuationDeviceMeasurement sadm3 = new SeparatorActuationDeviceMeasurement();
			sadm3.setSeparatorActuationDevice(sad3);
			sadm3.setInitialTemperature(96);
			sadm3.setMeasuredTemperature(94);
			separatorActuationDeviceMeasurementRepository.save(sadm3);

			log.info("ending separator actuation device initialization...");

	    };
	}

	@Bean
	public CommandLineRunner loadSwitchActuationDevice(SwitchActuationDeviceRepository switchActuationDeviceRepository,SwitchActuationDeviceMeasurementRepository switchActuationDeviceMeasurementRepository)
	{
	    return (args) -> {	
			log.info("starting transformation units initialization...");
				        
			
			SwitchActuationDevice swad1 = new SwitchActuationDevice();
			swad1.setRatedVoltageUnitOfMeasure("kV");
			swad1.setRatedVoltageValue(12);
			swad1.setRatedVoltageDataPresentedByTheBinder("IEC 62271-1");
			swad1.setOperatingVoltageUnitOfMeasure("V");
			swad1.setOperatingVoltageValue("230 V AC");
			swad1.setOperatingPresentedByTheBinder("IEC 60947-1");
			swad1.setRatedCurrentUnitOfMeasure("A");
			swad1.setRatedCurrentValue(630);
			swad1.setRatedCurrentDataPresentedByTheBinder("IEC 62271-100");
			swad1.setWorkingDisconnectingCapacityUnitOfMeasure("kA");
			swad1.setWorkingDisconnectingCapacityValue(25);
			swad1.setWorkingDisconnectingCapacityDataPresentedByTheBinder("IEC 62271-102");
			swad1.setMaximumDisconnectingCapacityUnitOfMeasure("kA");
			swad1.setMaximumDisconnectingCapacityValue(31);
			swad1.setMaximumDisconnectingCapacityDataPresentedByTheBinder("IEC 62271-102");
			swad1.setProtectionDegreeUnitOfMeasure("IP");
			swad1.setProtectionDegreeValue("IP54");
			swad1.setProtectionDegreeDataPresentedByTheBinder("IEC 60529");
			swad1.setAuxiliarySignalingContactsUnitOfMeasure("Number");
			swad1.setAuxiliarySignalingContactsValue(4);
			swad1.setAuxiliarySignalingContactsDataPresentedByTheBinder("IEC 60947-5-1");
			
			swad1.setEquipmentSeries("SWAD-TECH-2024");
			swad1.setManufacturingYear(2023);
			swad1.setPifYear(2024);
			swad1.setInstallationCompany("VoltSafe Installations Ltd.");
			swad1.setPifBulletin("");
			swad1.setExploitationCenter("Western Grid Operations");
			swad1.setTransformationStation("Ridgeway Substation");
			swad1.setVoltageLevel("12 kV");
			swad1.setAssemblyCell("Cell C5");
			swad1.setAssemblyPhase("Phase 3");
			swad1.setAccountingorWarehouseRegistrationCode("REG-SWAD-2024-004");
			swad1.setFixedAssetComponentNumber("FM-0045678");
			switchActuationDeviceRepository.save(swad1);
			
			SwitchActuationDevice swad2 = new SwitchActuationDevice();
			swad2.setRatedVoltageUnitOfMeasure("kV");
			swad2.setRatedVoltageValue(24);
			swad2.setRatedVoltageDataPresentedByTheBinder("IEC 60038");
			swad2.setOperatingVoltageUnitOfMeasure("V");
			swad2.setOperatingVoltageValue("110 V AC");
			swad2.setOperatingPresentedByTheBinder("IEC 60947-3");
			swad2.setRatedCurrentUnitOfMeasure("A");
			swad2.setRatedCurrentValue(800);
			swad2.setRatedCurrentDataPresentedByTheBinder("IEC 62271-200");
			swad2.setWorkingDisconnectingCapacityUnitOfMeasure("kA");
			swad2.setWorkingDisconnectingCapacityValue(20);
			swad2.setWorkingDisconnectingCapacityDataPresentedByTheBinder("IEC 60947-2");
			swad2.setMaximumDisconnectingCapacityUnitOfMeasure("kA");
			swad2.setMaximumDisconnectingCapacityValue(40);
			swad2.setMaximumDisconnectingCapacityDataPresentedByTheBinder("IEC 62271-102");
			swad2.setProtectionDegreeUnitOfMeasure("IP");
			swad2.setProtectionDegreeValue("IP65");
			swad2.setProtectionDegreeDataPresentedByTheBinder("IEC 60529");
			swad2.setAuxiliarySignalingContactsUnitOfMeasure("Number");
			swad2.setAuxiliarySignalingContactsValue(6);
			swad2.setAuxiliarySignalingContactsDataPresentedByTheBinder("IEC 60947-5-1");
			
			swad2.setEquipmentSeries("SWAD-PRO-2024");
			swad2.setManufacturingYear(2022);
			swad2.setPifYear(2023);
			swad2.setInstallationCompany("ElectroSecure Installations Ltd.");
			swad2.setPifBulletin("");
			swad2.setExploitationCenter("Southern Grid Operations");
			swad2.setTransformationStation("Bluewater Substation");
			swad2.setVoltageLevel("24 kV");
			swad2.setAssemblyCell("Cell B2");
			swad2.setAssemblyPhase("Phase 5");
			swad2.setAccountingorWarehouseRegistrationCode("REG-SWAD-2024-005");
			swad2.setFixedAssetComponentNumber("FM-0056789");
			switchActuationDeviceRepository.save(swad2);
			
			SwitchActuationDevice swad3 = new SwitchActuationDevice();
			swad3.setRatedVoltageUnitOfMeasure("kV");
			swad3.setRatedVoltageValue(36);
			swad3.setRatedVoltageDataPresentedByTheBinder("IEC 62271-1");
			swad3.setOperatingVoltageUnitOfMeasure("V");
			swad3.setOperatingVoltageValue("48 V AC");
			swad3.setOperatingPresentedByTheBinder("IEC 60947-1");
			swad3.setRatedCurrentUnitOfMeasure("A");
			swad3.setRatedCurrentValue(1000);
			swad3.setRatedCurrentDataPresentedByTheBinder("IEC 62271-200");
			swad3.setWorkingDisconnectingCapacityUnitOfMeasure("kA");
			swad3.setWorkingDisconnectingCapacityValue(16);
			swad3.setWorkingDisconnectingCapacityDataPresentedByTheBinder("IEC 60947-3");
			swad3.setMaximumDisconnectingCapacityUnitOfMeasure("kA");
			swad3.setMaximumDisconnectingCapacityValue(50);
			swad3.setMaximumDisconnectingCapacityDataPresentedByTheBinder("IEC 62271-102");
			swad3.setProtectionDegreeUnitOfMeasure("IP");
			swad3.setProtectionDegreeValue("IP66");
			swad3.setProtectionDegreeDataPresentedByTheBinder("IEC 60529");
			swad3.setAuxiliarySignalingContactsUnitOfMeasure("Number");
			swad3.setAuxiliarySignalingContactsValue(8);
			swad3.setAuxiliarySignalingContactsDataPresentedByTheBinder("IEC 60947-5-1");
			
			swad3.setEquipmentSeries("SWAD-ELITE-2024");
			swad3.setManufacturingYear(2023);
			swad3.setPifYear(2024);
			swad3.setInstallationCompany("PowerSafe Installations Ltd.");
			swad3.setPifBulletin("");
			swad3.setExploitationCenter("Eastern Grid Operations");
			swad3.setTransformationStation("Cedar Hill Substation");
			swad3.setVoltageLevel("36 kV");
			swad3.setAssemblyCell("Cell D6");
			swad3.setAssemblyPhase("Phase 2");
			swad3.setAccountingorWarehouseRegistrationCode("REG-SWAD-2024-006");
			swad3.setFixedAssetComponentNumber("FM-0067890");
			switchActuationDeviceRepository.save(swad3);

			SwitchActuationDeviceMeasurement swadm1 = new SwitchActuationDeviceMeasurement();
			swadm1.setSwitchActuationDevice(swad1);
			swadm1.setFourtyPercentOfTheLengthOfTheBottomOfTheSwitch(1.2);
			swadm1.setWidthOne(0.62);
			swadm1.setWidthTwo(0.29);
			swadm1.setInitialTemperature(74);
			swadm1.setMeasuredTemperature(73);
			switchActuationDeviceMeasurementRepository.save(swadm1);

			SwitchActuationDeviceMeasurement swadm1a = new SwitchActuationDeviceMeasurement();
			swadm1a.setSwitchActuationDevice(swad1);
			swadm1a.setFourtyPercentOfTheLengthOfTheBottomOfTheSwitch(1.4);
			swadm1a.setWidthOne(0.42);
			swadm1a.setWidthTwo(0.20);
			swadm1a.setInitialTemperature(54);
			swadm1a.setMeasuredTemperature(52);
			switchActuationDeviceMeasurementRepository.save(swadm1a);

			SwitchActuationDeviceMeasurement swadm2 = new SwitchActuationDeviceMeasurement();
			swadm2.setSwitchActuationDevice(swad2);
			swadm2.setFourtyPercentOfTheLengthOfTheBottomOfTheSwitch(1.6);
			swadm2.setWidthOne(0.45);
			swadm2.setWidthTwo(0.25);
			swadm2.setInitialTemperature(68);
			swadm2.setMeasuredTemperature(66);
			switchActuationDeviceMeasurementRepository.save(swadm2);
			
			SwitchActuationDeviceMeasurement swadm3 = new SwitchActuationDeviceMeasurement();
			swadm3.setSwitchActuationDevice(swad3);
			swadm3.setFourtyPercentOfTheLengthOfTheBottomOfTheSwitch(0.8);
			swadm3.setWidthOne(0.38);
			swadm3.setWidthTwo(0.18);
			swadm3.setInitialTemperature(83);
			swadm3.setMeasuredTemperature(81);
			switchActuationDeviceMeasurementRepository.save(swadm3);

			log.info("ending transformation units initialization...");

	    };
	}

	@Bean
	public CommandLineRunner loadSwitchEquipment(SwitchEquipmentRepository switchEquipmentRepository,SwitchEquipmentMeasurementRepository switchEquipmentMeasurementRepository)
	{
	    return (args) -> {	
			log.info("starting switch equipment initialization...");
				        
			
			SwitchEquipment se1 = new SwitchEquipment();
			se1.setEquipmentCode("SW-2024-001");
			se1.setCircuitBreakerType("Vacuum Circuit Breaker");
			se1.setNominalBreakingCapacityOfSmallInductiveCurrents("20 A");
			se1.setKilometricFaultDisconnectingCapacity("10 kA");
			se1.setNominalOperatingSequence("O-0.3s-CO-3min-CO");
			se1.setPoleNumber(3);
			se1.setNominalVoltage(33);
			se1.setNominalInsulation(70);
			se1.setNominalCurrent(1250);
			se1.setNominalBreakingCapacityOfNoLoadLineCurrents(50);
			se1.setNominalBreakingCapacityOfNoLoadCableCurrents(75);
			se1.setNominalBreakingCapacityOfCapacitorBankCurrents(100);
			se1.setMaximumShortCircuitClosingCapacity(32);
			se1.setNominalClosingTime(50);
			se1.setNominalOpeningTime(40);
			se1.setNominalInterruptionTime(60);
			se1.setNominalBreakingCapacityOfShortCircuitCurrentsAtTerminal(25.0);
			se1.setAdmissibleDurationOfShortCircuitCurrents(3.0);
			
			se1.setEquipmentSeries("SW-ELITE-2024");
			se1.setManufacturingYear(2023);
			se1.setPifYear(2024);
			se1.setInstallationCompany("HighPower Electrical Installations Ltd.");
			se1.setPifBulletin("");
			se1.setExploitationCenter("Northern Grid Operations");
			se1.setTransformationStation("Highland Substation");
			se1.setVoltageLevel("32 kV");
			se1.setAssemblyCell("Cell A2");
			se1.setAssemblyPhase("Phase 4");
			se1.setAccountingorWarehouseRegistrationCode("REG-SW-2024-001");
			se1.setFixedAssetComponentNumber("FM-0012345");
			switchEquipmentRepository.save(se1);
			
			SwitchEquipment se2 = new SwitchEquipment();
			se2.setEquipmentCode("SW-2024-002");
			se2.setCircuitBreakerType("SF6 Circuit Breaker");
			se2.setNominalBreakingCapacityOfSmallInductiveCurrents("25 A");
			se2.setKilometricFaultDisconnectingCapacity("12 kA");
			se2.setNominalOperatingSequence("CO-15s-CO-3min-CO");
			se2.setPoleNumber(3);
			se2.setNominalVoltage(11);
			se2.setNominalInsulation(28);
			se2.setNominalCurrent(2000);
			se2.setNominalBreakingCapacityOfNoLoadLineCurrents(60);
			se2.setNominalBreakingCapacityOfNoLoadCableCurrents(80);
			se2.setNominalBreakingCapacityOfCapacitorBankCurrents(120);
			se2.setMaximumShortCircuitClosingCapacity(40);
			se2.setNominalClosingTime(45);
			se2.setNominalOpeningTime(35);
			se2.setNominalInterruptionTime(55);
			se2.setNominalBreakingCapacityOfShortCircuitCurrentsAtTerminal(30.0);
			se2.setAdmissibleDurationOfShortCircuitCurrents(2.0);
			
			se2.setEquipmentSeries("SW-PRO-2024");
			se2.setManufacturingYear(2022);
			se2.setPifYear(2023);
			se2.setInstallationCompany("SafeGrid Installations Ltd.");
			se2.setPifBulletin("");
			se2.setExploitationCenter("Southern Grid Operations");
			se2.setTransformationStation("Valley Forge Substation");
			se2.setVoltageLevel("11 kV");
			se2.setAssemblyCell("Cell B4");
			se2.setAssemblyPhase("Phase 2");
			se2.setAccountingorWarehouseRegistrationCode("REG-SW-2024-002");
			se2.setFixedAssetComponentNumber("FM-0023456");
			switchEquipmentRepository.save(se2);
			
			SwitchEquipment se3 = new SwitchEquipment();
			se3.setEquipmentCode("SW-2024-003");
			se3.setCircuitBreakerType("Air Circuit Breaker");
			se3.setNominalBreakingCapacityOfSmallInductiveCurrents("15 A");
			se3.setKilometricFaultDisconnectingCapacity("8 kA");
			se3.setNominalOperatingSequence("O-0.1s-CO-1min-CO");
			se3.setPoleNumber(4);
			se3.setNominalVoltage(7);
			se3.setNominalInsulation(15);
			se3.setNominalCurrent(1600);
			se3.setNominalBreakingCapacityOfNoLoadLineCurrents(40);
			se3.setNominalBreakingCapacityOfNoLoadCableCurrents(55);
			se3.setNominalBreakingCapacityOfCapacitorBankCurrents(90);
			se3.setMaximumShortCircuitClosingCapacity(25);
			se3.setNominalClosingTime(60);
			se3.setNominalOpeningTime(50);
			se3.setNominalInterruptionTime(70);
			se3.setNominalBreakingCapacityOfShortCircuitCurrentsAtTerminal(20.0);
			se3.setAdmissibleDurationOfShortCircuitCurrents(1.5);
			
			se3.setEquipmentSeries("SW-ADVANCE-2024");
			se3.setManufacturingYear(2023);
			se3.setPifYear(2024);
			se3.setInstallationCompany("PowerLine Installations Ltd.");
			se3.setPifBulletin("");
			se3.setExploitationCenter("Eastern  Grid Operations");
			se3.setTransformationStation("Oakwood Substation");
			se3.setVoltageLevel("6.6 kV");
			se3.setAssemblyCell("Cell C2");
			se3.setAssemblyPhase("Phase 1");
			se3.setAccountingorWarehouseRegistrationCode("REG-SW-2024-003");
			se3.setFixedAssetComponentNumber("FM-0034567");
			switchEquipmentRepository.save(se3);

			SwitchEquipmentMeasurement sem1 = new SwitchEquipmentMeasurement();
			sem1.setSwitchHeightOne(3);
			sem1.setSwitchHeightTwo(3);
			sem1.setSwitchBaseHeight(2);
			sem1.setSwitchEquipment(se1);
			sem1.setInitialTemperature(76);
			sem1.setMeasuredTemperature(74);
			switchEquipmentMeasurementRepository.save(sem1);

			SwitchEquipmentMeasurement sem1a = new SwitchEquipmentMeasurement();
			sem1a.setSwitchHeightOne(12);
			sem1a.setSwitchHeightTwo(5);
			sem1a.setSwitchBaseHeight(3);
			sem1a.setSwitchEquipment(se1);
			sem1a.setInitialTemperature(85);
			sem1a.setMeasuredTemperature(82);
			switchEquipmentMeasurementRepository.save(sem1a);

			SwitchEquipmentMeasurement sem2 = new SwitchEquipmentMeasurement();
			sem2.setSwitchHeightOne(30);
			sem2.setSwitchHeightTwo(2);
			sem2.setSwitchBaseHeight(10);
			sem2.setSwitchEquipment(se2);
			sem2.setInitialTemperature(95);
			sem2.setMeasuredTemperature(94);
			switchEquipmentMeasurementRepository.save(sem2);
			
			SwitchEquipmentMeasurement sem3 = new SwitchEquipmentMeasurement();
			sem3.setSwitchHeightOne(23);
			sem3.setSwitchHeightTwo(5);
			sem3.setSwitchBaseHeight(11);
			sem3.setSwitchEquipment(se3);
			sem3.setInitialTemperature(63);
			sem3.setMeasuredTemperature(63);
			switchEquipmentMeasurementRepository.save(sem3);

			log.info("ending switch equipment initialization...");

	    };
	}

	@Bean
	public CommandLineRunner loadVoltageMeasuringTransformer(VoltageMeasuringTransformerRepository voltageMeasuringTransformerRepository,VoltageMeasuringTransformerMeasurementRepository voltageMeasuringTransformerMeasurementRepository)
	{
	    return (args) -> {	
			log.info("starting transformation units initialization...");
				        
			VoltageMeasuringTransformer vmt1 = new VoltageMeasuringTransformer();
			vmt1.setMaximumNetworkVoltageUnitOfMeasure("kV");
			vmt1.setMaximumNetworkVoltageRequired("132 kV");
			vmt1.setMaximumNetworkVoltageOffered("145 kV");
			vmt1.setRatedVoltageOfThePrimaryWindingUnitOfMeasure("kV");
			vmt1.setRatedVoltageOfThePrimaryWindingRequired("110 kV");
			vmt1.setRatedVoltageOfThePrimaryWindingOffered("115 kV");
			vmt1.setEquipmentSeries("VMT-POWER-2024");
			vmt1.setManufacturingYear(2023);
			vmt1.setPifYear(2024);
			vmt1.setInstallationCompany("HighVolt Installation Services");
			vmt1.setPifBulletin("");
			vmt1.setExploitationCenter("Southern Grid Operations");
			vmt1.setTransformationStation("Hilltop Substation");
			vmt1.setVoltageLevel("110 kV");
			vmt1.setAssemblyCell("Cell B6");
			vmt1.setAssemblyPhase("Phase 2");
			vmt1.setAccountingorWarehouseRegistrationCode("REG-VMT-2024-015");
			vmt1.setFixedAssetComponentNumber("FM-0156234");
			voltageMeasuringTransformerRepository.save(vmt1);
			
			VoltageMeasuringTransformer vmt2 = new VoltageMeasuringTransformer();
			vmt2.setMaximumNetworkVoltageUnitOfMeasure("kV");
			vmt2.setMaximumNetworkVoltageRequired("220 kV");
			vmt2.setMaximumNetworkVoltageOffered("230 kV");
			vmt2.setRatedVoltageOfThePrimaryWindingUnitOfMeasure("kV");
			vmt2.setRatedVoltageOfThePrimaryWindingRequired("200 kV");
			vmt2.setRatedVoltageOfThePrimaryWindingOffered("205 kV");
			vmt2.setEquipmentSeries("VMT-ELITE-2024");
			vmt2.setManufacturingYear(2022);
			vmt2.setPifYear(2023);
			vmt2.setInstallationCompany("PowerGrid Solutions Ltd.");
			vmt2.setPifBulletin("");
			vmt2.setExploitationCenter("Central Grid Operations");
			vmt2.setTransformationStation("Redwood Substation");
			vmt2.setVoltageLevel("200 kV");
			vmt2.setAssemblyCell("Cell D3");
			vmt2.setAssemblyPhase("Phase 1");
			vmt2.setAccountingorWarehouseRegistrationCode("REG-VMT-2024-016");
			vmt2.setFixedAssetComponentNumber("FM-0167482");
			voltageMeasuringTransformerRepository.save(vmt2);
			
			VoltageMeasuringTransformer vmt3 = new VoltageMeasuringTransformer();
			vmt3.setMaximumNetworkVoltageUnitOfMeasure("kV");
			vmt3.setMaximumNetworkVoltageRequired("400 kV");
			vmt3.setMaximumNetworkVoltageOffered("420 kV");
			vmt3.setRatedVoltageOfThePrimaryWindingUnitOfMeasure("kV");
			vmt3.setRatedVoltageOfThePrimaryWindingRequired("380 kV");
			vmt3.setRatedVoltageOfThePrimaryWindingOffered("385 kV");
			vmt3.setEquipmentSeries("VMT-PRO-2024");
			vmt3.setManufacturingYear(2023);
			vmt3.setPifYear(2024);
			vmt3.setInstallationCompany("MegaVolt Electrical Installations");
			vmt3.setPifBulletin("");
			vmt3.setExploitationCenter("Eastern Grid Operations");
			vmt3.setTransformationStation("Greenfield Substation");
			vmt3.setVoltageLevel("380 kV");
			vmt3.setAssemblyCell("Cell F7");
			vmt3.setAssemblyPhase("Phase 3");
			vmt3.setAccountingorWarehouseRegistrationCode("REG-VMT-2024-017");
			vmt3.setFixedAssetComponentNumber("FM-0178923");
			voltageMeasuringTransformerRepository.save(vmt3);

			VoltageMeasuringTransformerMeasurement vmtm1 = new VoltageMeasuringTransformerMeasurement();
			vmtm1.setVoltageMeasuringTransformerHeightOne(3);
			vmtm1.setVoltageMeasuringTransformerHeightTwo(4);
			vmtm1.setVoltageMeasuringTransformer(vmt1);
			vmtm1.setTimeStamp(LocalDateTime.of(2023, 07, 4, 16, 55));
			vmtm1.setInitialTemperature(65);
			vmtm1.setMeasuredTemperature(64);
			voltageMeasuringTransformerMeasurementRepository.save(vmtm1);

			VoltageMeasuringTransformerMeasurement vmtm1a = new VoltageMeasuringTransformerMeasurement();
			vmtm1a.setVoltageMeasuringTransformerHeightOne(12);
			vmtm1a.setVoltageMeasuringTransformerHeightTwo(8);
			vmtm1a.setVoltageMeasuringTransformer(vmt1);
			vmtm1a.setTimeStamp(LocalDateTime.of(2024, 04, 22, 12, 35));
			vmtm1a.setInitialTemperature(65);
			vmtm1a.setMeasuredTemperature(64);
			voltageMeasuringTransformerMeasurementRepository.save(vmtm1a);
			
			VoltageMeasuringTransformerMeasurement vmtm1b = new VoltageMeasuringTransformerMeasurement();
			vmtm1b.setVoltageMeasuringTransformerHeightOne(12);
			vmtm1b.setVoltageMeasuringTransformerHeightTwo(8);
			vmtm1b.setVoltageMeasuringTransformer(vmt1);
			vmtm1b.setTimeStamp(LocalDateTime.of(2023, 04, 22, 12, 35));
			vmtm1b.setInitialTemperature(58);
			vmtm1b.setMeasuredTemperature(56);
			voltageMeasuringTransformerMeasurementRepository.save(vmtm1b);
			
			VoltageMeasuringTransformerMeasurement vmtm1c = new VoltageMeasuringTransformerMeasurement();
			vmtm1c.setVoltageMeasuringTransformerHeightOne(12);
			vmtm1c.setVoltageMeasuringTransformerHeightTwo(8);
			vmtm1c.setVoltageMeasuringTransformer(vmt1);
			vmtm1c.setTimeStamp(LocalDateTime.of(2022, 04, 22, 12, 35));
			vmtm1c.setInitialTemperature(85);
			vmtm1c.setMeasuredTemperature(83);
			voltageMeasuringTransformerMeasurementRepository.save(vmtm1c);

			VoltageMeasuringTransformerMeasurement vmtm2 = new VoltageMeasuringTransformerMeasurement();
			vmtm2.setVoltageMeasuringTransformerHeightOne(7);
			vmtm2.setVoltageMeasuringTransformerHeightTwo(9);
			vmtm2.setVoltageMeasuringTransformer(vmt2);
			vmtm2.setInitialTemperature(93);
			vmtm2.setMeasuredTemperature(91);
			voltageMeasuringTransformerMeasurementRepository.save(vmtm2);
			
			VoltageMeasuringTransformerMeasurement vmtm3 = new VoltageMeasuringTransformerMeasurement();
			vmtm3.setVoltageMeasuringTransformerHeightOne(5);
			vmtm3.setVoltageMeasuringTransformerHeightTwo(2);
			vmtm3.setVoltageMeasuringTransformer(vmt3);
			vmtm3.setInitialTemperature(74);
			vmtm3.setMeasuredTemperature(73);
			voltageMeasuringTransformerMeasurementRepository.save(vmtm3);

			log.info("ending transformation units initialization...");

	    };
	}
}

