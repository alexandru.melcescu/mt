package ro.upt.ac.mtb.users;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface UserRepository extends JpaRepository<User,Integer>
{
	//List<User> findByNameContainingOrFullnameContaining(String text, String textAgain);
	User findById(Long id);
}
