package ro.upt.ac.mtb.users;

import jakarta.persistence.Entity;
import jakarta.persistence.GeneratedValue;
import jakarta.persistence.GenerationType;
import jakarta.persistence.Id;

@Entity
public class User 
{
	@Id
    @GeneratedValue(strategy = GenerationType.AUTO)
	private long id;
	
	private String firstName;
	
    private String lastName;
	
    private String password;

    public User()
    {
    }
    
    public User(String firstName, String lastName, String password)
    {
    	this.firstName=firstName;
    	this.lastName=lastName;
    	this.password=password;
    }
        
    public long getId() 
    {
		return id;
	}

	public void setId(long id) 
	{
		this.id = id;
	}
	
	public String getFirstName()
	{
		return firstName;
	}

	public void setFirstName(String firstName)
	{
		this.firstName = firstName;
	}

	public String getLastName()
	{
		return lastName;
	}

	public void setLastName(String lastName)
	{
		this.lastName = lastName;
	}

	public String getPassword() 
	{
		return password;
	}

	public void setPassword(String password) 
	{
		this.password = password;
	}

	@Override
    public String toString() 
    {
    	return firstName+" "+lastName;
    }
}
