package ro.upt.ac.mete.echipamente;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PostMapping;

import ro.upt.ac.mete.echipamente.tc.TC;
import ro.upt.ac.mete.echipamente.tc.TCRepository;

@Controller
public class EController
{
	@Autowired
    TCRepository tcRepository;
	
	@GetMapping("/tc_create")
	public String create(Model model) 
	{
	    model.addAttribute("tc", new TC());
	    return "tc_create_form";
	}
	
	@PostMapping("/tc_create")
	public String doCreate(@ModelAttribute TC tc, Model model) 
	{
	    model.addAttribute("tc", tc);
	    tcRepository.save(tc);
	    return "tc_create_result";
	}
}
