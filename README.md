# mt

## Introduction

The steps from below depend on running a mySQL service on the server

## Description of deliverables

Entities:

  * All of the equipment entities extend the parent class Equipment Manual which in turn implements the ExportableToCSV  	Class.
  * Each equipment entity includes all necessary Object Oriented Programming functionalities to make the class 	executable.
  * The attributes in each of the entities reflect the technical data of the respective equipment.
  * All attributes contain the setters and getters.
  * The information contained inside each of the entities is displayed on the browser using the Controller classes, 	Repositories, and HTML file for the respective equipment.
  
Controllers:

1. The search() method:

  * This method uses two request parameters, one for the searchField, and one for the searchTerm.
  * The method then invokes the performSearch() method.
  * The method prepares the model for the html template with all required display fields using the addAttribute calls.
  
2. The performSearch() method:

  * This method conditionally invokes specific findBy methods on the repository depending on the search field
  * This method returns the data matching the criteria
  
3. The uploadFile() method:

  * This method stores binary large objects, for example pdf, inside the database 
  * It uses the postMapping from an html form and a request parameter containing the contents of the specific file
  * The file is stored in the sql database using the repository
  
4. The downloadFile() method:

  * This method allows the binary large object to be downloaded for a specific equipment type
  * Not every equipment has a specific manual, in which case the endpoint returns a 404 not found error using the code:  	return ResponseEntity.notFound().build();
  
5. The create() method:

  * This method processes the form from the equipment's html create template
  
6. The create-save() method:

  * This method processes the form from the equipment's html create template
  
7. The read() method:

  * This method actually loads all arresters
  * The method also returns the read html template
  
8. The edit() method:

  * This method uses the path variable for the id of the equipment to allow editing
  * It loads the record for the specific id
  * It returns the update html template 
  
9. The update() method:

  * This method uses the path variable for the id of the equipment to allow updating
  * IMPORTANT: Prevents concurrent update conflicts by loading the equipment using the last update time stamp!!!!
  * If the last update time stamp does not match the last update time stamp in the form an error 
  * IF the form has any errors, they will also be displayed
  * Otherwise, the time stamp is reset and the record is saved
	 
10. The delete() method:
  * The endpoint deletes the record whose id matches the path variable
  * It returns the user to the read html template using a redirect to avoid the double submission problem
	 
11. The exportToCSV() method:
  * This method uses APACHE commons csv to export data in csv format compatible with excel
  * Using a response header, the browser stores the csv file in the user's download folder on the local machine
  * Using optional request parameters, the csv contents can be filtered by a search field and value
	
12. The getHeadings() method:
  * The method prepares a String array containing column heading descriptions for each attribute of the equipment

13. The getFieldNames() method:
  * This prepares the String array containing the attribute names for each attribute of the equipment
  
14. The getHeadingsWithFields() method:
  * This prepares a key-value data structure (map) which associates descriptive column headings with their respective 	attribute names
  * It is used for the drop-down function in the search form of the equipment read html template
	 
15. The exportToPDF() method:
  * This method uses the flying-saucer-pdf project to generate pdf file from a ThymeLeaf template
  * Using a response header, the browser will store the pdf file to the user's download folder on the local machine
	 
16. The parseThymeleafTemplate() method:
  * This is a helper method to locate the pdf ThymeLeaf template and populate it with respective data


Repositories:

  * Each findBy methods are used for the performSearch() function and require an exact match.
  * The @Query annotation allows for a partial keyword search using all attributes of the equipment

Templates:

  * The create HTML file of each equipment is responsible for adding/creating a new equipment. The create HTML file of each of the equipment's measurements is responsible for adding/creating a new measurement for the respective equipment.
  * The read HTML file of each equipment is resposible for displaying all the information of each equipment into a Bootstrap data table. The read HTML file of each equipment's measurements is responsible for diplaying all the information about the measurements for each entry of equipments into a Bootstrap data table. 
  * The pdf HTML file of each equipment is responsible for displaying a new PDF file containing the technical information about the respective equipment, whenever the user clicks on the PDf file button.  The pdf HTML file of each equipment's measurements is responsible for displaying a new PDF file containing the technical information about the respective measurement., whenever the user clicks on the PDf file button.
  * The update HTML file of each equipment is responsible for editing each technical data information entry of the respective equipment.The update HTML file of each equipment's measurements is responsible for editing each technical data information entry of the measurement for the respective equipment.


[Bootstrap](https://getbootstrap.com/docs/5.1/getting-started/introduction/):

  * The Bootstrap Framework is a framework that is used for designing web pages and applications. 
  * It is commonly referred to as being open-source, meaning that any developer can use the framework to view, modify and 	distribute the implemented code.

[Chart.js](https://www.chartjs.org/):
  * This resouce was used to render each of the graphs for the measurements of the equipments. 
  * All different graphs of the application were created by javascript code using this resource
  * Some of the charts include: bar, line, bubble, step-line, etc.

## Repository Address

https://gitlab.upt.ro/alexandru.melcescu/mt

## Application Build Steps

1. git clone https://gitlab.upt.ro/alexandru.melcescu/mt
2. mvn clean
3. mvn compile

## Application Installation

mvn install

## Launch Steps

mvn spring-boot:run